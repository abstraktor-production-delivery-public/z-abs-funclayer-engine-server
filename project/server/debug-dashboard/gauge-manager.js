
'use strict';

const Gauge = require('./gauge');


class GaugeManager {
  constructor() {
    this.gauges = new Map();
  }
  
  create(group, family, name, minValue, maxValue) {
    const gaugeData = {
      group: group,
      family: family,
      name: name,
      key: `${group}:${family}:${name}`,
      value: 0,
      samples: [0],
      nbrOfSamples: 0,
      meanValue: 0,
      minValue: minValue,
      maxValue: maxValue,
      update: () => {
        ddb.writeln('GAUGE', gaugeData.key + ':', gaugeData.value);
      }
    };
    this.gauges.set(gaugeData.key, gaugeData);
    return new Gauge(gaugeData);
  }
  
  exit() {
    this.gauges = new Map();
  }
  
  print() {
    this.gauges.forEach((gaugeData) => {
   //   ddb.writeln('GAUGE', gaugeData.key + ':', gaugeData.value);
    });
  }
}


module.exports = GaugeManager;
