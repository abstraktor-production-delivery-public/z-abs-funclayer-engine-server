
'use strict';

const GaugeManager = require('./gauge-manager');


class DebugDashboard {
  constructor() {
    this.gaugeManager = new GaugeManager();
  }
}


module.exports = DebugDashboard;
