
'use strict';


class Gauge {
  constructor(gaugeData) {
    this.gaugeData = gaugeData;
  }
  
  set(value) {
    this.gaugeData.value = value;
//    this.gaugeData.samples.push(value);
    this.gaugeData.meanValue = (this.gaugeData.meanValue + (value - this.gaugeData.meanValue) / ++this.gaugeData.nbrOfSamples);
    this.gaugeData.update();
  }
  
  inc() {
    ++this.gaugeData.value;
//    this.gaugeData.samples.push(this.gaugeData.value);
    this.gaugeData.meanValue = (this.gaugeData.meanValue + (this.gaugeData.value - this.gaugeData.meanValue) / ++this.gaugeData.nbrOfSamples);
    this.gaugeData.update();
  }
  
  dec() {
    --this.gaugeData.value;
//    this.gaugeData.samples.push(this.gaugeData.value);
    this.gaugeData.meanValue = (this.gaugeData.meanValue + (this.gaugeData.value - this.gaugeData.meanValue) / ++this.gaugeData.nbrOfSamples);
    this.gaugeData.update();
  }
  
  print() {
//    ddb.writeln('- GAUGE', this.gaugeData.key + ':', this.gaugeData.value);
  }
}


module.exports = Gauge;
