
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseDebugActorIndex {
  constructor(actorIndex) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_ACTOR_INDEX;
    this.actorIndex = actorIndex;
  }
}


module.exports = MessageTestCaseDebugActorIndex;