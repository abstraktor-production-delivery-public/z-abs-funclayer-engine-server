
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseDebugActorIndexOk {
  constructor(sessionId, actorIndex) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_ACTOR_INDEX_OK;
    this.sessionId = sessionId;
    this.actorIndex = actorIndex;
  }
}

module.exports = MessageTestCaseDebugActorIndexOk;
