
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseStopped {
  constructor(index, iterationTs, duration, resultId, resultIdPre, resultIdExec, resultIdPost, last) {
    this.msgId = AppProtocolConst.TEST_CASE_STOPPED;
    this.index = index;
    this.iterationTs = iterationTs;
    this.duration = duration;
    this.resultId = resultId;
    this.resultIdPre = resultIdPre;
    this.resultIdExec = resultIdExec;
    this.resultIdPost = resultIdPost;
    this.last = last;
  }
}


module.exports = MessageTestCaseStopped;
