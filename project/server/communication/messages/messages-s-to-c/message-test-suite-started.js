
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestSuiteStarted {
  constructor(index, timestamp) {
    this.msgId = AppProtocolConst.TEST_SUITE_STARTED;
    this.index = index;
    this.timestamp = timestamp;
  }
}


module.exports = MessageTestSuiteStarted;
