
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseLog  {
  constructor(type, date, actor, actorPath, log, group, fileName, lineNumber, sourceType, inners, data) {
    this.msgId = AppProtocolConst.LOG;
    this.type = type;
    this.date = date;
    this.actor = actor;
    this.actorPath = actorPath;
    this.log = log;
    this.group = group;
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.sourceType = sourceType;
    this.inners = inners;
    this.data = data;
  }
}


module.exports = MessageTestCaseLog;
