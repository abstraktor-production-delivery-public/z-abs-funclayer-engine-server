
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestStageStarted {
  constructor(index, type, iterationTs, timestamp, isExecuted) {
    this.msgId = AppProtocolConst.TEST_STAGE_STARTED;
    this.index = index;
    this.type = type;
    this.iterationTs = iterationTs;
    this.timestamp = timestamp;
    this.isExecuted = isExecuted;
  }
}


module.exports = MessageTestStageStarted;
