
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseStarted {
  constructor(index, iterationTs, name, sutName, actors, timestamp, isExecuted) {
    this.msgId = AppProtocolConst.TEST_CASE_STARTED;
    this.index = index;
    this.iterationTs = iterationTs;
    this.name = name;
    this.sutName = sutName;
    this.actors = actors;
    this.timestamp = timestamp;
    this.isExecuted = isExecuted;
  }
}


module.exports = MessageTestCaseStarted;
