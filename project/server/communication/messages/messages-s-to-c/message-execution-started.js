
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageExecutionStarted {
  constructor(chosen, clientLogSettings, clientConsoleLogSettings) {
    this.msgId = AppProtocolConst.EXECUTION_STARTED;
    this.chosen = chosen;
    this.clientLogSettings = clientLogSettings;
    this.clientConsoleLogSettings = clientConsoleLogSettings;
  }
}


module.exports = MessageExecutionStarted;
