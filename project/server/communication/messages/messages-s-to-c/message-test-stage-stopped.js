
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestStageStopped {
  constructor(index, type, iterationTs, duration, resultId) {
    this.msgId = AppProtocolConst.TEST_STAGE_STOPPED;
    this.index = index;
    this.type = type;
    this.iterationTs = iterationTs;
    this.duration = duration;
    this.resultId = resultId;
  }
}


module.exports = MessageTestStageStopped;
