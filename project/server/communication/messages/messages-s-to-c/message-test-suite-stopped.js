
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestSuiteStopped {
  constructor(index, timestamp, duration, last) {
    this.msgId = AppProtocolConst.TEST_SUITE_STOPPED;
    this.index = index;
    this.timestamp = timestamp;
    this.duration = duration;
    this.last = last;
  }
}


module.exports = MessageTestSuiteStopped;
