
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseDebugPaused {
  constructor(stack, scriptId, script, actorIndex) {
    this.msgId = AppProtocolConst.TEST_CASE_DEBUG_PAUSED;
    this.stack = stack;
    this.scriptId = scriptId;
    this.script = script;
    this.actorIndex = actorIndex;
  }
}


module.exports = MessageTestCaseDebugPaused;
