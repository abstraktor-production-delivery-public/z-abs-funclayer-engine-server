
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageExecutionStopped {
  constructor() {
    this.msgId = AppProtocolConst.EXECUTION_STOPPED;
  }
}


module.exports = MessageExecutionStopped;
