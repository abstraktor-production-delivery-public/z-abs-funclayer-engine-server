
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseState {
  constructor(actorIndex, actorOrderIndex, stateIndex, stateResultIndex) {
    this.msgId = AppProtocolConst.TEST_CASE_STATE;
    this.actorIndex = actorIndex;
    this.actorOrderIndex = actorOrderIndex;
    this.stateIndex = stateIndex;
    this.stateResultIndex = stateResultIndex;
  }
}


module.exports = MessageTestCaseState;
