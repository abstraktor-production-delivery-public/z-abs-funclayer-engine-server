
'use strict';

const Subnet = require('./subnet');
const Os = require('os');
const V8 = require('v8');


class RealNetworks {
  constructor(loginReport) {
    this.loginReport = loginReport;
    this._realNetworksIPv4 = new Map();
    this._realNetworksIPv6 = new Map();
    this._localNetworksIPv4 = new Map();
    this._localNetworksIPv6 = new Map();
    this._realAddresses = new Map();
  }
  
  read() {
    const realInterfaces = Os.networkInterfaces();
    for(let k in realInterfaces) {
      for(let k2 in realInterfaces[k]) {
        const realInterface = realInterfaces[k][k2];
        if(!realInterface.internal) {
          if(realInterface.mac !== '00:00:00:00:00:00') {
            if('win32' === Os.platform() && k.startsWith('vEthernet')) {
              // Filter out virtual Ethernet adapters on windows.
              continue;
            }
            if('IPv4' === realInterface.family) {
              this._setRealAddress(realInterface, this._realNetworksIPv4);
            }
            else if('IPv6' === realInterface.family) {
              if(0 === realInterface.scopeid) { // Filter out Link-local IPv6 Address
                this._setRealAddress(realInterface, this._realNetworksIPv6);
              }
            }
          }
        }
        else {
          if('IPv4' === realInterface.family) {
            this._setRealAddress(realInterface, this._localNetworksIPv4);
          }
          else if('IPv6' === realInterface.family) {
            this._setRealAddress(realInterface, this._localNetworksIPv6);
          }
        }
      }
    }
    this._realNetworksIPv4 = this._sortRealOrLocalNetworks(this._realNetworksIPv4);
    this._realNetworksIPv6 = this._sortRealOrLocalNetworks(this._realNetworksIPv6);
    this._localNetworksIPv4 = this._sortRealOrLocalNetworks(this._localNetworksIPv4);
    this._localNetworksIPv6 = this._sortRealOrLocalNetworks(this._localNetworksIPv6);
  }
  
  get() {
    const result = {
      realNetworksIPv4: this._realNetworksIPv4,
      realNetworksIPv6: this._realNetworksIPv6,
      localNetworksIPv4: this._localNetworksIPv4,
      localNetworksIPv6: this._localNetworksIPv6,
      realAddresses: this._realAddresses
    };
    return V8.deserialize(V8.serialize(result));
  }
  
  _setRealAddress(realInterface, networks) {
    this.loginReport.realAddressLog('found', 'ok', `Real address found. IP-Address: '${realInterface.address}'. Netmask: '${realInterface.netmask}'`);
    const subnet = Subnet.calculate(realInterface);
    realInterface.subnet = subnet;
    if(!networks.has(subnet)) {
      this.loginReport.networkLog('new', 'ok', `Network subnet created: '${subnet}'`);
      networks.set(subnet, {
        subnet: subnet,
        addresses: [],
        amount: 0
      });
    }
    const network = networks.get(subnet);
    network.addresses.push({
      address: realInterface,  // rename: address => interface 
      amount: new Map()
    });
    this._realAddresses.set(realInterface.address, realInterface);
  }
  
  _sortRealOrLocalNetworks(map) {
    return new Map([...map.entries()].sort((a, b) => {
      if(b[1].addresses.length < a[1].addresses.length) {
        return -1;
      }
      else if(b[1].addresses.length > a[1].addresses.length) {
        return 1;
      }
      else {
        return 0;
      }
    }));
  }
}

module.exports = RealNetworks;
