
'use strict';

const Address = require('./address');
const NetworkType = require('../stack/network/network-type');


class AddressDefaultManager {
  constructor(addresses) {
    this.addresses = addresses;
  }
  
  setAddresses(addresses) {
    this.addresses = addresses;
  }
  
  getSrc(transportType, index) {
    let newAddress = null;
    if(0 !== this.addresses.srcs.size) {
      const address = this.addresses.srcs.values().next().value;
      if(NetworkType.MC !== transportType) {
        newAddress = new Address(`defaultSrc_${index}`, address.host, address.family, address.port ? address.port : 0, 'client');
      }
      else {
        newAddress = new Address(`defaultSrc_${index}`, address.host, address.family, AddressDefaultManager.BASE_PORT + index, 'client');
      }
    }
    else {
      if(NetworkType.MC !== transportType) {
        newAddress = new Address(`defaultSrc_${index}`, '127.0.0.1', 'IPv4', 0, 'client');
      }
      else {
        newAddress = new Address(`defaultSrc_${index}`, '127.0.0.1', 'IPv4', AddressDefaultManager.BASE_PORT + index, 'client');
      }
    }
    newAddress.setLogName('Default-src');
    return newAddress;
  }
  
  getDst(transportType, index) {
    let newAddress = null;
    if(0 !== this.addresses.srvs.size) {
      const address = this.addresses.srvs.values().next().value;
      if(NetworkType.MC !== transportType) {
        newAddress = new Address(`defaultDst_${index}`, address.host, address.family, AddressDefaultManager.BASE_PORT + index, 'server');
      }
      else {
        newAddress = new Address(`defaultDst_${index}`, '239.1.2.3', address.family, AddressDefaultManager.BASE_PORT + index, 'server');
      }
    }
    else {
      if(NetworkType.MC !== transportType) {
        newAddress = new Address(`defaultDst_${index}`, '127.0.0.1', 'IPv4', AddressDefaultManager.BASE_PORT + index, 'server');
      }
      else {
        newAddress = new Address(`defaultDst_${index}`, '239.1.2.3', 'IPv4', AddressDefaultManager.BASE_PORT + index, 'server');
      }
    }
    newAddress.setLogName('Default-dst');
    return newAddress;
  }
  
  getSrv(index) {
    let newAddress = null;
    if(0 !== this.addresses.srvs.size) {
      const address = this.addresses.srvs.values().next().value;
      newAddress = new Address(`defaultSrv_${index}`, address.host, address.family, AddressDefaultManager.BASE_PORT + index, 'server');
    }
    else {
      newAddress = new Address(`defaultSrv_${index}`, '127.0.0.1', 'IPv4', AddressDefaultManager.BASE_PORT + index, 'server');
    }
    newAddress.setLogName('Default-srv');
    return newAddress;
  }
}


AddressDefaultManager.BASE_PORT = 9900;

module.exports = AddressDefaultManager;