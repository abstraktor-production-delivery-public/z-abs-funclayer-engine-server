
'use strict';

const Net = require('net');
const Dns = require('dns');


class Address {
  constructor(addressName, host, family, port, type, page, incognitoBrowser, uri) {
    this.addressName = addressName;
    this.host = host;
    this.family = family ? family : 'IPv4';
    this.port = 'number' === typeof port ? port: (!!port ? Number.parseInt(port) : null);
    this.type = type;
    this.page = page;
    this.incognitoBrowser = incognitoBrowser;
    this.uri = uri
    this.domain = null;
    this.proxy = false;
    this.isResolved = false;
    this.logName = addressName;
  }
  
  clone(port) {
    const address = new Address(this.addressName, this.host, this.family, port ? port : this.port, this.type, this.page, this.incognitoBrowser, this.uri);
    address.domain = this.domain;
    address.proxy = this.proxy;
    address.isResolved = this.isResolved;
    address.logName = this.logName;
    return address;
  }
  
  setLogName(logName) {
    this.logName = logName;
  }
  
  static from(address) {
    const newAddress = new Address(address.addressName, address.host, address.family, address.port, address.type, address.page, address.incognitoBrowser, address.uri);
    newAddress.domain = address.domain;
    newAddress.proxy = address.proxy;
    newAddress.isResolved = address.isResolved;
    address.logName = this.logName;
    return newAddress;
  }
  
  static netAddress(srcAddress, dstAddress, port=-1, protocol='') {
    if(!protocol) {
      if(-1 !== port) {
        return Address._netAddressTcp(srcAddress, dstAddress, port);
      }
      else {
        return Address._netAddressTcp(srcAddress, dstAddress.host, dstAddress.port);
      }
    }
    else {
      if(-1 !== port) {
        return Address._netAddressTls(srcAddress, dstAddress, port);
      }
      else {
        return Address._netAddressTls(srcAddress, dstAddress.host, dstAddress.port);
      }
    }
  }
  
  static _netAddressTcp(srcAddress, address, port) {
    if('DEFAULT_SRC' !== srcAddress.addressName) {
      return {
        localAddress: srcAddress.host,
        localPort: srcAddress.port,
        host: address,
        port: port,
        allowHalfOpen: true
      };
    }
    else {
      return {
        host: address,
        port: port,
        allowHalfOpen: true
      };
    }
  }
  
  static _netAddressTls(srcAddress, address, port) {
    if('DEFAULT_SRC' !== srcAddress.addressName) {
      return {
        localAddress: srcAddress.host,
        localPort: srcAddress.port,
        host: address,
        port: port,
        allowHalfOpen: true,
        checkServerIdentity: (host, cert) => {}
      };
    }
    else {
      return {
        host: address,
        port: port,
        allowHalfOpen: true,
        checkServerIdentity: (host, cert) => {}
      };
    }
  }
  
  resolve(portDefault) {
    if(this.isResolved) {
      return true;
    }
    if(!!this.uri) {
      const url = new URL(this.uri);
      this.domain = url.hostname;
      this.port = url.port ? Number.parseInt(url.port) : this.port;
    }
    if(!this.port) {
      if(!portDefault) {
        this.port = 0;
      }
      else {
        this.port = portDefault;
      }
    }
    return 'DEFAULT_SRC' === this.addressName;
  }
  
  setResolved(address) {
    this.host = address;
    this.isResolved = true;
  }
  
  setProxy(address) {
    this.host = address.host;
    this.port = address.port;
    this.proxy = true;
  }
}

Address.CLIENT = 'client';
Address.SUT = 'sut';
Address.SERVER = 'server';

Address.LOCAL = true;
Address.REMOTE = false;


module.exports = Address;
