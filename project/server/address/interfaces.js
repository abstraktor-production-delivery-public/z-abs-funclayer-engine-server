
'use strict';

const Address = require('./address');


class Interfaces {
  constructor(loginReport) {
    this.loginReport = loginReport;
  }
  
  calculate(networks, srcs, dsts, srvs) {
    const addresses = {
      srcs: [],
      dsts: [],
      srvs: []
    };
    const addressesSutSearch = new Map();
    const addressesServerSearch = new Map();
    const addressesClientSearch = new Map();
    const networkSearch = new Map();
    
    networks.forEach((network) => {
      networkSearch.set(network.networkName, network);
      network.suts.forEach((sut) => {
        addressesSutSearch.set(sut.interfaceName, sut);
      });
      network.servers.forEach((server) => {
        addressesServerSearch.set(server.interfaceName, server);
      });
      network.clients.forEach((client) => {
        addressesClientSearch.set(client.interfaceName, client);
      });
    });
    srcs.forEach((src) => {
      const addressClient = addressesClientSearch.get(src.interfaceName);
      if(!!addressClient) {
        const network = networkSearch.get(addressClient.networkName);
        if(!!network) {
          addresses.srcs.push([src.addressName, new Address(src.addressName, addressClient.address, network.family, src.port, Address.CLIENT, src.page, src.incognitoBrowser)]);
        }
        else {
          this.loginReport.networkLog(`Src network '${addressClient.networkName}' does not exist.`);
          // TODO: create log
          ddb.error(`src network '${addressClient.networkName}' does not exist.`);
        }
      }
      else {
        // TODO: create log
        ddb.error(`src address interface '${src.interfaceName}' does not exist.`);
      }
    });
    dsts.forEach((dst) => {
      const addressSut = addressesSutSearch.get(dst.interfaceName);
      if(!!addressSut) {
        const network = networkSearch.get(addressSut.networkName);
        if(!!network) {
          addresses.dsts.push([dst.addressName, new Address(dst.addressName, addressSut.address, network.family, dst.port, Address.SUT, undefined, undefined, dst.uri)]);
        }
        else {
          this.loginReport.networkLog(`Dst network '${addressSut.networkName}' does not exist.`);
          // TODO: create log
          ddb.error(`dst network '${addressSut.networkName}' does not exist.`);
        }
      }
      else if(!!dst.uri) {
        addresses.dsts.push([dst.addressName, new Address(dst.addressName, null, null, dst.port, Address.SUT, undefined, undefined, dst.uri)]);
      }
      else {
        // TODO: create log
        ddb.error(`dst address interface '${dst.interfaceName}' does not exist.`);
      }
    });
    const srvAddresses = new Map();
    srvs.forEach((srv) => {
      const addressServer = addressesServerSearch.get(srv.interfaceName);
      if(!!addressServer) {
        const network = networkSearch.get(addressServer.networkName);
        if(!!network) {
          addresses.srvs.push([srv.addressName, new Address(srv.addressName, addressServer.address, network.family, srv.port, Address.SERVER, undefined, undefined, srv.uri)]);
        }
        else {
          this.loginReport.networkLog(`Srv network '${addressServer.networkName}' does not exist.`);
          // TODO: create log
          ddb.error(`srv network '${addressServer.networkName}' does not exist.`);
        }
      }
      else if(!!srv.uri) {
        addresses.srvs.push([srv.addressName, new Address(srv.addressName, null, null, srv.port, Address.SERVER, undefined, undefined, srv.uri)]);
      }
      else {
        // TODO: create log
        ddb.error(`srv address interface '${srv.interfaceName}' does not exist.`);
      }
    });
    return addresses;
  }
}

module.exports = Interfaces;
