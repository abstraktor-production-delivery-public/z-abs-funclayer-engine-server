
'use strict';

const Addresses = require('./addresses');
const AddressData = require('./address-data');
const Interfaces = require('./interfaces');
const LocalDns = require('./local-dns');
const Networks = require('./networks');
const RealNetworks = require('./real-networks');
const LoginReport = require('./login-report');


class AddressCalculator {
  constructor() {
    this.loginReport = new LoginReport();
    this.addressData = new AddressData();
    this.interfaces = new Interfaces(this.loginReport);
    this.realNetworks = new RealNetworks(this.loginReport);
    this.networksLogin = null;
    this.addressesLogin = null;
    this.realNetworksLogin = null;
  }
  
  calculateAndSave(labId, userId, systemUnderTest, systemUnderTestInstance, forceLocalhost, done) {
    this.calculateInit((err, results) => {
      this.realNetworksLogin = this.realNetworks.get();
      this.addressesLogin = this.addressData.get(labId, userId, systemUnderTest);
      this.networksLogin = new Networks(this.loginReport, forceLocalhost);
      this.networksLogin.setRealNetworksLogin(this.realNetworksLogin);
      this._setNetworks(this.addressesLogin.interfacesNetwork);
      this._replacePorts(this.addressesLogin.addressesSrc);
      this._replacePorts(this.addressesLogin.addressesDst);
      this._replacePorts(this.addressesLogin.addressesSrv);
      this._calclulateAddresses(forceLocalhost);
      const networks = this.networksLogin.calculate(forceLocalhost);
      const addresses = this.interfaces.calculate(networks, this.addressesLogin.addressesSrc, this.addressesLogin.addressesDst, this.addressesLogin.addressesSrv);
      const dns = this._calclulateLocalDns();
      let pendings = 3;
      this.networksLogin.save(networks, (err) => {
        if(0 === --pendings) {
          done(this.loginReport.logs, this.networksLogin.getChosenAddresses());
        }
      });
      Addresses.save(addresses, (err) => {
        if(0 === --pendings) {
          done(this.loginReport.logs, this.networksLogin.getChosenAddresses());
        }
      });
      LocalDns.save(dns, (err) => {
        if(0 === --pendings) {
          done(this.loginReport.logs, this.networksLogin.getChosenAddresses());
        }
      });
    });
  }
  
  calculateInit(done) {
    this.realNetworks.read();
    this.addressData.read((err, results) => {
      done(err, results);
    });
  }
  
  calculate(labId, userId, systemUnderTest, systemUnderTestInstance, forceLocalhost=false) {
    this.realNetworksLogin = this.realNetworks.get();
    this.addressesLogin = this.addressData.get(labId, userId, systemUnderTest);
    this.networksLogin = new Networks(this.loginReport, forceLocalhost);
    this.networksLogin.setRealNetworksLogin(this.realNetworksLogin);
    this._setNetworks(this.addressesLogin.interfacesNetwork);
    this._replacePorts(this.addressesLogin.addressesSrc);
    this._replacePorts(this.addressesLogin.addressesDst);
    this._replacePorts(this.addressesLogin.addressesSrv);
    this._calclulateAddresses(forceLocalhost);
    const networks = this.networksLogin.calculate(forceLocalhost);
    const addresses = this.interfaces.calculate(networks, this.addressesLogin.addressesSrc, this.addressesLogin.addressesDst, this.addressesLogin.addressesSrv);
    const dns = this._calclulateLocalDns();
    return {
      addresses: addresses,
      dns: new Map(dns)
    };
  }
  
  _setNetworks(interfacesNetwork) {
    interfacesNetwork.forEach((interfaceNetwork) => {
      this.networksLogin.setNetwork(interfaceNetwork);
    });
  }
  
  _replacePorts(addresses) {
    addresses.forEach((address) => {
      if(Number.isNaN(Number.parseInt(address.port))) {
        const port = this.addressesLogin.addressesPorts.get(address.port);
        if(undefined !== port) {
          address.port = port.port;
         }
      }
    });
  }
  
  _calclulateAddresses(forceLocalHost) {
    this.addressesLogin.interfacesClient.forEach((interfaceClient) => {
      this.networksLogin.addInterfaceClientToVirtualNetwork(interfaceClient);
    });
    this.addressesLogin.interfacesServer.forEach((interfaceServer) => {
      this.networksLogin.addInterfaceServerToVirtualNetwork(interfaceServer);
    });
    this.addressesLogin.interfacesSut.forEach((interfaceSut) => {
      this.networksLogin.addInterfaceSutToVirtualNetwork(interfaceSut);
    });

    this._generateInferfaceData();
    this._setAddressesStatic();
    this._setAddressesDynamic(forceLocalHost);
  }
  
  _getInterface(name) {
    let _interface = this.addressesLogin.interfacesSut.get(name);
    if(!_interface) {
      _interface = this.addressesLogin.interfacesServer.get(name);
      if(!_interface) {
        _interface = this.addressesLogin.interfacesClient.get(name);
      }
    }
    return _interface;
  }
  
  _calclulateLocalDns() {
    const localDns = new Map();
    this.addressesLogin.addressesDns.forEach((dns) => {
      const interfaceNames = Array.isArray(dns.interfaceName) ? dns.interfaceName : [dns.interfaceName];
      interfaceNames.forEach((interfaceName) => {
        const _interface = this._getInterface(interfaceName);
        if(_interface) {
          if(!localDns.has(dns.uri)) {
            this.loginReport.dnsLog('new', 'ok', `Uri: '${dns.uri}' gets IP-Address: '${_interface.address}' from interface: '${_interface.interfaceName}' on network: '${_interface.networkName}'.`);
            localDns.set(dns.uri, [_interface]);
          }
          else {
            const _interfaces = localDns.get(dns.uri);
            const index = _interfaces.indexOf(interfaceName);
            if(-1 === index) {
              this.loginReport.dnsLog('new', 'ok', `Uri: '${dns.uri}' gets IP-Address: '${_interface.address}' from interface: '${_interface.interfaceName}' on network: '${_interface.networkName}'.`);
              _interfaces.push(_interface);
            }
            else {
              this.loginReport.dnsLog('new', 'error', `Uri: '${dns.uri}' does already contain interfaceName: '${interfaceName}'..`);
            }
          }
        }
        else {
          this.loginReport.dnsLog('new', 'error', `Uri: '${dns.uri}' interface: '${_interface.interfaceName}' not found.`);        
        }
      });
    });
    const resultLocalDns = [];
    localDns.forEach((_interfaces, uri) => {
      const ipAddresses = [];
      _interfaces.forEach((_interface) => {
        ipAddresses.push(_interface.address);
      });
      resultLocalDns.push([uri, {
        position: 0,
        addresses: ipAddresses
      }]);
    });
    return resultLocalDns;
  }
  
  _generateInferfaceData() {
    this.networksLogin.generateInferfaceData();
  }
  
  _setAddressesStatic() {
    this.networksLogin.setAddressesSutStatic(this.addressesLogin.addressesSut);
    this.networksLogin.setAddressesServerStatic(this.addressesLogin.addressesServer);
    this.networksLogin.setAddressesClientStatic(this.addressesLogin.addressesClient);
  }
  
  _setAddressesDynamic(forceLocalHost) {
    this.networksLogin.sort();
    if(0 !== this.networksLogin.virtualNetworksIPv4.size) {
      if(0 !== this.realNetworksLogin.realNetworksIPv4.size && !forceLocalHost) {
        this._calculateAddressesNetwork(this.networksLogin.virtualNetworksIPv4, this.realNetworksLogin.realNetworksIPv4);
      }
      else {
        this._calculateAddressesNetwork(this.networksLogin.virtualNetworksIPv4, this.realNetworksLogin.localNetworksIPv4);
      }
    }
    if(0 !== this.networksLogin.virtualNetworksIPv6.size) {
      if(0 !== this.realNetworksLogin.realNetworksIPv6.size && !forceLocalHost) {
        this._calculateAddressesNetwork(this.networksLogin.virtualNetworksIPv6, this.realNetworksLogin.realNetworksIPv6);
      }
      else {
        this._calculateAddressesNetwork(this.networksLogin.virtualNetworksIPv6, this.realNetworksLogin.localNetworksIPv6);
      }
    }
  }

  _calculateAddressesNetwork(virtualNetworks, realOrLocalNetworks) {
    virtualNetworks.forEach((virtualNetwork) => {
      const realOrLocalNetwork = this._getNetwork(virtualNetwork.subnet, realOrLocalNetworks);
      if(undefined !== realOrLocalNetwork) {
        realOrLocalNetwork.addresses.forEach((address) => {
          address.amount.set(virtualNetwork.interfaceNetwork.networkName, {
            client: 0,
            server: 0,
            sut: 0
          });
        });
        if(realOrLocalNetwork.amount >= 2) {
          virtualNetwork.interfaceNetwork.reduced = true;
        }
        this._calculateAddressesUsage(virtualNetwork, realOrLocalNetwork);
        this._handOutAddresses(virtualNetwork, realOrLocalNetwork);
      }
    });
  }
  
  _getNetwork(subnet, realOrLocalNetworks) {
    const realOrLocalNetwork = realOrLocalNetworks.get(subnet);
    if(undefined !== realOrLocalNetwork) {
      return realOrLocalNetwork;
    }
    else {
      let minRealOrLocalNetwork = {
        amount: 10000000
      };
      realOrLocalNetworks.forEach((realOrLocalNetwork) => {
        if(realOrLocalNetwork.amount < minRealOrLocalNetwork.amount) {
          minRealOrLocalNetwork = realOrLocalNetwork;
        }
      });
      ++minRealOrLocalNetwork.amount;
      return minRealOrLocalNetwork;    
    }
  }
  
  _calculateAddressesUsage(virtualNetwork, realOrLocalNetwork) {
    virtualNetwork.interfacesSut.forEach((interfaceSut) => {
      this._calculateAddressUsage(interfaceSut, realOrLocalNetwork, (realOrLocalAddress) => {
        ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).sut;
      });
    });
    virtualNetwork.interfacesServer.forEach((interfaceServer) => {
      this._calculateAddressUsage(interfaceServer, realOrLocalNetwork, (realOrLocalAddress) => {
        ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).server;
      });
    });
    virtualNetwork.interfacesClient.forEach((interfaceClient) => {
      this._calculateAddressUsage(interfaceClient, realOrLocalNetwork, (realOrLocalAddress) => {
        ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).client;
      });
    });
  }
  
  _calculateAddressUsage(interfaceX, realOrLocalNetwork, cbAmount) {
    if(interfaceX.static) {
      let found = false;
      realOrLocalNetwork.addresses.forEach((address) => {
        if(address.address.address === interfaceX.address) {
          found = true;
          cbAmount(address);
        }
      });
      if(!found) {
        
      }
    }
  }
  
  _handOutAddresses(virtualNetwork, realOrLocalNetwork) {
    const freeAddresses = this._getFreeAddresses(virtualNetwork, realOrLocalNetwork);
    if(0 !== freeAddresses.addresses.length) {
      this._handOutChosenAddresses(virtualNetwork, freeAddresses);
    }
    else {
      this._handOutChosenAddresses(virtualNetwork, realOrLocalNetwork);
    }
  }

  _handOutChosenAddresses(virtualNetwork, chosenAddresses) {
    let uniqueAddressIndices = {
      sut: {
        start: 0,
        stop: 0
      },
      server: {
        start: 0,
        stop: 0
      },
      client: {
        start: 0,
        stop: 0
      }
    };
    if(0 !== chosenAddresses.addresses.length) {
      const neededDynamicAddressesSut = virtualNetwork._amountSutDynamic;
      const neededDynamicAddressesServer = virtualNetwork._amountServerDynamic;
      const neededDynamicAddressesClient = virtualNetwork._amountClientDynamic;
      let neededDynamicAddresses = neededDynamicAddressesSut + neededDynamicAddressesServer + neededDynamicAddressesClient;
      let existingAddresses = chosenAddresses.addresses.length;
      let index = 0;
      if(neededDynamicAddressesSut < existingAddresses) {
        uniqueAddressIndices.sut.stop = neededDynamicAddressesSut;
        if(neededDynamicAddressesServer < existingAddresses - neededDynamicAddressesSut) {
          uniqueAddressIndices.server.start = uniqueAddressIndices.sut.stop;
          uniqueAddressIndices.server.stop = uniqueAddressIndices.server.start + neededDynamicAddressesServer;
          uniqueAddressIndices.client.start = uniqueAddressIndices.server.stop;
          if(neededDynamicAddressesClient < existingAddresses - neededDynamicAddressesSut - neededDynamicAddressesServer) {
            uniqueAddressIndices.client.stop = uniqueAddressIndices.client.start + neededDynamicAddressesClient;
          }
          else {
            let clients = Math.min(neededDynamicAddressesClient, existingAddresses - neededDynamicAddressesSut - neededDynamicAddressesServer);
            uniqueAddressIndices.client.stop = uniqueAddressIndices.client.start + clients;
          }
        }
        else {
          let servers = 0;
          let clients = 0;
          if(0 === neededDynamicAddressesClient) {
            servers = Math.min(neededDynamicAddressesServer, existingAddresses - neededDynamicAddressesSut);
          }
          else
          {
            servers = Math.min(neededDynamicAddressesServer, existingAddresses - neededDynamicAddressesSut - 1);
            clients = Math.min(neededDynamicAddressesClient, existingAddresses - neededDynamicAddressesSut - neededDynamicAddressesServer);
          }
          uniqueAddressIndices.server.start = uniqueAddressIndices.sut.stop;
          uniqueAddressIndices.server.stop = uniqueAddressIndices.server.start + servers;
          uniqueAddressIndices.client.start = uniqueAddressIndices.server.stop;
          uniqueAddressIndices.client.stop = uniqueAddressIndices.client.start + clients;
        }
      }
      else {
        if(0 === neededDynamicAddressesServer && 0 === neededDynamicAddressesClient) {
          uniqueAddressIndicesIndices.sut.stop = existingAddresses;
        }
        else {
          uniqueAddressIndices.sut.stop = existingAddresses - 1;
          if(0 !== neededDynamicAddressesServer) {
            uniqueAddressIndices.server.start = existingAddresses - 1;
            uniqueAddressIndices.server.stop = existingAddresses;
          }
          if(0 !== neededDynamicAddressesClient) {
            uniqueAddressIndices.client.start = existingAddresses - 1;
            uniqueAddressIndices.client.stop = existingAddresses;
          }
        }
      }
    }
    this.networksLogin.setAddressesDynamic(virtualNetwork.interfacesSut, 'sut', virtualNetwork, chosenAddresses, uniqueAddressIndices.sut, (realOrLocalAddress) => {
      ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).sut;
    });
    this.networksLogin.setAddressesDynamic(virtualNetwork.interfacesServer, 'server', virtualNetwork, chosenAddresses, uniqueAddressIndices.server, (realOrLocalAddress) => {
      ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).server;
    });
    this.networksLogin.setAddressesDynamic(virtualNetwork.interfacesClient, 'client', virtualNetwork, chosenAddresses, uniqueAddressIndices.client, (realOrLocalAddress) => {
      ++realOrLocalAddress.amount.get(virtualNetwork.interfaceNetwork.networkName).client;
    });
  }
  
  _getFreeAddresses(virtualNetwork, realOrLocalNetwork) {
    const freeAddresses = {
      addresses: []
    };
    realOrLocalNetwork.addresses.forEach((address) => {
      const amount = address.amount.get(virtualNetwork.interfaceNetwork.networkName);
      if(0 === amount.sut && 0 === amount.server && 0 === amount.client) {
        freeAddresses.addresses.push(address);
      }
    });
    return freeAddresses;
  }
}


module.exports = AddressCalculator;
