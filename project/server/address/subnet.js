
'use strict';


class Subnet {
  static calculate(virtualInterface) {
    if('IPv4' === virtualInterface.family) {
      const addressSplit = 'localhost' !== virtualInterface.address ? virtualInterface.address : '127.0.0.1';
      return Subnet.subnet(addressSplit.split('.'), virtualInterface.netmask.split('.'), 10);
    }
    else if('IPv6' === virtualInterface.family) {
      const address = 'localhost' !== virtualInterface.address ? virtualInterface.address : '::1';
      return Subnet.subnet(Subnet.formatIPv6(virtualInterface.address), Subnet.formatIPv6(virtualInterface.netmask), 16);
    }
  }
  
  static formatIPv6(address) {
    const addressParts = address.split(':');
    const index = addressParts.findIndex((addressPart) => {
      return '' === addressPart;
    });
    if(-1 !== index) {
      const size = addressParts.length - 2;
      addressParts.splice(index, 2, ...Array(8 - size).fill(0));
    }
    return addressParts;
  }
  
  static subnet(addressParts, netmaskParts, base) {
    let subnet = '';
    let i = 0
    for(; i < addressParts.length - 1; ++i) {
      subnet += (Number.parseInt(addressParts[i], base) & Number.parseInt(netmaskParts[i], base)) + (10 === base ? '.' : ':');
    }
    subnet += (Number.parseInt(addressParts[i], base) & Number.parseInt(netmaskParts[i], base));
    return `${subnet}/${Subnet.netmaskBits(netmaskParts, base)}`;
  }
  
  static netmaskBits(netmaskParts, base) {
    if(10 === base) {
      let sum = 0;
      for(let i = 0; i < netmaskParts.length; ++i) {
        const binary = Number.parseInt(netmaskParts[i]).toString(2).padStart(8, '0');
        const firstZero = binary.indexOf('0');
        if(-1 === firstZero) {
          sum += 8;
        }
        else {
          sum += firstZero;
          return sum;
        }
      }
    }
    else {
      return 'ToBeImpl';
    }
  }
}

module.exports = Subnet;
