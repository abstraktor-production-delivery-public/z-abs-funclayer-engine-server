
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const Fs = require('fs');


class LocalDns {
  static save(dns, done) {
    Fs.writeFile(ActorPathGenerated.getLocalDnsFile(), JSON.stringify(dns, null, 2), (err) => {
      done(err);
    });
  }
  
  static load(localDns, done) {
    Fs.readFile(ActorPathGenerated.getLocalDnsFile(), (err, data) => {
      if(err) {
        return done(err);
      }
      try {
        const parsedDns = JSON.parse(data);
        parsedDns.forEach((dns) => {
          localDns.set(dns[0], dns[1]);
        });
        done();
      }
      catch(e) {
        done(e);  
      }
    });
  }
}


module.exports = LocalDns;
