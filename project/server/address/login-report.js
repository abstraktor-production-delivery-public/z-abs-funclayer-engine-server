
'use strict';


class LoginReport {
  constructor() {
    this.logs = [];
  }
  
  addressLog(action, status, log) {
    this.logs.push({
      reportType: 'address',
      action: action,
      status: status,
      log: log
    });
  }
  
  realAddressLog(action, status, log) {
    this.logs.push({
      reportType: 'real-address',
      action: action,
      status: status,
      log: log
    });
  }
  
  networkLog(action, status, log) {
    this.logs.push({
      reportType: 'network',
      action: action,
      status: status,
      log: log
    });
  }
  
  interfaceLog(action, status, log) {
    this.logs.push({
      reportType: 'interface',
      action: action,
      status: status,
      log: log
    });
  }
  
  dnsLog(action, status, log) {
    this.logs.push({
      reportType: 'dns',
      action: action,
      status: status,
      log: log
    });
  }
}

module.exports = LoginReport;
