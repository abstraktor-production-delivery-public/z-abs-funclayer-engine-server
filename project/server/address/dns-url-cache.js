
'use strict';

const Address = require('./address');
const Dns = require('dns');
const Net = require('net');


class DnsUrlCache {
  constructor() {
    this.localDns = new Map();
    this.cache = new Map();
  }
  
  resolveAddress(address, local, done) {
    if(address.resolve()) {
      process.nextTick((done) => {
        done(null);
      }, done);
    }
    else {
      this.get(local ? address.host : (address.domain ? address.domain : address.host), address.family, (err, ipAddress) => {
        process.nextTick((done, address, ipAddress) => {
          address.setResolved(ipAddress);
          done(null);
        }, done, address, ipAddress);
      });
    }
  }
  
  setLocalDns(localDns) {
    this.localDns = localDns;
  }
  
  getLocalDns() {
    return this.localDns;
  }
  
  get(domain, family, done) {
    const recordLocal = this.localDns.get(domain);
    if(recordLocal) {
      const position = recordLocal.position++;
      if(position !== recordLocal.addresses.length) {
        return done(null, recordLocal.addresses[position]);
      }
      else {
        recordLocal.position = 0;
        return done(null, recordLocal.addresses[0]);
      }
    }
    const record = this.cache.get(domain);
    if(record) {
      const position = record.position++;
      if(position !== record.addresses.length) {
        return done(null, record.addresses[position]);
      }
      else {
        record.position = 0;
        return done(null, record.addresses[0]);
      }
    }
    if('IPv4' === family) {
      if(Net.isIPv4(domain)) {
        this._Add(domain, [domain]);
        return done(null, domain);
      }
      else {
        Dns.resolve4(domain, (err, addresses) => {
          if(err) {
            return done(err);
          }
          this._Add(domain, addresses);
          return done(null, addresses[0]);
        });
      }
    }
    else if('IPv6' === family) {
      if(Net.isIPv6(domain)) {
        this._Add(domain, [domain]);
        return done(null, domain);
      }
      else {
        Dns.resolve6(domain, (err, addresses) => {
          if(err) {
            return done(err);
          }
          this._Add(domain, addresses);
          return done(null, addresses[0]);
        });
      }
    }
    else {
      return done(new Error(`Unknown IP family '${family}'.`));
    }
  }
  
  _Add(domain, addresses) {
    this.cache.set(domain, {
      position: 0,
      addresses: addresses
    });
  }
  
  override(address) {
    if(address.proxy) {
      this.cache.set(address.uri, {
        position: 0,
        addresses: [address]
      });
    }
  }
}

DnsUrlCache.id = 0;


module.exports = DnsUrlCache;
