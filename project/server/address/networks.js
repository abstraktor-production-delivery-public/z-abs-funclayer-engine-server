
'use strict';

const Subnet = require('./subnet');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const Fs = require('fs');


class Networks {
  constructor(loginReport, forceLocalhost) {
    this.loginReport = loginReport;
    this.forceLocalhost = forceLocalhost;
    this.realNetworksLogin = null;
    this.virtualNetworksIPv4 = new Map();
    this.virtualNetworksIPv6 = new Map();
    this.virtualNetworksIPv4Original = this.virtualNetworksIPv4;
    this.virtualNetworksIPv6Original = this.virtualNetworksIPv6;
    this.chosenInterfaces = [];
    this.missingInterfaces = [];
    this.externalInterfaces = [];
  }
  
  setRealNetworksLogin(realNetworksLogin) {
    this.realNetworksLogin = realNetworksLogin;
  }
  
  calculate(forceLocalhost) {
    const virtualNetworksResult = [];
    this._formatNetworks(virtualNetworksResult, this.virtualNetworksIPv4Original, forceLocalhost);
    this._formatNetworks(virtualNetworksResult, this.virtualNetworksIPv6Original, forceLocalhost);
    this.postAnalyzeFailedInterfaces();
    return virtualNetworksResult;
  }
  
  getChosenAddresses() {
    const realAddressesList = [];
    this.realNetworksLogin.realAddresses.forEach((realAddress) => {
      realAddressesList.push(realAddress);
    });
    return {
      realAddresses: realAddressesList,
      chosenAddresses: this.chosenInterfaces,
      missingAddresses: this.missingInterfaces,
      externalAddresses: this.externalInterfaces
    };
  }
  
  sort() {
    this.virtualNetworksIPv4 = this._sortVirtualNetworks(this.virtualNetworksIPv4);
    this.virtualNetworksIPv6 = this._sortVirtualNetworks(this.virtualNetworksIPv6);
  }
  
  save(virtualNetworksResult, done) {
    Fs.writeFile(ActorPathGenerated.getNetworksFile(), JSON.stringify(virtualNetworksResult, (key, value) => {
	  if(key.startsWith('_xxx')) {
        return undefined;
      }
      return value;
    }, 2), (err) => {
      done(err);
    });
  }
  
  getDefaultNetwork(family) {
    if('IPv4' === family) {
      return Networks.DEAFULT_IPv4;
    }
    else {
      return Networks.DEAFULT_IPv6;
    }
  }
  
  _formatNetworks(virtualNetworksResult, virtualNetworks, forceLocalhost) {
    virtualNetworks.forEach((virtualNetwork) => {
      const family = virtualNetwork.interfaceNetwork.family;
      const network = {
        networkName: virtualNetwork.interfaceNetwork.networkName,
        sut: virtualNetwork.interfaceNetwork.sut,
        family: family,
        subnet: virtualNetwork.interfaceNetwork.subnet,
        description: virtualNetwork.interfaceNetwork.description,
        valid: virtualNetwork.interfaceNetwork.valid,
        reduced: virtualNetwork.interfaceNetwork.reduced,
        forceLocalhost: forceLocalhost,
        suts: [],
        servers: [],
        clients: []
      };
      virtualNetwork.interfacesSut.forEach((sut) => {
        if('' === sut.networkName) {
          sut.networkName = this.getDefaultNetwork(family);
          this.loginReport.networkLog('default', 'not found', `Sut Interface: '${sut.interfaceName}' missing networkName. Choosing default networkName '${this.getDefaultNetwork(family)}'. `);
        }
        network.suts.push(sut);
      });
      virtualNetwork.interfacesServer.forEach((server) => {
        if('' === server.networkName) {
          server.networkName = this.getDefaultNetwork(family);
          this.loginReport.networkLog('default', 'not found', `Server Interface: '${server.interfaceName}' missing networkName. Choosing default networkName '${this.getDefaultNetwork(family)}'. `);
        }
        network.servers.push(server);
      });
      virtualNetwork.interfacesClient.forEach((client) => {
        if('' === client.networkName) {
          client.networkName = this.getDefaultNetwork(family);
          this.loginReport.networkLog('default', 'not found', `Client Interface: '${client.interfaceName}' missing networkName. Choosing default networkName '${this.getDefaultNetwork(family)}'. `);
        }
        network.clients.push(client);
      });
      virtualNetworksResult.push(network);
    });
    if(0 === virtualNetworksResult.length) {
      this.loginReport.networkLog('default', 'not found', `No virtual networks. Creating virtual network '${this.getDefaultNetwork('IPv4')}'.`);
    }
  }
  
  setNetwork(interfaceNetwork) {
    const virtualNetworks = 'IPv4' === interfaceNetwork.family ? this.virtualNetworksIPv4 : this.virtualNetworksIPv6;
    interfaceNetwork.valid = false;
    interfaceNetwork.reduced = false;
    interfaceNetwork.subnet = '';
    const virtualNetwork = {
      interfaceNetwork: interfaceNetwork,
      _amountClientStatic: 0,
      _amountClientDynamic: 0,
      _amountServerStatic: 0,
      _amountServerDynamic: 0,
      _amountSutStatic: 0,
      _amountSutDynamic: 0,
      _amountTotalInternal: 0,
      interfacesClient: new Map(),
      interfacesServer: new Map(),
      interfacesSut: new Map(),
    };
    virtualNetworks.set(interfaceNetwork.networkName, virtualNetwork);
  }
  
  addInterfaceClientToVirtualNetwork(interfaceClient) {
    let virtualNetwork = this._getVirtualNetwork(interfaceClient.networkName);
    if(undefined === virtualNetwork) {
      virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);
      if(undefined === virtualNetwork) {
        this.setNetwork({
          networkName: Networks.DEAFULT_IPv4,
          sut: interfaceClient.sut,
          family: 'IPv4',
          description: 'Default network'
        });
        virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);  
      }
    }
    virtualNetwork.interfacesClient.set(interfaceClient.interfaceName, interfaceClient);
  }
  
  addInterfaceServerToVirtualNetwork(interfaceServer) {
    let virtualNetwork = this._getVirtualNetwork(interfaceServer.networkName);
    if(undefined === virtualNetwork) {
      virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);
      if(undefined === virtualNetwork) {
        this.setNetwork({
          networkName: Networks.DEAFULT_IPv4,
          sut: interfaceServer.sut,
          family: 'IPv4',
          description: 'Default network'
        });
        virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);  
      }
    }
    virtualNetwork.interfacesServer.set(interfaceServer.interfaceName, interfaceServer);
  }
  
  addInterfaceSutToVirtualNetwork(interfaceSut) {
    let virtualNetwork = this._getVirtualNetwork(interfaceSut.networkName);
    if(undefined === virtualNetwork) {
      virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);
      if(undefined === virtualNetwork) {
        this.setNetwork({
          networkName: Networks.DEAFULT_IPv4,
          sut: interfaceSut.sut,
          family: 'IPv4',
          description: 'Default network'
        });
        virtualNetwork = this.virtualNetworksIPv4.get(Networks.DEAFULT_IPv4);  
      }
    }
    virtualNetwork.interfacesSut.set(interfaceSut.interfaceName, interfaceSut);
  }
  
  generateInferfaceData() {
    this._generateInferfaceData(this.virtualNetworksIPv4);
    this._generateInferfaceData(this.virtualNetworksIPv6);
  }
  
  _generateInferfaceData(virtualNetworks) {
    virtualNetworks.forEach((virtualNetwork) => {
      virtualNetwork.interfacesClient.forEach((virtualInterface) => {
        this._setInterfaceData(virtualInterface, virtualNetwork);
      });
      virtualNetwork.interfacesSut.forEach((virtualInterface) => {
        this._setInterfaceData(virtualInterface, virtualNetwork);
      });
      virtualNetwork.interfacesServer.forEach((virtualInterface) => {
        this._setInterfaceData(virtualInterface, virtualNetwork);
      });
    });
  }
  
  _setInterfaceData(virtualInterface, virtualNetwork) {
    virtualInterface.address = null;
    virtualInterface.netmask = null;
    virtualInterface.family = virtualNetwork.interfaceNetwork.family;
    virtualInterface.subnet = null;
    virtualInterface.static = false;
    virtualInterface.multicast = false;
    virtualInterface.external = false;
    virtualInterface.reduced = false;
    virtualInterface.localhost = false;
    virtualInterface.validExist = false;
    virtualInterface.validAddress = false;
    virtualInterface.validNetmask = false;
    virtualInterface.validFamily = false;
    virtualInterface.validSubnet = false;
    virtualInterface.valid = false;
  }
  
  _setInterfaceValid(virtualInterface) {
    virtualInterface.valid = virtualInterface.validExist && virtualInterface.validAddress && (virtualInterface.multicast || virtualInterface.external || virtualInterface.validNetmask) && virtualInterface.validFamily && virtualInterface.validSubnet;
    return virtualInterface.valid;
  }
  
  setAddressesClientStatic(addressesClient) {
    this._setAddressesClientStatic(addressesClient, this.virtualNetworksIPv4);
    this._setAddressesClientStatic(addressesClient, this.virtualNetworksIPv6);
  }
  
  setAddressesServerStatic(addressesServer) {
    this._setAddressesServerStatic(addressesServer, this.virtualNetworksIPv4);
    this._setAddressesServerStatic(addressesServer, this.virtualNetworksIPv6);
  }
  
  setAddressesSutStatic(addressesSut) {
    this._setAddressesSutStatic(addressesSut, this.virtualNetworksIPv4);
    this._setAddressesSutStatic(addressesSut, this.virtualNetworksIPv6);
  }
  
  _setAddressesClientStatic(addressesClient, virtualNetworks) {
    virtualNetworks.forEach((virtualNetwork) => {
      virtualNetwork.interfacesClient.forEach((interfaceClient) => {
        if(this._setAddressStatic(addressesClient, interfaceClient, virtualNetwork, Networks.INTERFACE_CLIENT)) {
          ++virtualNetwork._amountClientStatic;
        }
        else {
          ++virtualNetwork._amountClientDynamic;
          ++virtualNetwork._amountTotalInternal;
        }
      });
    });
  }
  
  _setAddressesServerStatic(addressesServer, virtualNetworks) {
    virtualNetworks.forEach((virtualNetwork) => {
      virtualNetwork.interfacesServer.forEach((interfaceServer) => {
        if(this._setAddressStatic(addressesServer, interfaceServer, virtualNetwork, Networks.INTERFACE_SERVER)) {
          ++virtualNetwork._amountServerStatic;
        }
        else {
          ++virtualNetwork._amountServerDynamic;
          ++virtualNetwork._amountTotalInternal;
        }
      });
    });
  }

  _setAddressesSutStatic(addressesSut, virtualNetworks) {
    virtualNetworks.forEach((virtualNetwork) => {
      virtualNetwork.interfacesSut.forEach((interfaceSut) => {
        if(this._setAddressStatic(addressesSut, interfaceSut, virtualNetwork, Networks.INTERFACE_SUT)) {
          ++virtualNetwork._amountSutStatic;
        }
        else if('ip' === interfaceSut.type) {
          ++virtualNetwork._amountSutDynamic;
          ++virtualNetwork._amountTotalInternal;
        }
      });
    });
  }
  
  _getRealAddress(address, virtualInterface) {
    if('localhost' !== address) {
      return this.realNetworksLogin.realAddresses.get(address);
    }
    else if('IPv4' === virtualInterface.family) {
      return this.realNetworksLogin.realAddresses.get('127.0.0.1');
    }
    else if('IPv6' === virtualInterface.family) {
      return this.realNetworksLogin.realAddresses.get('::1');
    }
  }
  
  _setAddressStatic(addresses, virtualInterface, virtualNetwork, interfaceType) {
    const address = addresses.get(virtualInterface.interfaceName);
    if(undefined !== address) {
      virtualInterface.address = address.address;
      virtualInterface.netmask = address.netmask;
      virtualInterface.external = 'true' === address.external ? true : false;
      virtualInterface.static = true;
      if('IPv4' === virtualInterface.family) {
        virtualInterface.localhost = '127.0.0.1' === virtualInterface.address || 'localhost' === virtualInterface.address;
      }
      else {
        virtualInterface.localhost = '::1' === virtualInterface.address || 'localhost' === virtualInterface.address;
      }
      virtualInterface.validAddress = this._verifyAddress(virtualInterface.address, virtualInterface.localhost);
      virtualInterface.validNetmask = this._verifyNetmask(virtualInterface.netmask);
      const realAddress = this._getRealAddress(address.address, virtualInterface);
      if(Networks.INTERFACE_SUT === interfaceType) {
        virtualInterface.multicast = this._isMulticast(virtualInterface.family, virtualInterface.address);
        if(virtualInterface.external) {
          this.externalInterfaces.push(virtualInterface);
        }
        this.loginReport.interfaceLog('new', 'not found', `Static Address '${address.address}' does not exist.`);
      }
      else if(undefined !== realAddress) {
        virtualInterface.validExist = true;
        virtualInterface.validFamily = virtualInterface.family === realAddress.family;
        this.loginReport.interfaceLog('new', 'ok',  `Static Address '${address.address}'.`);
      }
      else if(this._isMulticast(virtualInterface.family, virtualInterface.address)) {
        virtualInterface.multicast = true;
        virtualInterface.validExist = true;
        virtualInterface.validSubnet = true;
        virtualInterface.validFamily =  this._getIpFamily(virtualInterface.address) === virtualNetwork.interfaceNetwork.family;
      }
      else {
        virtualInterface.validFamily = virtualInterface.family === virtualNetwork.interfaceNetwork.family;
        this.loginReport.interfaceLog('new', 'not found', `Static Address '${address.address}' does not exist.`);
      }
      if(virtualInterface.validExist) {
        this._verifySubnet(virtualNetwork, virtualInterface);
      }
      else if(!virtualInterface.external) {
        if(virtualInterface.validAddress && virtualInterface.validNetmask && virtualInterface.validFamily) {
          const subnet = Subnet.calculate(virtualInterface);
          virtualInterface.subnet = subnet;
        }
        this.missingInterfaces.push(virtualInterface);
      }
      if(this._setInterfaceValid(virtualInterface)) {
        this.chosenInterfaces.push(virtualInterface);
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  _postAnalyzeFailedInterfaces(virtualInterface, analyzeSubnet = false) {
    const virtualNetwork = this._getVirtualNetwork(virtualInterface.networkName);
    if(undefined !== virtualNetwork) {
      virtualInterface.validFamily = virtualInterface.family === virtualNetwork.interfaceNetwork.family;
      if(analyzeSubnet) {
        const subnet = Subnet.calculate(virtualInterface);
        virtualInterface.validSubnet = subnet === virtualNetwork.interfaceNetwork.subnet || '' === virtualNetwork.interfaceNetwork.subnet;
      }
    }
  }
  
  postAnalyzeFailedInterfaces() {
    this.externalInterfaces.forEach((externalInterface) => {
      this._postAnalyzeFailedInterfaces(externalInterface);
    });
    this.missingInterfaces.forEach((missingInterface) => {
      this._postAnalyzeFailedInterfaces(missingInterface, true);
    });
  }
    
  _verifyAddress(address, localhost = false) {
    if(localhost) {
      return true;
    }
    const family = this._getIpFamily(address);
    if('IPv4' === family) {
      const partsIPv4 = address.split('.');
      if(4 !== partsIPv4.length) {
        return false;
      }
      else {
        for(let i = 0; i < 4; ++i) {
          const part = partsIPv4[i];
          if(0 >= part.length || 4 <= part.length) {
            return false;
          }
          else {
            for(let i = 0; i < part.length; ++i) {
              const ascii = part.charCodeAt(i);
              if(48 > ascii || 57 < ascii) {
                return false;
              }
            }
          }
          const val = Number.parseInt(part);
          if(-1 >= val || 256 <= val) {
            return false;
          }
        }
      }
      return true;
    }
    else if('IPv6' === family) {
      const partsIPv6 = address.split(':');
      if(0 >= partsIPv6.length && 9 <= partsIPv6.length) {
        return false;
      }
      else {
        for(let i = 0; i < partsIPv6.length; ++i) {
          const part = partsIPv6[i];
          if(-1 >= part.length || 5 <= part.length) {
            return false;
          }
          else {
            for(let i = 0; i < part.length; ++i) {
              const ascii = part.charCodeAt(i);
              if((48 > ascii || 57 < ascii) && (97 > ascii || 102 < ascii)) {
                return false;
              }
            }
          }
          const val = Number.parseInt(part, 16);
          if(-1 >= val || 0x10000 <= val) {
            return false;
          }
        }
      }
    }
    else {
      return false;
    }
  }
  
  _verifyNetmask(netmask) {
    if(!this._verifyAddress(netmask)) {
      return false;
    }
    const family = this._getIpFamily(netmask);
    if('IPv4' === family) {
      const partsIPv4 = netmask.split('.');
      let mergedBinary = '';
      for(let i = 0; i < partsIPv4.length; ++i) {
        mergedBinary += Number.parseInt(partsIPv4[i]).toString(2).padStart(8, '0');
      }
      const firstZero = mergedBinary.indexOf('0');
      if(-1 === firstZero) {
        return true;
      }
      const lastOne = mergedBinary.lastIndexOf('1');
      return lastOne === firstZero - 1;
    }
    else if('IPv6' === family) {
      return true;
    }
    else {
      return false;
    }
  }
  
  _getIpFamily(address) {
    const partsIPv4 = address.split('.');
    if(4 === partsIPv4.length) {
      return 'IPv4';
    }
    const partsIPv6 = address.split(':'); // ???
    if(1 <= partsIPv6.length) {
      return 'IPv6';
    }
    return '';
  }
  
  _getRealOrLocalNetworks(family, realNetworkLogin, localhost) {
    if('IPv4' === family) {
      if(0 !== realNetworkLogin.realNetworksIPv4.size && !this.forceLocalhost && !localhost) {
        return realNetworkLogin.realNetworksIPv4;
      }
      else {
        return realNetworkLogin.localNetworksIPv4;
      }
    }
    else {
      if(0 !== realNetworkLogin.realNetworksIPv6.size && !this.forceLocalhost) {
        return realNetworkLogin.realNetworksIPv6;
      }
      else {
        return realNetworkLogin.localNetworksIPv6;
      }
    }
  }
  
  _verifySubnet(virtualNetwork, virtualInterface) {
    if(virtualInterface.validAddress && virtualInterface.validNetmask && virtualInterface.validFamily) {
      const subnet = Subnet.calculate(virtualInterface);
      virtualInterface.subnet = subnet;
      if(virtualInterface.validExist) {
        const realOrLocalNetworks = this._getRealOrLocalNetworks(virtualInterface.family, this.realNetworksLogin, virtualInterface.localhost);
        if(realOrLocalNetworks.has(subnet)) {
          virtualInterface.validSubnet = true;
          if('' === virtualNetwork.interfaceNetwork.subnet) {
            virtualNetwork.interfaceNetwork.subnet = subnet;
            virtualNetwork.interfaceNetwork.valid = true;
          }
        }
      }
    }
  }
  
  setAddressesDynamic(virtualInterfaces, interfaceGroup, virtualNetwork, chosenAddresses, uniqueAddressIndices, cbAmount) {
    let index = uniqueAddressIndices.start;
    virtualInterfaces.forEach((virtualInterface) => {
      if(!virtualInterface.static && !('sut' === interfaceGroup && 'gui' === virtualInterface.type)) {
        if(0 !== chosenAddresses.addresses.length) {
          const address = chosenAddresses.addresses[index];
          virtualInterface.address = address.address.address;
          virtualInterface.netmask = address.address.netmask;
          virtualInterface.reduced = this._reduced(address);
          const realAddress = this.realNetworksLogin.realAddresses.get(virtualInterface.address);
          if(undefined !== realAddress) {
            virtualInterface.validExist = true;
            virtualInterface.validFamily = virtualInterface.family === realAddress.family;
          }
          virtualInterface.validAddress = this._verifyAddress(virtualInterface.address, virtualInterface.localhost);
          virtualInterface.validNetmask = this._verifyNetmask(virtualInterface.netmask);
          this._verifySubnet(virtualNetwork, virtualInterface);
          if(this._setInterfaceValid(virtualInterface)) {
            this.chosenInterfaces.push(virtualInterface);
          }
          cbAmount(address);
          if(++index >= uniqueAddressIndices.stop) {
            index = uniqueAddressIndices.start;
          }
        }
      }
    });
  }
  
  _reduced(address) {
    let sumAmount = 0;
    address.amount.forEach((amount) => {
      sumAmount += amount.sut + amount.server + amount.client;
    });
    return 0 !== sumAmount;
  }
  
  _getVirtualNetwork(name) {
    let virtualNetwork = this.virtualNetworksIPv4.get(name);
    if(undefined === virtualNetwork) {
      virtualNetwork = this.virtualNetworksIPv6.get(name);
    }
    return virtualNetwork;
  }
  
  _isMulticast(family, address) {
    if('IPv4' === family) {
      // Do the address start with 1110
      const parts = address.split('.');
      if(4 !== parts.length) {
        return false;
      }
      const firstPart = Number.parseInt(parts[0]);
      return 14 === firstPart >> 4; 
    }
    else {
      ddb.warning('Calculation of multicast IPv6 is not implemented.');  
    }
  }

  _sortVirtualNetworks(map) {
    return new Map([...map.entries()].sort((a, b) => {
      if(b[1]._amountTotalInternal < a[1]._amountTotalInternal) {
        return -1;
      }
      else if(b[1]._amountTotalInternal > a[1]._amountTotalInternal) {
        return 1;
      }
      else {
        return 0;
      }
    }));
  }
}

Networks.DEAFULT_IPv4 = 'DEFAULT_IPv4';
Networks.DEAFULT_IPv6 = 'DEFAULT_IPv6';

Networks.INTERFACE_CLIENT = 0;
Networks.INTERFACE_SUT = 1;
Networks.INTERFACE_SERVER = 2;


module.exports = Networks;
