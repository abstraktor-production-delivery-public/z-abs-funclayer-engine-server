
'use strict';

const Address = require('./address');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');


class Addresses {
  constructor() {
    this.srcs = new Map();
    this.sutSrvs = new Map();
    this.srvs = new Map();
  }
  
  getSrc(name) {
    if(undefined !== name) {
      const src = this.srcs.get(name);
      if(src) {
        return src;
      }
      else {
        return this.sutSrvs.get(name);
      }
    }
    else {
      const firstSrc = this.srcs.values().next().value;
      if(firstSrc) {
        return firstSrc;
      }
      else {
        return this.sutSrvs.values().next().value;
      }
    }
  }
  
  getDst(name) {
    if(name) {
      const dst = this.sutSrvs.get(name);
      if(undefined !== dst) {
        return dst;
      }
      else {
        return this.srvs.get(name);
      }
    }
    else {
      const dst = this.sutSrvs.values().next().value;
      if(dst) {
        return dst;
      }
      else {
        return this.srvs.values().next().value;
      }
    }
  }
  
  getSrv(name) {
    if(undefined !== name) {
      const srv = this.srvs.get(name);
      if(srv) {
        return srv;
      }
      else {
        return this.sutSrvs.get(name);
      }
    }
    else {
      const firstSrv = this.srvs.values().next().value;
      if(firstSrv) {
        return firstSrv;
      }
      else {
        return this.sutSrvs.values().next().value;
      }
    }
  }
  
  getDefaultSrc() {
    
  }
  
  sut(name) {
    return this.srvs.has(name);
  }
    
  static save(addresses, done) {
    Fs.writeFile(ActorPathGenerated.getAddressesFile(), JSON.stringify(addresses, null, 2), (err) => {
      done(err);
    });
  }

  static set(from, to) {
    if(undefined !== from) {
      from.forEach((value) => {
        const address = value[1];
        to.set(value[0], new Address(address.addressName, address.host, address.family, address.port, address.type, address.page, address.incognitoBrowser, address.uri));
      });
    }
  }
  
  static load(addresses, done) {
    Fs.readFile(ActorPathGenerated.getAddressesFile(), (err, data) => {
      if(err) {
        return done(err);
      }
      try {
        const parsedAddresses = JSON.parse(data);
        Addresses.set(parsedAddresses.srcs, addresses.srcs);
        Addresses.set(parsedAddresses.dsts, addresses.sutSrvs);
        Addresses.set(parsedAddresses.srvs, addresses.srvs);
        done();
      }
      catch(e) {
        done(e);  
      }
    });
  }
}

module.exports = Addresses;
