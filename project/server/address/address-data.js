
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');
const V8 = require('v8');


class AddressData {
  constructor() {
    this.interfacesNetworkGlobal = null;
    this.interfacesNetworkLocal = null;
    this.interfacesClientGlobal = null;
    this.interfacesClientLocal = null;
    this.interfacesServerGlobal = null;
    this.interfacesServerLocal = null;
    this.interfacesSutGlobal = null;
    this.interfacesSutLocal = null;
    this.addressesSutGlobal = null;
    this.addressesSutLocal = null;
    this.addressesClientGlobal = null;
    this.addressesClientLocal = null;
    this.addressesServerGlobal = null;
    this.addressesServerLocal = null;
    this.addressesSrcGlobal = null;
    this.addressesSrcLocal = null;
    this.addressesDstGlobal = null;
    this.addressesDstLocal = null;
    this.addressesSrvGlobal = null;
    this.addressesSrvLocal = null;
    this.addressesPortsGlobal = null;
    this.addressesPortsLocal = null;
    this.addressesDnsGlobal = null;
    this.addressesDnsLocal = null;
  }
  
  get(labId, userId, sut) {
    const result = {
      interfacesNetwork: [],
      interfacesClient: new Map(),
      interfacesServer: new Map(),
      interfacesSut: new Map(),
      addressesClient: new Map(),
      addressesServer: new Map(),
      addressesSut: new Map(),
      addressesSrc: new Map(),
      addressesDst: new Map(),
      addressesSrv: new Map(),
      addressesPorts: new Map(),
      addressesDns: new Map()
    };
    this.interfacesNetworkGlobal.forEach((interfaceNetwork) => {
      if(sut === interfaceNetwork.sut) {
        result.interfacesNetwork.push(interfaceNetwork);
      }
    });
    this.interfacesNetworkLocal.forEach((interfaceNetwork) => {
      if(sut === interfaceNetwork.sut) {
        result.interfacesNetwork.push(interfaceNetwork);
      }
    });
    this.interfacesClientGlobal.forEach((interfaceClient) => {
      if(sut === interfaceClient.sut) {
        interfaceClient.address = null;
        result.interfacesClient.set(interfaceClient.interfaceName, interfaceClient);
      }
    });
    this.interfacesClientLocal.forEach((interfaceClient) => {
      if(sut === interfaceClient.sut) {
        interfaceClient.address = null;
        result.interfacesClient.set(interfaceClient.interfaceName, interfaceClient);
      }
    });
    this.interfacesServerGlobal.forEach((interfaceServer) => {
      if(sut === interfaceServer.sut) {
        interfaceServer.address = null;
        result.interfacesServer.set(interfaceServer.interfaceName, interfaceServer);
      }
    });
    this.interfacesServerLocal.forEach((interfaceServer) => {
      if(sut === interfaceServer.sut) {
        interfaceServer.address = null;
        result.interfacesServer.set(interfaceServer.interfaceName, interfaceServer);
      }
    });
    this.interfacesSutGlobal.forEach((interfaceSut) => {
      if(sut === interfaceSut.sut) {
        interfaceSut.address = null;
        result.interfacesSut.set(interfaceSut.interfaceName, interfaceSut);
      }
    });
    this.interfacesSutLocal.forEach((interfaceSut) => {
      if(sut === interfaceSut.sut) {
        interfaceSut.address = null;
        result.interfacesSut.set(interfaceSut.interfaceName, interfaceSut);
      }
    });
    this.addressesSutGlobal.forEach((addressSut) => {
      this._addStaticAddress(sut, labId, userId, addressSut, result.addressesSut);
    });
    this.addressesSutLocal.forEach((addressSut) => {
      this._addStaticAddress(sut, labId, userId, addressSut, result.addressesSut);
    });
    this.addressesClientGlobal.forEach((addressClient) => {
      this._addStaticAddress(sut, labId, userId, addressClient, result.addressesClient);
    });
    this.addressesClientLocal.forEach((addressClient) => {
      this._addStaticAddress(sut, labId, userId, addressClient, result.addressesClient);
    });
    this.addressesServerGlobal.forEach((addressServer) => {
      this._addStaticAddress(sut, labId, userId, addressServer, result.addressesServer);
    });
    this.addressesServerLocal.forEach((addressServer) => {
      this._addStaticAddress(sut, labId, userId, addressServer, result.addressesServer);
    });
    this.addressesSrcGlobal.forEach((addressSrc) => {
      this._addAddress(sut, labId, userId, addressSrc, result.addressesSrc);
    });
    this.addressesSrcLocal.forEach((addressSrc) => {
      this._addAddress(sut, labId, userId, addressSrc, result.addressesSrc);
    });
    this.addressesDstGlobal.forEach((addressDst) => {
      this._addAddress(sut, labId, userId, addressDst, result.addressesDst);
    });
    this.addressesDstLocal.forEach((addressDst) => {
      this._addAddress(sut, labId, userId, addressDst, result.addressesDst);
    });
    this.addressesSrvGlobal.forEach((addressSrv) => {
      this._addAddress(sut, labId, userId, addressSrv, result.addressesSrv);
    });
    this.addressesSrvLocal.forEach((addressSrv) => {
      this._addAddress(sut, labId, userId, addressSrv, result.addressesSrv);
    });
    this.addressesPortsGlobal.forEach((port) => {
      this._addPort(sut, labId, userId, port, result.addressesPorts);
    });
    this.addressesPortsLocal.forEach((port) => {
      this._addPort(sut, labId, userId, port, result.addressesPorts);
    });
    this.addressesDnsGlobal.forEach((addressDns) => {
      this._addDns(sut, labId, userId, addressDns, result.addressesDns);
    });
    this.addressesDnsLocal.forEach((addressDns) => {
      this._addDns(sut, labId, userId, addressDns, result.addressesDns);
    });
    return V8.deserialize(V8.serialize(result));
  }
  
  read(done) {
    const parallel = {
      pendings: 24,
      err: undefined
    };
    const iterationDone = (err, data, setFunc) => {
      if(!err) {
        setFunc(JSON.parse(data));
      }
      else {
        parallel.err = err;
      }
      if(0 === --parallel.pendings) {
        done(parallel.err);
      }
    };
    Fs.readFile(ActorPathData.getAddressesGlobalNetworksFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesNetworkGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalNetworksFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesNetworkLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalInterfacesClientFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesClientGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalInterfacesClientFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesClientLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalInterfacesServerFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesServerGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalInterfacesServerFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesServerLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalInterfacesSutFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesSutGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalInterfacesSutFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.interfacesSutLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalAddressesSutFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesSutGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalAddressesSutFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesSutLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalAddressesClientFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesClientGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalAddressesClientFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesClientLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalAddressesServerFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesServerGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalAddressesServerFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesServerLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalSrcFile(), (err, data) => {
      iterationDone(err, data, (srcs) => {
        this.addressesSrcGlobal = srcs;
        if(srcs && Array.isArray(srcs)) { // BACKWARD COMPATIBILITY - 0.0.0-aj-beta.2
          srcs.forEach((src) => {
            if(src.browser && ! src.page) {
              src.page = src.browser;
              src.browser = undefined;
            }
          });
        } 
      });
    });
    Fs.readFile(ActorPathData.getAddressesLocalSrcFile(), (err, data) => {
      iterationDone(err, data, (srcs) => {
        this.addressesSrcLocal = srcs;
        if(srcs && Array.isArray(srcs)) { // BACKWARD COMPATIBILITY - 0.0.0-aj-beta.2
          srcs.forEach((src) => {
            if(src.browser && ! src.page) {
              src.page = src.browser;
              src.browser = undefined;
            }
          });
        }
      });
    });
    Fs.readFile(ActorPathData.getAddressesGlobalDstFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesDstGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalDstFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesDstLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalSrvFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesSrvGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalSrvFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesSrvLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalPortsFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesPortsGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalPortsFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesPortsLocal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesGlobalDnsFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesDnsGlobal = object;});
    });
    Fs.readFile(ActorPathData.getAddressesLocalDnsFile(), (err, data) => {
      iterationDone(err, data, (object) => {this.addressesDnsLocal = object;});
    });
  }
  
  _addStaticAddress(sut, labId, userId, address, addresses) {
    if(sut === address.sut) {
      if(labId === address.labId && userId === address.userId) {
        if(!addresses.has(address.interfaceName)) {
          addresses.set(address.interfaceName, address);
        }
        else {
          const existing = addresses.get(address.interfaceName);
          if(existing.labId === '' &&  existing.userId == '') {
            addresses.set(address.interfaceName, address);
          }
          else {
            // TODO: ERROR LOG - Does already exist.
            ddb.error(`address: '${address.interfaceName} does already exist`);
          }
        }
      }
      else if('' === address.labId && '' === address.userId) {
        if(!addresses.has(address.interfaceName)) {
          addresses.set(address.interfaceName, address);
        }
      }
    }
  }
  
  _addAddress(sut, labId, userId, address, addresses) {
    if(sut === address.sut) {
      if(labId === address.labId && userId === address.userId) {
        if(!addresses.has(address.addressName)) {
          addresses.set(address.addressName, address);
        }
        else {
          const existing = addresses.get(address.addressName);
          if(existing.labId === '' &&  existing.userId == '') {
            addresses.set(address.addressName, address);
          }
          else {
            // TODO: ERROR LOG - Does already exist.
            ddb.error(`address: '${address.addressName} does already exist`);
          }
        }
      }
      else if('' === address.labId && '' === address.userId) {
        if(!addresses.has(address.addressName)) {
          addresses.set(address.addressName, address);
        }
      }
    }
  }
  
  _addPortIf(port, addressesPorts) {
    if(!addressesPorts.has(port.portName)) {
      addressesPorts.set(port.portName, port);
    }
    else {
      const existing = addressesPorts.get(port.portName);
      if(existing.labId === '' &&  existing.userId == '') {
        addressesPorts.set(port.portName, port);
      }
      else {
        ddb.error(`port: '${port.portName} does already exist for SUT '${port.sut}'`);
      }
    }
  }
  
  _addPort(sut, labId, userId, port, addressesPorts) {
    if(sut === port.sut) {
      if('' === port.labId && '' === port.userId) {
        this._addPortIf(port, addressesPorts);
      }
      else if(labId === port.labId && userId === port.userId) {
        this._addPortIf(port, addressesPorts);
      }
    }
    else if('' === port.sut) {
      this._addPortIf(port, addressesPorts);
    }
  }
  
  _addDns(sut, labId, userId, addressDns, dnses) {
    if(sut === addressDns.sut) {
      if(labId === addressDns.labId && userId === addressDns.userId) {
        if(!dnses.has(addressDns.uri)) {
          dnses.set(addressDns.uri, addressDns);
        }
        else {
          const existing = dnses.get(addressDns.uri);
          if(existing.labId === '' &&  existing.userId == '') {
            dnses.set(addressDns.uri, addressDns);
          }
          else {
            // TODO: ERROR LOG - Does already exist.
            ddb.error(`dns: '${addressDns.uri} does already exist`);
          }
        }
      }
      else if('' === addressDns.labId && '' === addressDns.userId) {
        if(!dnses.has(addressDns.uri)) {
          dnses.set(addressDns.uri, addressDns);
        }
      }
    }
  }
}

module.exports = AddressData;
