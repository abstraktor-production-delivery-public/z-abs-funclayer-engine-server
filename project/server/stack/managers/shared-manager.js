
'use strict';


class SharedManager {
  constructor() {
    this.connections = new Map();
    this.servers = [];
  }
  
  hasServer(connectionType) {
    let has = false;
    this.servers.forEach((server) => {
      if(server.messageSelector) {
        has = has || (connectionType === server.messageSelector.type);
      }
    });
    return has;
  }
  
  getServers(connectionType, interceptor=false) {
    const servers = [];
    this.servers.forEach((server) => {
      if(server.messageSelector) {
        if(connectionType === server.messageSelector.type && (!interceptor || interceptor && server.messageSelector.interceptor)) {
          if(server.messageSelector.onSelect) {
            servers.push({
              dstAddress: server.srvAddress,
              members: JSON.stringify(server.messageSelector),
              onSelectFunction: server.messageSelector.onSelect.toString()
            });
          }
        }
      }
    });
    return servers;
  }
  
  reset() {
    this.connections = new Map();
    this.servers = [];
  }
  
  addClient(actorName, local, remote, messageSelector) {
    const key = this._makeKey('client', local.transportType, actorName, local, remote);
    if(this.connections.has(key)) {
      const data = this.connections.get(key);
      data.clients.set(actorName, {
        local: local,
        remote: remote,
        messageSelector: messageSelector
      });
    }
    else {
      const clients = new Map();
      clients.set(actorName, {
        local: local,
        remote: remote,
        messageSelector: messageSelector
      });
      this.connections.set(key, {
        clients: clients,
        servers: new Map()
      });
    }
  }
  
  addServer(srvAddress, messageSelector) {
    this.servers.push({
      srvAddress: srvAddress,
      messageSelector: messageSelector
    });      
  }
  
  updateServerConnection(actorName, local, remote, messageSelector) {
    if(messageSelector) {
      const key = this._makeKey('server', local.transportType, actorName, local, remote);
      if(this.connections.has(key)) {
        const data = this.connections.get(key);
        data.servers.set(actorName, {
          local: local,
          remote: remote,
          messageSelector: messageSelector
        });
      }
      else {
        const servers = new Map();
        servers.set(actorName, {
          local: local,
          remote: remote,
          messageSelector: messageSelector
        });
        this.connections.set(key, {
          clients: new Map(),
          servers: servers
        });
      }
    }
  }
  
  _makeKey(type, transportType, actorName, local, remote) {
    if("client" === type) {
      if('mc' === transportType) {
        return `c_${actorName}_${local.host}:${local.port}_s_${remote.host}:${remote.port}`;
      }
      else {
        return `c_${local.host}:${local.port}_s_${remote.host}:${remote.port}`;
      }
    }
    else if("server" === type) {
      return `c_${remote.host}:${remote.port}_s_${local.host}:${local.port}`;
    }
  }
}

module.exports = SharedManager;
