
'use strict';

const Manager = require('../manager');
const BrowserData = require('../../stacks/browser-data');
const ConnectionClient = require('../../stacks/connection-client');
const PendingContext = require('../../stacks/pending-context');
const ConnectionWeb = require('../../stacks/connection-web');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const LogDataStackType = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-stack-type');


class ClientManager extends Manager {
  constructor(executionContext) {
    super('client', executionContext);
    this.connections = new Map();
  }
  
  getConnection(stackName, actor, reuse, srcAddress, dstAddress, options) {
    let connection;
    let browserData;
    if(options.messageSelector) {
      throw new Error('messageSelector is not implemented.');
    }
    else if(reuse) {
      connection = this.getToReuse(stackName, (type) => {
        if('client' === type) {
          return this._getConnectionClientKey(stackName, options, srcAddress, dstAddress);
        }
        else if('web' === type) {
          browserData = this._createBrowserData(srcAddress, dstAddress);
          return this._getConnectionWebKey(stackName, browserData);
        }
      });
      if(connection) {
        connection.actor = actor;
        connection.sharedManager = this.sharedManager;
        connection.logIpStack(() => `USE STACK: ${connection.name}`, LogDataStackType.USE, '74ee23f9-b5b1-4dcf-91d4-4de46aac4767');
        if(connection.onReuse) {
          const pendingContext = new PendingContext();
          connection.pendingStartContext(pendingContext, 'reuse');
          connection.browserData = browserData;
          return connection.onReuse(browserData, (c, err) => {
            if(!err) {
              connection.pendingDoneContext(pendingContext, 'reuse', connection);
            }
            else {
              connection.pendingErrorContext(pendingContext, 'reuse', err);
            }
          });
        }
        connection.pendingResultContext('reuse', connection);
      }
    }
    if(!connection) {
      const factory = StackComponentsFactory.createClient(stackName);
      connection = new factory(++this.id, this.type, actor, options);
      connection.sharedManager = this.sharedManager;
      connection.executionId = this.executionContext.executionData.id;
      if(!srcAddress) {
        const transportType = connection.options.transportLayer.transportType;
        srcAddress = this.addressDefaultManager.getSrc(transportType, this.actors.get(actor));
        connection.logWarning(() => `There is no client src address name. Default client src address will be used. ${srcAddress.host}:${srcAddress.port} - name: ${srcAddress.addressName}`, 'core', '26e8e0eb-aee4-49ec-a099-cc80f02631de', 1);
      }
      if(!dstAddress) {
        const transportType = connection.options.transportLayer.transportType;
        dstAddress = this.addressDefaultManager.getDst(transportType, this.actors.get(actor));
        connection.logWarning(() => `There is no client dst address name. Default client dst address will be used. ${dstAddress.host}:${dstAddress.port} - name: ${dstAddress.addressName}`, 'core', 'acc9b872-4fae-4e1b-810c-fa2541a76a2a', 1);
      }
      if(!reuse) {
        this.connections.set(connection.id, connection);
      }
      else {
        this.setUsing(connection);
      }
      if(connection instanceof ConnectionClient) {
        connection.connect(dstAddress, srcAddress, this.dnsUrlCache, !reuse ? (connection, err) => {
          this.connections.delete(connection.id);
          connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'a9185bb6-82c5-4495-9416-3c26269c8944');
        } : (connection, err) => {
          if(!err) {
            connection.errorData = [];
            this.storeForReuse(stackName, 'client', this._getConnectionClientKey(stackName, options, srcAddress, dstAddress), connection);
            connection.logIpStack(() => `STORE STACK: ${connection.name}`, LogDataStackType.STORE, '54532ce8-8cd9-49ff-95c0-0254f56b2d49');
          }
          else {
            this.executionContext.testCaseStatistics.addErrorData(connection);
            this.removeUsing(connection.id);
            this.usingReuseConnections.delete(connection.id);
            connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'c17524db-0da5-4a5d-b41e-9d7b7c8b80e0');
          }
        });
        if(reuse) {
          this.addConnectionType(stackName, 'client', connection);
        }
      }
      else if(connection instanceof ConnectionWeb) {
        !browserData && (browserData = this._createBrowserData(srcAddress, dstAddress));
        connection.connect(browserData, this.dnsUrlCache, reuse, !reuse ? (connection, err) => {
          this.connections.delete(connection.id);
          connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'ca515a42-bec3-489c-81b1-55854bbaa132');
        } : (connection, err) => {
          if(!err) {
            connection.errorData = [];
            this.storeForReuse(stackName, 'web', this._getConnectionWebKey(stackName, browserData), connection);
            connection.logIpStack(() => `STORE STACK: ${connection.name}`, LogDataStackType.STORE, '20ab42ef-473a-4520-a06c-1a465b691121');
          }
          else {
            this.executionContext.testCaseStatistics.addErrorData(connection);
            this.removeUsing(connection.id);
            this.usingReuseConnections.delete(connection.id);
            connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'ca515a42-bec3-489c-81b1-55854bbaa132');
          }
        });
        if(reuse) {
          this.addConnectionType(stackName, 'web', connection);
        }
      }
    }
  }
  
  switchProtocol(stackName, currentConnection, actor) {
    const factory = StackComponentsFactory.createClient(stackName);
    const connection = new factory(++this.id, this.type, actor, currentConnection.options);
    currentConnection.detachTo(connection);
    this.connections.set(connection.id, connection);
    return connection;
  }
  
  close(done) {
    let connections = this.connections.size;
    let usingReuseConnections = this.usingReuseConnections.size;
    if(0 === connections && 0 === usingReuseConnections) {
      done();
    }
    this.connections.forEach((connection) => {
      connection.closeForgottenConnections(() => {
        connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'b0ca9eeb-8d69-4692-8dc2-cf28c3c56982');
        if(0 === --connections) {
          this.connections.clear();
          done();
        }
      });
    });
    this.usingReuseConnections.forEach((connection) => {
      connection.closeForgottenConnections(() => {
        connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'd2f333e0-8aef-422e-8709-d3f26225ce47');
        if(0 === --usingReuseConnections) {
          this.usingReuseConnections.clear();
          done();
        }
      });
    });
  }
  
  _getConnectionClientKey(stackName, options, srcAddress, dstAddress) {
    return `${stackName}_${options.transportLayer.transportType}_${srcAddress.host}:${srcAddress.port} => ${dstAddress.host}:${dstAddress.port}`;
  }
  
  _getConnectionWebKey(stackName, browserData) {
    return `${stackName}_${browserData.srcAddress.name}:${browserData.srcAddress.page}:${browserData.srcAddress.incognitoBrowser}`;
  }
  
  _createBrowserData(srcAddress, dstAddress) {
    if(!Array.isArray(dstAddress)) {
      return new BrowserData(srcAddress, dstAddress);
    }
    else {
      if(2 !== dstAddress.length) {
      }
      else {
        const address = dstAddress[0].clone();
        address.setProxy(dstAddress[1]);
        return new BrowserData(srcAddress, address);
      }
    }
  }
}

module.exports = ClientManager;
