
'use strict';

const NetworkType = require('../../network/network-type');


class ConnectionData {
  static NOT_CONNECTED = 0;
  static CONNECTED = 1;
  static DISCONNECTED = 2;
  
  constructor(ownerId, id, actorIndex, transportLayer, address, isShared) {
    this.ownerId = ownerId;
    this.id = id;
    this.actorIndex = actorIndex;
    this.actionId = 0;
    this.host = address.host ? address.host : '*';
    this.port = 0 !== address.port ? address.port : '*';
    this.transportLayer = transportLayer;
    this.addressName = address.logName;
    this.family = address.family;
    this.type = address.type;
    this.isShared = isShared;
    this.secure = false;
    this.dataId = -1;
    this._id = `${this._getTypeLog()} ${this.host}:${this.port}:${this.addressName ? this.addressName : ''}${this._getProperties()}`;
  }
  
  static create(ownerId, id, actorIndex, transportLayer, addressName, type, host, port, family, isShared) {
    return new ConnectionData(ownerId, id, actorIndex, transportLayer, {addressName, host, port, type, family}, isShared);
  }
  
  update(host, port, family) {
    this.host = host;
    this.port = port;
    this.family = family;
    this._id = `${this._getTypeLog()} ${host}:${port}:${this.addressName ? this.addressName : ''}${this._getProperties()}`;
  }
  
  getId() {
    return this._id;
  }
  
  _getTypeLog() {
    if('client' === this.type) {
      return '\u25A0\u2192';
    }
    else {
      return '\u2192\u25A0';
    }
  }
  
  _getProperties() {
    if(this.transportLayer.transportProperties) {
      const properties = [];
      this.transportLayer.transportProperties.forEach((value, name) => {
        if('string' === typeof value) {
          properties.push(`${name}: ${value}`);
        }
        else {
          properties.push(`${name}: "${value}"`);
        }
      });
      return ` \uD83D\uDD27 [${properties.join(', ')}]`;
    }
    else {
      return '';
    }
  }
  
  toJSON() {
    return {
      ownerId: this.ownerId,
      id: this.id,
      actorIndex: this.actorIndex,
      actionId: this.actionId,
      host: this.host,
      port: this.port,
      transportLayer: this.transportLayer,
      name: this.addressName, // RENAME: affects sequence diagram
      family: this.family,
      type: this.type,
      isShared: this.isShared,
      secure: this.secure,
      dataId: this.dataId
    };
  }
}

module.exports = ConnectionData;
