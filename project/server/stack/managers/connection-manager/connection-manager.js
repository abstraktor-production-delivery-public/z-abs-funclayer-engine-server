
'use strict';

const ConnectionData = require('./connection-data');
const ConnectionObject = require('./connection-object');


class ConnectionManager {
  static dataId = 0;
  static ACTION_NONE = 0;
  static ACTION_CONNECTED = 1;
  static ACTION_DISCONNECTED = 2;
  
  constructor(executionContext) {
    this.executionContext = executionContext;
    this.sutNodeServerCriterias = [];
    this.defaultServerColumnId = -1;
    this.sutNodeClientCriterias = [];
    this.defaultClientColumnId = -1;
    this.serverConnectionsKey = new Map();
    this.sutServerOwnerId = 0;
    this.sutServerConnectionId = 0;
    this.sutClientOwnerId = 0;
    this.sutClientConnectionId = 0;
  }
  
  init() {
    this.sutNodeServerCriterias = [];
    this.defaultServerColumnId = -1;
    this.sutNodeClientCriterias = [];
    this.defaultClientColumnId = -1;
    this.serverConnectionsKey.clear();
    const sutNodes = this.executionContext.nodes.filter((node) => {
      if(!node.actor) {
        return node;
      }
    });
    const stagedSutNodes = this.executionContext.stageData.stagedSutNodes;
    if(0 !== stagedSutNodes.length) {
      const serverColomnIndexes = [];
      const clientColomnIndexes = [];
      sutNodes.forEach((sutNode) => {
        if('server' === sutNode.criteriaType) {
          serverColomnIndexes.push(sutNode.columnIndex);
          this.sutNodeServerCriterias.push((remote) => {
            return -1;
          });
        }
        else if('client' === sutNode.criteriaType) {
          clientColomnIndexes.push(sutNode.columnIndex);
          this.sutNodeClientCriterias.push((remote) => {
            return -1;
          });
        }
      });
      if(0 !== serverColomnIndexes.length) {
        this.defaultServerColumnId = serverColomnIndexes[0];
      }
      else {
        this.defaultServerColumnId = sutNodes[0].columnIndex;
      }
      if(0 !== clientColomnIndexes.length) {
        this.defaultClientColumnId = clientColomnIndexes[0];
      }
      else {
        this.defaultClientColumnId = sutNodes[0].columnIndex;
      }
    }
    else {
      this.defaultServerColumnId = sutNodes[0].columnIndex;
      this.defaultClientColumnId = sutNodes[0].columnIndex;
    }
  }
  
  reset() {
    this.sutNodeServerCriterias = [];
    this.defaultServerColumnId = -1;
    this.sutNodeClientCriterias = [];
    this.defaultClientColumnId = -1;
    this.serverConnectionsKey.clear();
    this.sutServerOwnerId = 0;
    this.sutServerConnectionId = 0;
    this.sutClientOwnerId = 0;
    this.sutClientConnectionId = 0;
  }
  
  registerServerConnectionData(connection, ownerId, address, isShared) {
    const serverConnectionData = new ConnectionData(ownerId, connection.id, connection.actor.columnIndex, connection.options.transportLayer, address, isShared);
    const serverKey = `${serverConnectionData.host}:${serverConnectionData.port}`;
    //console.log('registerServerConnectionData.server:', serverKey);
    return this._addServer(serverKey, serverConnectionData, isShared, true);
  }
  
  registerClientConnectionData(connectionId, ownerId, columnIndex, srcAddress, dstAddress, transportLayer, isShared) {
    const clientConnectionData = new ConnectionData(ownerId, connectionId, columnIndex, transportLayer, srcAddress, isShared);
    const serverKey = `${dstAddress.host}:${dstAddress.port}`;
    //console.log('registerClientConnectionData.server:', serverKey);
    if(!this.serverConnectionsKey.has(serverKey)) {
      const columnIndex = this._getSutServerColumnIndex(dstAddress);
      const serverConnectionData = new ConnectionData(++this.sutServerOwnerId, ++this.sutServerConnectionId, columnIndex, transportLayer, dstAddress, false);
      this._addServer(serverKey, serverConnectionData, false, false);
    }
    const connectionObjectData = this.serverConnectionsKey.get(serverKey);
    const connectionObject = connectionObjectData.connectionObjects.shift();
    connectionObject.addClient(clientConnectionData, true);
    if(0 === connectionObjectData.connectionObjects.length) {
      this.serverConnectionsKey.delete(serverKey);
    }
    return connectionObject;
  }
  
  registerSutClientConnectionData(srvAddress, transportLayer, srcHost, srcPort, srcFamily) {
    const serverKey = `${srvAddress.host}:${srvAddress.port}`;
    //console.log('registerSutClientConnectionData.server:', serverKey);
    const connectionObjectData = this.serverConnectionsKey.get(serverKey);
    const connectionObject = connectionObjectData.connectionObjects.shift();
    const columnIndex = this._getSutClientColumnIndex(srvAddress);
    const clientConnectionData = ConnectionData.create(++this.sutClientOwnerId, ++this.sutClientConnectionId, columnIndex, transportLayer, 'UNKNOWN', 'sut', srcHost, srcPort, srcFamily, false);
    connectionObject.addClient(clientConnectionData, false);
    if(0 === connectionObjectData.connectionObjects.length) {
      this.serverConnectionsKey.delete(serverKey);
    }
  }
  
  _addServer(serverKey, serverConnectionData, isShared, serverInternal) {
    if(!this.serverConnectionsKey.has(serverKey)) {
      const connectionObject = new ConnectionObject(serverConnectionData, serverInternal, ++ConnectionManager.dataId);
      this.serverConnectionsKey.set(serverKey, {
        connectionObjects: [connectionObject],
        sharedData: isShared ? connectionObject.addSharedServer() : null
      });
      return connectionObject;
    }
    else {
      const connectionObjectData = this.serverConnectionsKey.get(serverKey);
      if(!isShared) {
        const connectionObject = new ConnectionObject(serverConnectionData, serverInternal, ++ConnectionManager.dataId);
        connectionObjectData.connectionObjects.push(connectionObject);
        return connectionObject;
      }
      else {
        if(!connectionObjectData.sharedData) {
          const connectionObject = new ConnectionObject(serverConnectionData, serverInternal, ++ConnectionManager.dataId);
          connectionObjectData.connectionObjects.push(connectionObject);
          connectionObjectData.sharedData = connectionObject.addSharedServer();
          return connectionObject;
        }
        else {
          connectionObjectData.sharedData.connectionObject.addSharedServer(serverConnectionData);
          return connectionObjectData.sharedData.connectionObject;
        }
      }
    }
  }
    
  _getSutServerColumnIndex(remote) {
    for(let i = 0; i < this.sutNodeServerCriterias.length; ++i) {
      const criteria = this.sutNodeServerCriterias[i];
      const columnId = criteria(remote);
      if(-1 !== columnId) {
        return columnId;
      }
    }
    return this.defaultServerColumnId;
  }
  
  _getSutClientColumnIndex(remote) {
    for(let i = 0; i < this.sutNodeClientCriterias.length; ++i) {
      const criteria = this.sutNodeClientCriterias[i];
      const columnId = criteria(remote);
      if(-1 !== columnId) {
        return columnId;
      }
    }
    return this.defaultClientColumnId;
  }
}


module.exports = ConnectionManager;
