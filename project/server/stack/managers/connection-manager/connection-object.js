
'use strict';

const ConnectionData = require('./connection-data');
const SharedData = require('./shared-data');


class ConnectionObject {
  static ACTION_NONE = 0;
  static ACTION_CONNECTED = 1;
  static ACTION_DISCONNECTED = 2;
  
  constructor(serverConnectionData, serverInternal, dataId) {
    try{
    this.clients = new Map();
    this.clientStatus = ConnectionData.NOT_CONNECTED;
    this.clientSharedData = null;
    this.clientInternal = true;
    this.servers = new Map([[serverConnectionData.id, serverConnectionData]]);
    this.serverStatus = ConnectionData.NOT_CONNECTED;
    this.serverSharedData = null;
    this.serverInternal = serverInternal;
    this.dataId = dataId;
    }
    catch(a) {console.log(a);}
  }
  
  registerConnected(local, isConnected, cb) {
    if(isConnected) {
      this.clientStatus = ConnectionData.CONNECTED;
    }
    let twoNodeAction = false;
    if(this.serverStatus === ConnectionData.NOT_CONNECTED) {
      local.actionId = ConnectionObject.ACTION_CONNECTED;
      twoNodeAction = true;
    }
    this._logClientCb(twoNodeAction, cb);
  }
  
  registerAccepted(local, cb) {
    this.serverStatus = ConnectionData.CONNECTED;
    if(this.clientStatus === ConnectionData.NOT_CONNECTED) {
       if(this.serverSharedData) {
         ++this.serverSharedData.connectedServers;
       }
      local.actionId = ConnectionObject.ACTION_CONNECTED;
    }
    return 0 !== this.clients.size;
  }
  
  registerClosed(local, reverse, cb) {
    if('client' === local.type) {
      if(!reverse) {
        this.clientStatus = ConnectionData.DISCONNECTED;
      }
      else {
        this.serverStatus = ConnectionData.DISCONNECTED;
      }
    }
    else {
      if(!reverse) {
        this.serverStatus = ConnectionData.DISCONNECTED;
      }
      else {
        this.clientStatus = ConnectionData.DISCONNECTED;
      }
    }
    let twoNodeAction = false;
    if(ConnectionData.DISCONNECTED === this.clientStatus && ConnectionData.DISCONNECTED === this.serverStatus) {
      local.actionId = ConnectionObject.ACTION_DISCONNECTED;
      twoNodeAction = true;
    }
    else {
      local.actionId = ConnectionObject.ACTION_NONE;
    }
    if('client' === local.type) {
      if(!reverse) {
        this._logClientCb(twoNodeAction, cb);
      }
      else {
        const connectionDataServer = this.servers.entries().next().value[1];
        cb(connectionDataServer);
      }
    }
    else {
      if(!reverse) {
        const connectionDataClient = this.clients.entries().next().value[1];
        cb(connectionDataClient);
      }
      else {
        const connectionDataServer = this.servers.entries().next().value[1];
        cb(connectionDataServer);
      }
    }
  }

  addClient(clientConnectionData, clientInternal) {
    this.clients.set(clientConnectionData.id, clientConnectionData);
    this.clientInternal = clientInternal;
  }
  
  addSharedServer(serverConnectionData) {
    if(!this.serverSharedData) {
      this.serverSharedData = new SharedData(this);
    }
    else {
      ++this.serverSharedData.serverShares;
    }
    if(serverConnectionData) {
      this.servers.set(serverConnectionData.id, serverConnectionData);
    }
    return this.serverSharedData;
  }
    
  getServerConnectionData(id) {
    return this.servers.get(id);
  }
  
  getServerConnectionDatas() {
    return this.servers;
  }
  
  getClientConnectionData(id) {
    return this.clients.get(id);
  }
  
  getClientConnectionDatas() {
    return this.clients;
  }
  
  _logClientCb(twoNodeAction, cb) {
    if(!this.serverSharedData) {
      const connectionDataServer = this.servers.entries().next().value[1];
      cb(connectionDataServer);
    }
    else {
      if(twoNodeAction) {
        this.servers.forEach((connectionDataServer) => {
          cb(connectionDataServer);
        });
      }
      else {
        const connectionDataServer = this.servers.entries().next().value[1];
        cb(connectionDataServer);
      }
    }
  }
}


module.exports = ConnectionObject;
