
'use strict';


class SharedData {
  constructor(connectionObject) {
    this.serverShares = 1;
    this.connectedServers = 0;
    this.connectionObject = connectionObject;
  }
}


module.exports = SharedData;
