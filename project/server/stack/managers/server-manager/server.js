
'use strict';

const NetworkType = require('../../network/network-type');
const NetworkConnectionShared = require('../../network/network-connection-shared');
const TcpServer = require('../../network/tcp/tcp-server');
const UdpServer = require('../../network/udp/udp-server');
const McServer = require('../../network/mc/mc-server');
const TlsServer = require('../../network/tls/tls-server');
const TcpConnectionServer = require('../../network/tcp/tcp-connection-server');
const TcpConnectionShared = require('../../network/tcp/tcp-connection-shared');
const UdpConnectionServer = require('../../network/udp/udp-connection-server');
const UdpConnectionServerShared = require('../../network/udp/udp-connection-server-shared');
const McConnectionServer = require('../../network/mc/mc-connection-server');
const McConnectionServerShared = require('../../network/mc/mc-connection-server-shared');
const TlsConnectionServer = require('../../network/tls/tls-connection-server');
const TlsConnectionShared = require('../../network/tls/tls-connection-shared');
const PendingContext = require('../../stacks/pending-context');
const LogDataAction = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-action');
const LogMsgServer = require('z-abs-funclayer-engine-cs/clientServer/log/msg/log-msg-server');
const LogDataStackType = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-stack-type');
const LogObjectIp = require('z-abs-funclayer-engine-cs/clientServer/log/log-object-ip');


class Server {
  static STATE_NONE = 0;
  static STATE_STARTING = 1;
  static STATE_STARTED = 2;
  static STATE_STOPPING = 3;
  static STATE_STOPPED = 4;
  static STATES = ['STATE_NONE', 'STATE_STARTING', 'STATE_STARTED', 'STATE_STOPPING', 'STATE_STOPPED'];

  constructor(id, actor, cbRemove) {
    this.id = id;
    this.actor = actor;
    this.cbRemove = cbRemove;
    this.connections = new Map();
    this.pendingStartedConnections = [];
    this.pendingAcceptedConnections = [];
    this.acceptingConnectionCallbacks = [];
    this.stoppingCallbacks = [];
    this.srvAddress = null;
    this.networkServer = null;
    this.state = Server.STATE_NONE;
  }
  
  start(srvAddress, dstAddress, connection, options) {
    this.srvAddress = srvAddress;
    this.dstAddress = dstAddress;
    connection.init(this);
    const connectionDataLocal = connection.getConnectionDataLocal();
    this._createServer(options, connection);
    const pendingContext = new PendingContext();
    connection.pendingStartContext(pendingContext, 'serverStart', () => {
      // TODO: CANCEL PENDING server.start() NOT IMPLEMENTED
    });
    this.actor.logIp(() => this.actor.createLogMessage(`STARTING${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.STARTING, connection.name, connectionDataLocal), null), 'core', '99006ce3-5816-4a03-b813-220a9ad5a7d9');
    this._setState(Server.STATE_STARTING);
    connection.sharedManager.addServer(this.srvAddress, connection.messageSelector);
    this.networkServer.init(this.srvAddress, this.dstAddress, options.transportLayer.transportProperties, (err) => {
      if(err) {
        this._setState(Server.STATE_NONE);
        this.actor.logIp(() => this.actor.createLogMessage(`NOT STARTED${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.NOT_STARTED, connection.name, connectionDataLocal), null), 'core', '40b807ca-f7fc-4092-84ca-9450148645b4');
        connection.pendingErrorContext(pendingContext, 'serverStart', err);
        while(0 !== this.pendingStartedConnections.length) {
          const pendingConnection = this.pendingStartedConnections.shift();
          const pendingConnectionDataLocal = pendingConnection.connection.getConnectionDataLocal();
          pendingConnection.connection.actor.logIp(() => pendingConnection.connection.actor.createLogMessage(`NOT ATTACHED{pendingConnection.connection.stackLog} ${pendingConnectionDataLocal.getId()}`, undefined, new LogMsgServer(LogDataAction.NOT_ATTACHED, connection.name, pendingConnectionDataLocal), null), 'core', 'afc7033e-2570-4667-8b44-02e52bc986ef');
          pendingConnection.connection.pendingErrorContext(pendingConnection.pendingContext, 'serverAttach', err);
        }
      }
      else {
        this._setState(Server.STATE_STARTED);
        this.actor.logIp(() => this.actor.createLogMessage(`STARTED${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.STARTED, connection.name, connectionDataLocal), null), 'core', '7747f18e-61de-4a41-8cbc-c175bd1a26de');
        connection.pendingDoneContext(pendingContext, 'serverStart', connection);
        while(0 !== this.pendingStartedConnections.length) {
          const pendingConnection = this.pendingStartedConnections.shift();
          const pendingConnectionDataLocal = pendingConnection.connection.getConnectionDataLocal();
          pendingConnection.connection.actor.logIp(() => pendingConnection.connection.actor.createLogMessage(`ATTACHED${pendingConnection.connection.stackLog} ${pendingConnectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.ATTACHED, connection.name, pendingConnectionDataLocal), null), 'core', '768d00e9-600e-43dd-9848-831bda064870');
          pendingConnection.connection.pendingDoneContext(pendingConnection.pendingContext, 'serverAttach', pendingConnection.connection);
        }
      }
    });
  }
  
  attachConnection(connection) {
    connection.init(this);
    let cancel = false;
    const pendingContext = new PendingContext();
    connection.pendingStartContext(pendingContext, 'serverAttach', () => {
      cancel = true;
    });
    if(Server.STATE_STARTED === this.state) {
      process.nextTick(() => {
        if(!cancel) {
          connection.pendingDoneContext(pendingContext, 'serverAttach', connection);
        }
      });
    }
    else if(Server.STATE_STARTING === this.state) {
      this.pendingStartedConnections.push({
        connection: connection,
        pendingContext: pendingContext
      });
    }
  }
  
  // TODO: Remove this method and solve its dependencies.
  addConnection(connection) {
    this.connections.set(connection.id, connection);
  }
  
  detachConnection(connection) {
    this.connections.delete(connection.id);
  }
  
  removeConnection(connection, done) {
    this.connections.delete(connection.id);
    connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, '8622d6d0-1c42-4ebc-9c13-36adaf89ed48');
    if(0 === this.connections.size) {
      this._closeServer(connection, () => {
        this.cbRemove();
        done();
      });
    }
    else {
      const connectionDataLocal = connection.getConnectionDataLocal();
      connection.actor.logIp(() => connection.actor.createLogMessage(`DETACHED${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.DETACHED, connection.name, connectionDataLocal), null), 'core', 'aa1e6f3e-ae58-48a2-a993-2d2721faa643');
      done();
    }
  }
  
  _findPendingAccepted(messageSelector) {
    const foundIndex = this.pendingAcceptedConnections.findIndex((pendingAcceptedConnection) => {
      return !(pendingAcceptedConnection instanceof NetworkConnectionShared) === !messageSelector;
    });
    if(-1 !== foundIndex) {
      const connection = this.pendingAcceptedConnections[foundIndex];
      this.pendingAcceptedConnections.splice(foundIndex, 1);
      return connection
    }
    else {
      return null;
    }
  }
  
  _findSharedAccepted(messageSelector) {
    if(messageSelector) {
      let foundConnection = null;
      for(let [key, connection] of this.connections) {
        if(connection.messageSelector && connection.networkConnection) {
          foundConnection = connection;
          break;
        }
      }
      return foundConnection;
    }
    else {
      return null;
    }
  }
  
  accept(connection, done) {
    if(0 !== this.pendingAcceptedConnections.length) {
      const foundNetworkConnection = this._findPendingAccepted(connection.messageSelector);
      if(null !== foundNetworkConnection) {
        if(foundNetworkConnection instanceof NetworkConnectionShared) {
          foundNetworkConnection.ref();
        }
        done(foundNetworkConnection);
      }
      else {
        this.acceptingConnectionCallbacks.push({
          connection: connection,
          done: done
        });
      }
    }
    else {
      const foundConnection = this._findSharedAccepted(connection.messageSelector);
      if(null !== foundConnection) {
        connection.messageSelector._setId(foundConnection.networkConnection.ref());
        done(foundConnection.networkConnection);
      }
      else {
        this.acceptingConnectionCallbacks.push({
          connection: connection,
          done: done
        });
      }
    }
  }
  
  close(cbClose) {
    let connections = this.connections.size;
    if(0 === connections) {
      let pendingAcceptedConnections = this.pendingAcceptedConnections.length;
      if(0 === this.pendingAcceptedConnections.length) {
        this._closeServer(null, () => {
          cbClose();
        });        
      }
      else {
        this.pendingAcceptedConnections.forEach((connection) => {
          if(0 === --pendingAcceptedConnections) {
            this._closeServer(connection, () => {
              cbClose();
            });
          }
        });
      }
    }
    else {
      this.connections.forEach((connection) => {
        connection.closeForgottenConnections(() => {
          if(0 === --connections) {
            this._closeServer(connection, () => {
              this.connections.clear();
              cbClose();
            });
          }
        });
      });
    }
  }
  
  _handleIncommingConnection(connection, networkConnection) {
    if(0 !== this.acceptingConnectionCallbacks.length) {
      const foundIndex = this.acceptingConnectionCallbacks.findIndex((acceptingConnectionCallback) => {
        return !acceptingConnectionCallback.connection.messageSelector === !connection.messageSelector;
      });
      if(-1 !== foundIndex) {
        const callbackData = this.acceptingConnectionCallbacks[foundIndex];
        this.acceptingConnectionCallbacks.splice(foundIndex, 1);
        if(callbackData.connection._shared()) {
          const ref = networkConnection.ref();
          if(callbackData.connection.messageSelector) {
            callbackData.connection.messageSelector._setId(ref);
          }
          const callbackDatas = [];
          for(let i = 0; i < this.acceptingConnectionCallbacks.length;) {
            const acceptingConnectionCallback = this.acceptingConnectionCallbacks[i];
            if(!!acceptingConnectionCallback.connection.messageSelector) {
              callbackDatas.push(acceptingConnectionCallback);
              acceptingConnectionCallback.connection.messageSelector._setId(networkConnection.ref());
              this.acceptingConnectionCallbacks.splice(i, 1);
            }
            else {
              ++i;
            }
          }
          callbackDatas.forEach((callbackData) => {
            process.nextTick(() => {
              callbackData.done(networkConnection);
            });
          });
        }
        callbackData.done(networkConnection);
      }
      else {
        this.pendingAcceptedConnections.push(networkConnection);
      }
    }
    else {
      this.pendingAcceptedConnections.push(networkConnection);
    }
  }
  
  _createServer(options, connection) {
    switch(options.transportLayer.transportType) {
      case NetworkType.UDP:
        this.networkServer = new UdpServer((socket, remote) => {
          let udpConnection = null;
          const con = new UdpConnectionServer(this.networkServer, options.transportLayer.transportProperties);
          if(!connection._shared()) {
            udpConnection = con;
          }
          else {
            udpConnection = new UdpConnectionServerShared(con, 'udp server');  
          }
          udpConnection.attach(socket);
          udpConnection.setRemoteAddress(remote);
          this._handleIncommingConnection(connection, udpConnection);
          return udpConnection;
        });
        break;
      case NetworkType.MC:
        this.networkServer = new McServer((socket, remote) => {
          let mcConnection = null;
          const con = new McConnectionServer(this.networkServer, options.transportLayer.transportProperties);
          if(!connection._shared()) {
            mcConnection = con;
          }
          else {
            mcConnection = new McConnectionServerShared(con, 'mc server');  
          }
          mcConnection.attach(socket);
          mcConnection.setRemoteAddress(remote);
          this._handleIncommingConnection(connection, mcConnection);
          return mcConnection;
        });
        break;
      case NetworkType.TLS:
        this.networkServer = new TlsServer((socket) => {
          let tlsConnection = null;
          const con = new TlsConnectionServer(options.transportLayer.transportProperties);
          if(!connection._shared()) {
            tlsConnection = con;
          }
          else {
            tlsConnection = new TlsConnectionShared(con, 'tls server');  
          }
          tlsConnection.attach(socket);
          this._handleIncommingConnection(connection, tlsConnection);
        });
        break;
      case NetworkType.TCP:
      default:
        this.networkServer = new TcpServer((socket) => {
          let tcpConnection = null;
          const con = new TcpConnectionServer(options.transportLayer.transportProperties);
          if(!connection._shared()) {
            tcpConnection = con;
          }
          else {
            tcpConnection = new TcpConnectionShared(con, 'tcp server');  
          }
          tcpConnection.attach(socket);
          this._handleIncommingConnection(connection, tcpConnection);
        });
        break;
    }
  }
  
  _closeNetworkServer(connection, done) {
    this.networkServer.close(() => {
      this._setState(Server.STATE_STOPPED);
      const connectionDataLocal = connection.getConnectionDataLocal();
      connection.actor.logIp(() => connection.actor.createLogMessage(`STOPPED${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.STOPPED, connection.name, connectionDataLocal), null), 'core', 'de9ce489-08bb-44b4-bf31-9aa41a178c7d');
      this.networkServer = null;
      done();
      this.stoppingCallbacks.forEach((cbDone) => {
        cbDone();
      });
      this.stoppingCallbacks = [];
    });
    /*this.pendingAcceptedConnections.forEach((connection) => {
      connection.close((done) => {
      });
    });*/
  }
  
  _closeServer(connection, done) {
    //ddb.info('STATE[' + this.actor.logName + ']:', Server.STATES[this.state]);
    if(Server.STATE_STOPPING === this.state) {
      this.stoppingCallbacks.push(done);
      return;
    }
    else if(Server.STATE_STARTED !== this.state) {
      done();
      return;
    }
    const connectionDataLocal = connection.getConnectionDataLocal();
    connection.actor.logIp(() => connection.actor.createLogMessage(`STOPPING${connection.stackLog} ${connectionDataLocal.getId()}`, null, new LogMsgServer(LogDataAction.STOPPING, connection.name, connectionDataLocal), null), 'core', 'd5caafe5-e17f-4825-9620-4bffc2402c15');
    this._setState(Server.STATE_STOPPING);
    this.pendingAcceptedConnections.forEach((connection) => {
      connection.close((done) => {
      });
    });
    this._closeNetworkServer(connection, done);
  }
  
  _setState(state) {
    const currentState = this.state;
    this.state = state;
    //ddb.info('STATE[' + this.actor.logName + ']:', Server.STATES[currentState], '=>', Server.STATES[state]);
  }
}


module.exports = Server;
