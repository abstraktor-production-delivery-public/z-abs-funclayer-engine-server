
'use strict';

const Manager = require('../manager');
const Server = require('./server');
const NetworkType = require('../../network/network-type');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');


class ServerManager extends Manager {
  constructor(executionContext) {
    super('server', executionContext);
    this.servers = new Map();
    this.serverId = 0;
  }
  
  getServer(stackName, actor, srvAddress, dstAddress, options) {
    const factory = StackComponentsFactory.createServer(stackName);
    const connection = new factory(++this.id, this.type, actor, options);
    connection.executionId = this.executionContext.executionData.id;
    if(!dstAddress) {
      const transportType = connection.options.transportLayer.transportType;
      if(NetworkType.MC === transportType) {
        dstAddress = this.addressDefaultManager.getDst(NetworkType.MC, this.actors.get(actor));
        connection.logWarning(() => `There is no server dst address name. Default server dst address will be used. ${dstAddress.host}:${dstAddress.port} - name: ${dstAddress.addressName}`, 'core', '0362e23d-a07c-409f-92d8-f849437956ca', 1);
      }
    }
    connection.sharedManager = this.sharedManager;
    connection.dnsUrlCache = this.dnsUrlCache;
    const key = `${stackName}_${srvAddress.addressName}`;
    let server = this.servers.get(key);
    if(!server) {
      server = new Server(++this.serverId, actor, () => {
        this.servers.delete(key);
      });
      this.servers.set(key, server);
      server.start(srvAddress, dstAddress, connection, options);
    }
    else {
      server.attachConnection(connection);
    }
    //connection.init(server);
  }
  
  switchProtocol(stackName, currentConnection, actor) {
    const factory = StackComponentsFactory.createServer(stackName);
    const connection = new factory(++this.id, this.type, actor, currentConnection.options);
    currentConnection.server.addConnection(connection);
    currentConnection.server.detachConnection(currentConnection);
    currentConnection.detachTo(connection);
    currentConnection.server = null;
    return connection;
  }
  
  close(cbClose) {
    let servers = this.servers.size;
    if(0 === servers) {
      cbClose();
    }
    else {
      this.servers.forEach((server) => {
        server.close(() => {
          if(0 === --servers) {
            this.servers.clear();
            cbClose();
          }
        });
      });
    }
  }
}

module.exports = ServerManager;
