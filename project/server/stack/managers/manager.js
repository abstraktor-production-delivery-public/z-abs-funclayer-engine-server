
'use strict';

const LogDataStackType = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-stack-type');


class Manager {
  constructor(type, executionContext) {
    this.type = type;
    this.addressDefaultManager = executionContext.addressDefaultManager;
    //this.connectionManager = connectionManager;
    this.sharedManager = executionContext.sharedManager;
    this.dnsUrlCache = executionContext.dnsUrlCache;
    this.executionContext = executionContext;
    this.id = 0;
    this.reuseConnections = new Map();
    this.usingReuseConnections = new Map();
    this.actors = new Map();
  }
  
  register(actor) {
    this.actors.set(actor, this.actors.size);
  }
  
  reset() {
    this.actors = new Map();
  }
  
  addConnectionType(stackName, type, connection) {
    if(!this.reuseConnections.has(stackName)) {
      this.reuseConnections.set(stackName, {
        type: type,
        clear: undefined !== connection.clear ? connection.clear.bind(connection) : undefined,
        connectionsMap: new Map()
      });
    }
  }
  
  setUsing(connection) {
    this.usingReuseConnections.set(connection.id, connection);
  }
  
  removeUsing(connection) {
    this.usingReuseConnections.delete(connection.id);
  }
  
  getToReuse(stackName, cbKey) {
    const stackMap = this.reuseConnections.get(stackName);
    if(stackMap) {
      const connections = stackMap.connectionsMap.get(cbKey(stackMap.type));
      if(connections?.length >= 1) {
        const connection = connections.shift();
        this.setUsing(connection);
        return connection;
      }
    }
  }
  
  storeForReuse(stackName, type, key, connection) {
    this.removeUsing(connection);
    const stackMap = this.reuseConnections.get(stackName);
    const connections = stackMap.connectionsMap.get(key);
    if(connections) {
      connections.push(connection);
    }
    else {
      stackMap.connectionsMap.set(key, [connection]);
    }
  }
  
  closeReusedConnections(done) {
    let pendings = 0;
    this.reuseConnections.forEach((stackMap) => {
      if(stackMap.clear) {
        ++pendings;
        stackMap.clear(() => {
          stackMap.connectionsMap.forEach((connections) => {
            connections.forEach((connection) => {
              connection.logIpStack(() => `DEL STACK: ${connection.name}`, LogDataStackType.DEL, 'd7d78504-9322-4ced-9b2b-22a2098f55d1');
            });
          });
          if(0 === --pendings) {
            this.reuseConnections.clear();
            done();
          }
        });
      }
    });
    if(0 === pendings) {
      this.reuseConnections.clear();
      done();
    }
  }
}

module.exports = Manager;
