
'use strict';

const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class StackComponentsTemplateBaseActors {
  constructor(templates) {
    this.templates = [
      {
        name: 'Local',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Condition',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Originating',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'RealSut',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Sut',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Proxy',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Terminating',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Intercepting',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'ActorPartPre',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'ActorPartPost',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      },
      {
        name: 'Msg',
        templateNames: [],
        templates: [],
        markupNodes: [],
        markupStyles: [],
        markups: []
      }
    ];
    templates.forEach((template) => {
      let id = ActorTypeConst.getId(template.type);
      if(!id) {
        if('actorpartpre' === template.type) {
          id = 8;
        }
        else if('actorpartpost' === template.type) {
          id = 9;
        }
        else if('msg' === template.type) {
          id = 10;
        }
        else {
          return;
        }
      }
      this.templates[id].templateNames.push(template.displayName);
      this.templates[id].templates.push(template.template);
      this.templates[id].markupNodes.push(template.markupNodes);
      this.templates[id].markupStyles.push(template.markupStyle),
      this.templates[id].markups.push(template.markup);
    });
  }
}

module.exports = StackComponentsTemplateBaseActors;
