
'use strict';


class StackComponentsTemplateBaseTestCases {
  constructor(templates) {
    this.templates = new Map();
    templates.forEach((template) => {
      this.templates.set(template.displayName, {
        displayName: template.displayName,
        regressionFriendly: template.regressionFriendly,
        template: template.template,
        testDataViews: template.testDataViews,
        markupNodes: template.markupNodes,
        markup: template.markup,
        createTestCase: template.createTestCase
      });
    });
  }
}

module.exports = StackComponentsTemplateBaseTestCases;
