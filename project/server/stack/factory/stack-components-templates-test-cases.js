
'use strict';

const StackComponentsHelpers = require('./stack-components-helpers');


class StackComponentsTemplatesTestCases {
    
  static _template(strings, ...keys) {
    return StackComponentsHelpers._template(strings, ...keys);
  }
}


StackComponentsTemplatesTestCases.VALIDATE_NONE = 0;
StackComponentsTemplatesTestCases.VALIDATE_SUCCESS = 1;
StackComponentsTemplatesTestCases.VALIDATE_WARNING = 2;
StackComponentsTemplatesTestCases.VALIDATE_ERROR = 3;

module.exports = StackComponentsTemplatesTestCases;
