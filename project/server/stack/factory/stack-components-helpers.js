
'use strict';

const Os = require('os');


class StackComponentsHelpers {
  static _template(strings, ...keys) {
    return ((...values) => {
      const dict = values[values.length - 1] || {};
      const result = [strings[0].replace(new RegExp('\n', 'g'), Os.EOL)];
      keys.forEach((key, i) => {
        const value = Number.isInteger(key) ? values[key] : dict[key];
        result.push(value, strings[i + 1].replace(new RegExp('\n', 'g'), Os.EOL));
      });
      return result.join('');
    });
  }
}


module.exports = StackComponentsHelpers;
