
'use strict';

const StackComponentsHelpers = require('./stack-components-helpers');


class StackComponentsTemplatesActors {
  
  static _template(strings, ...keys) {
    return StackComponentsHelpers._template(strings, ...keys);
  }
}


module.exports = StackComponentsTemplatesActors;
