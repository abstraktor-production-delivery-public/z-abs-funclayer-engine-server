
'use strict';

const PendingContext = require('./pending-context');
const LogMsgStack = require('z-abs-funclayer-engine-cs/clientServer/log/msg/log-msg-stack');
const LogDataStackType = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-stack-type');


class Connection {
  constructor(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options) {
    this.id = id;
    this.type = type;
    this.stackType = 'client' === type ? 0 : 1;
    this.name = name;
    this.actor = actor;
    this.options = options;
    this.isShared = !!options.messageSelector;
    this.stackContentCache = this.options.stackContentCache;
    this.connectionOptions = this._handleConnectionOptions(options.connectionOptions, defaultConnectionOptions);
    this.errorData = [];
    this.executionId = '';
    if(this.isShared) {
      this.logIpStack(() => `SHARED STACK: ${this.name}`, LogDataStackType.SHARED, '5e36539c-f1db-433a-8b4d-655c14493bbc');
    }
    else {
      this.logIpStack(() => `NEW STACK: ${this.name}`, LogDataStackType.NEW, '5c76acbb-09e8-42d4-bcf0-24a2e458d790');
    }
  }
  
  _handleConnectionOptions(connectionOptions, defaultConnectionOptions) {
    const newConnectionOptions = defaultConnectionOptions.clone();
    if(newConnectionOptions.getTestData) {
      newConnectionOptions.getTestData(this.actor);
    }
    Reflect.ownKeys(defaultConnectionOptions).forEach((memberName) => {
      if(connectionOptions && Reflect.has(connectionOptions, memberName)) {
        const value = Reflect.get(connectionOptions, memberName);
        Reflect.set(newConnectionOptions, memberName, value);
        this.logTestData(() => `${this.constructor.name}Options.${memberName} | ${typeof value} | '${value}'`, 'core', 'b943da02-5836-4a5d-8a49-6beb74f884bc');
      }
    });
    return newConnectionOptions;
  }
  
  _getPendingObject(pendingContext) {
    return this._onGetPendingObject(pendingContext);
  }
  
  _pendingCancel(pendingContext, info, cbCancel) {
    this.logDebug(() => `PENDING CANCEL: '${pendingContext.pendingId}' - connection[${this.id}].${info}`, 'actor-pending', 'fde3322f-4488-4d33-9f68-88cb67070121');
    this._onPendingCancel(pendingContext);
    pendingContext.pendingId = '';
    pendingContext.generator = null;
    if(undefined !== cbCancel) {
      cbCancel();
    }
  }
  
  pendingStartContext(pendingContext, info, cbCancel) {
    this.logDebug(() => `PENDING START: '${pendingContext.pendingId}' - connection[${this.id}].${info}`, 'actor-pending', '8ff99e21-3fb5-431c-8bb8-83e4f157bf0b');
    this._onPendingStartContext(pendingContext, info, cbCancel);
  }
  
  _pendingDoneContext(pendingContext, info, msg) {
    this.logDebug(() => `PENDING DONE: '${pendingContext.pendingId}' - connection[${this.id}].${info}`, 'actor-pending', '4a86ef56-0185-41e7-935e-1520c2991e7a');
    if(this.actor.config.breakOnIpEvent) {
      debugger;
    }
    this._getPendingObject(pendingContext).pendingSuccess(pendingContext.pendingId, msg);
    pendingContext.pendingId = '';
    pendingContext.generator = null;
    if(null !== pendingContext.cbDone) {
      pendingContext.cbDone();
    }
  }
  
  _pendingDoneContextDelay(pendingContext, info, msg) {
    this.logDebug(() => `PENDING DONE: '${pendingContext.pendingId}' - connection[${this.id}].${info}`, 'actor-pending', '4a86ef56-0185-41e7-935e-1520c2991e7a');
    this._getPendingObject(pendingContext).pendingSuccess(pendingContext.pendingId, msg);
    pendingContext.pendingId = '';
    pendingContext.generator = null;
    if(this.actor.config.breakOnIpEvent) {
      debugger;
    }
  }
  
  pendingDoneContext(pendingContext, info, msg) {
    if(!pendingContext.delay) {
      if(this.actor.config.slow && this.actor.config.slowOnIpEvent) {
        setTimeout((pendingContext, info, msg) => {
          this._pendingDoneContext(pendingContext, info, msg);
        }, this.actor.slowAwaitTime.p, pendingContext, info, msg);
      }
      else {
        this._pendingDoneContext(pendingContext, info, msg);
      }
    }
    else {
      pendingContext.cbDone(msg, () => {
        if(this.actor.config.slow && this.actor.config.slowOnIpEvent) {
          setTimeout((pendingContext, info, msg) => {
            this._pendingDoneContextDelay(pendingContext, info, msg);
          }, this.actor.slowAwaitTime.p, pendingContext, info, msg);
        }
        else {
          this._pendingDoneContextDelay(pendingContext, info, msg);
        }
      });
    }
  }
  
  _pendingErrorContext(pendingContext, info, err) {
    this.logDebug(() => `PENDING ERROR: '${pendingContext.pendingId}' - connection[${this.id}].${info}`, 'actor-pending', '42f3d3f0-7db0-4820-8263-4f4588e7da4b');
    this._getPendingObject(pendingContext).pendingError(pendingContext.pendingId, err);
    pendingContext.pendingId = '';
    pendingContext.generator = null;
    if(null !== pendingContext.cbDone) {
      pendingContext.cbDone();
    }
    if(this.actor.config.breakOnIpEvent) {
      debugger;
    }
  }
  
  pendingErrorContext(pendingContext, info, err) {
    if(this.actor.config.slow && this.actor.config.slowOnIpEvent) {
      setTimeout((pendingContext, info, msg) => {
        this._pendingErrorContext(pendingContext, info, err);
      }, this.actor.slowAwaitTime.p, pendingContext, info, err);
    }
    else {
      this._pendingErrorContext(pendingContext, info, err);
    }
  }
  
  pendingResultContext(info, msg) {
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'reuse');
    this.pendingDoneContext(pendingContext, 'reuse', msg);
  }
    
  addErrorData(action) {
    // TODO: config
    this.errorData.push({
      time: process.hrtime.bigint(),
      action: action
    });
  }
  
  logIpStack(log, logDataStackType, guid) {
    this.logIp(() => (this.createLogMessage(log, null, new LogMsgStack(this.actor.columnIndex, logDataStackType, this.id, this.stackType, this.name), null)), 'Stack', guid);
  }
      
  createLogMessage(message, inners, data, dataBuffers) {
    return this.actor.createLogMessage(message, inners, data, dataBuffers);
  }
  
  logEngine(message, group, guid, depth=2) {
    this.actor.logEngine(message, group, guid, depth);
  }

  logDebug(message, group, guid, depth=2) {
    this.actor.logDebug(message, group, guid, depth);
  }
    
  logError(errOrMsg, guid, depth=2) {
    this.actor.logError(errOrMsg, guid, depth);
  }
  
  isLogIp() {
    return this.actor.isLogIp();
  }
  
  logIp(log, group, guid, depth=2) {
    this.actor.logIp(log, group, guid, depth);
  }
  
  isLogGui(group) {
    return this.actor.isLogGui(group);
  }
  
  logGui(log, group, guid, depth=1) {
    this.actor.logGui(log, group, guid, depth);
  }
  
  logWarning(message, group, guid, depth=2) {
    this.actor.logWarning(message, group, guid, depth);
  }
  
  logTestData(message, group, guid, depth=2) {
    this.actor.logTestData(message, group, guid, depth);
  }
  
  logBrowserLog(message, group, guid, depth=2) {
    this.actor.logBrowserLog(message, group, guid, depth);
  }
  
  logBrowserErr(message, group, guid, depth=2) {
    this.actor.logBrowserErr(message, group, guid, depth);
  }
  
  getTestDataNumber(key, defaultValue, formatter) {
    this.actor.getTestDataNumber(key, defaultValue, formatter);
  }
  
  getTestDataBoolean(key, defaultValue, formatter) {
    this.actor.getTestDataBoolean(key, defaultValue, formatter);
  }
  
  createGauge(group, family, name, minValue, maxValue) {
    return this.actor.executionContext.debugDashboard.gaugeManager.create(group, family, name, minValue, maxValue);
  }
  
  next(pendingContext, value, err) {
    if(!err) {
      try {
        pendingContext.generator.next(value);
      }
      catch(e) {
        if('' !== pendingContext.pendingId) {
          this.pendingErrorContext(pendingContext, 'catch - next', e);
        }
        else {
          this.logError(e, '44512a28-fc89-4119-927a-38c5c5d7987d');
        }
      }
    }
    else {
      if('' !== pendingContext.pendingId) {
        this.pendingErrorContext(pendingContext, 'callback - next', err);
      }
      else {
        this.logError(err, '26e89cfe-e421-4a19-b26a-71da2a1f245a');
      }
    }
  }
}


module.exports = Connection;
