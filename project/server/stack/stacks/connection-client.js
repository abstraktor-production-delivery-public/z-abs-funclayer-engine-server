
'use strict';

const Address = require('../../address/address');
const TcpConnectionClient = require('../network/tcp/tcp-connection-client');
const TcpConnectionShared = require('../network/tcp/tcp-connection-shared');
const UdpConnectionClient = require('../network/udp/udp-connection-client');
const UdpConnectionClientShared = require('../network/udp/udp-connection-client-shared');
const McConnectionClient = require('../network/mc/mc-connection-client');
const McConnectionClientShared = require('../network/mc/mc-connection-client-shared');
const TlsConnectionClient = require('../network/tls/tls-connection-client');
const TlsConnectionShared = require('../network/tls/tls-connection-shared');
const NetworkType = require('../network/network-type');
const LogMsgClient = require('z-abs-funclayer-engine-cs/clientServer/log/msg/log-msg-client');
const ConnectionIp = require('./connection-ip');
const PendingContext = require('./pending-context');
const LogDataAction = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-action');


class ConnectionClient extends ConnectionIp {
  constructor(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options) {
    super(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options);
    this.cbRemove = null;
    this.dnsUrlCache = null;
  }
  
  connect(dstAddress, srcAddress, dnsUrlCache, cbRemove) {
    this.dstAddress = dstAddress;
    this.dnsUrlCache = dnsUrlCache;
    this.cbRemove = cbRemove;
    switch(this.options.transportLayer.transportType) {
      case NetworkType.UDP: {
        const connection = new UdpConnectionClient(srcAddress, dstAddress, this.options.transportLayer.transportProperties);
        if(!this._shared()) {
          this.networkConnection = connection;
        }
        else {
           this.networkConnection = new UdpConnectionClientShared(connection, 'udp client');
        }
        break;
      }
      case NetworkType.MC: {
        const connection = new McConnectionClient(srcAddress, dstAddress, this.options.transportLayer.transportProperties);
        if(!this._shared()) {
          this.networkConnection = connection;
        }
        else {
          this.networkConnection = new McConnectionClientShared(connection, 'mc client');
        }
        break;
      }
      case NetworkType.TLS: {
        const connection = new TlsConnectionClient(srcAddress, dstAddress, this.options.transportLayer.transportProperties);
        if(!this._shared()) {
          this.networkConnection = connection;
        }
        else {
          this.networkConnection = new TlsConnectionShared(connection, 'tls client');
        }
        break;
      }
      case NetworkType.TCP:
      default: {
        const connection = new TcpConnectionClient(srcAddress, dstAddress, this.options.transportLayer.transportProperties);
        if(!this._shared()) {
          this.networkConnection = connection;
        }
        else {
          this.networkConnection = new TcpConnectionShared(connection, 'tcp client');
        }
        break;
      }
    }
    let cancel = false;
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'connect', () => {
      cancel = true;
    });
    this._resolve(srcAddress, dstAddress, (err, sourceAddress, destinationAddress) => {
      if(err) {
        return this.pendingErrorContext(pendingContext, 'connect', err);
      }
      this.connectionObject = this.actor.executionContext.connectionManager.registerClientConnectionData(this.id, this.id, this.actor.columnIndex, sourceAddress, destinationAddress, this.options.transportLayer, false);
      const connectionDataLocal = this.getConnectionDataLocal();
      const connectionDataRemote = this.getConnectionDataRemote();
      this.logIp(() => this.createLogMessage(`CONNECTING${this.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new LogMsgClient(LogDataAction.CONNECTING, this.name, connectionDataLocal, connectionDataRemote), null), 'core', 'ba1c1680-78a2-4e5d-ba24-0a3af3046607');
      if(this._shared()) {
        this.networkConnection.ref();
      }
      this.networkConnection.connect((err) => {
        if(!cancel) {
          if(err) {
            this.connectionObject.registerConnected(connectionDataLocal, false, (connectionDataRemote) => {
              this.logIp(() => this.createLogMessage(`NOT CONNECTED${this.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new LogMsgClient(LogDataAction.NOT_CONNECTED, this.name, connectionDataLocal, connectionDataRemote), null) , 'core', 'f5ba46f5-4e3f-4dec-9e85-17ca20c99923');
            });
            cbRemove(this);
            if(!cancel) {
              this.pendingErrorContext(pendingContext, 'connect', err);
            }
          }
          else {
            this.setState(ConnectionIp.STATE_CONNECTED);
            this.updateConnectionDataLocal();
            if(!cancel) {
              if(this.connectionWorker) {
                const pending = this.connectionWorker._onConnected((connection, err) => {
                  if(!cancel) {
                    if(!err) {
                      this._clientConnected(pendingContext, connection ? connection : this);
                    }
                    else {
                      this.pendingErrorContext(pendingContext, 'connect', err);
                    }
                  }
                });
                if(!pending) {
                  this._clientConnected(pendingContext, this);
                }
              }
              else {
                this._clientConnected(pendingContext, this);
              }
            }
            else {
              console.log('CLIENT - CANCELED');
            }
          }
        }
      });
    });
  }
  
  _clientConnected(pendingContext, connection) {
    connection.pendingDoneContext(pendingContext, 'connect', connection);
    const connectionDataLocal = connection.getConnectionDataLocal();
    connection.connectionObject.registerConnected(connectionDataLocal, true, (connectionDataRemote) => {
      connection.logIp(() => connection.createLogMessage(`CONNECTED${connection.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new LogMsgClient(LogDataAction.CONNECTED, connection.name, connectionDataLocal, connectionDataRemote), null), 'core', '19ca0edc-b1e3-44d1-b0ff-099cb936b61c');
    });
  }
  
  getDstAddress() {
    return this.dstAddress;
  }
  
  detachTo(connection) {
    const clientConnectionData = this.connectionObject.clients.get(this.id);
    this.connectionObject.clients.delete(this.id);
    clientConnectionData.id = connection.id;
    this.connectionObject.clients.set(clientConnectionData.id, clientConnectionData);
    connection.connectionObject = this.connectionObject;
    this.connectionObject = null;
    super.detachTo(connection);
    this.cbRemove(this);
    connection.cbRemove = this.cbRemove;
  }
  
  onClose(done) {
    this.cbRemove(this);
    done();
  }
    
  _resolveDone(cancel, pendings, err, srcAddress, dstAddress, done) {
    process.nextTick((cancel, pendings, err, srcAddress, dstAddress, done) => {
      if(!cancel.value) {
        if(err) {
          cancel.value = true;
          done(err);
        }
        else if(0 === --pendings.value) {
          done(null, srcAddress, dstAddress);
        }
      }
    }, cancel, pendings, err, srcAddress, dstAddress, done);
  }
  
  _resolve(srcAddress, dstAddress, done) {
    const pendings = {value: 2};
    const cancel = {value: false};
    if(Array.isArray(dstAddress)) {
      if(2 === dstAddress.length) {
        dstAddress = dstAddress[0];
      }
      else {
        process.nextTick((done) => {
          done(new Error('Wrong address format'));
        }, done);
      }
    }
    this.dnsUrlCache.resolveAddress(srcAddress, Address.LOCAL, (err) => {
      this._resolveDone(cancel, pendings, err, srcAddress, dstAddress, done);
    });
    this.dnsUrlCache.resolveAddress(dstAddress, Address.REMOTE, (err) => {
      this._resolveDone(cancel, pendings, err, srcAddress, dstAddress, done);
    });
  }
  
  getConnectionDataLocal() {
    return this.connectionObject.getClientConnectionData(this.id);
  }
  
  getConnectionDataRemotes() {
    return this.connectionObject.getServerConnectionDatas();
  }
  
  updateConnectionDataLocal() {
    const nc = this.networkConnection;
    if(nc) {
      const clientConnectionData = this.getConnectionDataLocal();
      clientConnectionData.update(nc.getLocalAddress(), nc.getLocalPort(), nc.getLocalFamily());
    }
  }
}


module.exports = ConnectionClient;
