
'use strict';

const IpLog = require('./ip-log');


class Encoder {
  constructor() {
    this.connection = null;
    this.connectionOptions = null;
    this.isLogIp = false;
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
    this.pendingContext = null;
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    this.connection = connection;
    this.connectionOptions = connection.connectionOptions;
    this.isLogIp = isLogIp;
    this.pendingContext = pendingContext;
  }
  
  *doEncode() {
    let cancel = false;
    this.connection.pendingStartContext(this.pendingContext, 'encoder', () => {
      cancel = true;
    });
    yield* this.encode();
    if(!cancel) {
      this.connection.pendingDoneContext(this.pendingContext, 'encoder');
    }
  }
  
  *send(msg) {
    yield this.connection._send(this.pendingContext, msg);
  }
  
  addLog(innerLog, dataBuffers) {
    if(innerLog) {
      this.ipLog.addLog(innerLog);
      if(dataBuffers) {
        dataBuffers.forEach((dataBuffer) => {
          this.ipLogDataBuffers.push(dataBuffer);
        });
      }
    }
  }
  
  setCaption(caption) {
    this.ipLog.setCaption(caption);
  }
  
  clearLog() {
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
  }
  
  logMessage() {
    if(this.isLogIp) {
      const ipLog = this.ipLog;
      const ipLogDataBuffers = this.ipLogDataBuffers;
      process.nextTick(() => {
        this.connection.logMessageSend(ipLog.innerLogs, ipLog.caption, ipLogDataBuffers);
      });
    }
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
  }
  
  logWarning(message, group, guid) {
    this.connection.logWarning(message, group, guid);
  }
}

module.exports = Encoder;
