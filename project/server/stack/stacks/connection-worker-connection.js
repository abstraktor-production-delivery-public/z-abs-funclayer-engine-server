
'use strict';


class ConnectionWorkerConnection {
  constructor() {
    this.connection = null;
    this.callFromConnectionWorker = false;
  }
  
  setConnection(connection) {
    this.connection = new Proxy(connection, {
      get: (target, property, receiver) => {
        if('function' === typeof target[property]) {
          return (...args) => {
            this.callFromConnectionWorker = true;
            target[property](...args);
            this.callFromConnectionWorker = false;
          };
        }
        else {
          return target[property];
        }
      }
    });
  }
}

module.exports = ConnectionWorkerConnection;
