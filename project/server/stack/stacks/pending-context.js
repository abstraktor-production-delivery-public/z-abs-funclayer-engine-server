
'use strict';


class PendingContext {
  constructor(generator, type = PendingContext.ACTOR, delay = false) {
    this.pendingId = '';
    this.type = type;
    this.delay = delay;
    this.generator = generator;
    this.cbDone = null;
  }
}

PendingContext.ACTOR = 0;
PendingContext.CONNECTION_WORKER = 1;

PendingContext.TYPE = [
  'ACTOR',
  'CONNECTION_WORKER'
];

module.exports = PendingContext;
