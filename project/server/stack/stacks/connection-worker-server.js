
'use strict';

const ConnectionWorker = require('./connection-worker');


class ConnectionWorkerServer extends ConnectionWorker {
  constructor() {
    super();
  }
  
  _selectChild(msg) {
    return this.onAccepted && 'a' === this.currentGeneratorName;
  }
  
  _onAccepted(done) {
    if(this.onAccepted) {
      this.done = done;
      this.currentGenerator = this.doOnAccepted();
      this.currentGeneratorName = 'a';
      process.nextTick(() => {
        this.next();
      });
      return true;
    }
    else {
      return false;
    }
  }
  
  *doOnAccepted() {
    const newConnection = yield* this.onAccepted(this.connection);
    const done = this.done;
    this.done = null;
    done(newConnection);
  }
}

module.exports = ConnectionWorkerServer;
