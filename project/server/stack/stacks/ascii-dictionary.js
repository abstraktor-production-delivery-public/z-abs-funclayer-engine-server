
'use strict';


class AsciiDictionary {
  static getSymbolString(text) {
    const length = text.length;
    const stringArray = [];
    for(let i = 0; i < length; ++i) {
      stringArray.push(AsciiDictionary.getSymbol(text.charCodeAt(i)));
    }
    return stringArray.join('');
  }
  
  static getSymbol(index) {
    return AsciiDictionary.dictionary[index][0];
  }
  
  static getHtmlName(index) {
    return AsciiDictionary.dictionary[index][1];
  }
  
  static getDesciption(index) {
    return AsciiDictionary.dictionary[index][2];
  }
}

// Done from: 'https://www.asciitabell.se/'
AsciiDictionary.dictionary = [
  ['NUL', '&#000;', 'Null char.'],                                      // 00
  ['[SOH]', '&#001;', 'Start of Header'],                               // 01
  ['[STX]', '&#002;', 'Start of Text'],                                 // 02
  ['[ETX]', '&#003;', 'End of Text'],                                   // 03
  ['[EOT]', '&#004;', 'End of Transmission'],                           // 04
  ['[ENQ]', '&#005;', 'Enquiry'],                                       // 05
  ['[ACK]', '&#006;', 'Acknowledgment'],                                // 06
  ['[BEL]', '&#007;', 'Bell'],                                          // 07
  ['[BS]', '&#008;', 'Backspace'],                                      // 08
  ['[HT]', '&#009;', 'Horizontal Tab'],                                 // 09
  ['[LF]', '&#010;', 'Line Feed'],                                      // 10
  ['[VT]', '&#011;', 'Vertical Tab'],                                   // 11
  ['[FF]', '&#012;', 'Form Feed'],                                      // 12
  ['[CR]', '&#013;', 'Carriage Return'],                                // 13
  ['[SO]', '&#014;', 'Shift Out'],                                      // 14
  ['[SI]', '&#015;', 'Shift In'],                                       // 15
  ['[DLE]', '&#016;', 'Data Link Escape'],                              // 16
  ['[Dcl]', '&#017;', 'XON Device Control 1'],                          // 17
  ['[DC2]', '&#018;', 'Device Control 2'],                              // 18
  ['[DC3]', '&#019;', 'XOFFDevice Control 3'],                          // 19
  ['[DC4]', '&#020;', 'Device Control 4'],                              // 20
  ['[NAK]', '&#021;', 'Negative Acknowledgement'],                      // 21
  ['[SYN]', '&#022;', 'Synchronous Idle'],                              // 22
  ['[ETB]', '&#023;', 'End of Trans. Block'],                           // 23
  ['[CAN]', '&#024;', 'Cancel'],                                        // 24
  ['[EM]', '&#025;', 'End of Medium'],                                  // 25
  ['[SUB]', '&#026;', 'Substitute'],                                    // 26
  ['[ESC]', '&#027;', 'Escape'],                                        // 27
  ['[FS]', '&#028;', 'File Separator'],                                 // 28
  ['[GS]', '&#029;', 'Group Separator'],                                // 29
  ['[RS]', '&#030;', 'Record Separator'],                               // 30
  ['[US]', '&#031;', 'Unit Separator'],                                 // 31
  [' ', '&#32;', 'Mellanslag'],                                         // 32
  ['!', '&#33;', 'Utropstecken'],                                       // 33
  ['"', '&#34;', '&quot;	Citationstecken (Citattecken)'],              // 34
  ['#', '&#35;', 'Nummertecken (staket)'],                              // 35
  ['$', '&#36;', 'Dollartecken'],                                       // 36
  ['%', '&#37;', 'Procenttecken'],                                      // 37
  ['&', '&#38;', '&amp;	Ochtecken'],                                    // 38
  ['\'', '&#39;', 'Apostrof'],                                          // 39
  ['(', '&#40;', 'Vänster parentes'],                                   // 40
  [')', '&#41;', 'Höger parentes'],                                     // 41
  ['*', '&#42;', 'Asterisk'],                                           // 42
  ['+', '&#43;', 'Plus'],                                               // 43
  [',', '&#44;', 'Komma'],                                              // 44
  ['-', '&#45;', 'Minus'],                                              // 45
  ['.', '&#46;', 'Punkt'],                                              // 46
  ['/', '&#47;', 'Divisionstecken (Slash)'],                            // 47
  ['0', '&#48;', 'Siffran 0'],                                          // 48
  ['1', '&#49;', 'Siffran 1'],                                          // 49
  ['2', '&#50;', 'Siffran 2'],                                          // 50
  ['3', '&#51;', 'Siffran 3'],                                          // 51
  ['4', '&#52;', 'Siffran 4'],                                          // 52
  ['5', '&#53;', 'Siffran 5'],                                          // 53
  ['6', '&#54;', 'Siffran 6'],                                          // 54
  ['7', '&#55;', 'Siffran 7'],                                          // 55
  ['8', '&#56;', 'Siffran 8'],                                          // 56
  ['9', '&#57;', 'Siffran 9'],                                          // 57
  [':', '&#58;', 'Kolon'],                                              // 58
  [';', '&#59;', 'Semikolon'],                                          // 59
  ['<', '&lt', '	Mindre än'],                                          // 60
  ['=', '&#61;', 'Lika med'],                                           // 61
  ['>', '&gt;	Större än'],                                              // 62
  ['?', '&#63;', 'Frågetecken'],                                        // 63
  ['@', '&#64;', 'Snabel-A (At)'],                                      // 64
  ['A', '&#65;', 'Versalt A'],                                          // 65
  ['B', '&#66;', 'Versalt B'],                                          // 66
  ['C', '&#67;', 'Versalt C'],                                          // 67
  ['D', '&#68;', 'Versalt D'],                                          // 68
  ['E', '&#69;', 'Versalt E'],                                          // 69
  ['F', '&#70;', 'Versalt F'],                                          // 70
  ['G', '&#71;', 'Versalt G'],                                          // 71
  ['H', '&#72;', 'Versalt H'],                                          // 72
  ['I', '&#73;', 'Versalt I'],                                          // 73
  ['J', '&#74;', 'Versalt J'],                                          // 74
  ['K', '&#75;', 'Versalt K'],                                          // 75
  ['L', '&#76;', 'Versalt L'],                                          // 76
  ['M', '&#77;', 'Versalt M'],                                          // 77
  ['N', '&#78;', 'Versalt N'],                                          // 78
  ['O', '&#79;', 'Versalt O'],                                          // 79
  ['P', '&#80;', 'Versalt P'],                                          // 80
  ['Q', '&#81;', 'Versalt Q'],                                          // 81
  ['R', '&#82;', 'Versalt R'],                                          // 82
  ['S', '&#83;', 'Versalt S'],                                          // 83
  ['T', '&#84;', 'Versalt T'],                                          // 84
  ['U', '&#85;', 'Versalt U'],                                          // 85
  ['V', '&#86;', 'Versalt V'],                                          // 86
  ['W', '&#87;', 'Versalt W'],                                          // 87
  ['X', '&#88;', 'Versalt X'],                                          // 88
  ['Y', '&#89;', 'Versalt Y'],                                          // 89
  ['Z', '&#90;', 'Versalt Z'],                                          // 90
  ['[', '&#91;', 'Vänster hakparentes'],                                // 91
  ['\'', '&#92;', 'Backslash'],                                         // 92
  [']', '&#93;', 'Höger hakparentes'],                                  // 93
  ['^', '&#94;', 'Utelämningstecken (Caret)'],                          // 94
  ['_', '&#95;', 'Horisontell linje'],                                  // 95
  ['`', '&#96;', 'Grav ( Acute accent )'],                              // 96
  ['a', '&#97;', 'Gement a'],                                           // 97
  ['b', '&#98;', 'Gement b'],                                           // 98
  ['c', '&#99;', 'Gement c'],                                           // 99
  ['d', '&#100;', 'Gement d'],                                          // 100
  ['e', '&#101;', 'Gement e'],                                          // 101
  ['f', '&#102;', 'Gement f'],                                          // 102
  ['g', '&#103;', 'Gement g'],                                          // 103
  ['h', '&#104;', 'Gement h'],                                          // 104
  ['i', '&#105;', 'Gement i'],                                          // 105
  ['j', '&#106;', 'Gement j'],                                          // 106
  ['k', '&#107;', 'Gement k'],                                          // 107
  ['l', '&#108;', 'Gement l'],                                          // 108
  ['m', '&#109;', 'Gement m'],                                          // 109
  ['n', '&#110;', 'Gement n'],                                          // 110
  ['o', '&#111;', 'Gement o'],                                          // 111
  ['p', '&#112;', 'Gement p'],                                          // 112
  ['q', '&#113;', 'Gement q'],                                          // 113
  ['r', '&#114;', 'Gement r'],                                          // 114
  ['s', '&#115;', 'Gement s'],                                          // 115
  ['t', '&#116;', 'Gement t'],                                          // 116
  ['u', '&#117;', 'Gement u'],                                          // 117
  ['v', '&#118;', 'Gement v'],                                          // 118
  ['w', '&#119;', 'Gement w'],                                          // 119
  ['x', '&#120;', 'Gement x'],                                          // 120
  ['y', '&#121;', 'Gement y'],                                          // 121
  ['z', '&#122;', 'Gement z'],                                          // 122
  ['{', '&#123;', 'Vänster krullparentes'],                             // 123
  ['|', '&#124;', 'Vertikal linje'],                                    // 124
  ['}', '&#125;', 'Höger krullparentes'],                               // 125
  ['~', '&#126;', 'Tilde'],                                             // 126
  ['[DEL]', '&#127;', 'Delete'],                                        // 127
  ['€', '&euro;', 'Euro'],                                              // 128
  ['[UNKNOWN]', '&#129', ''],                                           // 129
  ['‚', '&sbquo;', ''],                                                 // 130
  ['ƒ', '&fnof;', ''],                                                  // 131
  ['„', '&bdquo;', ''],                                                 // 132
  ['…', '&hellip;', 'VHorisontal ellipsis'],                            // 133
  ['†', '&dagger;', 'Dagger'],                                          // 134
  ['‡', '&Dagger;', 'Double dagger'],                                   // 135
  ['ˆ', '&circ;', 'Circumflex'],                                        // 136
  ['‰', '&permil;', 'Promille'],                                        // 137
  ['Š', '&Scaron;', ''],                                                // 138
  ['‹', '&lsaquo;', 'Enkelt vinklat citat vänster'],                    // 139
  ['Œ', '&OElig;', ''],                                                 // 140
  ['', '&#141;', ''],                                                   // 141
  ['Ž', '&#142;', ''],                                                  // 142
  ['', '&#143;', ' '],                                                  // 143
  ['', '&#144;', ''],                                                   // 144
  ['‘', '&lsquo;', 'Citat enkelt vänster'],                             // 145
  ['’', '&rsquo;', 'Citat enkelt höger'],                               // 146
  ['“', '&ldquo;', 'Citat dubbel vänster'],                             // 147
  ['”', '&rdquo;', 'Citat dubbel höger'],                               // 148
  ['•', '&bull;', '	 '],                                                // 149
  ['–', '&ndash;', 'Enkel dash'],                                       // 150
  ['—', '&mdash;', 'Dubbel dash'],                                      // 151
  ['', '&tilde;', ''],                                                  // 152
  ['™', '&trade;', 'Varumärke (Trademark)'],                            // 153
  ['š', '&scaron;', ''],                                                // 154
  ['›', '&rsaquo;', 'Enkelt vinklat citat höger'],                      // 155
  ['œ', '&oelig;', '	 '],                                              // 156
  ['', '&#157;', ''],                                                   // 157
  ['ž', '&#158;', ''],                                                  // 158
  ['Ÿ', '&Yuml;', ''],                                                  // 158	 
  ['', '&nbsp;	Icke-brytande blanksteg'],                              // 160
  ['¡', '&iexcl;	Upp-och-nedvänt utropstecken'],                       // 161
  ['¢', '&cent;	Centtecken'],                                           // 162
  ['£', '&pound;	Pundtecken'],                                         // 163
  ['¤', '&curren;	Allmän valutasymbol'],                                // 164
  ['¥', '&yen;	Yentecken'],                                            // 165
  ['¦', '&brvbar;	Pipe/Vertikalt brutet streck'],                       // 166
  ['§', '&sect;	Paragrafsymbol'],                                       // 167
  ['¨', '&uml;	Trema (dieresis, omljud)'],                             // 168
  ['©', '&copy;', 'Copyright-tecken'],                                  // 169
  ['ª', '&ordf;', 'Feminine ordinal'],                                  // 170
  ['«', '&laquo;', 'Dubbla vinkelcitationstecken (gåsögon), vänster'],  // 171
  ['¬', '&not;', 'Logiskt icke-tecken'],                                // 172
  ['­', '&shy;', 'Kort talstreck'],                                     // 173
  ['®', '&reg;', 'Registrerat varumärke'],                              // 174
  ['¯', '&macr;', 'Makron'],                                            // 175
  ['°', '&deg;', 'Gradtecken'],                                         // 176
  ['±', '&plusmn;', 'Plus-minus -tecken'],                              // 177
  ['²', '&sup2;', 'Upphöjt till 2'],                                    // 178
  ['³', '&sup3;', 'Upphöjt till 3'],                                    // 179
  ['´', '&acute;', 'Accent'],                                           // 180
  ['µ', '&micro;', 'Mikrotecken'],                                      // 181
  ['¶', '&para;', 'Paragraftecken'],                                    // 182
  ['·', '&middot;', 'Mittenpunkt (skalärprodukt)'],                     // 183
  ['¸', '&cedil;', 'Cedilj'],                                           // 184
  ['¹', '&sup1;', 'Mikro'],                                             // 185
  ['º', '&ordm;', 'Masculine ordinal'],                                 // 186
  ['»', '&raquo;', 'Dubbla vinkelcitationstecken (gåsögon), höge'],     // 187
  ['¼', '&fracl4;', 'En fjärdedel'],                                    // 188
  ['½', '&fracl2;', '	En halv'],                                        // 189
  ['¾', '&frac34;', 'Tre fjärdedelar'],                                 // 190
  ['¿', '&iquest;', 'Upp-och-nedvänt frågetecken'],                     // 191
  ['À', '&Agrave;', 'A med grav accent'],                               // 192
  ['Á', '&Aacute;', 'A med akut accent'],                               // 193
  ['Â', '&Acirc;', 'A med cirkumflex'],                                 // 194
  ['Ã', '&Atilde;', 'A med tilde'],                                     // 195
  ['Ä', '&Auml;', 'A med trema'],                                       // 196
  ['Å', '&Aring;', 'A med ring'],                                       // 197
  ['Æ', '&AElig;', 'Ligatur A+E'],                                      // 198
  ['Ç', '&Ccedil;', 'C med cedilj'],                                    // 199
  ['È', '&Egrave;', 'E med grav accent'],                               // 200
  ['É', '&Eacute;', 'E med akut accent'],                               // 201
  ['Ê', '&Ecirc;', 'E med cirkumflex'],                                 // 202
  ['Ë', '&Euml;', 'E med trema'],                                       // 203
  ['Ì', '&Igrave;	I med grav accent'],                                  // 204
  ['Í', '&Iacute;	I med akut accent'],                                  // 205
  ['Î', '&Icirc;	I med cirkumflex'],                                   // 206
  ['Ï', '&Iuml;	I med trema'],                                          // 207
  ['Ð', '&ETH;	(ETH)'],                                                // 208
  ['Ñ', '&Ntilde;	N med tilde'],                                        // 209
  ['Ò', '&Ograve;	O med grav accent'],                                  // 210
  ['Ó', '&Oacute;	O med akut accent'],                                  // 211
  ['Ô', '&Ocirc;	O med cirkumflex'],                                   // 212
  ['Õ', '&Otilde;	O med tilde'],                                        // 213
  ['Ö', '&Ouml;', 'O med trema'],                                       // 214
  ['×', '&times;', 'Multiplikationstecken'],                            // 215
  ['Ø', '&Oslash;', 'Snedstruket O'],                                   // 216
  ['Ù', '&Ugrave;', 'U med grav accent'],                               // 217
  ['Ú', '&Uacute;', 'U med akut accent'],                               // 218
  ['Û', '&Ucirc;', 'U med cirkumflex'],                                 // 219
  ['Ü', '&Uuml;', 'U med trema/umlaut'],                                // 220
  ['Ý', '&Yacute;', 'Tyskt dubbel-s'],                                  // 221
  ['Þ', '&THORN;', 'Isländska THORN-tecknet'],                          // 222
  ['ß', '&szlig;', 'Tyskt dubbel-S'],                                   // 223
  ['à', '&agrave;', 'a med grav accent'],                               // 224
  ['á', '&aacute;', 'a med akut accent'],                               // 225
  ['â', '&acirc;', 'a med cirkumflex'],                                 // 226
  ['ã', '&atilde;', 'a med tilde'],                                     // 227
  ['ä', '&auml;', 'a med trema'],                                       // 228
  ['å', '&aring;', 'a med ring'],                                       // 229
  ['æ', '&aelig;', 'aelig'],                                            // 230
  ['ç', '&ccedil;', 'c med cedilj'],                                    // 231
  ['è', '&egrave;', 'e med grav accent'],                               // 232
  ['é', '&eacute;', 'e med akut accent'],                               // 233
  ['ê', '&ecirc;', 'e med cirkumflex'],                                 // 234
  ['ë', '&euml;', 'e med trema'],                                       // 235
  ['ì', '&igrave;', 'i med grav accent'],                               // 236
  ['í', '&iacute;', 'i med akut accent'],                               // 237
  ['î', '&icirc;', 'i med cirkumflex'],                                 // 238
  ['ï', '&iuml;', 'i med trema'],                                       // 239
  ['ð', '&eth;', 'eth'],                                                // 240
  ['ñ', '&ntilde;', 'n med tilde'],                                     // 241
  ['ò', '&ograve;', 'o med grav accent'],                               // 242
  ['ó', '&oacute;', 'o med akut accent'],                               // 243
  ['ô', '&ocirc;', 'o med cirkumflex'],                                 // 244
  ['õ', '&otilde;', 'o med tilde'],                                     // 245
  ['ö', '&ouml;', 'o med trema'],                                       // 246
  ['÷', '&divide;', 'Divisionstecken'],                                 // 247
  ['ø', '&oslash;', 'Snedstruket o'],                                   // 248
  ['ù', '&ugrave;', 'u med grav accent'],                               // 249
  ['ú', '&uacute;', 'u med akut accent'],                               // 250
  ['û', '&ucirc;', 'u med cirkumflex'],                                 // 251
  ['ü', '&uuml;', 'u med trema/umlaut'],                                // 252
  ['ý', '&yacute;', 'y med akut accent'],                               // 253
  ['þ', '&thorn;', 'Isländska thorn-tecknet'],                          // 254
  ['ÿ', '&yuml;', 'y med trema ']                                       // 255
];

module.exports = AsciiDictionary;
