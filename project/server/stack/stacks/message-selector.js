
'use strict';


class MessageSelector {
  constructor(type, interceptor = false) {
    this.type = type;
    this.interceptor = interceptor;
    this.id = -1;
  }
  
  _setId(id) {
    this.id = id;
  }
  
  _select(msg) {
    return this.onSelect(msg);
  }
}


module.exports = MessageSelector;
