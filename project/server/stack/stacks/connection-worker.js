
'use strict';

const ConnectionWorkerConnection = require('./connection-worker-connection');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ConnectionWorker extends ConnectionWorkerConnection {
  constructor() {
    super();
    this.sending = false;
    this.sendingQueue = [];
    this.receiving = false;
    this.receivingQueue = [];
    this.id = ++ConnectionWorker.currentId;
    this.pendingIds = new Map();
    this.currentGenerator = null;
    this.currentGeneratorName = null;
    this.done = null;
  }
  
  _cancel(pendingContext) {
    this.pendingIds.get(pendingContext);
      process.nextTick(() => {
      
    });
  }
  
  _onDisconnecting(done) {
    if(this.onDisconnecting) {
      this.done = done;
      this.currentGenerator = this.doOnDisconnecting();
      this.currentGeneratorName = 'd';
      process.nextTick(() => {
        this.next();
      });
      return true;
    }
    else {
      return false;
    }
  }
  
  _select(msg) {
    if(this._selectChild(msg)) {
      return true;
    }
    else if(this.onDisconnecting && 'd' === this.currentGeneratorName) {
      return true;
    }
    else if(this.onSelect) {
      return this.onSelect(msg);
    }
    else {
      ddb.error('**************** CW.onSelect not implemented.');
      return false;
    }
  }
  
  *doOnDisconnecting() {
    yield* this.onDisconnecting(this.connection);
    const done = this.done;
    this.done = null;
    done();
  }
  
  setPending(pendingContext, cbCancel) {
    pendingContext.pendingId = GuidGenerator.create();
    this.logEngine(() => `PENDING START: '${pendingContext.pendingId}'`, 'connection-worker', '935eae45-d29c-4870-87f2-2eed69eec12b');
    this.pendingIds.set(pendingContext.pendingId, cbCancel);
  }
  
  deletePending(pendingId) {
    this.pendingIds.delete(pendingId);
    return 0 === this.pendingIds.size;
  }
  
  pendingSuccess(pendingId, value) {
    this.logEngine(() => `PENDING SUCCESS: '${pendingId}'`, 'connection-worker', '61ddfaff-19f1-44bb-a0bd-f180ad8d1470');
    process.nextTick(() => {
      if(this.pendingIds.has(pendingId)) {
        if(this.deletePending(pendingId)) {
          this.next(value);
        }
      }
      else {
        this.logWarning(() => `No matching pendingId on success response. pendingId: '${pendingId}'`, '099b797e-faf8-410e-ab87-dca78a3c273e');
      }
    });
  }

  pendingError(pendingId, err) {
    this.logEngine(() => `PENDING ERROR: '${pendingId}'`, 'core', '8bb0c795-56ab-472d-b0a2-87e66ee05330');
    process.nextTick(() => {
      if(this.pendingIds.has(pendingId)) {
        this.pendingIds.clear(); 
        this.next(undefined, err);
      }
      else {
        this.logWarning(() => `No matching pendingId on error response. pendingId: '${pendingId}'`, 'core', '95b57a64-b1ce-4e67-b0d5-b3e5ef7985ea');
      }
    });
  }
  
  next(value, err) {
    if(!err) {
      try {
        const result = this.currentGenerator.next(value);
        if(result.done) {
          this.currentGenerator = null;
          this.currentGeneratorName = '';
        }
        return;
      }
      catch(e) {
        err = e;
        this.currentGenerator = null;
        this.currentGeneratorName = '';
      }
    }
    const done = this.done;
    this.done = null;
    done(undefined, err);
  }
  
  switchProtocol(stackName, currentConnection) {
    return this.connection.actor.switchProtocol(stackName, currentConnection);
  }
  
  createLogMessage(message, inners, data) {
    return this.connection.createLogMessage(message, inners, data);
  }
  
  logEngine(message, group, guid, depth=3) {
    this.connection.logEngine(message, group, guid, depth);
  }

  logDebug(message, group, guid, depth=3) {
    this.connection.logDebug(message, group, guid, depth);
  }
    
  logError(errOrMsg, guid, depth=3) {
    this.connection.logError(errOrMsg, guid, depth);
  }
  
  isLogIp() {
    return this.connection.isLogIp();
  }
  
  logIp(log, group, guid, depth=3) {
    this.connection.logIp(log, group, guid, depth);
  }
  
  isLogGui(group) {
    return this.connection.isLogGui(group);
  }
  
  logGui(log, group, guid, depth=3) {
    this.connection.logGui(log, group, guid, depth);
  }
  
  logWarning(message, group, guid, depth=3) {
    this.connection.logWarning(message, group, guid, depth);
  }
  
  logTestData(message, group, guid, depth=3) {
    this.connection.logTestData(message, group, guid, depth);
  }
}

ConnectionWorker.currentId = 0;

module.exports = ConnectionWorker;
