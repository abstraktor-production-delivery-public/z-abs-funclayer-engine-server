
'use strict';

const CancelObject = require('./cancel-object');
const Connection = require('./connection');
const NetworkType = require('../network/network-type');
const PendingContext = require('./pending-context');
const MsgIp = require('./msg-ip');
const LogMsgMessage = require('z-abs-funclayer-engine-cs/clientServer/log/msg/log-msg-message');
const LogDataAction = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-action');


class ConnectionIp extends Connection {
  constructor(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options) {
    super(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options);
    this.networkConnection = null;
    this.connectionWorker = options.connectionWorker;
    if(options.connectionWorker) {
      options.connectionWorker.setConnection(this);
    }
    this.sharedManager = null;
    this.messageSelector = options.messageSelector;
    this.connectionObject = null;
    if(NetworkType.DEFAULT === options.transportLayer.transportType) {
      options.transportLayer.transportType = defaultTransportType;
    }
    this.stackLog = `[${name}/${NetworkType.getNetworkName(options.transportLayer.transportType)}]${options.messageSelector && -1 !== options.messageSelector.id ? ('.shared(' + options.messageSelector.id + ')') : ''}`;
    this.state = {
      state: ConnectionIp.STATE_NONE,
      pending: false,
      pendingContext: null,
      cancelObject: null
    };
  }
  
  setState(state) {
    const previousState = this.state.state;
    this.state.state = state;
    this.state.state = state;
  }
  
  _closed(pendingContext, cancelObject, cwErr) {
    this.setState(ConnectionIp.STATE_NONE);
    this.networkConnection = null;
    const connectionDataLocal = this.getConnectionDataLocal();
    this.connectionObject.registerClosed(connectionDataLocal, false, (connectionDataRemote) => {
      this.logIp(() => this.createLogMessage(`CLOSED${this.stackLog}${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new MsgIp(LogDataAction.CLOSED, this.id, this.name, connectionDataLocal, connectionDataRemote), null), 'core', 'bda23330-e507-4c8c-885b-d583beecc72a');
    });
    const cancel = this.state.pending ? this.state.cancelObject.cancel : cancelObject.cancel;
    const context = this.state.pending ? this.state.pendingContext : pendingContext;
    if(!cancel) {
      this.onClose((err) => {
        if(cwErr) {
          this.pendingErrorContext(context, 'close', cwErr);
        }
        else if(!err) {
          this.pendingDoneContext(context, 'close');
        }
        else {
          this.pendingErrorContext(context, 'close', err);
        }
      });
    }
    else if(cwErr) {
      this.pendingErrorContext(context, 'close', cwErr);
    }
    this.state.pendingContext = null;
    this.state.cancelObject = null;
    this.state.pending = false;
  }
  
  _closeTransportConnection(pendingContext, cancelObject, halfClose, cwErr) {
    if(ConnectionIp.STATE_CONNECTED === this.state.state) {
      this.networkConnection.close(() => {
        this._closed(pendingContext, cancelObject, cwErr);
      }, () => {
        this.setState(ConnectionIp.STATE_HALF_CLOSED);
        const connectionDataLocal = this.getConnectionDataLocal();
        const connectionDataRemote = this.getConnectionDataRemote();
        this.logIp(() => this.createLogMessage(`CLOSING${this.stackLog}${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new MsgIp(LogDataAction.CLOSING, this.id, this.name, connectionDataLocal, connectionDataRemote), null), 'core', '1893fff4-9280-434c-886a-08c8d6ad8a54');
        this.pendingDoneContext(pendingContext, 'close');
      });
    }
    else if(ConnectionIp.STATE_HALF_CLOSED === this.state.state) {
      this.state.pendingContext = pendingContext;
      this.state.cancelObject = cancelObject;
      this.state.pending = true;
    }
  }
  
  _close(pendingContext, cancelObject, halfClose) {
    const connectionDataLocal = this.getConnectionDataLocal();
    const connectionDataRemote = this.getConnectionDataRemote();
    this.logIp(() => this.createLogMessage(`CLOSING${this.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new MsgIp(LogDataAction.CLOSING, this.id, this.name, connectionDataLocal, connectionDataRemote), null), 'core', '9a220050-15c6-4748-ab98-d4fe70bda071');
    if(!this.networkConnection.closed()) {
      if(this.connectionWorker) {
        const pending = this.connectionWorker._onDisconnecting((connection, err) => {
          this._closeTransportConnection(pendingContext, cancelObject, halfClose, err);
        });
        if(!pending) {
          this._closeTransportConnection(pendingContext, cancelObject, halfClose);
        }
      }
      else {
        this._closeTransportConnection(pendingContext, cancelObject, halfClose);
      }
    }
    else {
      this._closed(pendingContext, cancelObject);
    }
  }
  
  close(halfClose=true) {
    const cancelObject = new CancelObject();
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'close', () => {
      cancelObject.set();
    });
    if(ConnectionIp.STATE_CONNECTED === this.state.state) {
      this._close(pendingContext, cancelObject, halfClose);
    }
    else if(ConnectionIp.STATE_HALF_CLOSED === this.state.state) {
      if(halfClose) {
        process.nextTick(() => {
          this.pendingDoneContext(pendingContext, 'close');
        });
      }
      else {
        this._close(pendingContext, cancelObject, halfClose);
      }
    }
    else if(ConnectionIp.STATE_NONE === this.state.state) {
      process.nextTick(() => {
        this.pendingDoneContext(pendingContext, 'close');
      });
    }
  }
  
  closeForgottenConnections(done) {
    this.logWarning(() => `${this.name}.connection should be closed when the Actor exits.`, 'core', 'ec2df420-ae82-4d5a-b4c6-42523052bdd4');
    if(null !== this.networkConnection) {
      if(!this.networkConnection.closed()) {
        this.networkConnection.close(() => {
          this.networkConnection = null;
          done();
        });
      }
      else {
        this.networkConnection = null;
        done();
      }
    }
    else {
      this.networkConnection = null;
      done();
    }
  }
  
  detachTo(connection) {
    connection.options = this.options;
    connection.errorData = this.errorData;
    connection.executionId = this.executionId;
    connection.state = this.state;
    connection.networkConnection = this.networkConnection;
    this.networkConnection = null;
    connection.connectionWorker = this.connectionWorker;
    if(connection.connectionWorker) {
      connection.connectionWorker.setConnection(connection); // TODO: Some kind of synchronization with worker.
    }
  }
    
  _sendMessage(encoder, type, cbUnlock) {
    const pendingContext = new PendingContext(encoder.doEncode(), type);
    pendingContext.cbDone = cbUnlock;
    encoder.setConnection(this, this.actor.isLogIp(), pendingContext);
    this.next(pendingContext);
  }
  
  sendMessage(encoder) {
    if(!this._shared()) {
      this._sendMessage(encoder, PendingContext.ACTOR, null);
    }
    else {
      this.networkConnection.sendLock(() => {
        this._sendMessage(encoder, this.connectionWorker && this.connectionWorker.callFromConnectionWorker ? PendingContext.CONNECTION_WORKER : PendingContext.ACTOR, () => {
          this.networkConnection.sendUnlock();
        });
      });
    }
  }
  
  _receiveMessage(decoder, type, delay, cbUnlock) {
    const pendingContext = new PendingContext(decoder.doDecode(), type, delay);
    pendingContext.cbDone = cbUnlock;
    decoder.setConnection(this, this.actor.isLogIp(), pendingContext);
    this.next(pendingContext);
  }
  
  receiveMessage(decoder) {
    if(!this._shared()) {
      this._receiveMessage(decoder, PendingContext.ACTOR, false, null);
    }
    else {
      this.networkConnection.receiveLock(() => {
        if(this.connectionWorker && this.connectionWorker.callFromConnectionWorker) {
          if(!this.networkConnection.receiveRequest(this.connectionWorker, () => {
            ddb.error('CW Msg exists.');
          })) {
            this._receiveMessage(decoder, PendingContext.CONNECTION_WORKER, true, (msg, cbPending) => {
              if(this.connectionWorker._select(msg)) {
                this.networkConnection.receiveUnlock();
                cbPending();
              }
              else {
                this.networkConnection.receiveUnlock();
              }
            });
          }
        }
        else {
          if(!this.networkConnection.receiveRequest(this.messageSelector, () => {
            ddb.error('MS Msg exists.');
          })) {
            this._receiveMessage(decoder, PendingContext.ACTOR, true, (msg, cbPending) => {
                this.networkConnection.receiveUnlock();
                cbPending();
              /*}
              else {
                ddb.error('Actor Msg not interested.');
                this.networkConnection.receiveUnlock();
              }*/
            });
          }
        }
      });
    }
  }
  
  _send(pendingContext, message) {
    this.networkConnection.send(message, () => {
      this.next(pendingContext);
    });
  }
  
  _receiveLine(pendingContext) {
    this.networkConnection.receiveLine((buffer) => {
      this.next(pendingContext, buffer);
    });
  }
  
  _receiveSize(pendingContext, size) {
    this.networkConnection.receiveSize(size, (buffer) => {
      this.next(pendingContext, buffer);
    });
  }
  
  _receive(pendingContext) {
    this.networkConnection.receive((buffer) => {
      this.next(pendingContext, buffer);
    });
  }
    
  logMessageSend(inner, caption, dataBuffers) {
    const connectionDataLocal = this.getConnectionDataLocal();
    const connectionDataRemotes = this.getConnectionDataRemotes();
    connectionDataRemotes.forEach((connectionDataRemote) => {
      this.logIp(() => this.createLogMessage(`SEND-MSG${this.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, inner, new LogMsgMessage(LogDataAction.SEND, this.name, connectionDataLocal, connectionDataRemote, caption), dataBuffers), 'core', 'd426d07a-a166-463f-a473-fe6273feb408');
    });
  }
  
  logMessageReceive(inner, caption, dataBuffers) {
    const connectionDataLocal = this.getConnectionDataLocal();
    const connectionDataRemotes = this.getConnectionDataRemotes();
    connectionDataRemotes.forEach((connectionDataRemote) => {
      this.logIp(() => this.createLogMessage(`RECEIVE-MSG${this.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, inner, new LogMsgMessage(LogDataAction.RECEIVE, this.name, connectionDataLocal, connectionDataRemote, caption), dataBuffers), 'core', '1f3a78c8-8ee0-4f26-a998-c7e5e2650204');
    });
  }
  
  _shared() {
    return !(!this.messageSelector && !this.connectionWorker);
  }
  
  _onGetPendingObject(pendingContext) {
    if(PendingContext.ACTOR === pendingContext.type) {
      return this.actor;
    }
    else if(PendingContext.CONNECTION_WORKER === pendingContext.type) {
      return this.connectionWorker;
    }
  }
  
  _onPendingCancel(pendingContext) {
    if(this.networkConnection) {
      this.networkConnection.cancel();
    }
    if(this.connectionWorker) {
      this.connectionWorker._cancel(pendingContext);
    }
  }
  
  _onPendingStartContext(pendingContext, info, cbCancel) {
    if(PendingContext.ACTOR === pendingContext.type) {
      pendingContext.pendingId = this.actor.setPending(() => {
        this._pendingCancel(pendingContext, info, cbCancel);
      });
    }
    else if(PendingContext.CONNECTION_WORKER === pendingContext.type) {
      this.connectionWorker.setPending(pendingContext, () => {
        this._pendingCancel(pendingContext, info, cbCancel);
      });
    }
  }
  
  getConnectionDataRemote() {
    const connectionDataRemotes = this.getConnectionDataRemotes();
    const connectionDataRemote = connectionDataRemotes.entries().next();
    if(!connectionDataRemote.done) {
      return connectionDataRemote.value[1];
    }
    else {
      return null;
    }
  }
}


ConnectionIp.STATE_NONE = 0;
ConnectionIp.STATE_CONNECTED = 1;
ConnectionIp.STATE_HALF_CLOSED = 2;
ConnectionIp.STATES = [
  'STATE_NONE',
  'STATE_CONNECTED',
  'STATE_HALF_CLOSED'
];


module.exports = ConnectionIp;
