
'use strict';


class MsgIp {
  constructor(actionId, stackId, stack, local, remote, caption, mock) {
    this.actionId = actionId;
    this.stackId = stackId;
    this.stack = stack;
    this.local = local;
    this.remote = remote;
    this.mock = mock;
    this.caption = caption;
  }
}


module.exports = MsgIp;
