
'use strict';

const IpLog = require('./ip-log');


class Decoder {
  constructor() {
    this.connection = null;
    this.isLogIp = false;
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
    this.pendingContext = null;
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    this.connection = connection;
    this.isLogIp = isLogIp;
    this.pendingContext = pendingContext;
  }
  
  *doDecode() {
    let cancel = false;
    this.connection.pendingStartContext(this.pendingContext, 'decoder', () => {
      cancel = true;
    });
    const msg = yield* this.decode();
    if(!cancel) {
      this.connection.pendingDoneContext(this.pendingContext, 'decoder', msg);
    }
  }
  
  *receiveLine() {
    return yield this.connection._receiveLine(this.pendingContext);
  }
  
  *receiveSize(size) {
    return yield this.connection._receiveSize(this.pendingContext, size);
  }
  
  *receive() {
    return yield this.connection._receive(this.pendingContext);
  }
  
  addLog(innerLog, dataBuffers) {
    if(innerLog) {
      this.ipLog.innerLogs.push(innerLog);
      if(dataBuffers) {
        dataBuffers.forEach((dataBuffer) => {
          this.ipLogDataBuffers.push(dataBuffer);
        });
      }
    }
  }
  
  setCaption(caption) {
    this.ipLog.caption = caption;
  }
  
  clearLog() {
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
  }
  
  logMessage() {
    if(this.isLogIp) {
      const ipLog = this.ipLog;
      const ipLogDataBuffers = this.ipLogDataBuffers;
      process.nextTick(() => {
        this.connection.logMessageReceive(ipLog.innerLogs, ipLog.caption, ipLogDataBuffers);
      });
    }
    this.ipLog = new IpLog();
    this.ipLogDataBuffers = [];
  }
}

module.exports = Decoder;
