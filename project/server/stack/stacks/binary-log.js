
'use strict';

const AsciiDictionary = require('./ascii-dictionary');
const LogInner = require('z-abs-funclayer-engine-cs/clientServer/log/log-inner');
const LogPartText = require('z-abs-funclayer-engine-cs/clientServer/log/log-part-text');


class BinaryLog {
  static generateLog(buffer, ipLog) {
    if(Buffer.isBuffer(buffer)) {
      return BinaryLog._generateLogBuffer(buffer, ipLog);
    }
    else if(typeof buffer === 'string') {
      const bufferFromString = Buffer.from(buffer);
      return BinaryLog._generateLogBuffer(bufferFromString, ipLog);
    }
  }
  
  static _generateLogBuffer(buffer, ipLog) {
    const rows = Math.floor(buffer.length / 32);
    const index = BinaryLog._logLoop(buffer, ipLog, -1, rows);
    BinaryLog._logEnding(buffer, ipLog, index, rows);
    if(!Array.isArray(ipLog)) {
      const readableMsg = ipLog.innerLogs[0].logParts[0].text.slice(0).trim();
      return readableMsg.length <= BinaryLog.MAX_CAPTION_SIZE ? readableMsg : readableMsg.substring(0, BinaryLog.BREAK_CAPTION_SIZE) + '...';
    }
    else {
      return null;
    }
  }
  
  static _logLoop(buffer, ipLog, index, rows) {
    for(let row = 0 ; row < rows; ++row) {
      const resultBuffer = Buffer.alloc(BinaryLog.ROW_FORMATTED_SIZE, ' ');
      for(let group = 0; group < 4; ++group) {
        for(let pos = 0; pos < 8; ++pos) {
          let i = BinaryLog.POS_INDEX[pos] + BinaryLog.GROUP_INDEX[group];
          let hex = buffer[++index].toString(16).padStart(2, '0');
          resultBuffer[i] = hex.charCodeAt(0);
          resultBuffer[i + 1] = hex.charCodeAt(1);
        }
      }
      if(Array.isArray(ipLog)) {
        ipLog.push(new LogPartText(resultBuffer.toString()));
      }
      else {
        ipLog.addLog(new LogInner(resultBuffer.toString()));
      }
    }
    return index;
  }
  
  static _logEnding(buffer, ipLog, index, rows) {
    let rowsLength = rows * BinaryLog.ROW_ORIGINAL_SIZE;
    if(rowsLength < buffer.length) {
      const resultBuffer = Buffer.alloc(BinaryLog.ROW_FORMATTED_SIZE, ' ');
      for(let group = 0; group < 4; ++group) {
        for(let pos = 0; pos < 8; ++pos) {
          if(++rowsLength > buffer.length) {
            if(Array.isArray(ipLog)) {
              ipLog.push(new LogPartText(resultBuffer.toString()));
            }
            else {
              ipLog.addLog(new LogInner(resultBuffer.toString()));
            }
            return;
          }
          let i = BinaryLog.POS_INDEX[pos] + BinaryLog.GROUP_INDEX[group];
          let hex = buffer[++index].toString(16).padStart(2, '0');
          resultBuffer[i] = hex.charCodeAt(0);
          resultBuffer[i + 1] = hex.charCodeAt(1);
        }
      }
    }
  }
}

BinaryLog.MAX_CAPTION_SIZE = 30;
BinaryLog.BREAK_CAPTION_SIZE = BinaryLog.MAX_CAPTION_SIZE - 3;

BinaryLog.ROW_ORIGINAL_SIZE = 103;
BinaryLog.ROW_FORMATTED_SIZE = 103;
BinaryLog.POS_INDEX = [0, 3, 6, 9, 12, 15, 18, 21];
BinaryLog.GROUP_INDEX = [0, 26, 52, 78];


module.exports = BinaryLog;
