
'use strict';

const CancelObject = require('./cancel-object');
const Connection = require('./connection');
const PendingContext = require('./pending-context');
const LogDataAction = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-action');


class ConnectionWeb extends Connection {
  constructor(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options) {
    super(id, type, name, actor, defaultTransportType, defaultConnectionOptions, options);
    this.browserData = null;
    this.dnsUrlCache = null;
    this.cbRemove = null;
  }
  
  connect(browserData, dnsUrlCache, reuse, cbRemove) {
    this.browserData = browserData;
    this.dnsUrlCache = dnsUrlCache;
    this.cbRemove = cbRemove;
    dnsUrlCache.override(browserData.dstAddress);
    const cancelObject = new CancelObject();
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'connect', () => {
      cancelObject.set();
    });
    if(undefined !== this.onConnect) {
      this._onConnect(browserData, reuse, pendingContext, cancelObject);
    }
    else {
      process.nextTick(() => {
        if(!cancelObject.cancel) {
          this.pendingDoneContext(pendingContext, 'connect', this);
        }
      });
    }
  }
  
  close() {
    let cancel = false;
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'close', () => {
      cancel = true;
    });
    if(!cancel) {
      this.onClose((err) => {
        this.cbRemove(this, err);
        this.pendingDoneContext(pendingContext, 'close', this);
      });
    }
  }
  
  closeForgottenConnections(done) {
    this.onClose(done);
  }
  
  registerClientConnectionData(connectionId, sourceAddress, destinationAddress, transportType) {
    return this.actor.executionContext.connectionManager.registerClientConnectionData(connectionId, connectionId, this.actor.columnIndex, sourceAddress, destinationAddress, transportType, false);
  }
    
  logGuiEvent(log, logDataGuiType, data, group, id) {
    this.logGui(() => (this.createLogMessage(log, null, {
      actionId: LogDataAction.GUI,
      actorIndex: this.actor.columnIndex,
      type: logDataGuiType,
      stackId: this.id,
      stack: this.name,
      data: data
    })), group, id);
  }
  
  _onConnect(browserData, reuse, pendingContext, cancelObject) {
    this.onConnect(browserData, reuse, (success, err) => {
      if(err) {
        if(!cancelObject.cancel) {
          return this.pendingErrorContext(pendingContext, 'connect', err);
        }
      }
      if(success) {
        if(!cancelObject.cancel) {
          this.pendingDoneContext(pendingContext, 'connect', this);
        }
      }
      else {
         process.nextTick(() => {
          this._onConnect(browserData, reuse, pendingContext, cancelObject);
        });
      }
    }); 
  }
  
  _onGetPendingObject(pendingContext) {
    return this.actor;
  }
  
  _onPendingCancel(pendingContext) {}
  
  _onPendingStartContext(pendingContext, info, cbCancel) {
    pendingContext.pendingId = this.actor.setPending(() => {
      this._pendingCancel(pendingContext, info, cbCancel);
    });
  }
  
  getConnectionDataLocal(connectionObject) {
    return connectionObject.getClientConnectionData(this.id);
  }
}


module.exports = ConnectionWeb;
