
'use strict';

const Address = require('../../address/address');
const ConnectionIp = require('./connection-ip');
const PendingContext = require('./pending-context');
const MsgIp = require('./msg-ip');
const LogDataAction = require('z-abs-funclayer-engine-cs/clientServer/log/log-data-action');


class ConnectionServer extends ConnectionIp {
  constructor(id, type, name, actor, defaultTransportType, connectionOptions, options) {
    super(id, type, name, actor, defaultTransportType, connectionOptions, options);
    this.server = null;
    this.dnsUrlCache = null;
  }
  
  init(server) {
    this.server = server;
    this.server.addConnection(this);
    const address = this.server.srvAddress;
    this.connectionObject = this.actor.executionContext.connectionManager.registerServerConnectionData(this, server.id, this.server.srvAddress, !!this.messageSelector);
  }
  
  accept(connectionWorker) {
    if(connectionWorker) {
      connectionWorker.setConnection(this);
      this.connectionWorker = connectionWorker;
    }
    let cancel = false;
    const pendingContext = new PendingContext();
    this.pendingStartContext(pendingContext, 'accept', () => {
      cancel = true;
    });
    this.dnsUrlCache.resolveAddress(this.server.srvAddress, Address.LOCAL, (err) => {
      if(!cancel) {
        if(err) {
          return this.pendingErrorContext(pendingContext, 'accept', err);
        }
        const connectionDataLocal = this.getConnectionDataLocal();
        this.logIp(() => this.createLogMessage(`ACCEPTING${this.stackLog} ${connectionDataLocal.getId()}`, null, new MsgIp(LogDataAction.ACCEPTING, this.id, this.name, connectionDataLocal, null), null), 'core', 'a8127b6b-e047-449b-9aa1-34b561d5db8c');
        this.server.accept(this, (networkConnection) => {
          if(!cancel) {
            this.setState(ConnectionIp.STATE_CONNECTED);
            this.networkConnection = networkConnection;
            this.updateConnectionDataLocal();
            // TODO: REMOTE FIX
            const connectionDataRemotes = this.getConnectionDataRemotes();
            connectionDataRemotes.forEach((connectionDataRemote) => {
              this.sharedManager.updateServerConnection(this.actor.logName, connectionDataLocal, connectionDataRemote, this.messageSelector);
            });
            if(!cancel) {
              if(this.connectionWorker) {
                const pending = this.connectionWorker._onAccepted((connection, err) => {
                  if(!cancel) {
                    if(!err) {
                      this._serverAccepted(pendingContext, connection ? connection : this);
                    }
                    else {
                      this.pendingErrorContext(pendingContext, 'accept', err);
                    }
                  }
                });
                if(!pending) {
                  this._serverAccepted(pendingContext, this);
                }
              }
              else {
                this._serverAccepted(pendingContext, this);
              }
            }
          }
        });
      }
    });
  }
  
  _serverAccepted(pendingContext, connection) {
    connection.updateConnectionDataLocal();
    connection.pendingDoneContext(pendingContext, 'accept', connection);
    const connectionDataLocal = connection.getConnectionDataLocal();
    if(!connection.connectionObject.registerAccepted(connectionDataLocal)) {
      const nc = this.networkConnection;
      this.actor.executionContext.connectionManager.registerSutClientConnectionData(this.server.srvAddress, connection.options.transportLayer, nc.getRemoteAddress(), nc.getRemotePort(), nc.getRemoteFamily());
    }
    const connectionDataRemotes = connection.getConnectionDataRemotes();
    connectionDataRemotes.forEach((connectionDataRemote) => {
      connection.logIp(() => connection.createLogMessage(`ACCEPTED${connection.stackLog} ${connectionDataLocal.getId()} ${connectionDataRemote.getId()}`, null, new MsgIp(LogDataAction.ACCEPTED, connection.id, connection.name, connectionDataLocal, connectionDataRemote, null, connection.messageSelector && connection.messageSelector.moc), null), 'core', '76bab2d7-b1b4-4bc5-882e-a020af84ac70');
    });
  }
  
  detachTo(connection) {
    connection.server = this.server;
    const serverConnectionData = this.connectionObject.servers.get(this.id);
    this.connectionObject.servers.delete(this.id);
    serverConnectionData.id = connection.id;
    this.connectionObject.servers.set(serverConnectionData.id, serverConnectionData);
    connection.connectionObject = this.connectionObject;
    this.connectionObject = null;
    super.detachTo(connection);
  }
  
  onClose(done) {
    if(null !== this.server) {
      this.server.removeConnection(this, done);
      this.server = null;
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }

  closeForgottenConnections(done) {
    if(null !== this.server) {
      super.closeForgottenConnections(() => {
        this.server = null;
        done();
      });
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
    
  getConnectionDataLocal() {
    return this.connectionObject.getServerConnectionData(this.id);
  }
  
  getConnectionDataRemotes() {
    return this.connectionObject.getClientConnectionDatas();
  }
  
  updateConnectionDataLocal() {
    const nc = this.networkConnection;
    if(nc) {
      const serverConnectionData = this.getConnectionDataLocal();
      serverConnectionData.update(nc.getLocalAddress(), nc.getLocalPort(), nc.getLocalFamily());
    }
  }
}

module.exports = ConnectionServer;
