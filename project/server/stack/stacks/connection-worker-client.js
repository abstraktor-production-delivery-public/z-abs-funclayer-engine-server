
'use strict';

const ConnectionWorker = require('./connection-worker');


class ConnectionWorkerClient extends ConnectionWorker {
  constructor() {
    super();
  }
  
  _selectChild(msg) {
    return this.onConnected && 'c' === this.currentGeneratorName;
  }
  
  _onConnected(done) {
    if(this.onConnected) {
      this.done = done;
      this.currentGenerator = this.doOnConnected();
      this.currentGeneratorName = 'c';
      process.nextTick(() => {
        this.next();
      });
      return true;
    }
    else {
      return false;
    }
  }
  
  *doOnConnected() {
    const newConnection = yield* this.onConnected(this.connection);
    const done = this.done;
    this.done = null;
    done(newConnection);
  }
}

module.exports = ConnectionWorkerClient;
