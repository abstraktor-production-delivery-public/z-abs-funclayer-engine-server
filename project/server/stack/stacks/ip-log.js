
'use strict';


class IpLog {
  constructor() {
    this.innerLogs = [];
    this.caption = '';
  }
  
  addLog(innerLog) {
    if(innerLog) {
      this.innerLogs.push(innerLog);
    }
    return innerLog;
  }
  
  setCaption(caption) {
    this.caption = caption;
  }
}


module.exports = IpLog;
