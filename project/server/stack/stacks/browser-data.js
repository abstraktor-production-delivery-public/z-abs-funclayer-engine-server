
'use strict';

const Address = require('../../address/address');


class BrowserData {
  constructor(srcAddress, dstAddress) {
    //this.srcAddress = 'DEFAULT_SRC' === srcAddress.addressName ? this._generateDefaultSrcAddress(srcAddress) : srcAddress;
    this.srcAddress = srcAddress;
    this.dstAddress = dstAddress;
    this.name = `${srcAddress.addressName}.${srcAddress.incognitoBrowser ? '.' + srcAddress.incognitoBrowser : ''}${srcAddress.page}`;
  }
  
  _generateDefaultSrcAddress(srcAddress) {
    const generatedSrcAddress = Address.from(srcAddress);
    generatedSrcAddress.host = '127.0.0.1';
    generatedSrcAddress.port = 0;
    generatedSrcAddress.family = 'IPv4';
    return generatedSrcAddress;
  }
}


module.exports = BrowserData;
