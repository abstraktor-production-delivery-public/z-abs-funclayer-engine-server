
'use strict';


class StackContentCache {
  constructor() {
    this.urlCache = new Set();
  }
  
  hasUrl(url) {
    if(!this.idCache.has(url)) {
      this.urlCache.add(url);
      return false;
    }
    else {
      return true;
    }
  }
  
  clear() {
    ddb.info('************************************************************************', this.urlCache.size);
    this.urlCache.clear();
  }
}


module.exports = StackContentCache;
