
'use strict';


// This is for protocols so bit 0 will give the value 128

//       0 1 2 3 4 5 6 7
//      +-+-+-+-+-+-+-+-+

class BitByte {
  static getBit(byte, bit) {
    return (byte & BitByte.BYTE_FILTER[bit][bit]) >> BitByte.BYTE[bit];
  }
  
  static getBits(byte, bitFrom, bitTo) {
    return (byte & BitByte.BYTE_FILTER[bitFrom][bitTo]) >> BitByte.BYTE[bitTo];
  }
  
  static setBit(byte, bit, value = 1) {
    if(1 === value) {
      return byte | BitByte.BYTE_FILTER[bit][bit];
    }
    else if(0 === value) {
      return byte & ~BitByte.BYTE_FILTER[bit][bit];
    }
    else {
      throw new Error('A bit can just be 0 or 1.'); 
    }
  }
  
  static setBits(byte, bitFrom, bitTo, value) {
    const zeroWHereValueVillBeSet = byte & ~BitByte.BYTE_FILTER[bitFrom][bitTo];
    const shiftedValue = value << BitByte.BYTE[bitTo];
    return zeroWHereValueVillBeSet | shiftedValue;
  }
}

BitByte.BYTE = [7, 6, 5, 4, 3, 2, 1, 0];

BitByte.BYTE_FILTER = [
  [0b10000000, 0b11000000, 0b11100000, 0b11110000, 0b11111000, 0b11111100, 0b11111110, 0b11111111],
  [NaN, 0b01000000, 0b01100000, 0b01110000, 0b01111000, 0b01111100, 0b01111110, 0b01111111],
  [NaN, NaN, 0b00100000, 0b00110000, 0b00111000, 0b00111100, 0b00111110, 0b00111111],
  [NaN, NaN, NaN, 0b00010000, 0b00011000, 0b00011100, 0b00011110, 0b00011111],
  [NaN, NaN, NaN, NaN, 0b00001000, 0b00001100, 0b00001110, 0b00001111],
  [NaN, NaN, NaN, NaN, NaN, 0b00000100, 0b00000110, 0b00000111],
  [NaN, NaN, NaN, NaN, NaN, NaN, 0b00000010, 0b00000011],
  [NaN, NaN, NaN, NaN, NaN, NaN, NaN, 0b00000001]
];

module.exports = BitByte;
