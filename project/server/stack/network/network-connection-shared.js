
'use strict';


class NetworkConnectionShared {
  constructor(networkConnection, connectionName) {
    this.networkConnection = networkConnection;
    this.connectionName = connectionName;
    this.instances = 0;
    this.sharedMessageSelectors = [];
    this.sending = false;
    this.sendingQueue = [];
    this.receiving = false;
    this.receivingQueue = [];
    this.messageRequestQueue = [];
    this.messageQueue = [];
  }
  
  closed() {
    return this.networkConnection.closed();
  }
  
  halfClosed() {
    return this.networkConnection.closed();
  }
  
  send(msg, done) {
    this.networkConnection.send(msg, () => {
      done();
    });
  }
  
  receiveLine(done) {
    this.networkConnection.receiveLine(done);
  }
  
  receiveSize(size, done) {
    this.networkConnection.receiveSize(size, done);
  }
  
  receiveRequest(sharedMessageSelector, cbMessage) {
    let messageData = null;
    for(let i = 0; i < this.messageQueue.length; ++i) {
      const msgData = this.messageQueue[i];
      if(sharedMessageSelector._select(msg)) {
        messageData = msg;
        break;
      }
    }
    if(null !== messageData) {
      messageData.cbMessage(messageData.msg);
      return true;
    }
    else {
      this.messageRequestQueue.push({
        sharedMessageSelector: sharedMessageSelector,
        cbMessage: cbMessage
      });
      return false;
    }
  }
  
  sendLock(cbExecute) {
    if(!this.sending) {
      this.sending = true;
      cbExecute();
    }
    else {
      this.sendingQueue.push({
        cbExecute: cbExecute
      });
    }
  }
  
  sendUnlock() {
    if(0 === this.sendingQueue.length) {
      this.sending = false;
    }
    else {
      const sendData = this.sendingQueue.shift();
      sendData.cbExecute();
    }
  }
  
  receiveLock(cbExecute) {
    if(!this.receiving) {
      this.receiving = true;
      cbExecute();
    }
    else {
      this.receivingQueue.push({
        cbExecute: cbExecute
      });
    }
  }
  
  receiveUnlock() {
    if(0 === this.receivingQueue.length) {
      this.receiving = false;
    }
    else {
      const receivingData = this.receivingQueue.shift();
      receivingData.cbExecute();
    }
  }
  
  cancel() {
    this.networkConnection.cancel();
  }
  
  onBuffer(buffer) {
    this.networkConnection.onBuffer(buffer);
  }
  
  ref() {
    return ++this.instances;
  }
  
  unref() {
    return --this.instances;
  }
}

module.exports = NetworkConnectionShared;
