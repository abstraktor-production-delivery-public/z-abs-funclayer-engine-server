
'use strict';

const NetworkConnection = require('../network-connection');
const Net = require('net');


class TcpConnection extends NetworkConnection {
  constructor(transportProperties, type) {
    super(transportProperties, 'tcp', type);
    this.socket = null;
    this.cbError = null;
  }
  
  attach(socket) {
    this.socket = socket;
    this.socket.on('data', (buffer) => {
      this.onBuffer(buffer);
    });
    this.socket.on('error', (err) => {
      if(this.cbError) {
        const cbError = this.cbError;
        this.cbError = null;
        cbError(err);
      }
      else {
        this.onError(err);
      }
    });
  }
  
  closed() {
    return !this.socket || this.socket && this.socket.destroyed;
  }
  
  halfClosed() {
    return !this.socket || this.socket && 'readOnly' === this.socket.readyState;
  }
  
  close(done, halfCloseDone) {
    if(null !== this.socket) {
      if(this.transportProperties?.has('allowHalfOpen')) {
        this.socket.once('close', (hadError) => {
          done();
        });
        this.socket.end();
        if(halfCloseDone) {
          process.nextTick(halfCloseDone);
        }
      }
      else {
        this.socket.once('close', (hadError) => {
          process.nextTick(done);
        });
        this.socket.end();
      }
    }
    else {
      process.nextTick(done);
    }
  }
  
  sendMessage(msg, done) {
    if(!this.socket.write(msg)) {
      this.socket.once('drain', done);
    }
    else {
      process.nextTick(done);
    }
  }
    
  getLocalAddress() {
    if(null !== this.socket) {
      return this.socket.localAddress;
    }
  }
  
  getLocalPort() {
    if(null !== this.socket) {
      return this.socket.localPort;
    }
  }
  
  getLocalFamily() {
    if(null !== this.socket) {
      return this.socket.localFamily;
    }
  }
  
  getRemoteAddress() {
    if(null !== this.socket) {
      return this.socket.remoteAddress;
    }
  }
  
  getRemotePort() {
    if(null !== this.socket) {
      return this.socket.remotePort;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.socket) {
      return this.socket.remoteFamily;
    }
  }
}


module.exports = TcpConnection;
