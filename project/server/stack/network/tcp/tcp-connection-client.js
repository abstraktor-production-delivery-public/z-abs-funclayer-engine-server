
'use strict';

const TcpConnection = require('./tcp-connection');
const Net = require('net');


class TcpConnectionClient extends TcpConnection {
  static TRANSPORT_PROPERTIES = new Set(['allowHalfOpen', 'noDelay', 'keepAlive', 'keepAliveInitialDelay']);
  
  constructor(srcAddress, dstAddress, transportProperties) {
    super(transportProperties, 'client');
    this.srcAddress = srcAddress;
    this.dstAddress = dstAddress;
  }
  
  connect(done) {
    let timeoutId = -1;
    this.cbError = (err) => {
      if(-1 !== timeoutId) {
        clearTimeout(timeoutId);
      }
      done(err);
    };
    const options = {
      host: this.dstAddress.host,
      port: this.dstAddress.port,
      localAddress: this.srcAddress.host,
      localPort: this.srcAddress.port
    };
    this.setTransportProperties(options, TcpConnectionClient.TRANSPORT_PROPERTIES);
    const socket = Net.connect(options, () => {
      if(this.cbError) {
        this.cbError = null;
        clearTimeout(timeoutId);
        done();
      }
    });
    timeoutId = setTimeout(() => {
      if(this.cbError) {
        this.cbError = null;
        socket.destroy();
        done(new Error('Timeout'));
      }
    }, 100);
    this.attach(socket);
  }
}


module.exports = TcpConnectionClient;
