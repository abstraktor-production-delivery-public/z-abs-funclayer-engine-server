
'use strict';

const NetworkConnectionShared = require('../network-connection-shared');


class TcpConnectionShared extends NetworkConnectionShared {
  constructor(networkConnection, connectionName) {
    super(networkConnection, connectionName);
  }
  
  connect(done) {
    this.networkConnection.connect(done);
  }
    
  attach(socket) {
    this.networkConnection.attach(socket);
  }
  
  close(done) {
    if(0 === this.unref()) {
      this.networkConnection.close(done);
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.networkConnection.sendMessage(msg, done);
  }
    
  getLocalAddress() {
    return this.networkConnection.getLocalAddress();
  }
  
  getLocalPort() {
    return this.networkConnection.getLocalPort();
  }
  
  getLocalFamily() {
    return this.networkConnection.getLocalFamily();
  }
  
  getRemoteAddress() {
    return this.networkConnection.getRemoteAddress();
  }
  
  getRemotePort() {
    return this.networkConnection.getRemotePort();
  }
  
  getRemoteFamily() {
    return this.networkConnection.getRemoteFamily();
  }
}


module.exports = TcpConnectionShared;
