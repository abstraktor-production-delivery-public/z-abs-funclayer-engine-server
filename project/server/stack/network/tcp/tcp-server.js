
'use strict';

const NetworkServer = require('../network-server');
const Net = require('net');


class TcpServer extends NetworkServer {
  static TRANSPORT_PROPERTIES = new Set(['allowHalfOpen', 'pauseOnConnect', 'noDelay', 'keepAlive', 'keepAliveInitialDelay']);
  
  constructor(cbConnection) {
    super(cbConnection, 'tcp');
  }
  
  init(srvAddress, dstAddress, transportProperties, cbListen) {
    const options = {};
    this.setTransportProperties(options, transportProperties, TcpServer.TRANSPORT_PROPERTIES);
    this.serverSocket = Net.createServer(options, (socket) => {
      this.cbConnection(socket);
    });
    this.serverSocket.on('error', (err) => {
      cbListen(err);
    });
    this.serverSocket.listen({
      host: srvAddress.host,
      port: srvAddress.port
    }, () => {
      cbListen();
    });
  }
}


module.exports = TcpServer;
