
'use strict';

const TcpConnection = require('./tcp-connection');


class TcpConnectionServer extends TcpConnection {
  constructor(transportProperties) {
    super(transportProperties, 'server');
  }
}


module.exports = TcpConnectionServer;
