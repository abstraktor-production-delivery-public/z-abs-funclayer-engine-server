
'use strict';

const Transport = require('./transport');


class NetworkServer {
  constructor(cbConnection, name) {
    this.cbConnection = cbConnection;
    this.name = name;
    this.prefixLength = name.length + 1;
    this.serverSocket = null;
  }
  
  close(cbClose) {
    if(null !== this.serverSocket) {
      this.serverSocket.close((err) => {
        cbClose();
      });
      this.serverSocket = null;
    }
    else {
      process.nextTick(cbClose);
    }
  }
  
  setTransportProperties(options, transportProperties, allowedProperties) {
    Transport.setTransportProperties(options, transportProperties, allowedProperties, this.prefixLength)
  }
}

module.exports = NetworkServer;
