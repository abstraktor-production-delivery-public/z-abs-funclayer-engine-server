
'use strict';


class NetworkType {
  static getNetworkName(networkId) {
    if(networkId < NetworkType.NetworkNames.length && networkId >= 0) {
      return NetworkType.NetworkNames[networkId];
    }
    else {
      return NetworkType.NetworkNameUnknown;
    }
  }
  static getNetworkType(networkName) {
    return _NetworkTypeConst_NetworkTypeNames.get(networkName);
  }
}

NetworkType.DEFAULT = -1;
NetworkType.TCP = 0;
NetworkType.UDP = 1;
NetworkType.MC = 2;
NetworkType.TLS = 3;
NetworkType.BC = 4;
NetworkType.SCTP = 5;
NetworkType.QUIC = 6;

NetworkType.NetworkNames = [
  'tcp',
  'udp',
  'mc',
  'tls',
  'bc',
  'sctp',
  'quic'
];

NetworkType.NetworkNameUnknown = 'unknown';

const _NetworkTypeConst_NetworkTypeNames = new Map();
for(let i = 0; i < NetworkType.NetworkNames.length; ++i) {
  _NetworkTypeConst_NetworkTypeNames.set(NetworkType.NetworkNames[i], i);
}


module.exports = NetworkType;
