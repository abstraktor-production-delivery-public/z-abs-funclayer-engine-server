
'use strict';

const McConnectionShared = require('./mc-connection-shared');


class McConnectionServerShared extends McConnectionShared {
  constructor(networkConnection, connectionName) {
    super(networkConnection, connectionName);
  }
  
  setRemoteAddress(remoteAddress) {
    this.networkConnection.setRemoteAddress(remoteAddress);
  }
  
  attach(socket) {
    this.networkConnection.attach(socket);
  }
  
  closed() {
    return this.networkConnection.closed();
  }
  
  close(done) {
    if(0 === this.unref()) {
      this.networkConnection.close(done);
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.networkConnection.sendMessage(msg, done);
  }
  
  getRemoteAddress() {
    return this.networkConnection.getRemoteAddress();
  }
  
  getRemotePort() {
    return this.networkConnection.getRemotePort();
  }
  
  getRemoteFamily() {
    return this.networkConnection.getRemoteFamily();
  }
}

module.exports = McConnectionServerShared;
