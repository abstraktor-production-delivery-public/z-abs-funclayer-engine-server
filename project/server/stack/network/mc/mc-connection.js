
'use strict';

const NetworkConnection = require('../network-connection');


class McConnection extends NetworkConnection {
  constructor(transportProperties, type) {
    super(transportProperties, 'mc', type);
    this.socket = null;
  }
  
  getLocalAddress() {
    if(null !== this.socket) {
      return this.socket.address().address;
    }
  }
  
  getLocalPort() {
    if(null !== this.socket) {
      return this.socket.address().port;
    }
  }
  
  getLocalFamily() {
    if(null !== this.socket) {
      return this.socket.address().family;
    }
  }
}


module.exports = McConnection;
