
'use strict';

const McConnectionShared = require('./mc-connection-shared');


class McConnectionClientShared extends McConnectionShared {
  constructor(networkConnection, connectionName) {
    super(networkConnection, connectionName);
  }
  
  addMembership(host, interfaceHost) {
    return this.networkConnection.addMembership(host, interfaceHost);
  }
  
  connect(done) {
    this.networkConnection.connect(done);
  }
  
  closed() {
    return this.networkConnection.closed();
  }
  
  close(done) {
    if(0 === this.unref()) {
      this.networkConnection.close(done);
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.networkConnection.sendMessage(msg, done);
  }
  
  getRemoteAddress() {
    return this.networkConnection.getRemoteAddress();
  }
  
  getRemotePort() {
    return this.networkConnection.getRemotePort();
  }
  
  getRemoteFamily() {
    return this.networkConnection.getRemoteFamily();
  }
}

module.exports = McConnectionClientShared;
