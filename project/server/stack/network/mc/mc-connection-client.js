
'use strict';

const McConnection = require('./mc-connection');
const Dgram = require('dgram');


class McConnectionClient extends McConnection {
  constructor(srcAddress, dstAddress, transportProperties) {
    super(transportProperties, 'client');
    this.srcAddress = srcAddress.clone(dstAddress.port);
    this.dstAddress = dstAddress;
    this.transportProperties = transportProperties;
    this.isOpen = false;
  }
  
  addMembership(host, interfaceHost) {
    try {
      this.socket.setMulticastTTL(40);
      this.socket.addSourceSpecificMembership(this.srcAddress.host, host, interfaceHost);
      return null;
    }
    catch(err) {
      return err;
    }
  }
  
  connect(done) {
    let bindDone = done;
    if('IPv4' === this.dstAddress.family) {
      this.socket = Dgram.createSocket({type: 'udp4', reuseAddr: true});
    }
    else if('IPv6' === this.dstAddress.family) {
      this.socket = Dgram.createSocket({type: 'udp6', reuseAddr: true});
    }
    else {
      return done && done(new Error(`No accepted IP family; '${dstAddress.family}'`));
    }
    this.socket.on('error', (err) => {
      if(bindDone) {
        bindDone(err);
      }
    });
    try {
      this.socket.bind({
        port: this.srcAddress.port,
        address: 'win32' === process.platform ? this.srcAddress.host : '0.0.0.0',
        exclusive: false
      }, (err) => {
        bindDone = null;
        if(err) {
          return done(err);
        }
        const error = this.addMembership(this.dstAddress.host, this.srcAddress.host);
        if(error) {
          return done(error);
        }
        this.isOpen = true;
        done();
      });
    }
    catch(err) {
      console.log('BIND:', err);
    }
    this.socket.on('message', (buffer, rinfo) => {
      /*if(rinfo.host != this.dstAddress.host || rinfo.port != this.dstAddress.port) {
        // TODO: addlog error here.
        return;
      }*/
      this.onBuffer(buffer);
    });
    this.socket.on('close', () => {
      this.isOpen = false;
    });
  }
  
  closed() {
    return !this.isOpen;
  }
  
  close(done) {
    if(null !== this.socket) {
      this.socket.close((err) => {
        // TODO: Log
        done(err);
      });
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.socket.send(msg, (err) => {
      done(err);
    });
  }
  
  getRemoteAddress() {
    if(null !== this.socket) {
      return this.dstAddress.host;
    }
  }
  
  getRemotePort() {
   if(null !== this.socket) {
      return this.dstAddress.port;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.socket) {
      return this.dstAddress.family;;
    }
  }
}



module.exports = McConnectionClient;
