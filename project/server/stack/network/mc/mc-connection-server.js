
'use strict';

const McConnection = require('./mc-connection');
const Dgram = require('dgram');


class McConnectionServer extends McConnection {
  constructor(mcServer, transportProperties) {
    super(transportProperties, 'server');
    this.mcServer = mcServer;
    this.remoteAddress = null;
  }
  
  setRemoteAddress(remoteAddress) {
    this.remoteAddress = remoteAddress;
  }
  
  attach(socket) {
    this.socket = socket;
  }
  
  closed() {
    return this.mcServer.closed();
  }
  
  close(done) {
    this.mcServer.closeConnection(this.remoteAddress);
    process.nextTick(() => {
      // TODO: Log
      done();
    });
  }
  
  sendMessage(msg, done) {
    this.mcServer.send(msg, this.remoteAddress, (err) => {
      done(err);
    });
  }
  
  getRemoteAddress() {
    if(null !== this.remoteAddress) {
      return this.remoteAddress.host;
    }
  }
  
  getRemotePort() {
   if(null !== this.remoteAddress) {
      return this.remoteAddress.port;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.remoteAddress) {
      return this.remoteAddress.family;
    }
  }
}


module.exports = McConnectionServer;
