
'use strict';

const NetworkServer = require('../network-server');
const Dgram = require('dgram');


class McServer extends NetworkServer {
  constructor(cbConnection) {
    super(cbConnection, 'mc');
    this.logicConnections = new Map();
    this.isOpen = false;
    this.srvAddress = null;
    this.dstAddress = null;
  }
  
  init(srvAddress, dstAddress, transportProperties, cbListen) {
    this.srvAddress = srvAddress;
    this.dstAddress = dstAddress;
    if(!this.dstAddress) {
      process.nextTick(() => {
        cbListen && cbListen(new Error('MC - A multicast server must have an dstAddress'));
      });
      return;
    }
    if('IPv4' === this.dstAddress.family) {
      this.serverSocket = Dgram.createSocket({type: 'udp4', reuseAddr: true});
    }
    else if('IPv6' === this.dstAddress.family) {
      this.serverSocket = Dgram.createSocket({type: 'udp6', reuseAddr: true});
    }
    else {
      process.nextTick(() => {
        cbListen && cbListen(new Error(`No accepted IP family; '${this.srvAddress.family}'`));
      });
      return;
    }
    this.serverSocket.on('error', (err) => { // ??? Isn't this part of 'connection.attach'.
      cbListen(err);
    });
    try {
      this.serverSocket.bind({
        port: this.srvAddress.port,
        address: this.srvAddress.host,
        exclusive: false
      }, (err) => {
        try {
          this.serverSocket.setMulticastInterface(this.srvAddress.host);
          this.serverSocket.setMulticastTTL(4);
        }
        catch(err) {
          return cbListen(err);
        }
        this.isOpen = true;
        cbListen();
        process.nextTick(() => {
          const key = `${this.dstAddress.host}:${this.dstAddress.port}`;
          const connection = this.cbConnection(this.serverSocket, {
            name: '',
            port: this.dstAddress.port,
            host: this.dstAddress.host,
            family: this.srvAddress.family,
            type: 'client'
          });
          this.logicConnections.set(key, connection);
        });
      });
    }
    catch(err) {
      console.log('BIND:', err);
    }
    this.serverSocket.on('close', () => {
      this.isOpen = false;
    });
  }
  
  closed() {
    return !this.isOpen;
  }

  closeConnection(address) {
    const key = `${address.address}:${address.port}`;
    this.logicConnections.delete(key);
  }
  
  send(msg, address, done) {
    this.serverSocket.send(msg, address.port, address.host, (err) => {
      done(err);
    });
  }
}


module.exports = McServer;
