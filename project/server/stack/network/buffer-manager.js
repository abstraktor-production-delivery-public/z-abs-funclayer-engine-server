
'use strict';


class BufferManager {
  constructor(id) {
    this.id = id;
    this.buffers = [];
    this.length = 0;
    this.searchBufferIndex = 0;
    this.bufferStartPos = 0;
    this.bufferStopIndex = 0;
    this.bufferStopPos = 0;
    this.crEnding = false;
  }
  
  _restore() {
    this.buffers = [];
    this.length = 0;
    this.searchBufferIndex = 0;
    this.bufferStartPos = 0;
    this.bufferStopIndex = 0;
    this.bufferStopPos = 0;
    this.crEnding = false;
  }
  
  getLine() {
    let line = '';
    if(0 === this.bufferStopIndex) {
      const totalLength = this.bufferStopPos + 2;
      line = this.buffers[0].toString('utf8', this.bufferStartPos, this.bufferStopPos);
      if(this.buffers[0].length === totalLength) {
        this.bufferStartPos = 0;
        this.buffers.shift();
      }
      else {
        this.bufferStartPos = totalLength;
      }
      this.bufferStopPos = -1;
      this.length -= (line.length + 2);
    }
    else {
      let buffersLength = 0;
      for(let i = 0; i <= this.bufferStopIndex; ++i) { 
        buffersLength += this.buffers[i].length;
      }
      const contentLength = buffersLength - this.bufferStartPos - (this.buffers[this.bufferStopIndex].length - this.bufferStopPos);
      line = Buffer.concat(this.buffers, buffersLength).toString('utf8', this.bufferStartPos, this.bufferStartPos + contentLength);
      if(buffersLength === line.length + 2) {
        this.bufferStartPos = 0;
        this.buffers.splice(0, this.bufferStopIndex + 1);
      }
      else {
        this.bufferStartPos = this.bufferStopPos + 2;
        this.buffers.splice(0, this.bufferStopIndex);
      }
      this.searchBufferIndex = 0;
      this.bufferStopPos = -1;
      this.length -= (contentLength + 2);
    }
    return line;
  }
  
  findLine() {
    if(0 === this.length) {
      return false;
    }
    // SEARCH FIRST BUFFER
    const currentBuffer = this.buffers[this.searchBufferIndex];
    if(0 === this.searchBufferIndex) {
      const posEnd = currentBuffer.indexOf(BufferManager.CRLF, this.bufferStartPos);
      if(-1 !== posEnd) {
        this.bufferStopIndex = this.searchBufferIndex;
        this.bufferStopPos = posEnd;
        return true;
      } 
      this.crEnding = BufferManager.CR === currentBuffer[currentBuffer.length - 1];
      ++this.searchBufferIndex;
    }
    for(; this.searchBufferIndex < this.buffers.length; ++this.searchBufferIndex) {
      if(this.crEnding) {
        if(BufferManager.LF === currentBuffer[0]) {
          this.bufferStopIndex = this.searchBufferIndex;
          this.bufferStopPos = -1;
          return true;
        }
      }
      const posEnd = currentBuffer.indexOf(BufferManager.CRLF);
      if(-1 !== posEnd) {
        this.bufferStopIndex = this.searchBufferIndex;
        this.bufferStopPos = posEnd;
        return true;
      }
      this.crEnding = BufferManager.CR === currentBuffer[currentBuffer.length - 1];
    }
    return false;
  }
    
  receiveSize(size) {
    if(0 === this.length || this.length < size) {
      return;
    }
    this.length -= size;
    const firstBufferLength = this.buffers[0].length;
    if(firstBufferLength - this.bufferStartPos === size) {
      if(0 === this.bufferStartPos) {
        const b = this.buffers.shift();
        return b;
      }
      else {
        const pos = this.bufferStartPos;
        this.bufferStartPos = 0;
        const buffer = this.buffers.shift();
        const b = buffer.slice(pos, pos + size);
        return b;
      }
    }
    else if(firstBufferLength - this.bufferStartPos > size) {
      const pos = this.bufferStartPos;
      this.bufferStartPos = 0;
      if(0 === pos) {
        const b = this.buffers[0].slice(pos, pos + size);
        this.buffers[0] = Buffer.from(Uint8Array.prototype.slice.call(this.buffers[0], pos + size, this.buffers[0].length));
        return b;
      }
      else {
        const resultBuffer = this.buffers[0].slice(pos, pos + size);
        this.buffers[0] = Buffer.from(Uint8Array.prototype.slice.call(this.buffers[0], pos + size, firstBufferLength));
        return resultBuffer;
      }
    }
    else {
      let totalSize = firstBufferLength - this.bufferStartPos;
      if(totalSize < 0) {
        ddb.warning(totalSize, firstBufferLength, this.bufferStartPos);
      }
      let index = 0;
      let previusTotalSize = 0;
      while(totalSize < size) {
        previusTotalSize = totalSize;
        totalSize += this.buffers[++index].length;
      }
      if(0 !== this.bufferStartPos) {
        this.buffers[0] = Buffer.from(Uint8Array.prototype.slice.call(this.buffers[0], this.bufferStartPos, firstBufferLength));
      }
      const restSize = size - previusTotalSize;
      if(this.buffers[index].length === restSize) {
        this.bufferStartPos = 0;
      }
      else {
        this.buffers.splice(index, 0, this.buffers[index].slice(0, restSize));
        this.bufferStartPos = restSize;
      }
      const buffer = Buffer.concat(this.buffers, size);
      this.buffers.splice(0, index + 1);
      return buffer;
    }
  }
  
  receive() {
    if(0 === this.length) {
      return;
    }
    else {
      const buffers = this.buffers;
      this._restore();
      if(1 === buffers.length) {
        return buffers[0];
      }
      else {
        return Buffer.concat(buffers);
      }
    }
  }
  
  addBuffer(buffer) {
    if(buffer.length === 0) {
      throw new Error('');
    }
    this.buffers.push(buffer);
    this.length += buffer.length;
  }
}

BufferManager.LF = '\n'.charCodeAt(0);
BufferManager.CR = '\r'.charCodeAt(0);
BufferManager.CRLF = '\r\n';


module.exports = BufferManager;
