
'use strict';


class Transport {
  static setTransportProperties(options, transportProperties, allowedProperties, prefixLength) {
    if(transportProperties) {
      transportProperties.forEach((value, key) => {
        const k = key.substring(prefixLength);
        if(allowedProperties.has(k)) {
          Reflect.set(options, k, value);
        }
      });
    }
  }
}


module.exports = Transport;
