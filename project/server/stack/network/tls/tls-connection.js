
'use strict';

const NetworkConnection = require('../network-connection');
const Fs = require('fs');
const Tls = require('tls');


class TlsConnection extends NetworkConnection {
  constructor(transportProperties, type) {
    super(transportProperties, 'tls', type);
    this.socket = null;
    this.cbError = undefined;
  }
  
  attach(socket) {
    this.socket = socket;
    this.socket.on('data', (buffer) => {
      this.onBuffer(buffer);
    });
    this.socket.on('error', (err) => {
      //ddb.writeln('error', err);
      if(undefined !== this.cbError) {
        this.cbError(err);
      }
      else {
        this.onError(err);
      }
    });
    this.socket.on('keylog', (line) => {
      //ddb.writeln('keylog', this.type, line.toString());
    });
    this.socket.on('OCSPRequest', (certificate, issuer, callback) => {
      //ddb.writeln('OCSPRequest', this.type, certificate, issuer);
      callback(null, null);
    });
    this.socket.on('secureConnect', () => {
    //  ddb.writeln('secureConnect');
    });
  }
  
  closed() {
    return !this.socket || this.socket && this.socket.destroyed;
  }
  
  halfClosed() {
    return !this.socket || this.socket && 'readOnly' === this.socket.readyState;
  }
  
  close(done, halfCloseDone) {
    if(null !== this.socket) {
      if(this.transportProperties?.has('tls-allowHalfOpen')) {
        this.socket.once('close', (hadError) => {
          done();
        });
        this.socket.end();
        process.nextTick(halfCloseDone);
      }
      else {
        this.socket.once('close', (hadError) => {
          process.nextTick(done);
        });
        this.socket.end();
      }
    }
    else {
      process.nextTick(done);
    }
  }
  
  sendMessage(msg, done) {
    if(!this.socket.write(msg)) {
      this.socket.once('drain', done);
    }
    else {
      process.nextTick(done);
    }
  }
    
  getLocalAddress() {
    if(null !== this.socket) {
      return this.socket.localAddress;
    }
  }
  
  getLocalPort() {
    if(null !== this.socket) {
      return this.socket.localPort;
    }
  }
  
  getLocalFamily() {
    if(null !== this.socket) {
      return this.socket.localFamily;
    }
  }
  
  getRemoteAddress() {
    if(null !== this.socket) {
      return this.socket.remoteAddress;
    }
  }
  
  getRemotePort() {
    if(null !== this.socket) {
      return this.socket.remotePort;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.socket) {
      return this.socket.remoteFamily;
    }
  }
}


module.exports = TlsConnection;
