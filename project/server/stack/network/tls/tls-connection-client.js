
'use strict';

const StackComponentsCerts = require('z-abs-funclayer-stack-server/server/factory/stack-components-certs');
const TlsConnection = require('./tls-connection');
const Tls = require('tls');
const Fs = require('fs');


class TlsConnectionClient extends TlsConnection {
  constructor(srcAddress, dstAddress, transportProperties) {
    super(transportProperties, 'client');
    this.srcAddress = srcAddress;
    this.dstAddress = dstAddress;
  }
  
  connect(done) {
    this.cbError = done;
    const options = {
      host: this.dstAddress.host,
      port: this.dstAddress.port,
      localAddress: this.srcAddress.host,
      localPort: this.srcAddress.port,
      checkServerIdentity: (host, cert) => {
        //ddb.writeln('checkServerIdentity', host, cert);
      },
      ca: StackComponentsCerts.ca
    };
    this.setTransportProperties(options, TlsConnectionClient.TRANSPORT_PROPERTIES);
    const socket = Tls.connect(options, () => {
      this.cbError = undefined;
      done();
    });
    this.attach(socket);
  }
}


TlsConnectionClient.TRANSPORT_PROPERTIES = new Set(['allowHalfOpen', 'noDelay', 'rejectUnauthorized', 'keepAlive', 'keepAliveInitialDelay']);

module.exports = TlsConnectionClient;
