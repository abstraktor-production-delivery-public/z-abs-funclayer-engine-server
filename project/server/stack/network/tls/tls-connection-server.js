
'use strict';

const TlsConnection = require('./tls-connection');


class TlsConnectionServer extends TlsConnection {
  constructor(transportProperties) {
    super(transportProperties, 'server');
  }
}

module.exports = TlsConnectionServer;
