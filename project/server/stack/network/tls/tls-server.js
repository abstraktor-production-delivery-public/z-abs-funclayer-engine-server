
'use strict';

const StackComponentsCerts = require('z-abs-funclayer-stack-server/server/factory/stack-components-certs');
const NetworkServer = require('../network-server');
const Fs = require('fs');
const Tls = require('tls');


class TlsServer extends NetworkServer {
  constructor(cbConnection) {
    super(cbConnection, 'tls');
  }
  
  init(srvAddress, dstAddress, transportProperties, cbListen) {
    const options = {
      cert: StackComponentsCerts.cert,
      key: StackComponentsCerts.key,
      ca: StackComponentsCerts.ca
      // This is necessary only if using client certificate authentication.
      // requestCert: true
    };
    this.setTransportProperties(options, transportProperties, TlsServer.TRANSPORT_PROPERTIES);
    this.serverSocket = Tls.createServer(options);
    this.serverSocket.on('secureConnection', (socket) => {
      //ddb.writeln('server connected', socket.authorized ? 'authorized' : 'unauthorized');
      this.cbConnection(socket);
    });
    this.serverSocket.on('error', (err) => {
      cbListen(err);
    });
    this.serverSocket.listen({
      host: srvAddress.host,
      port: srvAddress.port
    }, () => {
      cbListen();
    });
  }
}


TlsServer.TRANSPORT_PROPERTIES = new Set(['allowHalfOpen', 'pauseOnConnect', 'noDelay', 'keepAlive', 'keepAliveInitialDelay']);

module.exports = TlsServer;
