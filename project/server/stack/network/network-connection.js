
'use strict';

const BufferManager = require('./buffer-manager');
const Transport = require('./transport');


class NetworkConnection {
  constructor(transportProperties, name, type) {
    this.transportProperties = transportProperties;
    this.name = name;
    this.type = type;
    this.prefixLength = name.length + 1;
    this.bufferManager = new BufferManager();
    this.askedForReceiveLine = false;
    this.askedForReceiveSize = 0;
    this.askedForReceive = false;
    this.cbDone = null;
    this.isCancel = false;
    this.lockSend = false;
    this.lockReceive = false;
  }
  
  halfClosed() {
    return false;
  }
  
  setTransportProperties(options, allowedProperties) {
    Transport.setTransportProperties(options, this.transportProperties, allowedProperties, this.prefixLength)
  }
  
  send(msg, done) {
    this.isCancel = false;
    this.sendMessage(msg, done);
  }
  
  receiveLine(done) {
    this.isCancel = false;
    if(this.bufferManager.findLine()) {
      const line = this.bufferManager.getLine();
      process.nextTick(() => {
        done(line);
      });
      this.cbDone = null;
      this.askedForReceiveLine = false;  
    }
    else {
      this.cbDone = done;
      this.askedForReceiveLine = true;
    }
  }
  
  receiveSize(size, done) {
    this.isCancel = false;
    const buffer = this.bufferManager.receiveSize(size);
    if(undefined !== buffer) {
      process.nextTick(() => {
        done(buffer);
      });
      this.cbDone = null;
      this.askedForReceiveSize = 0;
    }
    else {
      this.cbDone = done;
      this.askedForReceiveSize = size;
    }
  }
  
  receive(done) {
    this.isCancel = false;
    const buffers = this.bufferManager.receive();
    if(buffers) {
      process.nextTick(() => {
        done(buffers);
      });
      this.cbDone = null;
      this.askedForReceive = false;  
    }
    else {
      this.cbDone = done;
      this.askedForReceive = true;
    }
  }
  
  cancel() {
    this.isCancel = true;
  }
  
  onBuffer(buffer) {
    if(this.isCancel) {
      // TODO: Log dropped buffer
      return;
    }
    this.bufferManager.addBuffer(buffer);
    if(this.askedForReceiveLine) {
      this.receiveLine(this.cbDone);
    }
    else if(0 < this.askedForReceiveSize) {
      this.receiveSize(this.askedForReceiveSize, this.cbDone);
    }
    else if(this.askedForReceive) {
      this.receive(this.cbDone);
    }
  }
  
  onError() {
    
  }
  
  sendLock() {
    if(!this.lockSend) {
      
    }
    else {
      
    }
  }
  
  receiveLock() {
    if(!this.lockReceive) {
      
    }
    else {
      
    }
  }
}

module.exports = NetworkConnection;
