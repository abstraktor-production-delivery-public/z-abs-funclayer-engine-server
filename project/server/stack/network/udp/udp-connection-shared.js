
'use strict';

const NetworkConnectionShared = require('../network-connection-shared');


class UdpConnectionShared extends NetworkConnectionShared {
  constructor(networkConnection, connectionName) {
    super(networkConnection, connectionName);
  }
  
  getLocalAddress() {
    return this.networkConnection.getLocalAddress();
  }
  
  getLocalPort() {
    return this.networkConnection.getLocalPort();
  }
  
  getLocalFamily() {
    return this.networkConnection.getLocalFamily();
  }
}

module.exports = UdpConnectionShared;
