
'use strict';

const UdpConnection = require('./udp-connection');
const Dgram = require('dgram');


class UdpConnectionServer extends UdpConnection {
  constructor(udpServer, transportProperties) {
    super(transportProperties, 'server');
    this.udpServer = udpServer;
    this.remoteAddress = null;
  }
  
  setRemoteAddress(remoteAddress) {
    this.remoteAddress = remoteAddress;
  }
  
  attach(socket) {
    this.socket = socket;
  }
  
  closed() {
    return this.udpServer.closed();
  }
  
  close(done) {
    this.udpServer.closeConnection(this.remoteAddress);
    process.nextTick(() => {
      // TODO: Log
      done();
    });
  }
  
  sendMessage(msg, done) {
    this.udpServer.send(msg, this.remoteAddress, (err) => {
      done(err);
    });
  }
  
  getRemoteAddress() {
    if(null !== this.remoteAddress) {
      return this.remoteAddress.host;
    }
  }
  
  getRemotePort() {
   if(null !== this.remoteAddress) {
      return this.remoteAddress.port;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.remoteAddress) {
      return this.remoteAddress.family;;
    }
  }
}

module.exports = UdpConnectionServer;
