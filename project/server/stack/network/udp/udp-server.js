
'use strict';

const NetworkServer = require('../network-server');
const Dgram = require('dgram');


class UdpServer extends NetworkServer {
  constructor(cbConnection) {
    super(cbConnection, 'udp');
    this.logicConnections = new Map();
    this.isOpen = false;
  }
  
  init(srvAddress, dstAddress, transportProperties, cbListen) {
    if('IPv4' === srvAddress.family) {
      this.serverSocket = Dgram.createSocket('udp4');
    }
    else if('IPv6' === srvAddress.family) {
      this.serverSocket = Dgram.createSocket('udp6');
    }
    else {
      process.nextTick(() => {
        return cbListen && cbListen(new Error(`No accepted IP family; '${srvAddress.family}'`));
      });
      return;
    }
    this.serverSocket.on('error', (err) => { // ??? Isn't this part of 'connection.attach'.
      cbListen(err);
    });
    this.serverSocket.bind({
      address: srvAddress.host,
      port: srvAddress.port
    }, () => {
      this.isOpen = true;
      cbListen();
    });
    this.serverSocket.on('message', (buffer, rinfo) => {
      const key = `${rinfo.address}:${rinfo.port}`;
      let connection = this.logicConnections.get(key);
      if(undefined === connection) {
        connection = this.cbConnection(this.serverSocket, {
          name: '',
          port: rinfo.port,
          host: rinfo.address,
          family: rinfo.family,
          type: 'client'
        });
        this.logicConnections.set(key, connection);
      }
      connection.onBuffer(buffer);
    });
    this.serverSocket.on('close', () => {
      this.isOpen = false;
    });
  }
  
  closed() {
    return !this.isOpen;
  }

  closeConnection(address) {
    const key = `${address.address}:${address.port}`;
    this.logicConnections.delete(key);
  }
  
  send(msg, address, done) {
    this.serverSocket.send(msg, address.port, address.host, (err) => {
      done(err);
    });
  }
}

module.exports = UdpServer;
