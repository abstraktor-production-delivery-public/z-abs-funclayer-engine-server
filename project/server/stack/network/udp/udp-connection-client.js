
'use strict';

const UdpConnection = require('./udp-connection');
const Dgram = require('dgram');


class UdpConnectionClient extends UdpConnection {
  constructor(srcAddress, dstAddress, transportProperties) {
    super(transportProperties, 'client');
    this.srcAddress = srcAddress;
    this.dstAddress = dstAddress;
    this.isOpen = false;
  }
  
  connect(done) {
    if('IPv4' === this.dstAddress.family) {
      this.socket = Dgram.createSocket('udp4');
    }
    else if('IPv6' === this.dstAddress.family) {
      this.socket = Dgram.createSocket('udp6');
    }
    else {
      return done && done(new Error(`No accepted IP family; '${this.dstAddress.family}'`));
    }
    this.socket.bind({
      address: this.srcAddress.host,
      port: this.srcAddress.port
    }, (err) => {
      if(err) {
        return done && done(err);
      }
      this.isOpen = true;
      this.socket.connect(this.dstAddress.port, this.dstAddress.host, (e) => {
        done && done(e);
      });
    });
    this.socket.on('message', (buffer, rinfo) => {
      /*if(rinfo.host != this.dstAddress.host || rinfo.port != this.dstAddress.port) {
        // TODO: addlog error here.
        return;
      }*/
      this.onBuffer(buffer);
    });
    this.socket.on('close', () => {
      this.isOpen = false;
    });
  }
  
  closed() {
    return !this.isOpen;
  }
  
  close(done) {
    if(null !== this.socket) {
      this.socket.close((err) => {
        // TODO: Log
        done(err);
      });
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.socket.send(msg, (err) => {
      done(err);
    });
  }
  
  getRemoteAddress() {
    if(null !== this.socket) {
      return this.socket.remoteAddress().address;
    }
  }
  
  getRemotePort() {
   if(null !== this.socket) {
      return this.socket.remoteAddress().port;
    }
  }
  
  getRemoteFamily() {
    if(null !== this.socket) {
      return this.socket.remoteAddress().family;
    }
  }
}

module.exports = UdpConnectionClient;
