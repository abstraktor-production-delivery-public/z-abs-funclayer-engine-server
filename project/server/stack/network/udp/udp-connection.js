
'use strict';

const NetworkConnection = require('../network-connection');


class UdpConnection extends NetworkConnection {
  constructor(transportProperties, name) {
    super(transportProperties, 'upd', name);
    this.socket = null;
  }
  
  getLocalAddress() {
    if(null !== this.socket) {
      return this.socket.address().address;
    }
  }
  
  getLocalPort() {
    if(null !== this.socket) {
      return this.socket.address().port;
    }
  }
  
  getLocalFamily() {
    if(null !== this.socket) {
      return this.socket.address().family;
    }
  }
}

module.exports = UdpConnection;
