
'use strict';

const UdpConnectionShared = require('./udp-connection-shared');


class UdpConnectionClientShared extends UdpConnectionShared {
  constructor(networkConnection, connectionName) {
    super(networkConnection, connectionName);
  }
  
  connect(done) {
    this.networkConnection.connect(done);
  }
  
  closed() {
    return this.networkConnection.closed();
  }
  
  close(done) {
    if(0 === this.unref()) {
      this.networkConnection.close(done);
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  sendMessage(msg, done) {
    this.networkConnection.close(msg, done);
  }
  
  getRemoteAddress() {
    return this.networkConnection.getRemoteAddress();
  }
  
  getRemotePort() {
    return this.networkConnection.getRemotePort();
  }
  
  getRemoteFamily() {
    return this.networkConnection.getRemoteFamily();
  }
}

module.exports = UdpConnectionClientShared;
