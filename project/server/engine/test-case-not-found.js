
'use strict';

const MessageTestCaseStarted = require('../communication/messages/messages-s-to-c/message-test-case-started');
const MessageTestCaseStopped = require('../communication/messages/messages-s-to-c/message-test-case-stopped');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class TestCaseNotFound {
  constructor(iterationTs, name, sutName, cbMessage) {
    this.index = -1;
    this.iterationTs = iterationTs;
    this.name = name;
    this.sutName = sutName;
    this.cbMessage = cbMessage;
  }
  
  run(cbDone) {
    const timestamp = process.hrtime.bigint();
    const msgTcStarted = new MessageTestCaseStarted(this.index, this.iterationTs, this.name, sutName, [], HighResolutionTimestamp.getHighResolutionDate(timestamp), false);
    this.cbMessage(msgTcStarted, null, this.debug);
    const msgTcStopped = new MessageTestCaseStopped(this.index, this.iterationTs, 0n, ActorResultConst.REFLECTION, ActorResultConst.NONE, ActorResultConst.NONE, ActorResultConst.NONE, true);
    this.cbMessage(msgTcStopped, null, this.debug);
    cbDone();
  }
}

module.exports = TestCaseNotFound;
