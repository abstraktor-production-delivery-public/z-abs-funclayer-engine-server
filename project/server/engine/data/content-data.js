
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathContent = require('z-abs-corelayer-server/server/path/actor-path-content');
const Fs = require('fs');


class ContentData {
  constructor() {
    this.pendings = 0;
    this.textContent = new Map();
    this.documentsContent = new Map();
    this.imageContent = new Map();
    this.audioContent = new Map();
    this.videoContent = new Map();
    this.otherContent = new Map();
  }
  
  // TODO: Handle multiple errors
  
 
  load(done) {
    this.pendings = 0;
    const repoPathGlobal = ActorPathContent.getContentGlobalFolder();
    const repoPathLocal = ActorPathContent.getContentLocalFolder();
    this._loadContent([
      {
        path: ActorPathData.getContentTextGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.textContent,
        type: 'text'
      },
      {
        path: ActorPathData.getContentDocumentsGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.documentsContent,
        type: 'documents'
      },
      {
        path: ActorPathData.getContentImageGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.imageContent,
        type: 'image'
      },
      {
        path: ActorPathData.getContentAudioGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.audioContent,
        type: 'audio'
      },
      {
        path: ActorPathData.getContentVideoGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.videoContent,
        type: 'video'
      },
      {
        path: ActorPathData.getContentOtherGlobalFile(),
        repoPath: repoPathGlobal,
        datas: this.otherContent,
        type: 'other'
      }
    ], (err) => {
      if(err) {
        return done(err);
      }
      this._loadContent([
        {
          path: ActorPathData.getContentTextLocalFile(),
          repoPath: repoPathLocal,
          datas: this.textContent,
          type: 'text'
        },
        {
          path: ActorPathData.getContentDocumentsLocalFile(),
          repoPath: repoPathLocal,
          datas: this.documentsContent,
          type: 'documents'
        },
        {
          path: ActorPathData.getContentImageLocalFile(),
          repoPath: repoPathLocal,
          datas: this.imageContent,
          type: 'image'
        },
        {
          path: ActorPathData.getContentAudioLocalFile(),
          repoPath: repoPathLocal,
          datas: this.audioContent,
          type: 'audio'
        },
        {
          path: ActorPathData.getContentVideoLocalFile(),
          repoPath: repoPathLocal,
          datas: this.videoContent,
          type: 'video'
        },
        {
          path: ActorPathData.getContentOtherLocalFile(),
          repoPath: repoPathLocal,
          datas: this.otherContent,
          type: 'other'
        }
      ], done);
    });
  }
  
  get(name, hint) {
    switch(hint) {
      case 'image': {
        const contentData = this.imageContent.get(name);
        if(undefined !== contentData) {
          return contentData;
        }
      }
      case 'audio': {
        const contentData = this.audioContent.get(name);
        if(undefined !== contentData) {
          return contentData;
        }
      }
    }
    let contentData = this.otherContent.get(name);
    if(undefined !== contentData) {
      return contentData;
    }
    contentData = this.imageContent.get(name);
    if(undefined !== contentData) {
      return contentData;
    }
    contentData = this.audioContent.get(name);
    if(undefined !== contentData) {
      return contentData;
    }
  }
  
  _loadContent(contentsToLoad, done) {
    let errorResponse = false;
    for(let i = 0; i < contentsToLoad.length; ++i) {
      ++this.pendings;
      Fs.readFile(contentsToLoad[i].path, (err, data) => {
        if(!errorResponse) {
          if(err) {
            errorResponse = true;
            done(err);
          }
          else {
            const contentDatas = JSON.parse(data);
            const type = contentsToLoad[i].type;
            const repoPath = contentsToLoad[i].repoPath;
            switch(type) {
              case 'text': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    encoding: contentData.encoding,
                    size: Number.parseInt(contentData.size),
                    description: contentData.description
                  });
                });
                break;
              }
              case 'documents': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    size: Number.parseInt(contentData.size),
                    description: contentData.description
                  });
                });
                break;
              }
              case 'image': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    size: Number.parseInt(contentData.size),
                    width: Number.parseInt(contentData.width),
                    height: Number.parseInt(contentData.height),
                    description: contentData.description
                  });
                });
                break;
              }
              case 'audio': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    size: Number.parseInt(contentData.size),
                    description: contentData.description
                  });
                });
                break;
              }
              case 'video': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    size: Number.parseInt(contentData.size),
                    width: Number.parseInt(contentData.width),
                    height: Number.parseInt(contentData.height),
                    description: contentData.description
                  });
                });
                break;
              }
              case 'other': {
                contentDatas.forEach((contentData) => {
                  contentsToLoad[i].datas.set(contentData.name, {
                    _repoPath: repoPath,
                    _type: type,
                    name: contentData.name,
                    path: contentData.path,
                    mime: contentData.mime,
                    size: Number.parseInt(contentData.size),
                    properties: contentData.properties,
                    description: contentData.description
                  });
                });
                break;
              }
            }
            if(0 === --this.pendings) {
              return done();
            }
          }
        }
      });
    }
  }
}

module.exports = ContentData;
