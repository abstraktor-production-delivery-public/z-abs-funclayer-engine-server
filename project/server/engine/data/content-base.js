
'use strict';


class ContentBase {
  constructor(buffers, readData) {
    this.buffers = undefined === buffers ? [] : (Array.isArray(buffers) ? buffers : [buffers]);
    this.readData = readData;
    this.currentBuffer = -1;
  }
  
  getBuffer() {
    if(!this.readData) {
      return null;
    }
    if(this.readData.allRead) {
      if(++this.currentBuffer < this.buffers.length) {
        return this.buffers[this.currentBuffer];
      }
      else {
        this.currentBuffer = -1;
        return null;
      }
    }
  }
  
  addBuffer(buffer) {
    this.buffers.push(buffer);
  }
  
  getLength() {
    if(!this.readData) {
      return 0;
    }
    else if(this.readData.allRead) {
      return this.buffers.reduce((acc, current) => {
        return acc + current.length;
      }, 0);
    }
    else {
      return -1;
    }
  }
  
  indexOf(value, start = 0) {
    let bufferIndex = -1;
    let index = start;
    let length = 0;
    let found = false;
    while(++bufferIndex < this.buffers.length) {
      if(start >= length) {
        index = start - length;
       // length = start;
        bufferIndex -= 1;
        found = true;
        break;
      }
      length += this.buffers[bufferIndex].length;
    }
    if(!found) {
      return -1;
    }
    while(++bufferIndex < this.buffers.length) {
      index = this.buffers[bufferIndex].indexOf(value, index);
      if(-1 !== index) {
        return length + index;
      }
      else {
        index = 0;
        length += this.buffers[bufferIndex].length;
      }
      // TODO: overlapp between two buffers.
    }
    return -1;
  }
  
  slice(start = 0, end = -1) {
    if(-1 === end) {
      end = this.getLength();
    }
    // TODO: support more than one buffer.
    return this.buffers[0].slice(start, end);
  }
  
  normalize() {
    this.currentBuffer = -1;
  }
}


module.exports = ContentBase;
