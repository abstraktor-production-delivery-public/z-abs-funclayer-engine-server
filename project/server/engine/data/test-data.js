
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');


class TestData {
  constructor(stagedSut, stagedSutInstance) {
    this.stagedSutStack = [[stagedSut, stagedSutInstance]];
    this.generalTestDataGlobal = new Map();
    this.generalTestDataLocal = new Map();
    this.systemTestDataGlobal = new Map();
    this.systemTestDataLocal = new Map();
    this.outputTestDataGlobal = new Map();
    this.outputTestDataLocal = new Map();
    this.environmentTestDataGlobal = new Map();
    this.environmentTestDataLocal = new Map();
    this.environmentTestDataStatic = new Map();
    this.overrideTestData = new Map();
    this.loaded = false;
  }
  
  pushStage(stagedSut, stagedSutInstance) {
    this.stagedSutStack.push([stagedSut, stagedSutInstance]);
    //const staged = this.stagedSutStack[this.stagedSutStack.length - 1];
  }
  
  popStage() {
    this.stagedSutStack.pop();
    //const staged = this.stagedSutStack[this.stagedSutStack.length - 1];
  }
  
  _getTestData(key, data) {
    for(let i = 0; i < this.stagedSutStack.length; ++i) {
      const staged = this.stagedSutStack[i];
      const stagedSut = staged[0];
      const stagedSutInstance = staged[1];
      if(stagedSut && stagedSutInstance) {
        const valueData = data.get(`${key}_${stagedSut}_${stagedSutInstance}`);
        if(undefined !== valueData) {
          return valueData;
        }
      }
    }
    for(let i = 0; i < this.stagedSutStack.length; ++i) {
      const staged = this.stagedSutStack[i];
      const stagedSut = staged[0];
      if(stagedSut) {
        const valueData = data.get(`${key}_${stagedSut}`);
        if(undefined !== valueData) {
          return valueData;
        }
      }
    }
    return data.get(key);
  }
  
  getTestData(key, cbGetTestDataTestCaseStatic) {
    let valueData = this._getTestData(key, this.generalTestDataLocal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.generalTestDataGlobal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.systemTestDataLocal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.systemTestDataGlobal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.outputTestDataLocal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.outputTestDataGlobal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.environmentTestDataLocal);
    if(undefined !== valueData) {
      return valueData;
    }
    valueData = this._getTestData(key, this.environmentTestDataGlobal);
    if(undefined !== valueData) {
      return valueData;
    }
    if(cbGetTestDataTestCaseStatic) {
      valueData = cbGetTestDataTestCaseStatic(key);
      if(undefined !== valueData) {
        return valueData;
      }
    }
    return this.environmentTestDataStatic.get(key);
  }
  
  getOverrideTestData(key) {
    return this.overrideTestData.get(key);
  }
  
  setOverrideTestData(key, value) {
    this.overrideTestData.set(key, value);
  }
  
  // TODO: Handle multiple errors
  
  load(done) {
    this._loadStaticTestData();
    this._loadDynamicTestData([
      {
        path: ActorPathData.getTestDataEnvironmentGlobalFile(),
        datas: this.environmentTestDataGlobal
      },
      {
        path: ActorPathData.getTestDataEnvironmentLocalFile(),
        datas: this.environmentTestDataLocal
      },
      {
        path: ActorPathData.getTestDataGeneralGlobalFile(),
        datas: this.generalTestDataGlobal
      },
      {
        path: ActorPathData.getTestDataGeneralLocalFile(),
        datas: this.generalTestDataLocal
      },
      {
        path: ActorPathData.getTestDataOutputGlobalFile(),
        datas: this.outputTestDataGlobal
      },
      {
        path: ActorPathData.getTestDataOutputLocalFile(),
        datas: this.outputTestDataLocal
      },
      {
        path: ActorPathData.getTestDataSystemGlobalFile(),
        datas: this.systemTestDataGlobal
      },
      {
        path: ActorPathData.getTestDataSystemLocalFile(),
        datas: this.systemTestDataLocal
      }
    ], done);
  }
  
  _loadStaticTestData() {
    this._addTestData('undefined', undefined, null, null, this.environmentTestDataStatic);
    this._addTestData('null', null, null, null, this.environmentTestDataStatic);
    this._addTestData('pipe', '|', null, null, this.environmentTestDataStatic);
    this._addTestData('space', ' ', null, null, this.environmentTestDataStatic);
  }
  
  _loadDynamicTestData(testDatasToLoad, done) {
    let pendings = 0;
    let errorResponse = false;
    for(let i = 0; i < testDatasToLoad.length; ++i) {
      ++pendings;
      Fs.readFile(testDatasToLoad[i].path, (err, data) => {
        if(!errorResponse) {
          if(err) {
            errorResponse = true;
            done(err);
          }
          else {
            const testDatas = JSON.parse(data);
            testDatas.forEach((testData) => {
              this._addTestData(testData.name, testData.value, testData.sut, testData.sutInstance, testDatasToLoad[i].datas);
            });
            if(0 === --pendings) {
              this.loaded = true;
              return done();
            }
          }
        }
      });
    }
  }
  
  _addTestData(key, value, sut, sutInstance, testDatas) {
    if(key.endsWith('[]')) {
      const arrayName = key.substr(0, key.length - 2);
      let valueData = this.testDatas.get(arrayName);
      if(undefined === valueData) {
        valueData = {
          type: 'array',
          value: []
        };
        if(!sut && !sutInstance) {
          testDatas.set(arrayName, valueData);
        }
        else if(sut && !sutInstance) {
          testDatas.set(`${arrayName}_${sut}`, valueData);
        }
        else if(sut && sutInstance) {
          testDatas.set(`${arrayName}_${sut}_${sutInstance}`, valueData);
        }
        else if(!sut && sutInstance) {
          ddb.error('Can not set global TestData for sutInstance without a sut defined.');
        }
      }
      valueData.value.push(value);
    }
    else {
      if(!sut && !sutInstance) {
        testDatas.set(key, {
          type: 'value',
          value: value
        });
      }
      else if(sut && !sutInstance) {
        testDatas.set(`${key}_${sut}`, {
          type: 'value',
          value: value
        });
      }
      else if(sut && sutInstance) {
        testDatas.set(`${key}_${sut}_${sutInstance}`, {
          type: 'value',
          value: value
        });
      }
      else if(!sut && sutInstance) {
        ddb.error('Can not set global TestData for sutInstance without a sut defined.');
      }
    }
  }
  
  addTestDataEmptyArray(key, testDatas) {
    testDatas.set(key, {
      type: 'array',
      value: []
    });
  }
}

module.exports = TestData;
