
'use strict';

const ContentBase = require('./content-base');


class ContentBinary extends ContentBase {
  constructor() {
    super(undefined, {
      allRead: true
    });
  }
}

module.exports = ContentBinary;
