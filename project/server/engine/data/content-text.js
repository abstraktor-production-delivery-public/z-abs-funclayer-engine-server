
'use strict';

const ContentBase = require('./content-base');


class ContentText extends ContentBase {
  constructor(contentData, buffers, readData) {
    super(buffers, readData);
    this.name = contentData.name;
    this.path = contentData.path;
    this.mime = contentData.mime;
    this.encoding = contentData.encoding;
    this.size = contentData.size;
    this.description = contentData.description;
  }
}

module.exports = ContentText;
