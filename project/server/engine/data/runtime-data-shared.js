
'use strict';


class RuntimeDataShared {
  constructor() {
    this.sharedDatas = new Map();
  }
  
  set(key, value) {
    const sharedDataObject = this.sharedDatas.get(key);
    if(sharedDataObject) {
      sharedDataObject.value = value;
      sharedDataObject.isSet = true;
      return sharedDataObject;
    }
    else {
      const newSharedDataObject = {
        value: value,
        isSet: true,
        pendings: []
      };
      this.sharedDatas.set(key, newSharedDataObject);
      return newSharedDataObject;
    }
  }
  
  wait(key, actor, expectedValue, cbGetPending) {
    const sharedDataObject = this.sharedDatas.get(key);
    if(sharedDataObject) {
      if(sharedDataObject.isSet && (expectedValue === sharedDataObject.value || undefined === expectedValue)) {
        return sharedDataObject.value;
      }
      else {
        const pending = cbGetPending();
        sharedDataObject.pendings.push({
          actor: actor,
          pendingId: pending.id,
          cancelObject: pending.cancelObject,
          expectedValue: expectedValue
        });
      }
    }
    else {
      const pending = cbGetPending();
      this.sharedDatas.set(key, {
        value: undefined,
        isSet: false,
        pendings: [{
          actor: actor,
          pendingId: pending.id,
          cancelObject: pending.cancelObject,
          expectedValue: expectedValue
        }]
      });
    }
    return null;
  }
  
  get(key) {
    return this.sharedDatas.get(key);
  }
  
  has(key) {
    return this.sharedDatas.has(key);
  }
  
  log() {
    this.sharedDatas.forEach((sharedData, key) => {
      ddb.info(`key: '${key}' - value: '${sharedData.value}' - pendings: ${shared.pendings.length}`);
    });
  }
}

module.exports = RuntimeDataShared;
