
'use strict';

const ContentBase = require('./content-base');


class ContentOther extends ContentBase {
  constructor(contentData, buffers, readData) {
    super(buffers, readData);
    this.name = contentData.name;
    this.path = contentData.path;
    this.mime = contentData.mime;
    this.size = contentData.size;
    this.properties = contentData.properties;
    this.description = contentData.description;
  }
}

module.exports = ContentOther;
