
'use strict';

const ContentBase = require('./content-base');


class ContentUndefined extends ContentBase {
  constructor() {
    super();
    this.size = 0;
  }
}

module.exports = ContentUndefined;
