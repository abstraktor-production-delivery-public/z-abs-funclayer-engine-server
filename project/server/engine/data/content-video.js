
'use strict';

const ContentBase = require('./content-base');


class ContentVideo extends ContentBase {
  constructor(contentData, buffers, readData) {
    super(buffers, readData);
    this.name = contentData.name;
    this.path = contentData.path;
    this.mime = contentData.mime;
    this.size = contentData.size;
    this.width = contentData.width;
    this.height = contentData.height;
    this.description = contentData.description;
  }
}

module.exports = ContentVideo;
