
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathContent = require('z-abs-corelayer-server/server/path/actor-path-content');
const Fs = require('fs');


class Content {
  constructor() {
    this.pendings = 0;
    this.imageContent = new Map();
  }
  
  // TODO: Handle multiple errors
  
  load(done) {
    this._loadGlobalContent([
      {
        path: ActorPathData.getContentImageGlobalFile(),
        datas: this.imageContent
      }
    ], done);
    this._loadGeneralContent(done);
  }
  
  _loadGlobalContent(contentsToLoad, done) {
    this.pendings = 0;
    let errorResponse = false;
    for(let i = 0; i < contentsToLoad.length; ++i) {
      ++this.pendings;
      Fs.readFile(contentsToLoad[i].path, (err, data) => {
        if(!errorResponse) {
          if(err) {
            errorResponse = true;
            done(err);
          }
          else {
            let contentDatas = JSON.parse(data);
            contentDatas.forEach((contentData) => {
              contentsToLoad[i].datas.set(contentData.name, {
                repoPath: ActorPathContent.getContentGlobalFolder(),
                value: contentData
              });
            });
            if(0 === --this.pendings) {
              return done();
            }
          }
        }
      });
    }
  }
  
  _loadGeneralGlobalContent(done) {
    
  }
}

module.exports = Content;
