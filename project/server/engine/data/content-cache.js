 
'use strict';

const ContentText = require('./content-text');
const ContentDocuments = require('./content-documents');
const ContentVideo = require('./content-video');
const ContentAudio = require('./content-audio');
const ContentImage = require('./content-image');
const ContentOther = require('./content-other');
const ActorPathContent = require('z-abs-corelayer-server/server/path/actor-path-content');
const Fs = require('fs');


class ContentCache {
  constructor(constantData) {
    this.constantData = constantData;
    this.contents = new Map();
  }
  
  *get(name, hint, actor) {
    const content = this.contents.get(name);
    if(undefined !== content) {
      return content;
    }
    return yield this._getContent(name, hint, actor);
  }
  
  _getContent(name, hint, actor) {
    const pendingId = actor.setPending();
    const contentDataC = this.constantData.get(name, hint);
    if(!contentDataC) {
      return actor.handlePendingId(pendingId, new Error(`Content with name: '${name}' does not exist.`));
    }
    Fs.readFile(ActorPathContent.getContentFile(contentDataC._repoPath, contentDataC.path), (err, buffer) => {
      if(err) {
        return actor.handlePendingId(pendingId, err);
      }
      const type = contentDataC._type;
      let content = null;
      if('text' === type) {
        content = new ContentText(contentDataC, buffer, {allRead: true});
      }
      else if('documents' === type) {
        content = new ContentDocuments(contentDataC, buffer, {allRead: true});
      }
      else if('video' === type) {
        content = new ContentVideo(contentDataC, buffer, {allRead: true});
      }
      else if('audio' === type) {
        content = new ContentAudio(contentDataC, buffer, {allRead: true});
      }
      else if('image' === type) {
        content = new ContentImage(contentDataC, buffer, {allRead: true});
      }
      else if('other' === type) {
        content = new ContentOther(contentDataC, buffer, {allRead: true});
      }
      else {
        return actor.handlePendingId(pendingId, new Error(`Content type: '${type}' does not exist.`));
      }
      this.contents.set(name, content);
      actor.handlePendingId(pendingId, err, content);
    });
  }
}

module.exports = ContentCache;
