
'use strict';

const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');


class ActorsPhase {
  constructor(phaseId) {
    this.phaseId = phaseId;
    this.phaseName = ActorPhaseConst.getName(phaseId);
    this.actors = [];
    this.notStartedActors = new Map();
  }
  
  add(actor) {
    this.actors.push(actor);
  }
  
  addFront(actor) {
    this.actors.splice(0, 0, actor);
  }
  
  setIndex() {
    this.actors.forEach((actor) => {
      this.notStartedActors.set(actor.getIndexFromPhase(this.phaseId), actor);
    });
  }
  
  getNextActors(actorDoneIndices) {
    const foundActors = [];
    this.notStartedActors.forEach((actor) => {
      const found = undefined !== actor.getDependencies(this.phaseId).find((dependentActorIndex) => {
        return undefined !== actorDoneIndices.find((actorDoneIndex) => {
          return actorDoneIndex === dependentActorIndex;
        });
      });
      if(found) {
        foundActors.push(actor);
      }
    });
    foundActors.forEach((actor) => {
      this.notStartedActors.delete(actor.getIndex());
    });
    return foundActors;
  }
}


module.exports = ActorsPhase;
