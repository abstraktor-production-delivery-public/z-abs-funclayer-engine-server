
'use strict';

const Addresses = require('../address/addresses');
const AddressCalculator = require('../address/address-calculator');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class StageSetter {
  constructor(executionContext, systemsUnderTestNames, systemsUnderTestNodes) {
    this.executionContext = executionContext;
    this.systemsUnderTestNames = systemsUnderTestNames;
    this.systemsUnderTestNodes = systemsUnderTestNodes;
    this.addressCalculator = new AddressCalculator();
    this.addresses = new Map();
    this.localDnses = new Map();
    this.queue = [];
    this.stack = [];
    this.supportInterceting = false;
    this.initialized = false;
  }
  
  push(name, stagedSut, stagedSutInstance) {
    this.supportInterceting = StackComponentsFactory.supportInterceting('puppeteer', 'client', this.executionContext.testData);
    this.executionContext.pushStage(name, stagedSut, stagedSutInstance);
    const success = this._set(name, stagedSut, stagedSutInstance);
    this.init();
    return success;
  }
  
  change(name, stagedSut, stagedSutInstance) {
    this.supportInterceting = StackComponentsFactory.supportInterceting('puppeteer', 'client', this.executionContext.testData);
    this.executionContext.changeStage(stagedSut, stagedSutInstance);
    const success = this._set(name, stagedSut, stagedSutInstance);
    this.init();
    return success;
  }
  
  pop() {
    this.supportInterceting = StackComponentsFactory.supportInterceting('puppeteer', 'client', this.executionContext.testData);
    this.executionContext.popStage();
    this.init();
  }
  
  _set(name, stagedSut, stagedSutInstance) {
    const sutIndex = this.systemsUnderTestNames.indexOf(stagedSut);
    if(-1 === sutIndex) {
      ddb.error(`Stage SUT '${stagedSut}', does not exist.`);
      return false;
    }
    // TODO: Same check in instances.
    const addresses = this.addresses.get(name);
    const localDns = this.localDnses.get(name);
    if(!addresses)  {
      ddb.error(`Addresses for SUT '${stagedSut}', does not exist.`);
      return false;
    }
    else {
      this.executionContext.setStage(name, stagedSut, stagedSutInstance, this.systemsUnderTestNodes[sutIndex], addresses, localDns);
      this.supportInterceting = StackComponentsFactory.supportInterceting('puppeteer', 'client', this.executionContext.testData);
      return true;
    }
  }
  
  init() {
    this.executionContext.executionData.id = GuidGenerator.create();
    StackComponentsFactory.initExecution(this.executionContext.executionData.id, this.executionContext.debugDashboard);
  }
  
  exit(done) {
    this.executionContext.exitTestSuite(() => {
      StackComponentsFactory.exitExecution(this.executionContext.executionData.id);
      done();
    });
  }
  
  setSupportInterceting(supportInterceting) {
    this.supportInterceting = supportInterceting;
  }
  
  calculateStages(done) {
    this.addressCalculator.calculateInit((err, results) => {
      this.queue.forEach((q) => {
        const stage = this.addressCalculator.calculate(...q.stageData);
        const addresses = new Addresses();
        Addresses.set(stage.addresses.srcs, addresses.srcs);
        Addresses.set(stage.addresses.dsts, addresses.sutSrvs);
        Addresses.set(stage.addresses.srvs, addresses.srvs);
        this.addresses.set(q.name, addresses);
        this.localDnses.set(q.name, stage.dns);
      });
      this.initialized = true;
      done();
    });
  }
  
  calculateStage(abstraction) {
    const data = {
      name: abstraction.name,
      stageData: this._parseStage(abstraction.name)
    };
    if(!this.addresses.has(abstraction.name)) {
      if(this.initialized) {
        const stage = this.addressCalculator.calculate(...data.stageData);
        const addresses = new Addresses();
        Addresses.set(stage.addresses.srcs, addresses.srcs);
        Addresses.set(stage.addresses.dsts, addresses.sutSrvs);
        Addresses.set(stage.addresses.srvs, addresses.srvs);
        this.addresses.set(abstraction.name, addresses);
        this.localDnses.set(abstraction.name, stage.dns);
      }
      else {
        this.queue.push(data);
      }
    }
    return data.stageData;
  }
  
  _parseStage(name) {
    try {
      return JSON.parse(name.substring(5));  // 'stage' length
    }
    catch(err) {
      return ['', '', '', false];
    }
  }
}


module.exports = StageSetter;
