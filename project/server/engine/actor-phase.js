
'use strict';

// TODO: remove
class ActorPhase {
  constructor(tabNames) {
    this.PRE = 0;
    this.EXEC = 1;
    this.POST = 2;
    this.NAME_PRE = 'pre';
    this.NAME_EXEC = 'exec';
    this.NAME_POST = 'post';
    this.phaseIds = new Map([[this.NAME_PRE, this.PRE], [this.NAME_EXEC, this.EXEC], [this.NAME_POST, this.POST], ['pre, post', this.PRE]]);
    this.phaseNames = [this.NAME_PRE, this.NAME_EXEC, this.NAME_POST];
  }
  
  getName(phaseId) {
    return this.phaseNames[phaseId];
  }
  
  getId(phaseName) {
    return this.phaseIds.get(phaseName);
  }
}

module.exports = new ActorPhase();
