
'use strict';

const TestOutput = require('./test-output');
const ActorStateMachineState = require('./actor-state-machine-state');
const ActorStateMachineProcess = require('./actor-state-machine-process');
const ActorState = require('./actor-state');
const MessageTestCaseState = require('../communication/messages/messages-s-to-c/message-test-case-state');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const ActorStateConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');
const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');


class ActorStateMachineBase {
  constructor(tc, executionContext, cbMessage, actorsPhase, actorPhaseId, debug) {
    this.tc = tc;
    this.executionContext = executionContext;
    this.cbMessage = cbMessage;
    this.cbResult = null;
    this.actorsPhase = actorsPhase;
    this.actorPhaseId = actorPhaseId;
    this.nbrOfActors = this.actorsPhase.actors.length;
    this.iterationsTc = 0;
    this.nbrOfExpectInterrupt = 0; 
    //this.callId = 0;
    this.process = !debug ? process : new ActorStateMachineProcess();
    this.actorsPhase.actors.forEach((actor) => {
      if(actor.expectInterrupt) {
        ++this.nbrOfExpectInterrupt;
      }
       actor.executeInit(debug, this.process);
    });
    this._init();
  }
  
  _init() {
    this.actorStateMachineState = new ActorStateMachineState(this.actorPhaseId);
    this.actorsRunning = new Map();
    this.actorsPending = new Map();
    this.actorsComming = new Map();
    this.actorsInitServerPending = [];
    this.actorsDoneIndices = [];
    this.termOrProxyActorsInInitServer = 0;
    this.nbrOfInit = 0;
    this.nbrOfDone = 0;
  }
  
  data(cbResult) {
    this.cbResult = cbResult;
    const actors = this.actorsPhase.actors;
    actors.forEach((actor) => {
      actor.setCurrentPhase(this.actorPhaseId);
      actor.setCbActorStateDone(this.dataActorStateDone.bind(this));
    });
    this._state();
  }
  
  dataActorStateDone(actor) {
    this._sendTestCaseState(actor);
    this.actorsRunning.delete(actor.getIndex());
    this.actorsPending.set(actor.getIndex(), actor);
    this._state();
  }
  
  run(resultId, cbResult) {
    this.cbResult = cbResult;
    if(ActorResultConst.SUCCESS < resultId) {
      this.actorStateMachineState.set(ActorStateMachineState.ERROR);
    }
    const actors = this.actorsPhase.actors;
    actors.forEach((actor) => {
      actor.setCurrentPhase(this.actorPhaseId);
      actor.setCbActorStateDone(this.runActorStateDone.bind(this));
    });
    this._state();
  }
  
  serverStarted(actor) {
    const actorTypeId = actor.typeId;
    if(ActorTypeConst.TERM === actorTypeId || ActorTypeConst.INTER === actorTypeId || ActorTypeConst.PROXY === actorTypeId || ActorTypeConst.SUT === actorTypeId) {
      if(0 === --this.termOrProxyActorsInInitServer) {
        const existingPendings = [];
        this.actorsPending.forEach((actor) => {
          existingPendings.push(actor);
        });
        this.actorsPending.clear();
        this.actorsInitServerPending.forEach((actor) => {
          this.actorsPending.set(actor.getIndex(), actor);
        });
        existingPendings.forEach((actor) => {
          this.actorsPending.set(actor.getIndex(), actor);
        });
        this.actorsInitServerPending = [];
      }
    }
  }
  
  runActorStateDone(actor) {
    this._sendTestCaseState(actor);
    this.actorsRunning.delete(actor.getIndex());
    let ready = false;
    if(ActorState.EXIT !== actor.getStateId()) {
      this.actorsPending.set(actor.getIndex(), actor);
    }
    else {
      this.actorsDoneIndices.push(actor.getIndex());
      ready = true;
    }
    if(ActorState.INIT_SERVER === actor.getStateId()) {
      this.serverStarted(actor);
    }
    if(ActorResultConst.SUCCESS < actor.getResultId() && ActorResultConst.INTERRUPT !== actor.getResultId()) {
      if(ActorStateMachineState.ERROR !== this.actorStateMachineState.get()) {
        this.actorStateMachineState.set(ActorStateMachineState.ERROR);
        this._interrupt();
      }
    }
    this._state(ready);
  }
  
  stop(done) {
    if(this.cbResult) {
      const cbResult = this.cbResult;
      this.cbResult = (dataResultId) => {
        cbResult(dataResultId);
        done();
      };
      this._interrupt();
    }
    else {
      this.process.nextTick(done);
    }
  }
  
  _state(done = false) {
    this.process.nextTick((done) => {
      switch(this.actorStateMachineState.get()) {
        case ActorStateMachineState.RUN:
        case ActorStateMachineState.ERROR:
          this._stateRun(done);
          break;
        case ActorStateMachineState.DATA:
          this._stateData();
          break;
        case ActorStateMachineState.INIT:
          this._stateInit();
          break;
        case ActorStateMachineState.DONE:
          this._stateDone();
          break;
      }
    }, done);
  }
  
  _stateData() {
    const actors = this.actorsPhase.actors;
    actors.forEach((actor) => {
      this._stateRunActor(actor);
    });
    this.actorStateMachineState.set(ActorStateMachineState.INIT);
  }
   
  _stateInit() {
    if(this.nbrOfActors === ++this.nbrOfInit) {
      this.actorStateMachineState.set(ActorStateMachineState.RUN);
      const actorsRun = this.actorsPhase.getNextActors([-1]);
      const actorsInit = this.actorsPhase.actors;
      actorsInit.forEach((actor) => {
        this.actorsComming.set(actor.getIndex(), actor);
      });
      this.actorsPending = new Map();
      actorsRun.forEach((newActor) => {
        const actorTypeId = newActor.typeId;
        if(ActorTypeConst.TERM === actorTypeId || ActorTypeConst.INTER === actorTypeId || ActorTypeConst.PROXY === actorTypeId || ActorTypeConst.SUT === actorTypeId) {
          ++this.termOrProxyActorsInInitServer;
        }
        this.actorsComming.delete(newActor.getIndex());
        this.actorsPending.set(newActor.getIndex(), newActor);
      });
      this.process.nextTick(() => {
        this.cbResult(this._calculateDataResultId());
      });
    }
  }
  
  _stateRun(done) {
    this.actorsPending.forEach((actor) => {
      this._stateRunActor(actor);
    })
    const actorsRun = this.actorsPhase.getNextActors(this.actorsDoneIndices);
    actorsRun.forEach((newActor) => {
      const actorTypeId = newActor.typeId;
      if(ActorTypeConst.TERM === actorTypeId || ActorTypeConst.INTER === actorTypeId || ActorTypeConst.PROXY === actorTypeId || ActorTypeConst.SUT === actorTypeId) {
        ++this.termOrProxyActorsInInitServer;
      }
      this.actorsComming.delete(newActor.getIndex());
      this.actorsPending.set(newActor.getIndex(), newActor);
      this._stateRunActor(newActor);
    });
    if(done) {
      if(this.nbrOfActors === ++this.nbrOfDone) {
        this.actorStateMachineState.set(ActorStateMachineState.DONE);
        this._state();
      }
      if(this.nbrOfActors - this.nbrOfExpectInterrupt === this.nbrOfDone) {
        for(let i = 0; i < this.actorsPhase.actors.length; ++i) {
          const actor = this.actorsPhase.actors[i];
          if(actor.expectInterrupt && ActorState.RUN === actor.getStateId()) { // TODO: if this fails we have to do it when the actor comes to RUN state.
            actor.doExpectedInterrupt();
          }
        }
      }
    }
  }
  
  _stateRunActor(actor) {
    this.actorsPending.delete(actor.getIndex());
    this.process.nextTick((actor) => {
      const actorStateMachineState = this.actorStateMachineState.get();
      if(ActorStateMachineState.RUN === actorStateMachineState || ActorStateMachineState.INIT === actorStateMachineState) {
        this._stateRunActorSuccess(actor);
      }
      else if(ActorStateMachineState.ERROR === actorStateMachineState) {
        this._stateRunActorError(actor);
      }
    }, actor);
  }
  
  _stateRunActorSuccess(actor) {
    actor.logDebug(() => `actor state: ${ActorStateConst.getName(actor.getStateId())} => success`, 'state-machine', '892a9473-61dc-48f2-ae23-afca4864f6be');
    switch(actor.getStateId()) {
      case ActorState.NONE:
        switch(actor.typeId) {
          case ActorTypeConst.ORIG:
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.LOCAL:
            actor.getState().setData();
            if(!actor.doData()) {
              actor.getCbActorStateDone()(actor);
            }
            break;
          case ActorTypeConst.COND:
            if(ActorPhaseConst.PRE === this.actorPhaseId) {
              actor.getState().setData();
              if(!actor.doData()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.NA);
            }
            break;
        }
        break;
      case ActorState.DATA:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
            actor.executionContext.serverManager.register(actor);
            actor.getState().setInitServer();
            if(!actor.doInitServer()) {
               actor.getCbActorStateDone()(actor);
            }
            break;
          case ActorTypeConst.ORIG:
          case ActorTypeConst.COND:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
          }
        break;
      case ActorState.INIT_SERVER:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.ORIG:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.COND:
            if(0 === this.termOrProxyActorsInInitServer) {
              actor.executionContext.clientManager.register(actor);
              actor.getState().setInitClient();
              if(!actor.doInitClient()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this.actorsPending.delete(actor.getIndex());
              this.actorsInitServerPending.push(actor);
            }
            break;
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
      case ActorState.INIT_CLIENT:
        this.actorsRunning.set(actor.getIndex(), actor);
        actor.getState().setRun();
        if(!actor.doRun()) {
          actor.getCbActorStateDone()(actor);
        }
        break;
      case ActorState.RUN:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.ORIG:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.COND:
            actor.getState().setExit();
            if(!actor.doExit()) {
              actor.getCbActorStateDone()(actor);
            }
            break;
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
    }
  }
  
  _stateDone() {
    let pendings = 2;
    this.executionContext.clientManager.close(() => {
      if(0 === --pendings) {
        const cbResult = this.cbResult;
        this.cbResult = null;
        cbResult(this._calculateResult());
      }
    });
    this.executionContext.serverManager.close(() => {
      if(0 === --pendings) {
        const cbResult = this.cbResult;
        this.cbResult = null;
        cbResult(this._calculateResult());
      }
    });
  }
  
  _nextState(actor, resultId) {
    actor.getState().setNext();
    actor._setActorResult(resultId);
    this._sendTestCaseState(actor);
    this.actorsRunning.delete(actor.getIndex());
    let ready = false;
    if(ActorState.EXIT !== actor.getStateId()) {
      this.actorsPending.set(actor.getIndex(), actor);
    }
    else {
      this.actorsDoneIndices.push(actor.getIndex());
      ready = true;
    }
    this._state(ready);
  }
  
  _sendTestCaseState(actor) {
    if(0 === this.executionContext.type || TestOutput.LOG_DETAIL_TC & this.executionContext.outputs.chosenClient) {
      const msg = new MessageTestCaseState(actor.columnIndex, actor.getOrderIndex(), actor.getStateId(), actor.getResultId());
      this.cbMessage(msg, null, actor.debug);
    }
  }
  
  _calculateDataResultId() {
    let actorResultId = ActorResultConst.NA;
    const actors = this.actorsPhase.actors;
    actors.forEach((actor) => {
      actorResultId = Math.max(actorResultId, actor.getStateResultId(ActorState.DATA));
    });
    return actorResultId;
  }
  
  _calculateResult() { // TODO: use ActorResults ???
    let actorResultId = ActorResultConst.NA;
    const actors = this.actorsPhase.actors;
    actors.forEach((actor) => {
      actorResultId = Math.max(actorResultId, actor.getActorResultId());
    });
    return actorResultId;
  }
}

module.exports = ActorStateMachineBase;
