
'use strict';

const AddressDefaultManager = require('../address/address-default-manager');
const Addresses = require('../address/addresses');
const ClientManager = require('../stack/managers/client-manager/client-manager');
const ConnectionManager = require('../stack/managers/connection-manager/connection-manager');
const ContentCache = require('./data/content-cache');
const ContentData = require('./data/content-data');
const DebugDashboard = require('../debug-dashboard/debug-dashboard');
const DnsUrlCache = require('../address/dns-url-cache');
const RuntimeDataShared = require('./data/runtime-data-shared');
const SharedManager = require('../stack/managers/shared-manager');
const StackContentCache = require('../stack/stacks/stack-content-cache');
const ServerManager = require('../stack/managers/server-manager/server-manager');
const TestCaseLoad = require('./test-case-load');
const TestData = require('./data/test-data');
const TestOutput = require('./test-output');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ExecutionContext {
  constructor(type, debug) {
    this.type = type;
    this.debug = debug;
    this.config = null;
    this.executionData = {
      id: GuidGenerator.create()
    };
    this.contentData = new ContentData();
    this.contentCache = new ContentCache(this.contentData);
    this.stackContentCache = new StackContentCache();
    this.testData = null;
    this.dnsUrlCache = new DnsUrlCache();
    this.testCaseLoad = new TestCaseLoad();
    this.addresses = new Addresses();
    this.addressDefaultManager = new AddressDefaultManager(this.addresses);
    this.connectionManager = new ConnectionManager(this);
    this.sharedManager = new SharedManager();
    this.clientManager = new ClientManager(this);
    this.serverManager = new ServerManager(this);
    this.sharedRuntimeData = new RuntimeDataShared();
    this.logCache = new Map();
    this.stageStack = [];
    this.outputs = new TestOutput((key) => {
      const testData = this.testData.getTestData(key, (key) => {});
      if(testData) {
        return testData.value;
      }
      else {
        return 'none';
      }
    });
    this.testCaseStatistics = null;
    this.debugDashboard = new DebugDashboard();
    this.nodes = [];
    this.stageData = {
      name: '',
      stagedSut: '',
      stagedSutInstance: '',
      stagedSutNodes: []
    };
  }
  
  initTestCase(tc) {
    this.nodes = tc.nodes;
    this.connectionManager.init();
  }
  
  setTestData(stagedSut, stagedSutInstance) {
    this.testData = new TestData(stagedSut, stagedSutInstance);
  }
  
  setStageData(name, stagedSut, stagedSutInstance, stagedSutNodes) {
    this.stageData.name = name;
    this.stageData.stagedSut = stagedSut;
    this.stageData.stagedSutInstance = stagedSutInstance;
    this.stageData.stagedSutNodes = stagedSutNodes;
  }
  
  setNodes(nodes) {
    this.nodes = nodes;
  }
}


module.exports = ExecutionContext;
