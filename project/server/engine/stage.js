
'use strict';

const TestOutput = require('./test-output');
const MessageTestStageStarted = require('../communication/messages/messages-s-to-c/message-test-stage-started');
const MessageTestStageStopped = require('../communication/messages/messages-s-to-c/message-test-stage-stopped');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class Stage {
  static INIT = 0;
  static EXIT = 1;
  static INIT_PUSH = 0;
  static INIT_CHANGE = 1;
  static EXIT_CHANGE = 0;
  static EXIT_POP = 1;
  
  constructor(name, type, subType, stageSut, stageSutInstance, outputs, cbMessage) {
    this.name = name;
    this.type = type;
    this.stageSut = stageSut;
    this.stageSutInstance = stageSutInstance;
    this.outputs = outputs;
    this.index = -1;
    this.iterationTs = 1;
    this.key = '';
    this.timestamp = null;
    this.cbMessage = cbMessage;
    this.cbInit = null;
    this.cbExit = null;
    this.subType = subType;
    this.noExecutionResultId = ActorResultConst.NONE;
    this.debug = false;
  }
  
  setCbs(cbInit, cbExit) {
    this.cbInit = cbInit;
    this.cbExit = cbExit;
  }
  
  run(cbDone) {
    this.timestamp = process.hrtime.bigint();
    const msgStarted = new MessageTestStageStarted(this.index, this.type, this.iterationTs, HighResolutionTimestamp.getHighResolutionDate(this.timestamp), true);
    this.cbMessage(msgStarted, null, this.debug);
    if(Stage.INIT === this.type) {
      this.cbInit(this.name, this.stageSut, this.stageSutInstance, this.subType, (success) => {
        const timestamp = process.hrtime.bigint();
        const resultId = success ? ActorResultConst.SUCCESS : ActorResultConst.FAILURE;
        const msgStopped = new MessageTestStageStopped(this.index, this.type, this.iterationTs, timestamp - this.timestamp, resultId)
        this.cbMessage(msgStopped, null, this.debug);
        if((TestOutput.LOG_CONSOLE & this.outputs.chosen) && (TestOutput.LOG_DETAIL_TC & this.outputs.chosenConsole)) {
          const indexText = (this.index + 1).toString().padStart(4);
          const iterationTsText =  this.iterationTs.toString().padStart(3);
          ddb.print(ddb.yellow(indexText), ddb.yellow(iterationTsText), ActorResultConst.RESULTS_CONSOLE[resultId], this.name);
        }
        cbDone(resultId);
      });
    }
    else {
      this.cbExit(this.name, this.stageSut, this.stageSutInstance, this.subType, (success) => {
        const timestamp = process.hrtime.bigint();
        const resultId = success ? ActorResultConst.SUCCESS : ActorResultConst.FAILURE;
        const msgStopped = new MessageTestStageStopped(this.index, this.type, this.iterationTs, timestamp - this.timestamp, resultId);
        this.cbMessage(msgStopped, null, this.debug);
        cbDone(resultId);
      });
    }
  }
  
  runCommentOut(cbDone) {
    this.timestampStart = process.hrtime.bigint();
    const msgStarted = new MessageTestStageStarted(this.index, this.type, this.iterationTs, HighResolutionTimestamp.getHighResolutionDate(this.timestampStart), false);
    this.cbMessage(msgStarted, null, this.debug);
    const timestampStop = process.hrtime.bigint();
    const msgStopped = new MessageTestStageStopped(this.index, this.type, this.iterationTs, timestampStop - this.timestampStart, ActorResultConst.NONE);
    this.cbMessage(msgStopped, null, this.debug);
    process.nextTick(cbDone, this.noExecutionResultId);
  }
}


module.exports = Stage;
