
'use strict';

const ActorStateConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const');


class ActorState {
  constructor(actor) {
    this.actor = actor;
    this.currentActorStateId = ActorState.NONE;
  }
  
  set(actorStateId) {
    this.currentActorStateId = actorStateId;
  }
  
  setNext() {
    if(ActorState.NONE !== this.currentActorStateId) {
      this.currentActorStateId += 1;
    }
    else {
      this.currentActorStateId = ActorState.DATA;
    }
  }
  
  getActorStateId() {
    return this.currentActorStateId;
  }

  getName() {
    return ActorState.actorStateNames[this.currentActorStateId];
  }
  
  getNameFromState(actorState) {
    return ActorState.actorStateNames[actorState];
  }
  
  setData() {
    this.set(ActorState.DATA);
  }
  
  setInitServer() {
    this.set(ActorState.INIT_SERVER);
  }
  
  setInitClient() {
    this.set(ActorState.INIT_CLIENT);
  }
  
  setRun() {
    this.set(ActorState.RUN);
  }
  
  setExit() {
    this.set(ActorState.EXIT);
  }
}

ActorState.actorStateNames = [
  'data',
  'initServer',
  'initClient',
  'run',
  'exit',
  'none'
];

ActorState.DATA = 0;
ActorState.INIT_SERVER = 1;
ActorState.INIT_CLIENT = 2;
ActorState.RUN = 3;
ActorState.EXIT = 4;
ActorState.NONE = 5;

module.exports = ActorState;
