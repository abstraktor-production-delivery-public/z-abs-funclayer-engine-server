
'use strict';

const Logger = require('z-abs-corelayer-server/server/log/logger');


class ActorStateMachineState {
  constructor(actorPhaseId) {
    this.actorPhaseId = actorPhaseId;
    this.previusState = ActorStateMachineState.NONE;
    this.currentState = ActorStateMachineState.DATA;
  }
  
  set(state) {
    LOG_ENGINE(Logger.ENGINE.AS, `Actor State Machine State[${this.actorPhaseId}]: [${this.getName()} => ${this.getNameFromState(state)}]`);
    this.previusState = this.currentState;
    this.currentState = state;
  }
  
  get() {
    return this.currentState;
  }
  
  getName() {
    return ActorStateMachineState.actorStateMachineStateNames[this.currentState];
  }
  
  getNameFromState(state) {
    return ActorStateMachineState.actorStateMachineStateNames[state];
  }
}

ActorStateMachineState.actorStateMachineStateNames = [
  'none',
  'data',
  'init',
  'run',
  'error',
  'done'
];

ActorStateMachineState.NONE = 0;
ActorStateMachineState.DATA = 1;
ActorStateMachineState.INIT = 2;
ActorStateMachineState.RUN = 3;
ActorStateMachineState.ERROR = 4;
ActorStateMachineState.DONE = 5;

module.exports = ActorStateMachineState;
