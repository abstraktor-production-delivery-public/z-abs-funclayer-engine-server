
'use strict';


class ActorStateMachineProcess {
  constructor() {
    this.stalled = false;
    this.stallQueue = [];
    this.timeouts = new Map();
  }
  
  stall() {
    this.stalled = true;
  }
  
  unstall() {
    this.stalled = false;
    while(0 !== this.stallQueue.length) {
      const stalledObject = this.stallQueue.shift();
      if(!stalledObject.timeout) {
        this.nextTick(stalledObject.cb , ...stalledObject.args);
      }
      else {
        const t = this.timeouts.get(stalledObject.timeout);
        this.nextTick((...args) => {
          this.timeouts.delete(stalledObject.timeout);
          if(!t.timedOut) {
            stalledObject.cb(...args);
          }
        }, ...stalledObject.args);
      }
    }
  }
  
  nextTick(cb , ...args) {
    process.nextTick(() => {
      if(!this.stalled) {
        cb(...args);
      }
      else {
        this.stallQueue.push({
          cb: cb,
          args: args,
          timeout: null
        });
      }
    });
  }

  setTimeout(cb, delay, ...args) {
    const timeout = setTimeout(() => {
      if(!this.stalled) {
        this.timeouts.delete(timeout);
        cb(...args);
      }
      else {
        this.timeouts.set(timeout, {
          queued: true
        });
        this.stallQueue.push({
          cb: cb,
          args: args,
          timeout: timeout
        });
      }
    }, delay);
    this.timeouts.set(timeout, {
      queued: false,
      timedOut: false
    });
    return timeout;
  }

  clearTimeout(timeout) {
    clearTimeout(timeout);
    const t = this.timeouts.get(timeout);
    if(!t.queued) {
      this.timeouts.delete(timeout);
    }
    else {
      t.timedOut = true;
    }
  }
}


module.exports = ActorStateMachineProcess;
