
'use strict';


const MessageExecutionStared = require('../communication/messages/messages-s-to-c/message-execution-started');
const MessageTestCaseStarted = require('../communication/messages/messages-s-to-c/message-test-case-started');
const MessageTestCaseStopped = require('../communication/messages/messages-s-to-c/message-test-case-stopped');
const ActorStateMachineExec = require('./actor-state-machine-exec');
const ActorStateMachinePre = require('./actor-state-machine-pre');
const ActorStateMachinePost = require('./actor-state-machine-post');
const Actors = require('./actors');
const RuntimeDataShared = require('./data/runtime-data-shared');
const TestCaseLoad = require('./test-case-load');
const TestOutput = require('./test-output');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
//const LogInner = require('z-abs-funclayer-engine-cs/clientServer/log/log-inner'); // TODO: Remove ???
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');


class TestCase {
  static RUN_NONE = 0;
  static RUN_DATA_PRE = 1;
  static RUN_DATA_EXEC = 2;
  static RUN_DATA_PRE_EXEC = 3;
  static RUN_DATA_POST = 4;
  static RUN_DATA_PRE_POST = 5;
  static RUN_DATA_EXEC_POST = 6;
  static RUN_DATA_PRE_EXEC_POST = 7;
  
  static NOT_RUNNING_IN_TS = -1;
  
  constructor(tc, nameParams, iterationTs, debug, executionContext, cbMessage) {
    this.nameParams = nameParams;
    this.tc = tc;
    this.index = -1;
    this.iterationTs = iterationTs;
    this.kindOfRun = 0;
    this.key = '';
    this.debug = debug;
    this.slowAwaitTime = {p: !executionContext.config.slow ? -1 : 250};
    this.cbDone = null;
    this.actors = null;
    this.nodes = null;
    this.executionContext = executionContext;
    this.testCaseLoad = executionContext.testCaseLoad;
    this.tcData = {
      testDatasTestCase: new Map(),
      testDatasTestCaseStatic: new Map(),
      testDatasActor: new Map(),
      verificationsTestCase: new Map(),
      sharedRuntimeData: new RuntimeDataShared(),
    };
    this.cbMessage = cbMessage;
    this.timestamp = null;
       
    this.iterationTc = 1;
    this.actorStateMachinePre = null;
    this.actorStateMachineExec = null;
    this.actorStateMachinePost = null;
    
    this.noExecutionResultId = ActorResultConst.NONE;
    this.TcDataInitialized = false;
  }
  
  getName() {
    return this.tc.name;
  }
  
  containsInterceptor() {
    const actors = this.tc.tc.actors;
    for(let i = 0; i < actors.length; ++i) {
      if('inter' === actors[i].type) {
        return true;
      }
    }
    return false;
  }
  
  load(cbResult) {
    // TODO: restore instead of create.
    this.actorStateMachinePre = null;
    this.actorStateMachineExec = null;
    this.actorStateMachinePost = null;
    this.tcData.sharedRuntimeData = new RuntimeDataShared();
    process.nextTick(() => {
      this.kindOfRun = 0;
      this.actors = new Actors(this.tc.tc, this.executionContext, this.tcData, this.debug, this.executionContext.config, this.slowAwaitTime, this.cbMessage);
      this.actors.loadActors((result) => {
        if(result) {
          if(!this.TcDataInitialized) {
            this.tcData.testDatasActor.set('pre', new Map());
            this.tcData.testDatasActor.set('exec', new Map());
            this.tcData.testDatasActor.set('post', new Map());
            this._setTestDataTestCases(this.tc.tc.testDataTestCases);
            this._setTestDataIteration(this.tc.tc.testDataIteration);
            this._setVerificationTestCases(this.tc.tc.verificationTestCases);
            const tcStaticData = this.getTestDataEngineBoolean('tc__staticData', false);
            if(tcStaticData) {
              this._setTestDatasTestCaseStatic(this.actors.preActors.actors, this.actors.execActors.actors, this.actors.postActors.actors);
            }
            this.TcDataInitialized = true;
          }
          this.nodes = this.actors.nodes;
          if(0 !== this.actors.preActors.actors.length) {
            this.actorStateMachinePre = new ActorStateMachinePre(this.tc, this.executionContext, this.cbMessage, this.actors.preActors, this.debug);
            this.kindOfRun += 1;
          }
          if(0 !== this.actors.execActors.actors.length) {
            this.actorStateMachineExec = new ActorStateMachineExec(this.tc, this.executionContext, this.cbMessage, this.actors.execActors, this.debug);
            this.kindOfRun += 2;
          }
          if(0 !== this.actors.postActors.actors.length) {
            this.actorStateMachinePost = new ActorStateMachinePost(this.tc, this.executionContext, this.cbMessage, this.actors.postActors, this.debug);
            this.kindOfRun += 4;
          }
          if(-1 !== this.slowAwaitTime.p) {
            const slowAwaitTime = this.getTestDataEngineInteger('tc__SlowTime', this.slowAwaitTime);
            this.slowAwaitTime.p = slowAwaitTime;
          }
          cbResult();
        }
        else {
          cbResult(new Error('Error loading the Actors.'));
        }
      });
    });
  }
  
  _restoreActors(cbResult) {
    this.load(cbResult);
  }
  
  _run() {
    //this.actors.executeInit();
    this.timestamp = process.hrtime.bigint();
    const msg = new MessageTestCaseStarted(this.index, this.iterationTs, this.tc.name, this.nameParams[0], this.actors.nodes, HighResolutionTimestamp.getHighResolutionDate(this.timestamp), true);
    this.cbMessage(msg, null, this.debug);
    switch(this.kindOfRun) {
      case TestCase.RUN_NONE:
        this._calculateSumResult(ActorResultConst.NONE, ActorResultConst.NONE, ActorResultConst.NONE);
        break;
      case TestCase.RUN_DATA_PRE:
        this._dataPre((dataResultId) => {
          this._runPre(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_EXEC:
        this._dataExec((dataResultId) => {
          this._runExec(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_PRE_EXEC:
        this._dataPreExec((dataResultId) => {
          this._runPreExec(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_POST:
        this._dataPost((dataResultId) => {
          this._runPost(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_PRE_POST:
        this._dataPrePost((dataResultId) => {
          this._runPrePost(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_EXEC_POST:
        this._dataExecPost((dataResultId) => {
          this._runExecPost(dataResultId);
        });
        break;
      case TestCase.RUN_DATA_PRE_EXEC_POST:
        this._dataPreExecPost((dataResultId) => {
          this._runPreExecPost(dataResultId);
        });
        break;
    }
  }
  
  runDistributionStatic(cbDone) {
    cbDone((commentOut, last) => {
      this._restoreActors((err) => {
        setTimeout((self) => {
          self._run();
        }, this.testCaseLoad.lambda, this);
      });
    });
  }
  
  run(cbDone) {
    //ddb.writeln('* * * * * * * *', this.nameParams[2]/*, this.testCaseLoad*/);
    this.cbDone = (done) => {
      if(++this.iterationTc <= this.testCaseLoad.iterationsTc) {
        done(false, false);
        if(TestCaseLoad.EXECUTION_SERIAL === this.testCaseLoad.execution) {
          if(TestCaseLoad.MODE_TC === this.testCaseLoad.mode) {
            this.runDistributionStatic(cbDone);
          }
        }
      }
      else {
        if(TestCase.NOT_RUNNING_IN_TS === this.index) {
          cbDone((commentOut, last) => {
            done(commentOut, last);
            StackComponentsFactory.exitExecution(this.executionContext.executionData.id);
          });
        }
        else {
          const result = done(false, true);
          cbDone(result);
        }
      }
    };
    if(TestCase.NOT_RUNNING_IN_TS === this.index) {
      StackComponentsFactory.initExecution(this.executionContext.executionData.id, this.executionContext.debugDashboard);
      const outputs = this.executionContext.outputs;
      const msg = new MessageExecutionStared(outputs.chosen, outputs.chosenClient, outputs.chosenClientConsole);
      this.cbMessage(msg, null, this.debug);
    }
    this._run();
  }
  
  runCommentOut(cbDone) {
    this.cbDone = (done) => {
      done(true);
      cbDone(this.noExecutionResultId);
    };
    this.timestamp = process.hrtime.bigint();
    const msg = new MessageTestCaseStarted(this.index, this.iterationTs, this.tc.name, this.nameParams[0], this.actors.nodes, HighResolutionTimestamp.getHighResolutionDate(this.timestamp), false);
    this.cbMessage(msg, null, this.debug);
    this._calculateSumResult(ActorResultConst.NONE, ActorResultConst.NONE, ActorResultConst.NONE, this.noExecutionResultId);
  }
    
  stop(done) {
    let pendings = 0;
    if(null !== this.actorStateMachinePre) {
      ++pendings;
      this.actorStateMachinePre.stop(() => {
        if(0 === --pendings) {
          done();
        }
      });
    }
    if(null !== this.actorStateMachineExec) {
      ++pendings;
      this.actorStateMachineExec.stop(() => {
        if(0 === --pendings) {
          done();
        }
      });
    }
    if(null !== this.actorStateMachinePost) {
      ++pendings;
      this.actorStateMachinePost.stop(() => {
        if(0 === --pendings) {
          done();
        }
      });
    }
    if(0 === pendings) {
      done();
    }
  }
  
  actorDebugContinue(index) {
    this.actors.actors[index].debugContinue();
  }
  
  getTestDataEngineString(key, defaultValue) {
    const valueData = this._getTestDataEngine(key);
    if(undefined !== valueData) {
      return valueData.value;
    }
    else {
      return defaultValue;
    }
  }
  
  getTestDataEngineInteger(key, defaultValue) {
    const valueData = this._getTestDataEngine(key);
    if(undefined !== valueData) {
      return Number.parseInt(valueData.value);
    }
    else {
      return defaultValue;
    }
  }
  
  getTestDataEngineBoolean(key, defaultValue) {
    const valueData = this._getTestDataEngine(key);
    if(undefined !== valueData) {
      if('true' === valueData.value) {
        return true;
      }
      else if('false' === valueData.value) {
        return false;
      }
      else {
        return undefined;
      }
    }
    else {
      return defaultValue;
    }
  }
    
  _getTestDataEngine(key) {
    let valueData = this.tcData.testDatasTestCase.get(key);
    if(undefined === valueData) {
      valueData = this.executionContext.testData.getTestData(key, (key) => {
        return this.tcData.testDatasTestCaseStatic.get(key);
      });
    }
    if(undefined !== valueData) {
      return valueData;
    }
  }
  
  _dataPre(cbDone) {
    this.actorStateMachinePre.data((dataResultIdPre) => {
      cbDone(dataResultIdPre);
    });
  }

  _dataExec(cbDone) {
    this.actorStateMachineExec.data((dataResultIdExec) => {
      cbDone(dataResultIdExec);
    });
  }

  _dataPreExec(cbDone) {
    this.actorStateMachinePre.data((dataResultIdPre) => {
      this.actorStateMachineExec.data((dataResultIdExec) => {
        cbDone(Math.max(dataResultIdPre, dataResultIdExec));
      });
    });
  }

  _dataPost(cbDone) {
    this.actorStateMachinePost.data((dataResultIdPost) => {
      cbDone(dataResultIdPost);
    });
  }
  
  _dataPrePost(cbDone) {
    this.actorStateMachinePre.data((dataResultIdPre) => {
      this.actorStateMachinePost.data((dataResultIdPost) => {
        cbDone(Math.max(dataResultIdPre, dataResultIdPost));
      });
    });
  }
  
  _dataExecPost(cbDone) {
    this.actorStateMachineExec.data((dataResultIdExec) => {
      this.actorStateMachinePost.data((dataResultIdPost) => {
        cbDone(Math.max(dataResultIdExec, dataResultIdPost));
      });
    });
  }
  
  _dataPreExecPost(cbDone) {
    this.actorStateMachinePre.data((dataResultIdPre) => {
      this.actorStateMachineExec.data((dataResultIdExec) => {
        this.actorStateMachinePost.data((dataResultIdPost) => {
          cbDone(Math.max(dataResultIdPre, dataResultIdExec, dataResultIdPost));
        });
      });
    });
  }

  _runPre(dataResultId) {
    this.actorStateMachinePre.run(dataResultId, (resultIdPre) => {
      this._calculateSumResult(resultIdPre, ActorResultConst.NONE, ActorResultConst.NONE);
    });
  }
    
  _runExec(dataResultId) {
    this.actorStateMachineExec.run(dataResultId, (resultIdExec) => {
      this._calculateSumResult(ActorResultConst.NONE, resultIdExec, ActorResultConst.NONE);
    });
  }
    
  _runPreExec(dataResultId) {
    this.actorStateMachinePre.run(dataResultId, (resultIdPre) => {
      this.actorStateMachineExec.run(Math.max(dataResultId, resultIdPre), (resultIdExec) => {
        this._calculateSumResult(resultIdPre, resultIdExec, ActorResultConst.NONE);
      });
    });    
  }
  
  _runPost(dataResultId) {
    this.actorStateMachinePost.run(dataResultId, (resultIdPost) => {
      this._calculateSumResult(ActorResultConst.NONE, ActorResultConst.NONE, resultIdPost);
    });
  }
  
  _runPrePost(dataResultId) {
    this.actorStateMachinePre.run(dataResultId, (resultIdPre) => {
      this.actorStateMachinePost.run(Math.max(dataResultId, resultIdPre), (resultIdPost) => {
        this._calculateSumResult(resultIdPre, ActorResultConst.NONE, resultIdPost);
      });
    });
  }
  
  _runExecPost(dataResultId) {
    this.actorStateMachineExec.run(dataResultId, (resultIdExec) => {
      this.actorStateMachinePost.run(Math.max(dataResultId, resultIdExec), (resultIdPost) => {
        this._calculateSumResult(ActorResultConst.NONE, resultIdExec, resultIdPost);
      });
    });
  }
  
  _runPreExecPost(dataResultId) {
    this.actorStateMachinePre.run(dataResultId, (resultIdPre) => {
      this.actorStateMachineExec.run(Math.max(dataResultId, resultIdPre), (resultIdExec) => {
        this.actorStateMachinePost.run(Math.max(dataResultId, resultIdPre, resultIdExec), (resultIdPost) => {
          this._calculateSumResult(resultIdPre, resultIdExec, resultIdPost);
        });
      });
    });
  }
  
  sendSumResult(index, iterationTs, duration, resultId, preResultId, execResultId, postResultId, last) {
    const msg = new MessageTestCaseStopped(index, iterationTs, duration, resultId, preResultId, execResultId, postResultId, last);
    this.cbMessage(msg, null, this.debug);
    if(TestOutput.LOG_CONSOLE & this.executionContext.outputs.chosen) {
      if(TestOutput.LOG_DETAIL_TC & this.executionContext.outputs.chosenConsole) {
        if(TestCase.NOT_RUNNING_IN_TS === index) {
          ddb.print(ActorResultConst.RESULTS_CONSOLE[resultId], this.nameParams[2]);
        }
        else {
          const indexText = (index + 1).toString().padStart(4);
          const iterationTsText = iterationTs.toString().padStart(3);
          ddb.print(ddb.yellow(indexText), ddb.yellow(iterationTsText), ActorResultConst.RESULTS_CONSOLE[resultId], this.nameParams[2]);
        }
      }
      if(TestOutput.LOG_DETAIL_ERR & this.executionContext.outputs.chosenConsole) {
        if(TestCase.NOT_RUNNING_IN_TS === index) {
          const errorDatass = this.executionContext.testCaseStatistics.errorDatass;
          errorDatass.forEach((errorDatas) => {
            errorDatas.forEach((errorData) => {
              errorData.errorData.forEach((data) => {
                if(data) {
                  ddb.print(ddb.green(errorData.name + '[') + errorData.id + ddb.green(']'), data.action);
                }
              });
            });
          });
        }
      }
    }
    return resultId;
  }
  
  _calculateSumResult(resultIdPre, resultIdExec, resultIdPost, tcResultId = -1) {
    const timestamp = process.hrtime.bigint();
    const settings = this.tc.tc.settings;
    this.cbDone((commentOut, last) => {
      if(undefined !== settings && 1 === settings.length && !commentOut) {
        if(ActorResultConst.getTableNameResultIds(settings[0].preResult, true) === resultIdPre && ActorResultConst.getTableNameResultIds(settings[0].execResult, true) === resultIdExec && ActorResultConst.getTableNameResultIds(settings[0].postResult, true) === resultIdPost) {
          return this.sendSumResult(this.index, this.iterationTs, timestamp - this.timestamp, ActorResultConst.SUCCESS, resultIdPre, resultIdExec, resultIdPost, last);
        }
        else {
          return this.sendSumResult(this.index, this.iterationTs, timestamp - this.timestamp, ActorResultConst.FAILURE, resultIdPre, resultIdExec, resultIdPost, last);
        }
      }
      else {
        const resultId = -1 === tcResultId ? Math.max(resultIdPre, resultIdExec, resultIdPost) : tcResultId;
        return this.sendSumResult(this.index, this.iterationTs, timestamp - this.timestamp, resultId, resultIdPre, resultIdExec, resultIdPost, last);
      }
    });
  }
  
  _setTestDatasTestCaseStatic(preActors, execActors, postActors) {
    this.executionContext.testData.addTestDataEmptyArray('preActorNames', this.tcData.testDatasTestCaseStatic);
    this.executionContext.testData.addTestDataEmptyArray('preActorInstanceIndices', this.tcData.testDatasTestCaseStatic);
    this.executionContext.testData.addTestDataEmptyArray('execActorNames', this.tcData.testDatasTestCaseStatic);
    this.executionContext.testData.addTestDataEmptyArray('execActorInstanceIndices', this.tcData.testDatasTestCaseStatic);
    this.executionContext.testData.addTestDataEmptyArray('postActorNames', this.tcData.testDatasTestCaseStatic);
    this.executionContext.testData.addTestDataEmptyArray('postActorInstanceIndices', this.tcData.testDatasTestCaseStatic);
    
    const preActorNames = this.tcData.testDatasTestCaseStatic.get('preActorNames');
    const preActorInstanceIndices = this.tcData.testDatasTestCaseStatic.get('preActorInstanceIndices');
    preActorNames.value = [];
    preActorInstanceIndices.value = [];
    preActors.forEach((actor) => {
      preActorNames.value.push(actor.name);
      preActorInstanceIndices.value.push(actor.instanceIndex);
    });
    
    const execActorNames = this.tcData.testDatasTestCaseStatic.get('execActorNames');
    const execActorInstanceIndices = this.tcData.testDatasTestCaseStatic.get('execActorInstanceIndices');
    execActorNames.value = [];
    execActorInstanceIndices.value = [];
    execActors.forEach((actor) => {
      execActorNames.value.push(actor.name);
      execActorInstanceIndices.value.push(actor.instanceIndex);
    });

    const postActorNames = this.tcData.testDatasTestCaseStatic.get('postActorNames');
    const postActorInstanceIndices = this.tcData.testDatasTestCaseStatic.get('postActorInstanceIndices');
    postActorNames.value = [];
    postActorInstanceIndices.value = [];
    postActors.forEach((actor) => {
      postActorNames.value.push(actor.name);
      postActorInstanceIndices.value.push(actor.instanceIndex);
    });
  }
  
  _setTestDataTestCases(testDataTestCases) {
    if(undefined !== testDataTestCases && Array.isArray(testDataTestCases)) {
      testDataTestCases.forEach((testDataTestCase) => {
        const key = testDataTestCase.key; 
        if(key.endsWith('[]')) {
          const arrayName = key.substr(0, key.length - 2);
          let valueData = this.tcData.testDatasTestCase.get(arrayName);
          if(undefined === valueData) {
            valueData = {
              type: 'array',
              value: []
            };
            this.tcData.testDatasTestCase.set(arrayName, valueData);
          }
          valueData.value.push(testDataTestCase.value);
        }
        else {
          this.tcData.testDatasTestCase.set(testDataTestCase.key, {
            type: 'value',
            value: testDataTestCase.value
          });
        }
      });
    }
  }
  
  _setTestDataIteration(testDataIteration) {
    if(undefined !== testDataIteration && Array.isArray(testDataIteration)) {
      testDataIteration.forEach((testDataActor) => {
        const testDatasPhase = this.tcData.testDatasActor.get(testDataActor.phase);
        if(undefined !== testDatasPhase) {
          let testDatasType = testDatasPhase.get(testDataActor.type);
          if(undefined === testDatasType) {
            testDatasType = new Map();
            testDatasPhase.set(testDataActor.type, testDatasType);
          }
          this._setTestDataIterationValue(testDatasType, testDataActor);
        }
      });
    }
  }
  
  _setTestDataIterationValue(testDatasType, testDataActor) {
    const key = testDataActor.key; 
    if(/\[(.*)\]$/.test(key)) {
      const match = key.match(/\[(.*)\]$/);
      if(null != match) {
        const arrayName = key.substr(0, key.length - match[0].length);
        let testDatasActor = testDatasType.get(arrayName);
        if(undefined === testDatasActor) {
          testDatasActor = {
            currentIndex: 0,
            values: []
          };
          testDatasType.set(arrayName, testDatasActor);
        }
        const valueArray = testDatasActor.values.find((data) => {
          return data.name === key;
        });
        if(undefined === valueArray) {
          valueArray = {
            name: key,
            type: 'array',
            value: []
          };
        }
        testDatasActor.values.push(valueArray);
        valueArray.value.push(testDataActor.value);
      }
    }
    else {
      let testDatasActor = testDatasType.get(key);
      if(undefined === testDatasActor) {
        testDatasActor = {
          currentIndex: 0,
          values: []
        };
        testDatasType.set(key, testDatasActor);
      }
      testDatasActor.values.push({
        type: 'value',
        value: testDataActor.value
      });
    }
  }

  _setVerificationTestCases(verificationTestCases) {
    if(undefined !== verificationTestCases && Array.isArray(verificationTestCases)) {
      verificationTestCases.forEach((verificationTestCase) => {
        const key = verificationTestCase.key; 
        if(key.endsWith('[]')) {
          const arrayName = key.substr(0, key.length - 2);
          let valueData = this.tcData.verificationsTestCase.get(arrayName);
          if(undefined === valueData) {
            valueData = {
              type: 'array',
              value: [],
              operation: [],
              valueType: []
            };
            this.tcData.verificationsTestCase.set(arrayName, valueData);
          }
          valueData.value.push(verificationTestCase.value); 
          valueData.operation.push(0 === verificationTestCase.operation.length ? '===' : verificationTestCase.operation); 
          valueData.valueType.push(0 === verificationTestCase.type.length ? 'string' : verificationTestCase.type); 
        }
        else {
          this.tcData.verificationsTestCase.set(verificationTestCase.key, {
            type: 'value',
            value: verificationTestCase.value,
            operation: 0 === verificationTestCase.operation.length ? '===' : verificationTestCase.operation,
            valueType: 0 === verificationTestCase.type.length ? 'string' : verificationTestCase.type
          });
        }
      });
    }
  }
}

  
module.exports = TestCase;
