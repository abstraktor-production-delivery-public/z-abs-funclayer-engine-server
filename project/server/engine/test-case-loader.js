
'use strict';

const AddressDefaultManager = require('../address/address-default-manager');
const DnsUrlCache = require('../address/dns-url-cache');
const ExecutionContextTestCase = require('./execution-context-test-case');
const LocalDns = require('../address/local-dns');
const TestCase = require('./test-case');
const TestOutput = require('./test-output');
const ClientManager = require('../stack/managers/client-manager/client-manager');
const ServerManager = require('../stack/managers/server-manager/server-manager');
const SharedManager = require('../stack/managers/shared-manager');
const StackContentCache = require('../stack/stacks/stack-content-cache');
const ContentData = require('./data/content-data');
const ContentCache = require('./data/content-cache');
const TestCaseLoad = require('./test-case-load');
const TestData = require('./data/test-data');
const RuntimeDataShared = require('./data/runtime-data-shared');
const Addresses = require('../address/addresses');
const DebugDashboard = require('z-abs-funclayer-engine-server/server/debug-dashboard/debug-dashboard');
const StackComponentsCerts = require('z-abs-funclayer-stack-server/server/factory/stack-components-certs');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');


class TestCaseLoader {
  static load(repoName, sutName, futName, tcName, sutInstance, stagedNodes, debug, config, cbMessage, done) {
    #BUILD_RELEASE_START
    const executionContext = new ExecutionContextTestCase(debug);
    #BUILD_RELEASE_STOP
    #BUILD_DEBUG_START
    const executionContext = new Proxy(new ExecutionContextTestCase(debug), {
      set(obj, prop, value) {
        if(undefined === Reflect.get(obj, prop)) {
          ddb.warning(`member: "${prop}" is not defined in constructor.`);
        }
        return Reflect.set(obj, prop, value);
      }
    });
    #BUILD_DEBUG_STOP
    executionContext.setStageData('_BASE', sutName, sutInstance, stagedNodes);
    executionContext.setTestData(sutName, sutInstance);
    executionContext.config = config;
    
    let pendings = 0;
    let errorResponse = false;
    const loadDone = (err, testCase) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          done(err);
        }
        else if(0 === --pendings) {
          executionContext.outputs.load();
          testCase.load((loadErr) => {
            done(loadErr, testCase);
          });
        }
      }
    };
    let onTestDataLoaded = null;
    let testCase = null;
    ++pendings;
    Fs.readFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), (err, data) => {
      if(!err) {
        const tc = JSON.parse(data);
        testCase = new TestCase(tc, [sutName, futName, tcName], 0, debug, executionContext, cbMessage);
        if(testCase.containsInterceptor()) {
          if(executionContext.testData.loaded) {
            if(!StackComponentsFactory.supportInterceting('puppeteer', 'client', executionContext.testData)) { // HARDCODED
              err = new Error('N/A');
            }
          }
          else {
            onTestDataLoaded = () => {
              if(!StackComponentsFactory.supportInterceting('puppeteer', 'client', executionContext.testData)) { // HARDCODED
                loadDone(new Error('N/A'), testCase);
              }
              else {
                loadDone(null, testCase);
              }
            };
            return;
          }
        }
      }
      loadDone(err, testCase);
    });
    ++pendings;
    executionContext.contentData.load((err) => {
      loadDone(err, testCase);
    });
    ++pendings;
    executionContext.testData.load((err) => {
      if(!err) {
        if(config.asService) {
          executionContext.testData.setOverrideTestData('tc-state-timeout', {
            type: 'value',
            value: '0'
          });
        }
        if(onTestDataLoaded) {
          onTestDataLoaded();
        }
      }
      loadDone(err, testCase);
    });
    ++pendings;
    Addresses.load(executionContext.addresses, (err) => {
      loadDone(err, testCase);
    });
    ++pendings;
    LocalDns.load(executionContext.dnsUrlCache.localDns, (err) => {
      loadDone(err, testCase);
    });
    ++pendings;
    StackComponentsCerts.isLoaded((err) => {
      loadDone(err, testCase);
    });
  }
}

module.exports = TestCaseLoader;
