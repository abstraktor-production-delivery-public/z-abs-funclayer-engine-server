
'use strict';

const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');


class TestStatisticsTestCase {
  constructor() {
    this.resultsCurrent = [];
    this.errorDatass = [];
    this.errorDatas = [];
    this.errorDatass.push(this.errorDatas);
  }
  
  add(resultId) {
    this.resultsCurrent.push(resultId);
    this.errorDatas = [];
    this.errorDatass.push(this.errorDatas);
  }
  
  addErrorData(connection) {
    this.errorDatas.push({
      name: connection.name,
      id: connection.id,
      errorData: connection.errorData
    });
    connection.errorData = [];
  }

  calculate() {
    const result = {
      totalSum: ActorResultConst.NONE,
      sums: new Array(ActorResultConst.RESULTS_CONSOLE.length)
    };
    result.totalSum = this.resultsCurrent.reduce((acc, current) => {
      return Math.max(acc, current);
    }, ActorResultConst.NONE);
    if(!this.isSumOk(result.totalSum)) {
      for(let i = 0; i < ActorResultConst.RESULTS_CONSOLE.length; ++i) {
        result.sums[i] = 0;
      }
      this.resultsCurrent.forEach((resultsIteration) => {
        ++result.sums[resultsIteration];
      });
    }
    return result;
  }
  
  isSumOk(result) {
    return ActorResultConst.SUCCESS >= result;
  }
  
  outputResult(result) {
    return ActorResultConst.RESULTS_CONSOLE[result];
  }
}


module.exports = TestStatisticsTestCase;
