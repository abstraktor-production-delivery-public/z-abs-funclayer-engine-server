
'use strict';

const Stage = require('./stage');
const TestCase = require('./test-case');
const TestOutput = require('./test-output');
const TestStatisticsTestCase = require('./test-statistics-test-case');
const TestStatisticsTestSuite = require('./test-statistics-test-suite');
const MessageExecutionStarted = require('../communication/messages/messages-s-to-c/message-execution-started');
const MessageExecutionStopped = require('../communication/messages/messages-s-to-c/message-execution-stopped');
const MessageTestSuiteStarted = require('../communication/messages/messages-s-to-c/message-test-suite-started');
const MessageTestSuiteStopped = require('../communication/messages/messages-s-to-c/message-test-suite-stopped');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class TestSuite {
  constructor(ts, iterationsTs, stagedSut, stagedSutInstance, executionContext, testDataFunc, cbMessage) {
    this.ts = ts;
    this.outputs = executionContext.outputs;
    this.executionContext = executionContext;
    this.testStatistics = new TestStatisticsTestSuite();
    this.index = -1;
    this.abstractions = [];
    this.iterationsTs = iterationsTs;
    this.testDataFunc = testDataFunc;
    this.cbMessage = cbMessage;
    this.currentAbstractionIndex = 0;
    this.timestamp = null;
    this.running = false;
    this.stageSetter = null;
    this.debug = false;
    this.supportInterceting = false;
  }
  
  getName() {
    return this.ts.name;
  }
  
  addTestCase(key, testCase, commentOut) {
    testCase.key = key;
    const abstraction = {
      exec: testCase,
      commentOut: commentOut,
      containsInterceptor: testCase.containsInterceptor(),
      statistics: new TestStatisticsTestCase(),
      stageData: null
    };
    this.abstractions.push(abstraction);
    return abstraction;
  }
    
  addStage(key, stage, commentOut) {
    stage.key = key;
    this.abstractions.push({
      exec: stage,
      commentOut: commentOut,
      containsInterceptor: false,
      statistics: new TestStatisticsTestCase()
    });
    stage.setCbs((name, stagedSut, stagedSutInstance, subType, done) => {
      if(Stage.INIT_PUSH === subType) {
        const success = this.stageSetter.push(name, stagedSut, stagedSutInstance);
        process.nextTick(done, success);
      }
      else {
        const success = this.stageSetter.change(name, stagedSut, stagedSutInstance);
        process.nextTick(done, success);
      }
    }, (name, stagedSut, stagedSutInstance, subType, done) => {
      this.stageSetter.exit(() => {
        if(Stage.EXIT_CHANGE === subType) {
          process.nextTick(done, true);
        }
        else {
          this.stageSetter.pop();
          process.nextTick(done, true);
        }
      });
    });
  }
  
  addStageSetter(stageSetter) {
    this.stageSetter = stageSetter;
  }
  
  run(doneTestCase, doneTestSuite) {
    this.supportInterceting = StackComponentsFactory.supportInterceting('puppeteer', 'client', this.executionContext.testData);
    if(this.stageSetter) {
      this.stageSetter.setSupportInterceting(this.supportInterceting);
    }
    this.running = true;
    let index = -1
    // TODO: Temp fix.
    this.abstractions.sort((a, b) => {
      if(a.exec.key < b.exec.key) {
        return -1;
      }
      else if(a.exec.key > b.exec.key) {
        return 1;
      }
      else {
        return 0;
      }
    });
    const length = this.abstractions.length;
    for(let i = 0; i < length; ++i) {
      const abstraction = this.abstractions[i];
      ++index;
      abstraction.exec.index = index;
      if(abstraction.commentOut) {
        abstraction.exec.noExecutionResultId = ActorResultConst.NONE;
        abstraction.exec.run = abstraction.exec.runCommentOut;
      }
    }
    this.timestamp = process.hrtime.bigint();
    const outputs = this.outputs;
    const msgExecutionStarted = new MessageExecutionStarted(outputs.chosen, outputs.chosenClient, outputs.chosenClientConsole);
    this.cbMessage(msgExecutionStarted, null, this.debug);
    const msg = new MessageTestSuiteStarted(++this.index, HighResolutionTimestamp.getHighResolutionDate(this.timestamp));
    this.cbMessage(msg, null, this.debug);
    this._runAbstraction(this.currentAbstractionIndex, doneTestCase, doneTestSuite);
  }
  
  stop(done) {
    this.running = false;
    done();
  }
  
  _testSuiteDone(index, timestamp, duration, last) {
    const msg = new MessageTestSuiteStopped(index, timestamp, duration, last);
    this.cbMessage(msg, null, this.debug);
    if(last) {
      const msgExecutionStopped = new MessageExecutionStopped();
      this.cbMessage(msgExecutionStopped, null, this.debug);
    }
    if(TestOutput.EXECUTION_CONSOLE & this.outputs.chosen) {
      if((TestOutput.LOG_DETAIL_TS & this.outputs.chosenConsole) || (TestOutput.LOG_DETAIL_SUM & this.outputs.chosenConsole)) {
        this.testStatistics.calculate();
      }
      const result = this.index + 1 === this.iterationsTs ? this.testStatistics.calculateSum() : ActorResultConst.NONE;
      const totalSuccess = this.testStatistics.isSumOk(result.totalSum);
      if(TestOutput.LOG_DETAIL_TS & this.outputs.chosenConsole) {
        const currentIterationTs = (this.index + 1).toString().padStart(4);
        ddb.printTop();
        ddb.print(ddb.yellow(currentIterationTs), 'TS result:', this.testStatistics.outputLastIterationResult(), this.ts.name);
        ddb.printBottom();
      }
      if(TestOutput.LOG_DETAIL_SUM & this.outputs.chosenConsole) {
        if(this.index + 1 === this.iterationsTs) {
          ddb.printMiddle();
          ddb.print('TS-TS summary:', this.testStatistics.outputResult(result.totalSum));
          if(!totalSuccess) {
            ddb.printTop();
            result.sums.forEach((sum, index) => {
              if(0 !== sum) {
                ddb.print(this.testStatistics.outputResult(index), sum);
              }
            });
            ddb.printTop();
            ddb.print('TS-TC summary:');
            ddb.printBottom();
            this.abstractions.forEach((abstraction) => {
              const result = abstraction.statistics.calculate();
              if(!abstraction.statistics.isSumOk(result.totalSum)) {
                const nameParams = abstraction.exec.nameParams;
                const sut = `${abstraction.stageData.stageSut}${abstraction.stageData.stageSutInstance ? ('[' + abstraction.stageData.stageSutInstance + ']') : ''}`;
                if(nameParams) {
                  ddb.print('*', `${sut}.${nameParams[1]}.${nameParams[2]}`);
                }
                else {
                  ddb.print('*', `${sut} Reflection`);
                }
                result.sums.forEach((sum, index) => {
                  if(0 !== sum) {
                    ddb.print(this.testStatistics.outputResult(index), sum);
                  }
                });
                const errorDatass = abstraction.statistics.errorDatass;
                errorDatass.forEach((errorDatas) => {
                  errorDatas.forEach((errorData) => {
                    errorData.errorData.forEach((data) => {
                      if(data) {
                        ddb.print(ddb.green(errorData.name + '[') + errorData.id + ddb.green(']'), data.action);
                      }
                    });
                  });
                });
              }
            });
          }
          ddb.printBottom();
        }
      }
    }
  }
  
  _runAbstraction(index, doneTestCase, doneTestSuite) {
    if(index < this.abstractions.length) {
      const abstraction = this.abstractions[index];
      if(null === abstraction.stageData) {
        abstraction.stageData = this.executionContext.stageData;
      }
      if(abstraction.exec instanceof TestCase) {
        abstraction.exec.executionContext.initTestCase(abstraction.exec);
        abstraction.exec.executionContext.testCaseStatistics = abstraction.statistics;
      }
      if(!abstraction.commentOut && abstraction.containsInterceptor) {
        if(this.stageSetter) {
          if(!this.stageSetter.supportInterceting) {
            abstraction.exec.noExecutionResultId = ActorResultConst.NA;
            abstraction.exec.run = abstraction.exec.runCommentOut;
          }
        }
        else if(!this.supportInterceting) {
          abstraction.exec.noExecutionResultId = ActorResultConst.NA;
          abstraction.exec.run = abstraction.exec.runCommentOut;
        }
      }
      abstraction.exec.run((resultId) => {
        abstraction.statistics.add(resultId);
        this.testStatistics.add(resultId);
        doneTestCase();
        if(!this.running) {
          const timestamp = process.hrtime.bigint();
          doneTestSuite(() => {
            this._testSuiteDone(this.index, HighResolutionTimestamp.getHighResolutionDate(timestamp), timestamp - this.timestamp, true);
          });
          return;
        }
        process.nextTick(() => {
          if(!this._runAbstraction(++this.currentAbstractionIndex, doneTestCase, doneTestSuite)) {
            if(this.index + 1 === this.iterationsTs) {
              const timestamp = process.hrtime.bigint();
              doneTestSuite(() => {
                this._testSuiteDone(this.index, HighResolutionTimestamp.getHighResolutionDate(timestamp), timestamp - this.timestamp, true);
              });
            }
            else {
              const timestamp = process.hrtime.bigint();
              this._testSuiteDone(this.index, HighResolutionTimestamp.getHighResolutionDate(timestamp), timestamp - this.timestamp, false);
              let pendings = 0;
              this.abstractions.forEach((abstraction) => {
                if(abstraction.exec instanceof TestCase) {
                  ++pendings;
                  abstraction.exec.load(() => {
                    if(0 === --pendings) {
                      this.timestamp = process.hrtime.bigint();
                      const msg = new MessageTestSuiteStarted(++this.index, HighResolutionTimestamp.getHighResolutionDate(this.timestamp));
                      this.cbMessage(msg, null, this.debug);
                      this.currentAbstractionIndex = 0;
                      process.nextTick(() => {
                        this._runAbstraction(this.currentAbstractionIndex, doneTestCase, doneTestSuite);
                      });
                    }
                  });
                }
              });
            }
          }
        });
      });
      return true;
    }
    else {
      return false;
    }
  }
}


module.exports = TestSuite;
