
'use strict';

const ActorStateMachineBase = require('./actor-state-machine-base');
const ActorState = require('./actor-state');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const ActorStateConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorStateMachinePre extends ActorStateMachineBase {
  constructor(tc, executionContext, cbMessage, actorsPhase, debug) {
    super(tc, executionContext, cbMessage, actorsPhase, ActorPhaseConst.PRE, debug);
  }
  
  _interrupt() {
    this.actorsRunning.forEach((actor) => {
      let throwOrCancel = false;
      switch(actor.getStateId()) {
        case ActorState.DATA:
          break;
        case ActorState.INIT_SERVER:
        case ActorState.INIT_CLIENT:
          throwOrCancel = true;
          break;
        case ActorState.RUN:
          throwOrCancel = true;
          break;
        case ActorState.EXIT:
          break;
      };
      actor.doInterrupt(throwOrCancel);
    });
  }
  
  _stateRunActorError(actor) {
    actor.logDebug(() => `actor state: ${ActorStateConst.getName(actor.getStateId())} - error`, 'state-machine', 'cbe2ef65-c116-436a-bfc5-2f958d33e307');
    switch(actor.getStateId()) {
      case ActorState.DATA:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
            this._nextState(actor, ActorResultConst.N_EXEC);
            break;
          case ActorTypeConst.ORIG:
          case ActorTypeConst.COND:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
      case ActorState.INIT_SERVER:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
            this.serverStarted(actor);
        };
        switch(actor.typeId) {
          case ActorTypeConst.ORIG:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.COND:
            this._nextState(actor, ActorResultConst.N_EXEC);
            break;
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
      case ActorState.INIT_CLIENT:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.LOCAL:
            if(ActorResultConst.SUCCESS === actor.getStateResultId(ActorState.DATA)) {
              actor.getState().setRun();
              if(!actor.doRun()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          default:
            this._nextState(actor, ActorResultConst.N_EXEC);
        }
        break;
      case ActorState.RUN:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
          case ActorTypeConst.COND:
            if(ActorResultConst.SUCCESS === actor.getStateResultId(ActorState.INIT_CLIENT)) {
              actor.getState().setExit();
              if(!actor.doExit()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          default:
            this._nextState(actor, ActorResultConst.N_EXEC);
        }
        break;
    }
  }
}

module.exports = ActorStateMachinePre;
