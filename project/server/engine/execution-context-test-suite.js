
'use strict';

const ExecutionContext = require('./execution-context');


class ExecutionContextTestSuite extends ExecutionContext {
  constructor(debug) {
    super(1, debug);
  }
  
  pushStage() {
    const data = {
      addresses: this.addresses,
      localDns: this.dnsUrlCache.localDns,
      stageData: this.stageData
    };
    this.stageStack.push(data);
  }
  
  changeStage() {
    this.testData.popStage();
  }
  
  popStage() {
    this.testData.popStage();
    const data = this.stageStack.pop();
    this.addresses = data.addresses;
    this.addressDefaultManager.setAddresses(data.addresses);
    this.dnsUrlCache.setLocalDns(data.localDns);
  }
  
  setStage(name, stagedSut, stagedSutInstance, stagedSutNodes, addresses, localDns) {
    this.setStageData(name, stagedSut, stagedSutInstance, stagedSutNodes);
    this.testData.pushStage(stagedSut, stagedSutInstance);
    this.addresses = addresses;
    this.addressDefaultManager.setAddresses(addresses);
    this.dnsUrlCache.setLocalDns(localDns);
  }
  
  exitTestCase() {
    this.sharedManager.reset();
    this.clientManager.reset();
    this.serverManager.reset();
  }

  exitTestSuite(doneClean) {
    let clientDone = false;
    let serverDone = false;
    this.clientManager.closeReusedConnections(() => {
      clientDone = true;
      if(clientDone && serverDone) {
        this.connectionManager.reset();
        doneClean();
      }
    });
    this.serverManager.closeReusedConnections(() => {
      serverDone = true;
      if(clientDone && serverDone) {
        this.connectionManager.reset();
        doneClean();
      }
    });
  }
}


module.exports = ExecutionContextTestSuite;
