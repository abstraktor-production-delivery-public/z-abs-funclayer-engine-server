
'use strict';

const ActorStateMachineBase = require('./actor-state-machine-base');
const ActorState = require('./actor-state');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const ActorStateConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-state-const');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorStateMachinePost extends ActorStateMachineBase {
  constructor(tc, executionContext, cbMessage, actorsPhase, debug) {
    super(tc, executionContext, cbMessage, actorsPhase, ActorPhaseConst.POST, debug);
  }

  _interrupt() {
    this.actorsRunning.forEach((actor) => {
      let throwOrCancel = false;
      switch(actor.getStateId()) {
        case ActorState.DATA:
        case ActorState.INIT_SERVER:
        case ActorState.INIT_CLIENT:
        case ActorState.RUN:
        case ActorState.EXIT:
          break;
      };
      actor.doInterrupt(throwOrCancel);
    });
  }

  _stateRunActorError(actor) {
    actor.logDebug(() => `actor state: ${ActorStateConst.getName(actor.getStateId())} - error`, 'state-machine', '25a84770-a32d-48ad-9613-b9f96f321fb9');
    switch(actor.getStateId()) {
      case ActorState.DATA:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
            this._nextState(actor, ActorResultConst.N_EXEC);
            break;
          case ActorTypeConst.ORIG:
          case ActorTypeConst.COND:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
      case ActorState.INIT_SERVER:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
            this.serverStarted(actor);
        };
        switch(actor.typeId) {
          case ActorTypeConst.COND:
            if(ActorResultConst.SUCCESS >= actor.getActorResultId(ActorPhaseConst.PRE)) {
              if(0 === this.termOrProxyActorsInInitServer) {
                actor.getState().setInitClient();
                if(!actor.doInitClient()) {
                   actor.getCbActorStateDone()(actor);
                }
              }
              else {
                this.actorsPending.delete(actor.getIndex());
                this.actorsInitServerPending.push(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          case ActorTypeConst.ORIG:
          case ActorTypeConst.PROXY:
          case ActorTypeConst.SUT:
            this._nextState(actor, ActorResultConst.N_EXEC);
            break;
          case ActorTypeConst.TERM:
          case ActorTypeConst.INTER:
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
        }
        break;
      case ActorState.INIT_CLIENT:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.COND:
            if(ActorResultConst.SUCCESS >= actor.getActorResultId(ActorPhaseConst.PRE) && ActorResultConst.SUCCESS === actor.getStateResultId(ActorState.INIT_CLIENT)) {
              actor.getState().setRun();
              if(!actor.doRun()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          case ActorTypeConst.LOCAL:
            if(ActorResultConst.SUCCESS === actor.getStateResultId(ActorState.DATA) || ActorResultConst.N_IMPL === actor.getStateResultId(ActorState.DATA)) {
              actor.getState().setRun();
              if(!actor.doRun()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          default:
            this._nextState(actor, ActorResultConst.N_EXEC);
        }
        break;
      case ActorState.RUN:
        this.actorsRunning.set(actor.getIndex(), actor);
        switch(actor.typeId) {
          case ActorTypeConst.LOCAL:
            this._nextState(actor, ActorResultConst.NA);
            break;
          case ActorTypeConst.COND:
            if(ActorResultConst.SUCCESS === actor.getStateResultId(ActorState.INIT_CLIENT)) {
              actor.getState().setExit();
              if(!actor.doExit()) {
                actor.getCbActorStateDone()(actor);
              }
            }
            else {
              this._nextState(actor, ActorResultConst.N_EXEC);
            }
            break;
          default:
            this._nextState(actor, ActorResultConst.N_EXEC);
        }
        break;
    }
  }
}

module.exports = ActorStateMachinePost;
