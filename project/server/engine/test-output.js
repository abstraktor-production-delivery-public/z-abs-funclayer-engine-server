
'use strict';


class TestOutput {
  static LOG_CLIENT = 1;
  static LOG_CLIENT_CONSOLE = 2;
  static LOG_CONSOLE = 4;
  static LOG_FILE = 8;
  static LOG_PLUGIN = 16;
  static LOG_PUSH = 32;
  static LOG_CLIENT_TEXT = 'client';
  static LOG_CLIENT_CONSOLE_TEXT = 'client_console';
  static LOG_CONSOLE_TEXT = 'console';
  static LOG_FILE_TEXT = 'file';
  static LOG_PLUGIN_TEXT = 'plugin';
  static LOG_PUSH_TEXT = 'push';

  static LOG_DETAIL_LOG = 1;
  static LOG_DETAIL_TC = 2;
  static LOG_DETAIL_TS = 4;
  static LOG_DETAIL_SUM = 8;
  static LOG_DETAIL_ERR = 16;
  static LOG_DETAIL_LOG_TEXT = 'log';
  static LOG_DETAIL_TC_TEXT = 'tc';
  static LOG_DETAIL_TS_TEXT = 'ts';
  static LOG_DETAIL_SUM_TEXT = 'sum';
  static LOG_DETAIL_ERR_TEXT = 'err';

  constructor(testDataFunc) {
    this.testDataFunc = testDataFunc;
    this.chosen = 0;
    this.chosenClient = 0;
    this.chosenClientConsole = 0;
    this.chosenConsole = 0;
    this.chosenFile = 0;
    this.chosenPlugin = 0;
    this.chosenPush = 0;
  }
  
  load() {
    const destinations = this.testDataFunc('log-settings-destination');
    this.chosen = this._outputs(destinations);
    if(TestOutput.LOG_CLIENT & this.chosen) {
      const destinationsClient = this.testDataFunc('log-settings-details-client');
      this.chosenClient = this._outputDetails(destinationsClient);
    }
    if(TestOutput.LOG_CLIENT_CONSOLE & this.chosen) {
      const destinationsClientConsole = this.testDataFunc('log-settings-details-client_console');
      this.chosenClientConsole = this._outputDetails(destinationsClientConsole);
    }
    if(TestOutput.LOG_CONSOLE & this.chosen) {
      const destinationsConsole = this.testDataFunc('log-settings-details-console');
      this.chosenConsole = this._outputDetails(destinationsConsole);
    }
    if(TestOutput.LOG_FILE & this.chosen) {
      const destinationsFile = this.testDataFunc('log-settings-details-file');
      this.chosenFile = this._outputDetails(destinationsFile);
    }
    if(TestOutput.LOG_PLUGIN & this.chosen) {
      const destinationsPlugin = this.testDataFunc('log-settings-details-plugin');
      this.chosenPlugin = this._outputDetails(destinationsPlugin);
    }
    if(TestOutput.LOG_PUSH & this.chosen) {
      const destinationsPush = this.testDataFunc('log-settings-details-push');
      this.chosenPush = this._outputDetails(destinationsPush);
    }
  }
  
  _outputDetails(destinations) {
    const destinationsSplit = destinations.split(',');
    return destinationsSplit.reduce((acc, current) => {
      const dest = current.trim().toLowerCase();
      switch(dest) {
        case TestOutput.LOG_DETAIL_LOG_TEXT:
          return acc + TestOutput.LOG_DETAIL_LOG;
        case TestOutput.LOG_DETAIL_TC_TEXT:
          return acc + TestOutput.LOG_DETAIL_TC;
        case TestOutput.LOG_DETAIL_TS_TEXT:
          return acc + TestOutput.LOG_DETAIL_TS;
        case TestOutput.LOG_DETAIL_SUM_TEXT:
          return acc + TestOutput.LOG_DETAIL_SUM;
        case TestOutput.LOG_DETAIL_ERR_TEXT:
          return acc + TestOutput.LOG_DETAIL_ERR;
        default:
          return acc;
      }
    }, 0);
  }
  
  _outputs(destinations) {
    const destinationsSplit = destinations.split(',');
    return destinationsSplit.reduce((acc, current) => {
      const dest = current.trim().toLowerCase();
      switch(dest) {
        case TestOutput.LOG_CLIENT_TEXT:
          return acc + TestOutput.LOG_CLIENT;
        case TestOutput.LOG_CLIENT_CONSOLE_TEXT:
          return acc + TestOutput.LOG_CLIENT_CONSOLE;
        case TestOutput.LOG_CONSOLE_TEXT:
          return acc + TestOutput.LOG_CONSOLE;
        case TestOutput.LOG_FILE_TEXT:
          return acc + TestOutput.LOG_FILE;
        case TestOutput.LOG_PLUGIN_TEXT:
          return acc + TestOutput.LOG_PLUGIN;
        case TestOutput.LOG_PUSH_TEXT:
          return acc + TestOutput.LOG_PUSH;
        default:
          return acc;
      }
    }, 0);
  }
}


module.exports = TestOutput;
