
'use strict';

const ExecutionContext = require('./execution-context');
const TestStatisticsTestCase = require('./test-statistics-test-case');


class ExecutionContextTestCase extends ExecutionContext {
  constructor(debug) {
    super(0, debug);
    this.testCaseStatistics = new TestStatisticsTestCase();
    this.executingActorIndex = -1;
  }
  
  setExecutingActorIndex(index) {
    this.executingActorIndex = index;
  }
  
  _clearTestCase() {
    this.connectionManager.reset();
    this.sharedManager.reset();
    this.clientManager.reset();
    this.serverManager.reset();
  }
  
  exitTestCase(doneClean) {
    this.executingActorIndex = -1;
    let clientDone = false;
    let serverDone = false;
    this.clientManager.closeReusedConnections(() => {
      clientDone = true;
      if(clientDone && serverDone) {
        this._clearTestCase();
        doneClean(false, true);
      }
    });
    this.serverManager.closeReusedConnections(() => {
      serverDone = true;
      if(clientDone && serverDone) { 
        this._clearTestCase();
        doneClean(false, true);
      }
    });
  }
}


module.exports = ExecutionContextTestCase;
