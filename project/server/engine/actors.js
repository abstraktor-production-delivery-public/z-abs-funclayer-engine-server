
'use strict';

const ActorsPhase = require('./actors-phase');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');
const Logger = require('z-abs-corelayer-server/server/log/logger');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const Module = require('module');
const Path = require('path');


class Actors {
  constructor(tc, executionContext, tcData, debug, config, slowAwaitTime, cbMessage) {
    this.tc = tc;
    this.executionContext = executionContext;
    this.tcData = tcData;
    this.debug = debug;
    this.config = config;
    this.slowAwaitTime = slowAwaitTime;
    this.cbMessage = cbMessage;
    this.preActors = new ActorsPhase(ActorPhaseConst.PRE);
    this.execActors = new ActorsPhase(ActorPhaseConst.EXEC);
    this.postActors = new ActorsPhase(ActorPhaseConst.POST);
    this.actors = [];
    this.nodes = [];
  }
  
  loadActors(cb) {
    const actors = this.tc.actors;
    const size = actors.length;
    if(0 === size) {
      process.nextTick(() => {
        this._calculateIndices();
        cb(true);
      });
    }
    const results = [];
    const instanceIndices = new Map();
    for(let i = 0; i < size; ++i) {
      process.nextTick(() => {
        const actorData = actors[i];
        #BUILD_RELEASE_START
        const actor = !actorData.inlineCode ? this._loadFromFile(actorData) : this._loadFromString(actorData);
        #BUILD_RELEASE_STOP
        #BUILD_DEBUG_START
        const actor = new Proxy((!actorData.inlineCode ? this._loadFromFile(actorData) : this._loadFromString(actorData)), {
          set(obj, prop, value) {
            if(undefined === Reflect.get(obj, prop)) {
              ddb.warning(`member: "${prop}" is not defined in constructor.`);
            }
            return Reflect.set(obj, prop, value);
          }
        });
        #BUILD_DEBUG_STOP
        if(actor) {
          let instanceIndex = 0;
          const actorName = actorData.name.substr(actorData.name.lastIndexOf('.') + 1);
          if(instanceIndices.has(actorName)) {
            instanceIndex = instanceIndices.get(actorName) + 1;
          }
          else {
            instanceIndex = 1;
          }
          instanceIndices.set(actorName, instanceIndex);     
          if(actorData.type !== ActorTypeConst.getName(actor.inheritedType)) {
            results.push(false);
            LOG_ERROR(Logger.ERROR.ERR, `Actor '${actorName}[instanceIndex]': Declared type in Test Case '${actorData.type}'is not the same as the inherited type '${actor.inheritedType}'`, undefined);  
          }
          else {
            results.push(actor.init(actorData, instanceIndex, this.cbMessage, this.executionContext, this.tcData, this.config, this.slowAwaitTime, this.nodes));
            this._addActor(actor);
          }
        }
        else {
          results.push(false);
        }
        if(size === results.length) {
          const result = results.every((result) => {
            return result;
          });
          if(result) {
            this._calculateIndices();
            this._calculatDependencies(this.preActors.actors, ActorPhaseConst.PRE);
            this._calculatDependencies(this.execActors.actors, ActorPhaseConst.EXEC);
            this._calculatDependencies(this.postActors.actors, ActorPhaseConst.POST);
          }
          cb(result);
        }
      });
    }
  }
  
  /*executeInit() {
    this.nodes.forEach((node) => {
      if(node.actor) {
        node.actor.executeInit(this.debug);
      }
    });
  }*/
  
  _calculateIndices() {
    this.preActors.actors.forEach((actor) => {
      this.nodes.push({
        actor: actor,
        name: actor.logName,
        phaseId: ActorPhaseConst.PRE,
        typeId: actor.typeId,
        index: -1,
        columnIndex: -1
      });
    });
    this.execActors.actors.forEach((actor) => {
      this.nodes.push({
        actor: actor,
        name: actor.logName,
        phaseId: ActorPhaseConst.EXEC,
        typeId: actor.typeId,
        index: -1,
        columnIndex: -1
      });
    });
    if(0 !== this.executionContext.stageData.stagedSutNodes.length) {
      this.executionContext.stageData.stagedSutNodes.forEach((stagedSutNode) => {
        this.nodes.push({
          name: stagedSutNode.name,
          phaseId: ActorPhaseConst.EXEC,
          typeId: ActorTypeConst.REAL_SUT,
          index: -1,
          columnIndex: -1,
          criteriaType: stagedSutNode.criteriaType,
          criteria: stagedSutNode.criteria
        });
      });
    }
    else {
      this.nodes.push({
        name: this.executionContext.stageData.stagedSut,
        phaseId: ActorPhaseConst.EXEC,
        typeId: ActorTypeConst.REAL_SUT,
        index: -1,
        columnIndex: -1
      });
    }
    this.postActors.actors.forEach((actor) => {
      this.nodes.push({
        actor: actor,
        name: actor.logName,
        phaseId: ActorPhaseConst.POST,
        typeId: actor.typeId,
        index: -1,
        columnIndex: -1
      });
    });
    this._sortActors(this.nodes);
    let orderIndex = 0;
    let columnIndex = 0;
    this.nodes.forEach((node, index) => {
      if(ActorTypeConst.REAL_SUT !== node.typeId) {
        node.actor.setIndex(index);
        node.actor.setOrderIndex(orderIndex++);
        this.actors.push(node.actor);
      }
      node.index = index;
    });
    this.nodes = this.nodes.filter((node) => {
      return !(ActorTypeConst.COND === node.typeId && ActorPhaseConst.POST === node.phaseId);
    });
    this.nodes.forEach((node, index) => {
      node.columnIndex = index;
      if(ActorTypeConst.REAL_SUT !== node.typeId) {
        node.actor.setColumnIndex(index);
      }
    });
    this.preActors.setIndex();
    this.execActors.setIndex();
    this.postActors.setIndex();
  }
  
  _calculatDependencies(actors, phaseId) {
    let previusActors = [];
    actors.forEach((actor) => {
      if(0 === previusActors.length) {
        actor.setDependency(-1, phaseId);
      }
      else {
        if('serial' === actor.getSequence()) {
          this._dependOnPrevious(previusActors, actor, phaseId);
        }
        else if('parallel' === actor.getSequence()) {
          this._dependOnPreviousDependencies(previusActors, actor, phaseId);
        }
      }
      previusActors.push(actor);
    });
  }
  
  _dependOnPrevious(previusActors, actor, phaseId) {
    let index = -1;
    for(let i = previusActors.length - 1; i >= 0; --i) {
      index = previusActors[i].getIndexFromPhase(phaseId);
      break;
   }
   actor.setDependency(index, phaseId);
  }
  
  _dependOnPreviousDependencies(previusActors, actor, phaseId) {
    let indices = [-1];
    for(let i = previusActors.length - 1; i >= 0; --i) {
      indices = previusActors[i].getDependenciesFromPhase(phaseId);
      break;
    }
    indices.forEach((index) => {
      actor.setDependency(index, phaseId);
    });
  }
  
  _addActor(actor) {
    if(ActorTypeConst.COND === actor.typeId) {
      this.preActors.add(actor);
      this.postActors.addFront(actor);
    }
    else {
      switch(actor.phaseId) {
        case ActorPhaseConst.PRE:
          this.preActors.add(actor);
          break;
        case ActorPhaseConst.EXEC:
          this.execActors.add(actor);
          break;
        case ActorPhaseConst.POST:
          this.postActors.add(actor);
          break;
      }      
    }
  }
  
  _compareActors(a, b) {
    if(a < b) {
      return -1;
    }
    else if(b < a) {
      return 1;
    }
    else {
      return 0;
    }
  }
  
  _sortActors(actors) {
    actors.sort((a, b) => {
      const phase = this._compareActors(a.phaseId, b.phaseId);
      if(0 !== phase) {
        return phase;
      }
      else if(ActorPhaseConst.EXEC === a.phaseId) {
        const result = this._compareActors(a.typeId, b.typeId);
        return result;
      }
      else {
        return 0;
      }
    });
  }
  
  _loadFromFile(actorData) {
    const path = actorData.name.split('.');
    const file = `${ActorPathDist.getActorFile(path.join(Path.sep))}.js`;
    try {
      const factory = require(file);
      return new factory();
    }
    catch(err) {
      LOG_ERROR(Logger.ERROR.CATCH, `Could not create the Actor: '${actorData.name}'. File: '${file}'`, err, err.stack);
    }
    return null;
  }
  
  _loadFromString(actorData) {
    const path = actorData.name.split('.');
    const fileName = `${ActorPathDist.getActorFile(path.join(Path.sep))}.js`;
    const paths = Module._nodeModulePaths(Path.dirname(''));
    const parent = module.parent;
	  const m = new Module('', parent);
    m.filename = fileName;
	  m.paths = [].concat([]).concat(paths).concat([]);
    const code = actorData.inlineCode.replace(replaceCondition, (...parameters) => {
      const match = parameters.shift();
      return replaceHandle(match, parameters);
    });
	  m._compile(code, m.filename);
	  const exports = m.exports;
	  parent && parent.children && parent.children.splice(parent.children.indexOf(m), 1);
	  return new exports();
  }
}


const REPLACE_CONDITION = "(VERIFY_MANDATORY|VERIFY_OPTIONAL|VERIFY_VALUE|VERIFY_CONTENT_VALUE|VERIFY_CONTENT_MANDATORY|VERIFY_CONTENT_OPTIONAL|this.delay|this.delay|this.async\\(|this.callback\\(|this.setSharedData\\(|this.setSharedDataActor\\(|this.waitForSharedData\\(|this.waitForSharedDataActor\\(|this.setSharedExecutionData\\(|this.waitForSharedExecutionData\\(|this.getContent|this.getContentByName|this.closeConnection|this.createConnection|this.createServer|this.([^ ]*)Connection.send\\(|this.([^ ]*)Connection.sendLine\\(|this.([^ ]*)Connection.sendObject\\(|this.([^ ]*)Connection.sendHeader\\(|this.([^ ]*)Connection.sendRequestLine\\(|this.([^ ]*)Connection.sendHeaders\\(|this.([^ ]*)Connection.sendBody\\(|this.([^ ]*)Connection.receive\\(|this.([^ ]*)Connection.receiveLine\\(|this.([^ ]*)Connection.receiveObject\\(|this.([^ ]*)Connection.receiveSize\\(|this.([^ ]*)Connection.receiveHeader\\(|this.([^ ]*)Connection.receiveBody\\(|this.([^ ]*)Connection.accept\\(|this.([^ ]*)Connection.close\\(|this.([^ ]*)Connection.sendText\\(|this.([^ ]*)Connection.receiveText\\()";
const REPLACE_HANDLE = "\r\n  switch(parameters[0]) {\r\n    case 'VERIFY_MANDATORY':\r\n    case 'VERIFY_OPTIONAL':\r\n    case 'VERIFY_VALUE':\r\n    case 'VERIFY_CONTENT_VALUE':\r\n      return `this.${match}`;\r\n    case 'VERIFY_CONTENT_MANDATORY':\r\n    case 'VERIFY_CONTENT_OPTIONAL':\r\n      return `yield* this.${match}`;\r\n    case 'this.delay': \r\n    case 'this.async(': \r\n    case 'this.callback(': \r\n    case 'this.setSharedData(': \r\n    case 'this.setSharedDataActor(': \r\n    case 'this.waitForSharedData(': \r\n    case 'this.waitForSharedDataActor(': \r\n    case 'this.setSharedExecutionData(': \r\n    case 'this.waitForSharedExecutionData(': \r\n        case 'this.getContent': \r\n    case 'this.getContentByName': \r\n    case 'this.closeConnection': \r\n      return `yield* ${match}`;\r\n    case 'this.createConnection': \r\n    case 'this.createServer': \r\n      return `yield ${match}`;\r\n    case `this.${parameters[1]}Connection.send(`:\r\n    case `this.${parameters[2]}Connection.sendLine(`:\r\n    case `this.${parameters[3]}Connection.sendObject(`:\r\n    case `this.${parameters[4]}Connection.sendHeader(`:\r\n    case `this.${parameters[5]}Connection.sendRequestLine(`:\r\n    case `this.${parameters[6]}Connection.sendHeaders(`:\r\n    case `this.${parameters[7]}Connection.sendBody(`:\r\n    case `this.${parameters[8]}Connection.receive(`:\r\n    case `this.${parameters[9]}Connection.receiveLine(`:\r\n    case `this.${parameters[10]}Connection.receiveObject(`:\r\n    case `this.${parameters[11]}Connection.receiveSize(`:\r\n    case `this.${parameters[12]}Connection.receiveHeader(`:\r\n    case `this.${parameters[13]}Connection.receiveBody(`:\r\n    case `this.${parameters[14]}Connection.accept(`:\r\n    case `this.${parameters[15]}Connection.close(`:\r\n      case `this.${parameters[16]}Connection.sendText(`:\r\n      case `this.${parameters[17]}Connection.receiveText(`:\r\n      return `yield ${match}`;\r\n    default:\r\n      ddb.writeln('Error:', parameters[0]);\r\n      return match;\r\n  };\r\n";
const replaceCondition = new RegExp(REPLACE_CONDITION, 'g');
const replaceHandle = new Function('match', 'parameters', REPLACE_HANDLE);

    
module.exports = Actors;
