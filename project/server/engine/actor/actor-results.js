
'use strict';

const ActorState = require('../actor-state');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');


class ActorResults {
  constructor() {
    this.actorStateResult = [
      ActorResultConst.N_IMPL,
      ActorResultConst.N_IMPL,
      ActorResultConst.N_IMPL,
      ActorResultConst.N_IMPL,
      ActorResultConst.N_IMPL
    ];
  }
  
  getActorResultId() {
    return Math.max(...this.actorStateResult);
  }
  
  getResultId(actorStateId) {
    return this.actorStateResult[actorStateId];
  }
}

ActorResults.NAME = [
  'DATA',
  'INIT_SERVER',
  'INIT_CLIENT',
  'RUN',
  'EXIT'
];
module.exports = ActorResults;
