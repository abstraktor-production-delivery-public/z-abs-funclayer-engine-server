
'use strict';

const ActorState = require('../actor-state');
const Address = require('../../address/address');
const ActorResults = require('./actor-results');
const ActorStateTimeout = require('./actor-state-timeout');
const AsynchMixin = require('./actor-mixins/asynch-mixin');
const ContentMixin = require('./actor-mixins/content-mixin');
const LoggerMixin = require('./actor-mixins/logger-mixin');
const SharedDataMixin = require('./actor-mixins/shared-data-mixin');
const SharedExecutionDataMixin = require('./actor-mixins/shared-execution-data-mixin');
const TimerMixin = require('./actor-mixins/timer-mixin');
const TestDataMixin = require('./actor-mixins/test-data-mixin');
const VerifyMixinBase = require('./actor-mixins/verify-mixin-base');
const VerifyMixinMandatory = require('./actor-mixins/verify-mixin-mandatory');
const VerifyMixinOptional = require('./actor-mixins/verify-mixin-optional');
const VerifyMixinValue = require('./actor-mixins/verify-mixin-value');
const MessageTestCaseDebugActorIndex = require('z-abs-funclayer-engine-server/server/communication/messages/messages-w-to-s/message-test-case-debug-actor-index');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class Actor {
  constructor(inheritedType, actorParts, expectInterrupt=false) { 
    this.inheritedType = inheritedType; 
    this.expectInterrupt = expectInterrupt;
    this.name = '';
    this.index = -1;
    this.orderIndex = -1;
    this.columnIndex = -1;
    this.logName = '';
    this.logFileName = '';
    this.instanceIndex = 0;
    this.node = '';
    this.dependentOnIndices = [];
    this.type = '';
    this.src = [];
    this.dst = [];
    this.srv = [];
    this.execution = '';
    this.phaseId = -1;
    this.currentPhaseId = -1;
    this.typeId = 0;
    this.cbMessage = null;
    this.actorState = null;
    this.actorResults = null;
    this.cbActorStateDone = null;
    this.executionContext = null;
    this.tcData = null;
    this.config = null;
    this.slowAwaitTime = -1;
    this.testDataActor = new Map();
    this.verificationActor = new Map();
    this.nodes = [];
    this.currentGenerator = null;
    this.done = false;
    this.pendingIds = new Map();
    this.cbPendingCancel = null;
    this.interrupted = false;
    this.debug = false;
    this.stateTimeout = null;
    if(undefined !== actorParts) {
      let partIndex = 0;
      if(Array.isArray(actorParts)) {
        for(let i = 0; i < actorParts.length; ++i) {
          const actorPart = new actorParts[i]().init(this, ++partIndex);
          if(0 === actorPart.type) {
            if(undefined === this.preActorParts) {
              this.preActorParts = [actorPart];
            }
            else {
              this.preActorParts.push(actorPart);
            }
          }
          else {
            if(undefined === this.postActorParts) {
              this.postActorParts = [actorPart];
            }
            else {
              this.postActorParts.push(actorPart);
            }
          }
        }
      }
      else {
        const actorPart = new actorParts().init(this, ++partIndex);
        if(0 === actorPart.type) {
          this.preActorParts = [actorPart];
        }
        else {
          this.postActorParts = [actorPart];
        }
      }
    }
    this._next = null;
    this._debugData = null;
    this.process = process;
  }
  
  init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes) {
    this.name = actorData.name;
    this.index = -1;
    this.instanceIndex = instanceIndex;
    this.node = actorData.node;
    this.logName = `${this.name.substr(this.name.lastIndexOf('.') + 1)}[${this.instanceIndex}]`;
    this.logFileName = `${actorData.name.replace(/\./g, '/')}.js`;
    this.type = actorData.type;
    
    this._getAddress(this.src, actorData.src, Address.CLIENT);
    this._getAddress(this.dst, actorData.dst, Address.SUT);
    this._getAddress(this.srv, actorData.srv, Address.SERVER);
    this.typeId = ActorTypeConst.getId(actorData.type);
    this.execution = actorData.execution;
    this.cbMessage = cbMessage;
    this.executionContext = executionContext;
    this.tcData = tcData;
    this.config = config;
    this.slowAwaitTime = slowAwaitTime;
    this.nodes = nodes;
    this.actorState = new ActorState(this);
    this.actorResults = new ActorResults();
    if(undefined === actorData.phase || '' === actorData.phase) {
      if(ActorTypeConst.COND === this.typeId) {
        this.phaseId = ActorPhaseConst.PRE;
      }
      else {
        this.phaseId = ActorPhaseConst.EXEC;
      }
    }
    else {
      this.phaseId = ActorPhaseConst.getId(actorData.phase);
    }
    this._setTestDataActor('actor-name', this.logName, 'The name of the Actor.');
    this._setTestDatasActor(actorData.testData);
    this._setVerificationsActor(actorData.verification);
    
    return this.typeId === this.inheritedType;
  }
  
  executeInit(debug, process) {
    this.process = process;
    this.debug = debug;
    if(debug) {
      this._next = this.next;
      this.next = (value, err) => {
        if(this.debug && this.executionContext.executingActorIndex !== this.orderIndex) {
          this._debugData = {value, err};
          this.executionContext.setExecutingActorIndex(this.orderIndex);
          this.process.stall();
          this.cbMessage(new MessageTestCaseDebugActorIndex(this.orderIndex), null, this.debug);
        }
        else {
          return this._next(value, err);
        }
      }
    }
    this.initLoggerMixin();
    this.stateTimeout = new ActorStateTimeout(!debug ? null : process, !debug ? this.getTestDataNumber('tc-state-timeout', 5000) : this.getTestDataNumber('tc-state-debug-timeout', 90000));
  }
  
  debugContinue() {
    const _debugData = this._debugData;
    this._debugData = null;
    this.process.unstall();
    this._next(_debugData.value, _debugData.err);
  }
  
  setDependency(actorIndex) {
    this.dependentOnIndices.push(actorIndex);
  }
  
  getDependencies() {
    return this.dependentOnIndices;
  }
  
  getDependenciesFromPhase(phaseId) {
    return this.dependentOnIndices;
  }
  
  getName() {
    return this.name;
  }
  
  getPhaseLogNameState() {
    return `${ActorPhaseConst.getName(this.currentPhaseId)}.${this.logName}.${this.getState().getName()}`;
  }
  
  getIndex() {
    return this.index;
  }
  
  getIndexFromPhase(phaseId) {
    return this.index;
  }
  
  setIndex(index) {
    this.index = index;
  }
  
  getOrderIndex() {
    return this.orderIndex;
  }
  
  getOrderIndexFromPhase(phaseId) {
    return this.orderIndex;
  }
  
  setOrderIndex(index) {
    this.orderIndex = index;
  }
  
  setColumnIndex(index) {
    this.columnIndex = index;
  }
  
  getInstanceIndex() {
    return this.instanceIndex;
  }
    
  getSequence() {
    return this.execution;
  }
  
  getState() {
    return this.actorState;
  }
  
  getStateId() {
    return this.actorState.getActorStateId();
  }
  
  getStateName() {
    return this.actorState.getName();
  }
  
  getActorResultId() {
    return this.actorResults.getActorResultId();
  }
  
  getResultId() {
    return this.actorResults.getResultId(this.actorState.getActorStateId());
  }
  
  getStateResultId(state) {
    return this.actorResults.getResultId(state);
  }
  
  isSuccess() {
    return ActorResultConst.SUCCESS >= this.getActorResultId();
  }
  
  getResultName() {
    return ActorResultConst.getName(this.actorResults.getResultId(this.actorState.getActorStateId()));
  }
  
  getStateResultName(state) {
    return ActorResultConst.getName(this.getStateResultId(state));
  }
  
  setCurrentPhase(phaseId) {
    this.currentPhaseId = phaseId;
  }
    
  _getAddress(addresses, addressData, type) {
    if(undefined !== addressData && '' !== addressData) {
      try {
        const parsedAddressData = JSON.parse(addressData);
        if(Array.isArray(parsedAddressData)) {
          parsedAddressData.forEach((address) => {
            this._getAddress(addresses, address);
          });
        }
        else if(typeof parsedAddressData === 'object') {
          addresses.push({
            type: 1,
            value: new Address('hard coded', parsedAddressData.host, parsedAddressData.family, parsedAddressData.port, type, parsedAddressData.page, parsedAddressData.incognitoBrowser, parsedAddressData.uri)
          });
        }
      }
      catch(err) {
        addresses.push({
          type: 0,
          value: addressData
        });
      }
    }
  }
  
  setPending(cbCancel) {
    const pendingId = GuidGenerator.create();
    this.logEngine(() => `PENDING START: '${pendingId}'`, 'actor-pending', 'e8aa34cc-a49a-4827-969e-79140fb34ca6');
    this.pendingIds.set(pendingId, cbCancel);
    return pendingId;
  }
  
  deletePending(pendingId) {
    this.pendingIds.delete(pendingId);
    return 0 === this.pendingIds.size;
  }
  
  pendingSuccess(pendingId, value) {
    this.logEngine(() => `PENDING SUCCESS: '${pendingId}'`, 'actor-pending', '69c56938-9fac-4f21-8c32-ab0ba3997437');
    this.process.nextTick(() => {
      if(this.pendingIds.has(pendingId)) {
        if(this.deletePending(pendingId)) {
          this.next(value);
        }
      }
      else {
        this.logWarning(() => `No matching pendingId on success response. pendingId: '${pendingId}'`, 'core', 'd06dfd28-fdda-468a-99f5-c9b335614e5d');
      }
    });
  }
  
  pendingError(pendingId, err) {
    this.logEngine(() => `PENDING ERROR: '${pendingId}'`, 'actor-pending', '8d3d6e06-4277-48e5-a54f-394ec527dec3');
    this.process.nextTick(() => {
      if(this.pendingIds.has(pendingId)) {
        this.pendingIds.clear();
        this.next(undefined, err);
      }
      else {
        this.logWarning(() => `No matching pendingId on error response. pendingId: '${pendingId}'`, 'core', 'a1ccb550-7007-4eac-b020-af30799935be');
      }
    });
  }
  
  setCbActorStateDone(cbDone) {
    this.cbActorStateDone = cbDone;
  }
  
  getCbActorStateDone() {
    this.stateTimeout.stop();
    return this.cbActorStateDone;
  }
  
  next(value, err) {
    let result = null;
    if(this.done) {
      this.done = false;
      this._setActorResult(ActorResultConst.SUCCESS);
      this.getCbActorStateDone()(this);
    }
    try {
      if(err) {
        this.pendingIds.forEach((cbPendingCancel, pendingId) => {
          cbPendingCancel && cbPendingCancel();
          this.logEngine(() => `PENDING CANCEL: '${pendingId}' - ${cbPendingCancel ? '' : 'No cancel method implemented.'}`, 'actor-pending', 'f91be9d3-da8e-4096-ad97-1c803dd907d8');
        });
        this.pendingIds.clear();
        if('interrupted__asdf1234' === err.message) {
          this.interrupted = true;
          this._setActorResult(ActorResultConst.INTERRUPT);
          this.currentGenerator = null;
          return this.getCbActorStateDone()(this);
        }
        else if('timeout__asdf1234' === err.message) {
          this._setActorResult(ActorResultConst.TIMEOUT);
          this.currentGenerator = null;
          return this.getCbActorStateDone()(this);
        }
        else if('expected_interrupted__asdf1234' === err.message) {
          this._setActorResult(ActorResultConst.E_INTERRUPT);
          this.currentGenerator = null;
          return this.getCbActorStateDone()(this);
        }
        else {
          result = this.currentGenerator.throw(err);
        }
      }
      else {
        result = this.currentGenerator.next(value);
      }
    }
    catch(e) {
      this.pendingIds.forEach((cbPendingCancel, pendingId) => {
        cbPendingCancel && cbPendingCancel();
        this.logEngine(() => `PENDING CANCEL: '${pendingId}' - ${cbPendingCancel ? '' : 'No cancel method implemented.'}`, 'actor-pending', '452c6c1d-0025-4a3b-a981-7e7b5d5fd59c');
      });
      this.pendingIds.clear();
      this.done = false;
      if('verify__asdf1234' === e.message) {
        this._setActorResult(ActorResultConst.FAILURE);
      }
      else {
        this._setActorResult(ActorResultConst.ERROR);
        this.logError(e, '4d4f2872-b0e0-4d2f-bdfb-4dd89a981aac');
      }
      return this.getCbActorStateDone()(this);
    }
    if(result.done) {
      if(0 !== this.pendingIds.size) {
        this.logWarning('a pending without a yield.', 'core', '508421db-9d8c-46a3-83fa-bf7b977b1b9e');
        this.done = true;
      }
      else {
        this._setActorResult(ActorResultConst.SUCCESS);
        this.done = false;
        return this.getCbActorStateDone()(this);
      }
    }
    else if(0 === this.pendingIds.size) {
      this.logWarning('A yield without a pending.', 'core', '5a8aed39-766d-4134-a11b-d5609089de2d');
      this.process.nextTick(() => {
        this.next();
      });
    }
  }
  
  *partsRun() {
    if(undefined !== this.preActorParts) {
      for(let pre = 0; pre < this.preActorParts.length; ++pre) {
        yield* this.preActorParts[pre].run();
      }
    }
    if(undefined !== this.run) {
      yield* this.run();
    }
    if(undefined !== this.postActorParts) {
      for(let post = 0; post < this.postActorParts.length; ++post) {
        yield* this.postActorParts[post].run();
      }
    } 
  }
  
  executeState(generator) {
    this.currentGenerator = generator;
    this.process.nextTick(() => {
      if(ActorResultConst.INTERRUPT !== this.actorResults.actorStateResult[this.actorState.getActorStateId()]) {
        this.stateTimeout.start(() => {
          this.doTimeout();
        });
        this.next();
      }
    });
    return true;
  }
  
  doData() {
    this.logEngine('actor state: data', 'actor-state', 'c4675385-9403-4838-9d55-933cd551fd08');
    if(undefined === this.data) {
      return false;
    }
    return this.executeState(this.data());
  }

  doInitServer() {
    this.logEngine('actor state: initServer', 'actor-state', '5f9bfd4a-1d42-4a23-964d-5df3cabefa60');
    if(undefined === this.initServer) {
      return false;
    }
    return this.executeState(this.initServer());
  }
  
  doInitClient() {
    this.logEngine('actor state: initClient', 'actor-state', '381af64c-258a-452a-aed3-fe8857cdad9d');
    if(undefined === this.initClient) {
      return false;
    }
    return this.executeState(this.initClient());
  }
  
  doRun() {
    this.logEngine('actor state: run', 'actor-state', '7106fc36-a732-43ad-9cf0-e1b03a8a10b6');
    if(undefined === this.preActorParts && undefined === this.postActorParts) {
      if(undefined !== this.run) {
        return this.executeState(this.run());
      }
    }
    else {
      return this.executeState(this.partsRun());
    }
    return false;
  }
  
  doExit() {
    this.logEngine('actor state: exit', 'actor-state', '25b81886-fb3c-41d3-9262-92c7f9a21c35');
    if(undefined === this.exit) {
      return false;
    }
    return this.executeState(this.exit(this.interrupted));
  }
  
  doInterrupt(throwOrCancel) {
    this.logWarning('Interrupt', 'core', '7941f7ad-8717-452b-a45d-c83b7babe0f1');
    if(throwOrCancel) {
      this.next(undefined, new Error('interrupted__asdf1234'));
    }
  }
  
  doTimeout() {
    this.logWarning('Timeout', 'core', '1184e77d-8cb2-41af-9964-c2eba3185bf9');
    this.next(undefined, new Error('timeout__asdf1234'));
  }
  
  doExpectedInterrupt() {
    this.next(undefined, new Error('expected_interrupted__asdf1234'));
  }
  
  _setActorResult(actorResult) {
    this.actorResults.actorStateResult[this.actorState.getActorStateId()] = actorResult;
  }
}

Actor.instanceId = 0;

Object.assign(Actor.prototype, AsynchMixin);
Object.assign(Actor.prototype, ContentMixin);
Object.assign(Actor.prototype, SharedDataMixin);
Object.assign(Actor.prototype, SharedExecutionDataMixin);
Object.assign(Actor.prototype, LoggerMixin);
Object.assign(Actor.prototype, TimerMixin);
Object.assign(Actor.prototype, VerifyMixinBase);
Object.assign(Actor.prototype, VerifyMixinMandatory);
Object.assign(Actor.prototype, VerifyMixinOptional);
Object.assign(Actor.prototype, VerifyMixinValue);
Object.assign(Actor.prototype, TestDataMixin);


module.exports = Actor;
