
'use strict';


const PartAsynchMixin = {
  *all(array) {
    yield* this.parent.all(array);
  },
  *asyncMethod(method) {
    yield* this.parent.asyncMethod(method);
  },
  handlePendingIdSuccess(pendingId, value) {
    this.parent.handlePendingIdSuccess(pendingId, value);
  },
  handlePendingIdError(pendingId, err) {
    this.parent.handlePendingIdError(pendingId, err);
  },
  handlePendingId(pendingId, err, value) {
    this.parent.handlePendingId(pendingId, err, value);
  }
};

module.exports = PartAsynchMixin;
