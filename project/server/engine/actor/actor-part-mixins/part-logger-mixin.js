
'use strict';


const PartLoggerMixin = {
  createLogMessage(message, inners, data, dataBuffers) {
    this.parent.createLogMessage(message, inners, data, dataBuffers);
  },
  logEngine(message, group, guid, depth=0) {
    this.parent.logEngine(message, group, guid, depth + 1);
  },
  logDebug(message, group, guid, depth=0) {
    this.parent.logDebug(message, group, guid, depth + 1);
  },
  logError(errOrMsg, guid, depth=0) {
    this.parent.logError(errOrMsg, guid, depth + 1);
  },
  logWarning(message, group, guid, depth=0) {
    this.parent.logWarning(message, group, guid, depth + 1);
  },
  logIp(log, group, guid, depth=0) {
    this.parent.logIp(log, group, guid, depth + 1);
  },
  logGui(message, group, guid, depth=0) {
    this.parent.logGui(message, group, guid, depth + 1);
  },
  logVerifySuccess(message, guid, depth=0) {
    this.parent.logVerifySuccess(message, guid, depth + 1);
  },
  logVerifyFailure(message, guid, depth=0) {
    this.parent.logVerifyFailure(message, guid, depth + 1);
  },
  logTestData(message, group, guid, depth=0) {
    this.parent.logTestData(message, group, guid, depth + 1);
  }
};

module.exports = PartLoggerMixin;
