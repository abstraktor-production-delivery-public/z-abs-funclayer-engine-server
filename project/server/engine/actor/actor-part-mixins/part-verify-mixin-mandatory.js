
'use strict';


const PartVerifyMixinMandatory = {
  VERIFY_MANDATORY(expectedKey, actualValue, index1, index2) {
    this.parent.VERIFY_MANDATORY(expectedKey, actualValue, index1, index2);
  },
  *VERIFY_CONTENT_MANDATORY(expectedKey, actualSize, actualContent) {
    yield* this.parent.VERIFY_CONTENT_MANDATORY(expectedKey, actualSize, actualContent);
  }
};

module.exports = PartVerifyMixinMandatory;
