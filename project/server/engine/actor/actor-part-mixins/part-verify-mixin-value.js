
'use strict';


const PartVerifyMixinValue = {
  VERIFY_VALUE(expectedValue, actualValue, description, operation = '===') {
    this.parent.VERIFY_VALUE(expectedValue, actualValue, description, operation);
  },
  VERIFY_CONTENT_VALUE(expectedContent, actualSize, actualContent, description, operation = '===') {
    this.parent.VERIFY_CONTENT_VALUE(expectedContent, actualSize, actualContent, description, operation);
  }
};

module.exports = PartVerifyMixinValue;
