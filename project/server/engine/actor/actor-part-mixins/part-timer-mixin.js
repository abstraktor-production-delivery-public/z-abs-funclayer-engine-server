
'use strict';


const PartTimerMixin = {
  *asyncMethod(method) {
    yield* this.parent.asyncMethod(method);
  }
};

module.exports = PartTimerMixin;
