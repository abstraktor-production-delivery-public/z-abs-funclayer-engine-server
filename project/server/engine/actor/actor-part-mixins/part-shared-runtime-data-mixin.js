
'use strict';


const SharedRuntimeDataMixin = {
  setSharedData(key, value) {
    this.parent.setSharedData(key, value);
  },
  getSharedData(key, defaultValue) {
    return this.parent.getSharedData(key, defaultValue);
  },
  *waitForSharedData(key) {
    yield* this.parent.waitForSharedData(key);
  },
  *waitForSharedDataValue(key, value) {
    yield* this.parent.waitForSharedDataValue(key, value);
  },
  getSharedDataActor(key, name, index) {
    return this.parent.getSharedDataActor(key, name, index);
  },
  setSharedDataActor(key, value, name, index) {
    this.parent.getSharedDataActor(key, value, name, index);
  },
  setSharedDataExecution(key, value) {
    this.parent.setSharedDataExecution(key, value);
  },
  getSharedDataExecution(key, defaultValue) {
    this.parent.getSharedDataExecution(key, defaultValue);
  }
};

module.exports = SharedRuntimeDataMixin;
