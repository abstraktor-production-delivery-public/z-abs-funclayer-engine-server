
'use strict';


const PartVerifyMixinOptional = {
  VERIFY_OPTIONAL(expectedKey, actualValue) {
    this.parent.VERIFY_OPTIONAL(expectedKey, actualValue);
  },
  *VERIFY_CONTENT_OPTIONAL(expectedKey, actualSize, actualContent) {
    yield* this.parent.VERIFY_CONTENT_OPTIONAL(expectedKey, actualSize, actualContent);
  }
};

module.exports = PartVerifyMixinOptional;
