
'use strict';

const Actor = require('./actor');
const ProcessMixin = require('./actor-mixins/process-mixin');
const StackMixinClient = require('./actor-mixins/stack-mixin-client');
const StackMixin = require('./actor-mixins/stack-mixin');
const ActorResults = require('./actor-results');
const ActorState = require('../actor-state');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');
const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorCondition extends Actor {
  constructor(actorParts) {
    super(ActorTypeConst.COND, actorParts);
    this.postDependentOnIndices = [];
    this.postActorState = null;
    this.postActorResults = null;
    this.postIndex = -1;
    this.postOrderIndex = -1;
    this.cbPostActorStateDone = null;
  }
  
  init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes) {
    if('' === actorData.execution) {
      actorData.execution = 'serial';
    }
    this.postActorState = new ActorState(this);
    this.postActorResults = new ActorResults();
    return super.init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes);
  }
  
  setDependency(actorIndex, phaseId) {
    if(ActorPhaseConst.PRE === phaseId) {
      this.dependentOnIndices.push(actorIndex);
    }
    else if(ActorPhaseConst.POST === phaseId) {
      this.postDependentOnIndices.push(actorIndex); 
    }
  }
  
  getDependencies() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.dependentOnIndices;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postDependentOnIndices; 
    }
  }
  
  getDependenciesFromPhase(phaseId) {
    if(ActorPhaseConst.PRE === phaseId) {
      return this.dependentOnIndices;
    }
    else if(ActorPhaseConst.POST === phaseId) {
      return this.postDependentOnIndices; 
    }
  }
  
  getIndex() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.index;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postIndex;
    }
  }
  
  getIndexFromPhase(phaseId) {
    if(ActorPhaseConst.PRE === phaseId) {
      return this.index;
    }
    else if(ActorPhaseConst.POST === phaseId) {
      return this.postIndex;
    }
  }
  
  setIndex(index) {
    if(-1 === this.index) {
      this.index = index;
    }
    else {
      this.postIndex = index;
    }
  }
  
  getOrderIndexFromPhase(phaseId) {
    if(ActorPhaseConst.PRE === phaseId) {
      return this.orderIndex;
    }
    else if(ActorPhaseConst.POST === phaseId) {
      return this.postOrderIndex;
    }
  }
  
  getOrderIndex() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.orderIndex;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postOrderIndex;
    }
  }
  
  setOrderIndex(index) {
    if(-1 === this.orderIndex) {
      this.orderIndex = index;
    }
    else {
      this.postOrderIndex = index;
    }
  }
  
  setCbActorStateDone(cbDone) {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      this.cbActorStateDone = cbDone;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      this.cbPostActorStateDone = cbDone;
    }
  }
  
  getCbActorStateDone() {
    const duration = this.stateTimeout.stop();
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.cbActorStateDone;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.cbPostActorStateDone;
    }
  }
  
  getState() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.actorState;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postActorState;
    }
  }
  
  getStateId() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.actorState.getActorStateId();
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postActorState.getActorStateId();
    }
  }
  
  getStateName() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.actorState.getName();
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postActorState.getName();
    }
  }
  
  getActorResultId(phase) {
    phase = undefined === phase ? this.currentPhaseId : phase;
    if(ActorPhaseConst.PRE === phase) {
      return this.actorResults.getActorResultId();
    }
    else if(ActorPhaseConst.POST === phase) {
      return this.postActorResults.getActorResultId();
    }
  }
  
  getResultId() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.actorResults.getResultId(this.actorState.getActorStateId());
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postActorResults.getResultId(this.postActorState.getActorStateId());
    }
  }
  
  getStateResultId(state) {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return this.actorResults.getResultId(state);
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return this.postActorResults.getResultId(state);
    }
  }
  
  isSuccess() {
    return ActorResultConst.SUCCESS >= this.actorResults.getActorResultId() && ActorResultConst.SUCCESS >= this.postActorState.getActorResultId();
  }
  
  getResultName() {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      return ActorResultConst.getName(this.actorResults.getResultId(this.actorState.getActorStateId()));
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      return ActorResultConst.getName(this.postActorResults.getResultId(this.postActorState.getActorStateId()));
    }
  }
  
  getStateResultName(state, index) {
    if(undefined === index) {
      return ActorResultConst.getName(this.getStateResultId(state));
    }
    else {
      if(index === this.index) {
        return ActorResultConst.getName(this.actorResults.getResultId(state));
      }
      else if(index === this.postIndex) {
        return ActorResultConst.getName(this.postActorResults.getResultId(state));
      }
    }
  }
  
  _setActorResult(actorResult) {
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      this.actorResults.actorStateResult[this.actorState.getActorStateId()] = actorResult;
    }
    else if(ActorPhaseConst.POST === this.currentPhaseId) {
      this.postActorResults.actorStateResult[this.postActorState.getActorStateId()] = actorResult;
    }
  }
  
  doInitClient() {
    this.logEngine('actor state: initClient', 'actor-state', '8759f7cf-a5b1-4577-b60e-3765c95149fb');
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      if(undefined === this.initClientPre) {
        return false;
      }
      return this.executeState(this.initClientPre());
    }
    else {
      if(undefined === this.initClientPost) {
        return false;
      }
      return this.executeState(this.initClientPost());
    }
    return false;
  }
  
  doRun() {
    this.logEngine('actor state: run', 'actor-state', '646e0d00-ef7b-49b0-af84-abbc572f64de');
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      if(undefined === this.runPre) {
        return false;
      }
      return this.executeState(this.runPre());
    }
    else {
      if(undefined === this.runPost) {
        return false;
      }
      return this.executeState(this.runPost());
    }
    return false;
  }
  
  doExit() {
    this.logEngine('actor state: exit', 'actor-state', '458f76f9-6b1f-4e37-979b-7573f7aa0304');
    if(ActorPhaseConst.PRE === this.currentPhaseId) {
      if(undefined === this.exitPre) {
        return false;
      }
      return this.executeState(this.exitPre());
    }
    else {
      if(undefined === this.exitPost) {
        return false;
      }
      return this.executeState(this.exitPost());
    }
    return false;
  }
}

Object.assign(ActorCondition.prototype, ProcessMixin);
Object.assign(ActorCondition.prototype, StackMixinClient);
Object.assign(ActorCondition.prototype, StackMixin);


module.exports = ActorCondition;
