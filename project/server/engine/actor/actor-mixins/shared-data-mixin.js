
'use strict';

const CancelObject = require('../../../stack/stacks/cancel-object');


const SharedDataMixin = {
  *setSharedData(key, value) {
    const sharedDataobject = this.tcData.sharedRuntimeData.set(key, value);
    this._handlePendings(key, sharedDataobject);
    const cancelObject = new CancelObject();
    const pendingId = this.setPending(() => {
      cancelObject.set();
    });
    process.nextTick(() => {
      if(!cancelObject.cancel) {
        this.handlePendingIdSuccess(pendingId, value);
      }
    });
    return yield;
  },
  setSharedDataSync(key, value) {
    const sharedDataobject = this.tcData.sharedRuntimeData.set(key, value);
    this._handlePendings(key, sharedDataobject);
    return value;
  },
  getSharedData(key, defaultValue) {
    const sharedData = this.tcData.sharedRuntimeData.get(key);
    if(undefined !== sharedData) {
      return sharedData.value;
    }
    else {
      return defaultValue;
    }
  },
  *waitForSharedData(key, expectedValue) {
    const value = this.tcData.sharedRuntimeData.wait(key, this, expectedValue, () => {
      const cancelObject = new CancelObject();
      const pendingId = this.setPending(() => {
        cancelObject.set();
      });
      return {
        id: pendingId,
        cancelObject: cancelObject
      };
    });
    if(null !== value) {
      return value;
    }
    else {
      return yield;
    }
  },
  *setSharedDataActor(key, value, name, index) {
    if(undefined === name) {
      return yield* this.setSharedData(`${this.name}[${this.instanceIndex}].${key}`, value);
    }
    else {
      return yield* this.setSharedData(`${name}[${index}].${key}`, value);
    }
  },
  setSharedDataActorSync(key, value, name, index) {
    if(undefined === name) {
      return this.setSharedDataSync(`${this.name}[${this.instanceIndex}].${key}`, value);
    }
    else {
      return this.setSharedDataSync(`${name}[${index}].${key}`, value);
    }
  },
  getSharedDataActor(key, name, index) {
    if(undefined === name) {
      return this.getSharedData(`${this.name}[${this.instanceIndex}].${key}`);
    }
    else {
      return this.getSharedData(`${name}[${index}].${key}`);
    }
  },
  *waitForSharedDataActor(key, expectedValue) {
    if(undefined === name) {
      yield* this.waitForSharedData(`${this.name}[${this.instanceIndex}].${key}`, expectedValue);
    }
    else {
      yield* this.waitForSharedData(`${name}[${index}].${key}`, expectedValue);
    }
  },
  _handlePendings(key, sharedDataobject) {
    let found = true;
    while(found) {
      const index = sharedDataobject.pendings.findIndex((pending) => {
        return undefined === pending.expectedValue ? true : sharedDataobject.value === pending.expectedValue;
      });
      if(-1 !== index) {
        const sharedPendingData = sharedDataobject.pendings[index];
        sharedDataobject.pendings.splice(index, 1);
        process.nextTick(() => {
          if(!sharedPendingData.cancelObject.cancel) {
            sharedPendingData.actor.handlePendingIdSuccess(sharedPendingData.pendingId, sharedDataobject.value);
          }
        });
      }
      else {
        return;
      }
    }
  }
};


module.exports = SharedDataMixin;
