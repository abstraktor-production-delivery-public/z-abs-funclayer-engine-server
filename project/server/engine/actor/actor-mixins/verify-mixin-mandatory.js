
'use strict';

const ContentUndefined = require('../../data/content-undefined');


const VerifyMixinMandatory = {
  VERIFY_MANDATORY(expectedKey, actualValue, index1, index2) {
    const expected = this._getVerificationData(expectedKey);
    const mandatoryText = `Mandatory['${expectedKey}']`;
    if(undefined === expected) {
      this.logVerifyFailure(() => `${mandatoryText} is not set.`, '13ab504d-0d33-43c7-94f8-7cac8f009895');
      throw new Error('verify__asdf1234');
    }
    else {
      this._verify(mandatoryText, expected, actualValue, index1, index2);
    }
  },
  *VERIFY_CONTENT_MANDATORY(expectedKey, actualSize, actualContent) {
    const expected = this._getVerificationData(expectedKey);
    const mandatoryText = `Mandatory Content['${expectedKey}']`;
    if(undefined === expected) {
      this.logVerifyFailure(() => `${mandatoryText} is not set.`, '133a74d9-0c8b-4899-80f4-86c88d88dba8');
      throw new Error('verify__asdf1234');
    }
    else if('value' !== expected.type || 'string' !== expected.valueType) {
      this.logVerifyFailure(() => `${mandatoryText} the value must be the content key of type string`, '9665350f-de80-4bd2-8e3d-6809ca2c7c55');
      throw new Error('verify__asdf1234');
    }
    else {
      const expectedContent = yield* this.getContentByName(expected.value);
      if(expectedContent instanceof ContentUndefined) {
        this.logVerifyFailure(() => `${mandatoryText} is not set.`, 'e1e040eb-8b5b-4ef6-a169-bfd9d3a823ed');
        throw new Error('verify__asdf1234');
      }
      let result = this._verifyValue(expectedContent.size, actualSize, expected.operation, `Mandatory Content['${expectedKey}' is '${expected.value}'] size:`);
      result = this._verifyContent(expectedContent, actualContent, expected.operation, `Mandatory Content['${expectedKey}' is '${expected.value}'] buffer`) && result;
      if(!result) {
        throw new Error('verify__asdf1234');
      }
    }
  }
};

module.exports = VerifyMixinMandatory;
