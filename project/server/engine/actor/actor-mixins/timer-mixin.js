
'use strict';

const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


const TimerMixin = {
  *delay(milliSeconds) {
    if(0 <= milliSeconds) {
      let cancel = false;
      const pendingId = this.setPending(() => {
        cancel = true;
      });
      yield this._setTimeout(() => {
        if(!cancel) {
          this.handlePendingIdSuccess(pendingId);
        }
      }, milliSeconds, process.hrtime.bigint());
    }
  },
  _setTimeout(cb, milliSeconds, timestamp) {
    const executeStartTimestamp = process.hrtime.bigint();
    const newMilliSeconds = HighResolutionTimestamp.getMilliseconds(executeStartTimestamp) - HighResolutionTimestamp.getMilliseconds(timestamp);
    const timeLeft = milliSeconds - newMilliSeconds;
    if(0 < timeLeft) {
      if(12 >= timeLeft) {
        process.nextTick(() => {
          this._setTimeout(cb, milliSeconds, timestamp);
        });
        return;
      }
      else {
        const timeoutObj = setTimeout(() => {
          this._setTimeout(cb, milliSeconds, timestamp);
        }, timeLeft - 12 + 2);
        return;
      }
    }
    else {
      cb();
    }
  }
};

module.exports = TimerMixin;
