
'use strict';

const Address = require('../../../address/address');
const NetworkType = require('../../../stack/network/network-type');


const StackMixinClient = {
  createConnection(stackName, options) {
    const optionsConnection = {
      connectionOptions: null,
      transportLayer: {
        transportType: null,
        transportProperties: null
      },
      messageSelector: null,
      connectionWorker: null,
      stackContentCache: this.executionContext.stackContentCache
    };
    let dstIndex = 0;
    let srcIndex = 0;
    let reuse = false;
    this.getTransportLayerData(optionsConnection, options);
    if(undefined !== options) {
      if(undefined != options.dstIndex) {
        dstIndex = options.dstIndex;
      }
      if(undefined !== options.srcIndex) {
        srcIndex = options.srcIndex;
      }
      if(undefined !== options.reuse) {
        reuse = options.reuse;
      }
      else {
        reuse = this.getTestDataBoolean(`${stackName}-reuse`, reuse);
      }
      if(undefined !== options.messageSelector) {
        optionsConnection.messageSelector = options.messageSelector;
      }
      if(undefined !== options.connectionWorker) {
        optionsConnection.connectionWorker = options.connectionWorker;
      }
      if(undefined !== options.connectionOptions) {
        optionsConnection.connectionOptions = options.connectionOptions;
      }
    }
    else {
      reuse = this.getTestDataBoolean(`${stackName}-reuse`, reuse);
    }
    const srcAddress = this.getSrcAddress(srcIndex);
    const dstAddress = this.getDstAddress(dstIndex);
    this.executionContext.clientManager.getConnection(stackName, this, reuse, srcAddress, dstAddress, optionsConnection);
  },
  switchProtocol(stackName, currentConnection) {
    return this.executionContext.clientManager.switchProtocol(stackName, currentConnection, this);
  },
  getSrcAddress(srcIndex = 0) {
    // TODO: add link to help!!!
    if(0 === this.src.length) {
      // TODO: add link to help.
      return null;
    }
    else if(srcIndex >= this.src.length) {
      throw new Error(`No source address name with index: ${srcIndex}. Source address names: ${JSON.parse(this.src)}`);
    }
    const addressData = this.src[srcIndex];
    if(0 === addressData.type) {
      return this._getStackAddress((addressName) => {
        const srcAddress = this.executionContext.addresses.getSrc(addressName);
        if(srcAddress) {
          return srcAddress;
        }
        else if('DEFAULT_SRC' === addressName) {
          return new Address(addressName, null, null, null, 'client', 'default-page', null);
          }
        else {
          return null;
        }
      }, addressData.value);
    }
    else {
      return addressData.value;
    }
  },
  getDstAddress(dstIndex = 0) {
    // TODO: add link to help!!!
    if(0 === this.dst.length) {
      // TODO: add link to help.
      return null;
    }
    else if(dstIndex >= this.dst.length) {
      throw new Error(`No destination address name with index: ${dstIndex}. Destination address names: ${JSON.parse(this.dst)}`);
    }
    const addressData = this.dst[dstIndex];
    if(0 === addressData.type) {
      return this._getStackAddress((addressName) => {
        if(!Array.isArray(addressName)) {
          return this.executionContext.addresses.getDst(addressName);
        }
        else {
          const addresses = [];
          for(let i = 0; i < addressName.length; ++i) {
            const address = this.executionContext.addresses.getDst(addressName[i]);
            if(undefined === address) {
              return;
            }
            addresses.push(address);
          }
          return addresses;
        }
      }, addressData.value);
    }
    else {
      return addressData.value;
    }
  }
};

module.exports = StackMixinClient;
