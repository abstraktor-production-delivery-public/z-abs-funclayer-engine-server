
'use strict';

const NetworkType = require('../../../stack/network/network-type');


const StackMixin = {
  *closeConnection(connection) {
    if(connection) {
      yield connection.close(false);
    }
  },
  _getStackAddress(cbGetAddresses, addressName) {
    const srvAddress = cbGetAddresses(addressName);
    if(!srvAddress) {
      if('' === addressName) {
        throw new Error(`There are no default address name.`);
      }
      else {
        throw new Error(`There are no address with address name '${addressName}'. Have you forgot to stage or define the address?`);
      }
    }
    return srvAddress;
  },
  getTransportLayerData(optionsConnection, options) {
    const transportLayer = optionsConnection.transportLayer;
    if(options) {
      if(options.networkType) {
        transportLayer.transportType = options.networkType;
      }
      if(options.transportProperties) {
        transportLayer.transportProperties = new Map(options.transportProperties);
      }
    }
    if(!transportLayer.transportType) {
      transportLayer.transportType = this.getTestDataNumber('transport-type', 'default', {
        default: NetworkType.DEFAULT,
        tcp: NetworkType.TCP,
        udp: NetworkType.UDP,
        mc: NetworkType.MC,
        tls: NetworkType.TLS
      });
    }
    if(!transportLayer.transportProperties) {
      const transportProperties = this.getTestDataArrayStrings('transport-properties', []);
      if(0 < transportProperties.length) {
        transportLayer.transportProperties = new Map(transportProperties);
      }
    }
  }
};

module.exports = StackMixin;
