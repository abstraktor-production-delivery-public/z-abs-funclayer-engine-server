
'use strict';

const TestOutput = require('../../test-output');
const MessageTestCaseLog = require('../../../communication/messages/messages-s-to-c/message-test-case-log');
const LogType = require('z-abs-funclayer-engine-cs/clientServer/log/log-type');
const LogInner = require('z-abs-funclayer-engine-cs/clientServer/log/log-inner');
const LogPartError = require('z-abs-funclayer-engine-cs/clientServer/log/log-part-error');
const LogMessage = require('z-abs-funclayer-engine-cs/clientServer/log/log-message');
const LogSourceType = require('z-abs-funclayer-engine-cs/clientServer/log/log-source-type');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');
const Path = require('path');


const LoggerMixin = {
  initLoggerMixin() {
    const logTypeDefault = {
      done: true,
      result: false,
      value: ''
    };
    this._logDebugType = logTypeDefault;
    this._logEngineType = logTypeDefault;
    this._logIpType = logTypeDefault;
    this._logGuiType = logTypeDefault;
    this._logTestDataType = logTypeDefault;
    this._logBrowserLogType = logTypeDefault;
    this._logBrowserErrType = logTypeDefault;
    
    this._logDebugType = this._readGroupLog('log-type-debug', 'none');
    this._logEngineType = this._readGroupLog('log-type-engine', 'none');
    this._logErrorType = this._readLog('log-type-error', 'all');
    this._logIpType = this._readGroupLog('log-type-ip', 'none');
    this._logGuiType = this._readGroupLog('log-type-gui', 'none');
    this._logTestDataType = this._readGroupLog('log-type-test-data', 'none');
    this._logBrowserLogType = this._readGroupLog('log-type-browser-log', 'none');
    this._logBrowserErrType = this._readGroupLog('log-type-browser-err', 'none');
    this._logVerifySuccessType = this._readLog('log-type-success', 'all');
    this._logVerifyFailureType = this._readLog('log-type-failure', 'all');
    this._logWarningType = this._readLog('log-type-warning', 'all');
  },
  logEngine(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logEngine-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogEngine(group)) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logEngine, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.ENGINE, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logEngine(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logDebug(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logDebug-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogDebug(group)) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);   
        const logInfo = this._getLogInfo(this.logDebug, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.DEBUG, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logDebug(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logError(errOrMsg, guid, depth=0) {
    /*if(!guid) {
      ddb.info('logError');
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogError()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        let logMessage;
        let inners;
        if(typeof errOrMsg === 'string') {
          logMessage = errOrMsg;
        }
        else if(typeof errOrMsg === 'function') {
          const logErrOrMsg = errOrMsg();
          if(logErrOrMsg instanceof Error) {
            const errorData = this._handleError(logErrOrMsg);
            inners = new LogInner(new LogPartError(errorData));
            logMessage = errorData.error;
          }
          else {
            logMessage = logErrOrMsg;
          }
        }
        else if(errOrMsg instanceof Error) {
          const errorData = this._handleError(errOrMsg);
          inners = new LogInner(new LogPartError(errorData));
          logMessage = errorData.error;
        }
        else {
          logMessage = 'UNKNOWN ERROR TYPE';
        }
        const logInfo = this._getLogInfo(this.logError, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.ERROR, time, this.logName, this.logFileName, logMessage, '', logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, inners);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logError(time, logInfo.fileName, logInfo.lineNumber, logMessage);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logWarning(message, group, guid, depth=0) {
    /*if(!guid) {
      ddb.info('logDebug-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogWarning()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logWarning, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.WARNING, time, this.logName, this.logFileName, logObject.message, '', logInfo.fileName, logInfo.lineNumber, logInfo.sourceType);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logWarning(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logIp(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logIp, group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogIp()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logIp, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.IP, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, logObject.inners, logObject.data);            
          this.cbMessage(msg, logObject.dataBuffers, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logIp(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logGui(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logGui-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogGui(group)) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logGui, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.GUI, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, logObject.inners, logObject.data);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logGui(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logVerifySuccess(message, guid, depth=0) {
    /*if(!guid) {
      ddb.info('logVerifySuccess', ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogVerifySuccess()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logVerifySuccess, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.VERIFY_SUCCESS, time, this.logName, this.logFileName, logObject.message, '', logInfo.fileName, logInfo.lineNumber, logInfo.sourceType);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logVerifySuccess(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logVerifyFailure(message, guid, depth=0) {
    /*if(!guid) {
      ddb.info('logVerifyFailure', ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogVerifyFailure()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logVerifyFailure, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.VERIFY_FAILURE, time, this.logName, this.logFileName, logObject.message, 'ALL', logInfo.fileName, logInfo.lineNumber, logInfo.sourceType);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logVerifyFailure(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logTestData(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logTestData-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogTestData()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logTestData, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.TEST_DATA, time, this.logName, this.logFileName, logObject.message, group ? group : 'ALL', logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, logObject.inners);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logTestData(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logBrowserLog(message, group, guid, depth=0) {
    /*if(!group || !guid) {
      ddb.info('logBrowserLog-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogBrowserLog()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logBrowserLog, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.BROWSER_LOG, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, logObject.inners);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logBrowserLog(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  logBrowserErr(message, group, guid, depth=0) {
    /*if(!guid) {
     ddb.info('logBrowserErr-group:', group, ', guid:', guid);
    }*/
    const outputs = this.executionContext.outputs;
    if(0 !== outputs.chosen) {
      if(this.isLogBrowserErr()) {
        const time = HighResolutionTimestamp.getHighResolutionDate();
        const logObject = this._getLogObject(message);
        const logInfo = this._getLogInfo(this.logBrowserErr, guid, depth);
        if(((TestOutput.LOG_CLIENT & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClient)) || ((TestOutput.LOG_CLIENT_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenClientConsole))) {
          const msg = new MessageTestCaseLog(LogType.BROWSER_ERR, time, this.logName, this.logFileName, logObject.message, group, logInfo.fileName, logInfo.lineNumber, logInfo.sourceType, logObject.inners);
          this.cbMessage(msg, null, this.debug);
        }
        if((TestOutput.LOG_CONSOLE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenConsole)) {
          ddb.logBrowserErr(time, logInfo.fileName, logInfo.lineNumber, logObject.message);
        }
        if((TestOutput.LOG_FILE & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenFile)) {
          
        }
        if((TestOutput.LOG_PLUGIN & outputs.chosen) && (TestOutput.LOG_DETAIL_LOG & outputs.chosenPlugin)) {
          
        }
      }
    }
  },
  createLogMessage(message, inners, data, dataBuffers) {
    return new LogMessage(message, inners, data, dataBuffers);
  },
  _handleError(error) {
    const rawLog = error.stack;
    const parts = rawLog.split('\n    at ');
    const errorPart = parts.shift();
    const errorData = {
      error: errorPart,
      rows: []
    };
    parts.forEach((part) => {
      const subParts = part.split(' ');
      let func = '';
      let matchFileName = '';
      if(2 == subParts.length) {
        func = subParts[0];
        const match = subParts[1];
        matchFileName = match.substring(1, match.length - 1);
      }
      else if(1 == subParts.length) {
        matchFileName = subParts[0];
      }
      
      if(matchFileName.startsWith(LoggerMixinStatics.ServerPath)) {
        const fileNameParts = matchFileName.substring(LoggerMixinStatics.ServerPathLength).split(':');
        errorData.rows.push({
          func: func,
          fileName: fileNameParts[0],
          lineNumber: fileNameParts[1],
          sourceType: LogSourceType.SERVER,
          ref: true
        });
      }
      else if(matchFileName.startsWith(LoggerMixinStatics.ActorPath)) {
        const fileNameParts = matchFileName.substring(LoggerMixinStatics.ActorPathLength).split(':');
        errorData.rows.push({
          func: func,
          fileName: fileNameParts[0],
          lineNumber: fileNameParts[1],
          sourceType: LogSourceType.ACTOR,
          ref: true
        });
      }
      else if(matchFileName.startsWith(LoggerMixinStatics.StackPath)) {
        const fileNameParts = matchFileName.substring(LoggerMixinStatics.StackPathLength).split(':');
        errorData.rows.push({
          func: func,
          fileName: fileNameParts[0],
          lineNumber: fileNameParts[1],
          sourceType: LogSourceType.STACK,
          ref: true
        });
      }
      else {
        errorData.rows.push({
          func: subParts[0],
          fileName: matchFileName,
          lineNumber: -1,
          sourceType: LogSourceType.ACTOR,
          ref: false
        });
      }
    });
    return errorData;
  },
  _getStackTrace(stackDepth, structuredStackTrace) {
    const stack = structuredStackTrace[stackDepth];
    const lineNumber = stack.getLineNumber();
    if(null !== lineNumber) {
      const mName = stack.getMethodName();
      let fileName = stack.getFileName();
      let sourceType = LogSourceType.UNKNOWN;
      if(null !== fileName) {
        if(fileName.startsWith(LoggerMixinStatics.ServerPath)) {
          sourceType = LogSourceType.SERVER;
          fileName = fileName.substring(LoggerMixinStatics.ServerPathLength);
        }
        else if(fileName.startsWith(LoggerMixinStatics.ActorPath)) {
          sourceType = LogSourceType.ACTOR;
          fileName = fileName.substring(LoggerMixinStatics.ActorPathLength);
        }
        else if(fileName.startsWith(LoggerMixinStatics.StackPath)) {
          sourceType = LogSourceType.STACK;
          fileName = fileName.substring(LoggerMixinStatics.StackPathLength);
        }
      }
      return {
        fileName: fileName ? fileName : 'unknown',
        lineNumber: lineNumber,
        sourceType: sourceType
      }
    }
    else {
      return null;
    }
  },
  _getLogInfo(func, guid, depth) {
    if(guid) {
      if(this.executionContext.logCache.has(guid)) {
        return this.executionContext.logCache.get(guid);
      }
    }
    
    const origPrepare = Error.prepareStackTrace;
    const origLimit = Error.stackTraceLimit;
    const dummyError = {};
    
    Error.prepareStackTrace = (error, structuredStackTrace) => {
      let stackDepth = Math.min(depth, structuredStackTrace.length - 1);
      let result = null;
      do {
        result = this._getStackTrace(stackDepth, structuredStackTrace);
      } while(!result && --stackDepth >= 0);
      return result;
    };
    
    Error.stackTraceLimit = depth + 1;
    Error.captureStackTrace(dummyError, func);
    const start = process.hrtime.bigint();
    const logInfo = dummyError.stack;
    const stop = process.hrtime.bigint();
    Error.prepareStackTrace = origPrepare;
    Error.stackTraceLimit = origLimit;
    
    if(guid) {
      this.executionContext.logCache.set(guid, logInfo);
    }
    return logInfo;
  },
  isLogDebug(group) {
    if(this._logDebugType.done) {
      return this._logDebugType.result;
    }
    else {
      return this._logDebugType.value === group;
    }
  },
  isLogEngine(group) {
    if(this._logEngineType.done) {
      return this._logEngineType.result;
    }
    else {
      return this._logEngineType.value === group;
    }
  },
  isLogError() {
    return this._logErrorType;
  },
  isLogIp(group) {
    if(this._logIpType.done) {
      return this._logIpType.result;
    }
    else {
      return this._logIpType.value === group;
    }
  },
  isLogGui(group) {
    if(this._logGuiType.done) {
      return this._logGuiType.result;
    }
    else {
      return -1 !== this._logGuiType.value.indexOf(group);
    }
  },
  isLogTestData(group) {
    if(this._logTestDataType.done) {
      return this._logTestDataType.result;
    }
    else {
      return this._logTestDataType.value === group;
    }
  },
  isLogBrowserLog(group) {
    if(this._logBrowserLogType.done) {
      return this._logBrowserLogType.result;
    }
    else {
      return this._logBrowserLogType.value === group;
    }
  },
  isLogBrowserErr(group) {
    if(this._logBrowserErrType.done) {
      return this._logBrowserErrType.result;
    }
    else {
      return this._logBrowserErrType.value === group;
    }
  },
  isLogVerifySuccess() {
    return this._logVerifySuccessType;
  },
  isLogVerifyFailure() {
    return this._logVerifyFailureType;
  },
  isLogWarning() {
    return this._logWarningType;
  },
  _readGroupLog(logName, defaultValue) {
    const logData = this.getTestDataString(logName, defaultValue);
    return {
      done: 'none' === logData || 'all' === logData,
      result: 'none' === logData ? false : ('all' === logData ? true : false),
      value: logData
    };
  },
  _readLog(logName, defaultValue) {
    const logData = this.getTestDataString(logName, defaultValue);
    return 'all' === logData;
  },
  _getLogObject(log) {
    if('string' === typeof log) {
      return new LogMessage(log, null, null, null);
    }
    else if('function' === typeof log) {
      const functionResult = log();
      if('string' === typeof functionResult) {
        return new LogMessage(functionResult, null, null, null);
      }
      else if('object' === typeof functionResult) {
        return functionResult;
      }
      else {
        return new LogMessage('' + functionResult, null, null, null);
      }
    }
    else if('object' === typeof log) {
      return log;
    }
    else {
      return new LogMessage('' + log, null, null, null);
    }
  },
  _logDebugType: null,
  _logEngineType: null,
  _logErrorType: false,
  _logIpType: null,
  _logGuiType: null,
  _logTestDataType: null,
  _logBrowserLogType: null,
  _logBrowserErrType: null,
  _logVerifySuccessType: false,
  _logVerifyFailureType: false,
  _logWarningType: false
};

const LoggerMixinStatics = {
  ServerPath: ActorPathDist.getActorDistPath() + Path.sep + 'Layers' + Path.sep + 'AppLayer',
  ServerPathLength: (ActorPathDist.getActorDistPath() + Path.sep + 'Layers' + Path.sep + 'AppLayer').length + 1,
  ActorPath: ActorPathDist.getActorDistActorsPath(),
  ActorPathLength: ActorPathDist.getActorDistActorsPath().length + 1,
  StackPath: ActorPathDist.getActorDistStacksPath(),
  StackPathLength: ActorPathDist.getActorDistStacksPath().length + 1
};


module.exports = LoggerMixin;
