
'use strict';

const ContentBinary = require('../../data/content-binary');


const VerifyMixinBase = {
  getVerificationData(expectedKey) {
    const verificationData = this._getVerificationData(expectedKey);
    if('value' === verificationData.type) {
      if('string' === verificationData.valueType) {
        return this._formatTestDataStringValue(verificationData.value);
      }
      else if('number' === verificationData.valueType) {
        return Number.parseFloat(verificationData.value);
      }
      else if('boolean' === verificationData.valueType) {
        // TODO:
        return Number.parseFloat(verificationData.value);
      }
    }
    else if('array' === verificationData.type) {
      const verificationDatas = [];
      verificationData.value.forEach((value, index, array) => {
        if('string' === verificationData.valueType[index]) {
          verificationDatas.push(this._formatTestDataStringValue(value));
        }
        else if('[string]' === verificationData.valueType[index]) {
          let nextVerificationDatas = [];
          verificationDatas.push(nextVerificationDatas);
          let parsedValues = JSON.parse(value);
          parsedValues.forEach((parsedValue) => {
            nextVerificationDatas.push(this._formatTestDataStringValue(parsedValue));
          });
        }
        else if('[number]' === verificationData.valueType[index]) {
          // TODO:
        }
      });
      return verificationDatas;
    }
  },
  _setVerificationsActor(verificationActor) {
    //this.verificationActor;
  },
  _getVerificationData(key) {
    let verificationData = this.verificationActor.get(key);
    if(undefined === verificationData) {
       verificationData = this.tcData.verificationsTestCase.get(key);
    }
    return verificationData;
  },
  _getOperationActualType(value) {
    if(typeof value === 'string') {
      return `'${value}'`;
    }
    else if(Array.isArray(value)) {
      return `[]`;
    }
    else {
      return value;
    }
  },
  _getOperationFunction(expected, actual, operation) {
    let isExpected = -1 !== operation.indexOf('expected');
    let isActual = -1 !== operation.indexOf('actual');
    if(isExpected && isActual) {
      return {
        result: new Function('expected', 'actual', `return ${operation}`)(expected, actual),
        operation: operation.replace(/expected/g, this._getOperationActualType(expected)).replace(/actual/g, this._getOperationActualType(actual))
      };
    }
    else {
      return {
        result: new Function('expected', 'actual', `return expected ${operation} actual`)(expected, actual),
        operation: `${this._getOperationActualType(expected)} ${operation} ${this._getOperationActualType(actual)}`
      };
    }
  },
  _castBoolean(value) {
    if('true' === value) {
      return true;
    }
    else if('false' === value) {
      return false;
    }
    else {
      return value;
    }
  },   
  _verifyValue(expectedValue, actual, operation, text, index1, index2) {
    let indexText = undefined !== index1 ? `[${index1}]` : ' ';
    indexText +=  undefined !== index2 ? `[${index2}]` : ' ';
    let textText = undefined !== text ? text : '';
    let operationData = this._getOperationFunction(expectedValue, actual, operation, text);
    if(!operationData.result) {
      this.logVerifyFailure(() => `${textText}${indexText} ${operationData.operation} : [${typeof expectedValue}, ${typeof actual}]`, '5fa34754-55a3-4b06-a225-4bf6c13c3605');
      return false;
    }
    else {
      this.logVerifySuccess(() => `${textText}${indexText} ${operationData.operation}`, '9fc951f9-eaef-4a27-9dd5-914129c7b62b');
      return true;
    }
  },
  _verifyArrayIndex(expectedArray, description, index1, index2) {
    if(!Array.isArray(expectedArray)) {
      this.logVerifyFailure(() => `${description} is an Array but the actual value is not.`, '8c25f864-d219-4a03-b14d-96913aa2e6fe');
      throw new Error('verify__asdf1234');
    }
    else if(index1 >= expectedArray.length) {
      this.logVerifyFailure(() => `index1 is >= to the length of the expected array from ${description}. index1 = ${index1} and expectedArray.length = ${expectedArray.length} `, 'bfb702f0-377a-43b3-bf99-8fd402fd91be');
      throw new Error('verify__asdf1234');
    }
    else if(undefined !== index2) {
      if(!Array.isArray(expectedArray[index1])) {
        this.logVerifyFailure(() => `${description} index1[${index1}] is an Array but the actual value is not.`, 'ff2e1c4f-a6f5-4cfb-ab2d-a4bc79262e75');
        throw new Error('verify__asdf1234');
      }
      if(index2 >= expectedArray[index1].length) {
        this.logVerifyFailure(() => `${description} index2[${index2}] >= expectedArray[${index1}] length[${expectedArray[index1].length}]].`, 'b6effa55-6d37-4699-9707-9eac894a449a');
        throw new Error('verify__asdf1234');
      }
    }
  },
  _verifyArrayValue(expectedArray, actual, operation, text) {
    if(!Array.isArray(actual)) {
      this.logVerifyFailure(() => `${text} is an Array but the actual value is not.`, '6f1dacff-dd46-4241-b146-bb2439cd1a87');
      return false;
    }
    else if(expectedArray.length !== actual.length) {
      this.logVerifyFailure(() => `${text} Array length[${expectedArray.length}] !== Actual Array length[${actual.length}]].`, '3fc9e3f8-3c5a-40f6-bda7-741b519f3a05');
      return false;
    }
    if(0 !== expectedArray.length) {
      let result = true;
      for(let i = 0; i < expectedArray.length; ++i) {
        let operationData = this._getOperationFunction(expectedArray[i], actual[i], operation);
        if(!operationData.result) {
          this.logVerifyFailure(() => `${text}[${i}]; operation: (${operationData.operation}) : [${typeof expectedArray[i]}, ${typeof actual[i]}]`, 'eb3ad3cc-327e-4754-bb0b-29c3f047a61e');
          result = false;
        }
        else {
          this.logVerifySuccess(() => `${text}[${i}]; operation: (${operationData.operation})`, 'bafceab4-d963-4eee-9166-2e01d21dfd0d');
        }
      }
      return result;
    }
    else {
      let emptyArray = [];
      let operationData = this._getOperationFunction(emptyArray, emptyArray, operation);
      if(!operationData.result) {
        this.logVerifyFailure(() => `${text}; operation: (${operationData.operation}) : [${typeof expectedArray}, ${typeof actual}]`, '730590b6-bb31-4cdd-b4d5-4adee2f9aede');
        return false;
      }
      else {
        this.logVerifySuccess(() => `${text}; operation: (${operationData.operation})`, '1586f3b8-3601-4c70-826b-efa008471068');
        return true;
      }
    }
  },
  _verifyContent(expectedContent, actualContent, operation, description) {
    if(Buffer.isBuffer(actualContent)) {
      const tempActualContent = new ContentBinary();
      tempActualContent.addBuffer(actualContent);
      return this._verifyContent(expectedContent, tempActualContent, operation, description);
    }
    expectedContent.normalize();
    actualContent.normalize();
    let expectedBuffer = null;
    let expectedSize = 0;
    let expectedPos = 0;
    let actualBuffer = null;
    let actualSize = 0;
    let actualPos = 0;
    
    do {
      if(0 === expectedSize) {
        expectedBuffer = expectedContent.getBuffer();
        if(null !== expectedBuffer) {
          expectedSize = expectedBuffer.length;
          expectedPos = 0;
        }
        else {
          expectedSize = -1;
        }
      }
      if(0 === actualSize) {
        actualBuffer = actualContent.getBuffer();
        if(null !== actualBuffer) {
          actualSize = actualBuffer.length;
          actualPos = 0;
        }
        else {
          actualSize = -1;
        }
      }
      if(expectedSize === actualSize) {
        if(-1 === expectedSize) {
          this.logVerifySuccess(() => `${description} operation: (${operation})`, '165c2b2c-4de5-4df6-8994-abe14944bc73');
          return true;
        }
        else if(actualPos >= 1) {
          if(!expectedBuffer.equals(actualBuffer.slice(actualPos, actualPos + expectedSize))) {
            this.logVerifyFailure(() => `${description} operation: (${operation})`, '335c9159-d0e3-4610-b7dc-39e9aa1accb9');
            return false;
          }
          actualPos += expectedSize;
          actualSize -= expectedSize;
          expectedSize = 0;
        }
        else {
          if(!actualBuffer.equals(expectedBuffer.slice(expectedPos, expectedPos + actualSize))) {
            this.logVerifyFailure(() => `${description} operation: (${operation})`, '68a5dc9d-914e-4a46-bfe4-273a3470621c');
            return false;
          }
          expectedPos += actualSize;
          expectedSize -= actualSize;
          actualSize = 0;
        }
      }
      else if (expectedSize < actualSize) {
        if(!expectedBuffer.equals(actualBuffer.slice(actualPos, actualPos + expectedSize))) {
          this.logVerifyFailure(() => `${description} operation: (${operation})`, '53b4fe03-9d4d-42ff-9a18-7045217b6e03');
          return false;
        }
        actualPos += expectedSize;
        actualSize -= expectedSize;
        expectedSize = 0;
      }
      else {
        if(!actualBuffer.equals(expectedBuffer.slice(expectedPos, expectedPos + actualSize))) {
          this.logVerifyFailure(() => `${description} operation: (${operation})`, 'eea37b9c-0d7b-44a0-b0e3-4cb312baa21f');
          return false;
        }
        expectedPos += actualSize;
        expectedSize -= actualSize;
        actualSize = 0;
      }
    } while(true);
  },
  _verify(description, expected, actualValue, index1, index2) {
    if('array' === expected.type) {
      if(undefined !== index1) {
        if(undefined === index2) {
          let result = true;
          this._verifyArrayIndex(expected.valueType, `${description}[${index1}]`, index1);
          switch(expected.valueType[index1]) {
            case 'string':
              result = this._verifyValue(this._formatTestDataStringValue(expected.value[index1]), actualValue, expected.operation[index1], description, index1);
              break;
            case 'number':
              result = this._verifyValue(Number.parseFloat(expected.value[index1]), actualValue, expected.operation[index1], description, index1);
              break;
            case 'boolean':
              result = this._verifyValue(this._castBoolean(expected.value[index1]), actualValue, expected.operation[index1], description, index1);
              break;
            case '[string]':
              const strings = JSON.parse(expected.value[index1]);
              strings.forEach((string, index, array) => {
                array[index] = this._formatTestDataStringValue(string);
              });
              result = this._verifyArrayValue(strings, actualValue, expected.operation[index1], `${description}[${index1}]`) && result;
              break;
            case '[number]':
              const numbers = JSON.parse(expected.value[index1]);
              const formattedNumbers = [];
              numbers.forEach((number) => {
                if('string' === typeof number) {
                  formattedNumbers.push(Number.parseFloat(this._formatTestDataStringValue(number)));
                }
                else {
                  formattedNumbers.push(number);
                }
              });
              result = this._verifyArrayValue(formattedNumbers, actualValue, expected.operation[index1], `${description}[${index1}]`) && result;
              break;
            case '[boolean]':
              const booleans = JSON.parse(expected.value[index1]);
              const formattedBooleans = [];
              booleans.forEach((number) => {
                if('string' === typeof number) {
                  formattedBooleans.push(this._castBoolean(this._formatTestDataStringValue(number)));
                }
                else {
                  formattedBooleans.push(number);
                }
              });
              result = this._verifyArrayValue(formattedBooleans, actualValue, expected.operation[index1], `${description}[${index1}]`) && result;
              break;
            default:
              result = false;
              this.logWarning(() => `Verifing Array index for value type '${expected.valueType[index1]}' is not implemented.`, 'core', '239677a8-0ed1-47da-b903-f9afe35ede6d');
          }
          if(!result) {
            throw new Error('verify__asdf1234');
          }
        }
        else {
          let result = true;
          this._verifyArrayIndex(expected.valueType, `${description}[${index1}]`, index1);
          switch(expected.valueType[index1]) {
            case '[string]':
              const strings = JSON.parse(expected.value[index1]);
              if(Array.isArray(strings)) {
                result = this._verifyValue(this._formatTestDataStringValue(strings[index2]), actualValue, expected.operation[index2], description, index1, index2);
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayStrings: '${expected.value[index1]}' is not an Array.`, 'core', '0645d438-ee1e-40d4-9630-de4ced7d8cc8');
              }
              break;
            case '[number]':
              const numbers = JSON.parse(expected.value[index1]);
              if(Array.isArray(numbers)) {
                if('string' === numbers[index2]) {
                  result = this._verifyValue(Number.parseFloat(this._formatTestDataStringValue(numbers[index2])), actualValue, expected.operation[index2], description, index1, index2);
                }
                else {
                  result = this._verifyValue(numbers[index2], actualValue, expected.operation[index2], description, index1, index2);
                }
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayNumbers: '${expected.value[index1]}' is not an Array.`, 'core', '46f13c8d-50c0-41a6-bc39-dd20317847f7');
              }
              break;
            case '[boolean]':
              const booleans = JSON.parse(expected.value[index1]);
              if(Array.isArray(booleans)) {
                if('string' === booleans[index2]) {
                  result = this._verifyValue(this._castBoolean(this._formatTestDataStringValue(booleans[index2])), actualValue, expected.operation[index2], description, index1, index2);
                }
                else {
                  result = this._verifyValue(booleans[index2], actualValue, expected.operation[index2], description, index1, index2);
                }
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayBooleans: '${expected.value[index1]}' is not an Array.`, 'core', '46f13c8d-50c0-41a6-bc39-dd20317847f7');
              }
              break;
            default:
              result = false;
              this.logWarning(() => `Verifing Array index, index for value type '${expected.valueType[index1]}' is not implemented.`, 'core', '46d1625d-642b-4464-b8b2-d4c014ef8508');
              break;
          };
          if(!result) {
            throw new Error('verify__asdf1234');
          }
        }
      }
      else if(!Array.isArray(actualValue)) {
        this.logVerifyFailure(() => `${description} is an Array but the actual value is not.`, 'a8e5c0b1-38be-4ce9-9fc4-421745a42110');
        throw new Error('verify__asdf1234');
      }
      else if(expected.value.length !== actualValue.length) {
        this.logVerifyFailure(() => `${description} Array length[${expected.value.length}] !== Actual Array length[${actualValue.length}].`, '7fef53f5-90ac-4e81-9962-990cfc77affa');
        throw new Error('verify__asdf1234');
      }
      else {
        let values = this._formatTestDataString(expected);
        let result = values.length > 0;
        for(let i = 0; i < values.length; ++i) {
          switch(expected.valueType[i]) {
            case 'string':
              result = this._verifyValue(this._formatTestDataStringValue(values[i]), actualValue[i], expected.operation[i], description, i) && result;
              break;
            case 'number':
              if('string' === typeof values[i]) {
                result = this._verifyValue(Number.parseFloat(this._formatTestDataStringValue(values[i])), actualValue[i], expected.operation[i], description, i) && result;
              }
              else {
                result = this._verifyValue(values[i], actualValue[i], expected.operation[i], description, i) && result;
              }
              break;
            case 'boolean':
             if('string' === typeof values[i]) {
                result = this._verifyValue(this._castBoolean(this._formatTestDataStringValue(values[i])), actualValue[i], expected.operation[i], description, i) && result;
              }
              else {
                result = this._verifyValue(values[i], actualValue[i], expected.operation[i], description, i) && result;
              }
              break;
            case '[string]':
              if(Array.isArray(expected.value[i])) {
                result = this._verifyArrayValue(expected.value[i], actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else if('string' === typeof expected.value[i])
              {
                const strings = JSON.parse(expected.value[i]);
                strings.forEach((string, index, array) => {
                  array[index] = this._formatTestDataStringValue(string);
                });
                result = this._verifyArrayValue(strings, actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayStrings with type: '${expected.valueType[i]}' is not implemented.`, 'core', 'd3bdd7b5-a0ca-4f09-8225-23308bd98b0b');
              }
              break;
            case '[number]':
              if(Array.isArray(expected.value[i])) {
                result = this._verifyArrayValue(expected.value[i], actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else if('string' === typeof expected.value[i]) {
                const numbers = JSON.parse(expected.value[i]);
                const formattedNumbers = [];
                numbers.forEach((number, index, array) => {
                  if('string' === typeof number) {
                    formattedNumbers.push(Number.parseFloat(this._formatTestDataStringValue(number)));
                  }
                  else {
                    formattedNumbers.push(number);
                  }
                });
                result = this._verifyArrayValue(formattedNumbers, actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayNumbers with type: '${expected.valueType[i]}' is not implemented.`, 'core', '95657de6-7cc3-4522-ab99-d1d2d83fa40f');
              }
              break;
            case '[boolean]':
              if(Array.isArray(expected.value[i])) {
                result = this._verifyArrayValue(expected.value[i], actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else if('string' === typeof expected.value[i]) {
                const booleans = JSON.parse(expected.value[i]);
                const formattedBooleans = [];
                booleans.forEach((boolean, index, array) => {
                  if('string' === typeof boolean) {
                    formattedBooleans.push(this._castBoolean(this._formatTestDataStringValue(boolean)));
                  }
                  else {
                    formattedBooleans.push(boolean);
                  }
                });
                result = this._verifyArrayValue(formattedBooleans, actualValue[i], expected.operation[i], `${description}[${i}]`) && result;
              }
              else {
                result = false;
                this.logWarning(() => `Verifing ArrayNumbers with type: '${expected.valueType[i]}' is not implemented.`, 'core', '95657de6-7cc3-4522-ab99-d1d2d83fa40f');
              }
              break;
            default:
              this.logWarning(() => `Verifing Array values type '${expected.valueType[index1]}' is not implemented.`, 'core', '73c2036d-3668-4588-83cc-a1844c22648f');
              result = false;
              break;
          }
        }
        if(!result) {
          throw new Error('verify__asdf1234');
        }
      }
    }
    else if('value' === expected.type) {
      let result = true;
      switch(expected.valueType) {
        case 'string':
          result = this._verifyValue(this._formatTestDataString(expected), Buffer.isBuffer(actualValue) ? actualValue.toString() : actualValue, expected.operation, description);
          break;
        case 'number':
          if('string' === typeof expected.value) {
            result = this._verifyValue(Number.parseFloat(this._formatTestDataString(expected)), actualValue, expected.operation, description);
          }
          else {
            result = this._verifyValue(expected.value, actualValue, expected.operation, description);
          }
          break;
        case 'boolean':
          if('string' === typeof expected.value) {
            result = this._verifyValue(this._castBoolean(this._formatTestDataString(expected)), actualValue, expected.operation, description);
          }
          else {
            result = this._verifyValue(expected.value, actualValue, expected.operation, description);
          }
          break;
        case '[string]':
          const strings = JSON.parse(expected.value);
          if(undefined === index1) {
            strings.forEach((string, index, array) => {
              array[index] = this._formatTestDataStringValue(string);
            });
            result = this._verifyArrayValue(strings, actualValue, expected.operation, description);
          }
          else {
            this._verifyArrayIndex(strings, description, index1);
            result = this._verifyValue(this._formatTestDataStringValue(strings[index1]), actualValue, expected.operation, `${description}[${index1}]`);
          }
          break;
        case '[number]':
          const numbers = JSON.parse(expected.value);
          if(undefined === index1) {
            const formattedNumbers = [];
            numbers.forEach((number) => {
              if(typeof number === 'string') {
                formattedNumbers.push(Number.parseFloat(this._formatTestDataStringValue(number)));
              }
              else {
                formattedNumbers.push(number);
              }
            });
            result = this._verifyArrayValue(formattedNumbers, actualValue, expected.operation, description);
          }
          else {
            this._verifyArrayIndex(numbers, description, index1);
            if('string' === typeof numbers[index1]) {
              result = this._verifyValue(Number.parseFloat(this._formatTestDataStringValue(numbers[index1])), actualValue, expected.operation, `${description}[${index1}]`);
            }
            else {
              result = this._verifyValue(numbers[index1], actualValue, expected.operation, `${description}[${index1}]`);
            }
          }          
          break;
        case '[boolean]':
          const booleans = JSON.parse(expected.value);
          if(undefined === index1) {
            const formattedBooleans = [];
            booleans.forEach((boolaen) => {
              if(typeof boolaen === 'string') {
                formattedBooleans.push(this._castBoolean(this._formatTestDataStringValue(boolaen)));
              }
              else {
                formattedBooleans.push(boolaen);
              }
            });
            result = this._verifyArrayValue(formattedBooleans, actualValue, expected.operation, description);
          }
          else {
            this._verifyArrayIndex(booleans, description, index1);
            if('string' === typeof booleans[index1]) {
              result = this._verifyValue(this._castBoolean(this._formatTestDataStringValue(booleans[index1])), actualValue, expected.operation, `${description}[${index1}]`);
            }
            else {
              result = this._verifyValue(booleans[index1], actualValue, expected.operation, `${description}[${index1}]`);
            }
          }
          break;
        case '[[string]]':
          const arrayStrings = JSON.parse(expected.value);
          if(undefined !== index1) {
            if(undefined !== index2) {
              this._verifyArrayIndex(arrayStrings, description, index1, index2);
              result = this._verifyValue(this._formatTestDataStringValue(arrayStrings[index1][index2]), actualValue, expected.operation, description, index1, index2);
            }
            else {
              arrayStrings[index1].forEach((string, index, array) => {
                array[index] = this._formatTestDataStringValue(string);
              });
              result = this._verifyArrayValue(arrayStrings[index1], actualValue, expected.operation, `${description}[${index1}]`);
            }
          }
          else {
            arrayStrings.forEach((arrayString) => {
              arrayString.forEach((string, index, array) => {
                array[index] = this._formatTestDataStringValue(string);
              });
            });
            arrayStrings.forEach((arrayString, index) => {
              result = this._verifyArrayValue(arrayString, actualValue[index], expected.operation, `${description}[${index}]`) && result;
            });
          }
          break;
        case '[[number]]':
          const arrayNumbers = JSON.parse(expected.value);
          if(undefined !== index1) {
            if(undefined !== index2) {
              this._verifyArrayIndex(arrayNumbers, description, index1, index2);
              if('string' === arrayNumbers[index1][index2]) {
                result = this._verifyValue(Number.parseFloat(this._formatTestDataStringValue(arrayNumbers[index1][index2])), actualValue, expected.operation, description, index1, index2);                 
              }
              else {
                result = this._verifyValue(arrayNumbers[index1][index2], actualValue, expected.operation, description, index1, index2);
              }
            }
            else {
              const formattedNumbers = [];
              arrayNumbers[index1].forEach((number) => {
                if(typeof number === 'string') {
                  formattedNumbers.push(Number.parseFloat(this._formatTestDataStringValue(number)));
                }
                else {
                  formattedNumbers.push(number);
                }
              });
              result = this._verifyArrayValue(formattedNumbers, actualValue, expected.operation, `${description}[${index1}]`);
            }
          }
          else {
            const formattedArrayNumbers = [];
            arrayNumbers.forEach((numbers) => {
              const formattedNumbers = [];
              formattedArrayNumbers.push(formattedNumbers)
              numbers.forEach((number) => {
                if(typeof number === 'string') {
                  formattedNumbers.push(Number.parseFloat(this._formatTestDataStringValue(number)));
                }
                else {
                  formattedNumbers.push(number);
                }
              });
            });
            formattedArrayNumbers.forEach((formattedNumbers, index) => {
              result = this._verifyArrayValue(formattedNumbers, actualValue[index], expected.operation, `${description}[${index}]`) && result;
            });
          }
          break;
        case '[[boolean]]':
          const arrayBooleans = JSON.parse(expected.value);
          if(undefined !== index1) {
            if(undefined !== index2) {
              this._verifyArrayIndex(arrayBooleans, description, index1, index2);
              if('string' === arrayBooleans[index1][index2]) {
                result = this._verifyValue(this._castBoolean(this._formatTestDataStringValue(arrayBooleans[index1][index2])), actualValue, expected.operation, description, index1, index2);                 
              }
              else {
                result = this._verifyValue(arrayBooleans[index1][index2], actualValue, expected.operation, description, index1, index2);
              }
            }
            else {
              const formattedBooleans = [];
              arrayBooleans[index1].forEach((boolean) => {
                if(typeof boolean === 'string') {
                  formattedBooleans.push(this._castBoolean(this._formatTestDataStringValue(boolean)));
                }
                else {
                  formattedBooleans.push(boolean);
                }
              });
              result = this._verifyArrayValue(formattedBooleans, actualValue, expected.operation, `${description}[${index1}]`);
            }
          }
          else {
            const formattedArrayBooleans = [];
            arrayBooleans.forEach((booleans) => {
              const formattedBooleans = [];
              formattedArrayBooleans.push(formattedBooleans)
              booleans.forEach((boolean) => {
                if(typeof boolean === 'string') {
                  formattedBooleans.push(this._castBoolean(this._formatTestDataStringValue(boolean)));
                }
                else {
                  formattedBooleans.push(boolean);
                }
              });
            });
            formattedArrayBooleans.forEach((formattedBooleans, index) => {
              result = this._verifyArrayValue(formattedBooleans, actualValue[index], expected.operation, `${description}[${index}]`) && result;
            });
          }
          break;
        default:
          result = this._verifyValue(expected.value, actualValue, expected.operation, description);
          break;
      }
      if(!result) {
        throw new Error('verify__asdf1234');
      }
    }
    else {
      this.logVerifyFailure(() => `${description} unknown expected type '${expected.type}'`, '30cbc03b-42bd-4ca7-b5fe-6c28f5f6d4f2');
      throw new Error('verify__asdf1234');
    }
  }
};

module.exports = VerifyMixinBase;
