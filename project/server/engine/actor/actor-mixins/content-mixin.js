 
'use strict';

const ContentCache = require('../../data/content-cache');
const ContentUndefined = require('../../data/content-undefined');


const ContentMixin = {
  *getContent(name, hint) {
    const contentName = this.getTestDataString(name, null);
    if(null === contentName) {
      return new ContentUndefined();
    }
    return yield* this.executionContext.contentCache.get(contentName, hint, this);
  },
  *getContentByName(name, hint) {
    return yield* this.executionContext.contentCache.get(name, hint, this);
  }
};

module.exports = ContentMixin;
