
'use strict';

const LogInner = require('z-abs-funclayer-engine-cs/clientServer/log/log-inner');
const ActorPhaseConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const');


const TestDataMixin = {
  getTestDataString(key, defaultValue, formatter) {
    let testData = this._getTestData(key, defaultValue);
    if(formatter) {
      testData = Reflect.get(formatter, testData);
      if(undefined === testData) {
        testData = defaultValue;
      }
    }
    this.logTestData(() => `${key} | string | '${testData}'`, 'test-data-string', 'ad6085a3-28d9-44ca-9f4c-df7f362e3af9');
    return testData;
  },
  getTestDataStrings(key, defaultValue, formatter) {
    let testData = this._getTestData(key, defaultValue);
    if(formatter) {
      testData = Reflect.get(formatter, testData);
      if(undefined === testData) {
        testData = defaultValue;
      }
    }
    else if(!Array.isArray(testData)) {
      testData = JSON.parse(testData);
    }
    if(this.isLogTestData('test-data-strings')) {
      const stringifiedTestData = JSON.stringify(testData);
      this.logTestData(() => (this.createLogMessage(`${key} | [string] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testData, null, 2))))), 'test-data-strings', '68590e57-ed52-4248-abba-4f692ff5da99');
    }
    return testData;
  },
  getTestDataArrayStrings(key, defaultValue, formatter) {
    let testDatas = this._getTestData(key, defaultValue);
    if(formatter) {
      testDatas = Reflect.get(formatter, testDatas);
      if(undefined === testDatas) {
        testDatas = defaultValue;
      }
    }
    else if(Array.isArray(testDatas)) {
      testDatas = testDatas.map((testData) => {
        if(Array.isArray(testData)) {
          return testData;
        }
        else {
          return JSON.parse(testData);
        }
     });
    }
    else {
      testDatas = JSON.parse(testDatas);
    }
    if(this.isLogTestData('test-data-array-strings')) {
      const stringifiedTestData = JSON.stringify(testDatas);
      this.logTestData(() => (this.createLogMessage(`${key} | [[string]] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testDatas, null, 2))))), 'test-data-array-strings', 'c78bb068-daf3-489a-b132-c94da6bfbbd6');
    }
    return testDatas;
  },
  getTestDataNumber(key, defaultValue, formatter) {
    let testData = this._getTestData(key, defaultValue);
    let testDataNumber = 0.0;
    if(formatter) {
      testDataNumber = Reflect.get(formatter, testData);
      if(undefined === testDataNumber) {
        testDataNumber = defaultValue;
      }
    }
    else {
      if('number' === typeof testData) {
        testDataNumber = testData;
      }
      else {
        testDataNumber = Number.parseFloat(testData);
      }
    }
    this.logTestData(() =>`${key} | number | ${testDataNumber}`, 'test-data-number', '3b326fd5-05cc-4272-82b9-098fa6184a7b');
    return testDataNumber;
  },
  getTestDataNumbers(key, defaultValue, formatter) {
    let testDatas = this._getTestData(key, defaultValue);
    if(formatter) {
      testDatas = Reflect.get(formatter, testDatas);
      if(undefined === testDatas) {
        testDatas = defaultValue;
      }
    }
    else if(Array.isArray(testDatas)) {
      testDatas = testDatas.map((testData) => {
        if('number' === typeof testData) {
          return testData;
        }
        else {
          return Number.parseFloat(testData);
        }
      });
    }
    else {
      testDatas = JSON.parse(testDatas);
    }
    if(this.isLogTestData('test-data-numbers')) {
      let stringifiedTestData = JSON.stringify(testDatas);
      this.logTestData(() => (this.createLogMessage(`${key} | [number] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testDatas, null, 2))))), 'test-data-numbers', '9b60d396-7354-4ced-bfef-81b231bf918a');
    }
    return testDatas;
  },
  getTestDataArrayNumbers(key, defaultValue, formatter) {
    let testDatas = this._getTestData(key, defaultValue);
    if(formatter) {
      testDatas = Reflect.get(formatter, testDatas);
      if(undefined === testDatas) {
        testDatas = defaultValue;
      }
    }
    else if(Array.isArray(testDatas)) {
      testDatas = testDatas.map((testData) => {
        if(Array.isArray(testData)) {
          return testData;
        }
        else {
          return JSON.parse(testData);
        }
      });
    }
    else {
      testDatas = JSON.parse(testDatas);
    }
    if(this.isLogTestData('test-data-array-numbers')) {
      const stringifiedTestData = JSON.stringify(testDatas);
      this.logTestData(() => (this.createLogMessage(`${key} | [[number]] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testDatas, null, 2))))), 'test-data-array-numbers', 'c4555454-5f2b-4612-ad36-75548951d50d');
    }
    return testDatas;
  },
  getTestDataBoolean(key, defaultValue, formatter) {
    let testData = this._getTestData(key, defaultValue);
    let testDataBoolean = false;
    if(formatter) {
      testDataBoolean = Reflect.get(formatter, testData);
      if(undefined === testDataBoolean) {
        testDataBoolean = defaultValue;
      }
    }
    else {
      if('boolean' === typeof testData) {
        testDataBoolean = testData;
      }
      else {
        if('true' === testData) {
          testDataBoolean = true;
        }
        else if('false' === testData) {
          testDataBoolean = false;
        }
        else {
          testDataBoolean = undefined;
        }
      }
    }
    this.logTestData(() => `${key} | boolean | ${testDataBoolean}`, 'test-data-boolean', '21396f55-4a70-46bf-b6d1-9344ae941c48');
    return testDataBoolean;
  },
  getTestDataBooleans(key, defaultValue, formatter) {
    let testDatas = this._getTestData(key, defaultValue);
    if(formatter) {
      testDatas = Reflect.get(formatter, testDatas);
      if(undefined === testDatas) {
        testDatas = defaultValue;
      }
    }
    else if(Array.isArray(testDatas)) {
      testDatas = testDatas.map((testData) => {
        if('boolean' === typeof testData) {
          return testData;
        }
        else {
          if('true' === testData) {
            return true;
          }
          else if('false' === testData) {
            return false;
          }
          else {
            return undefined;
          }
        }
      });
    }
    else {
      testDatas = JSON.parse(testDatas);
    }
    if(this.isLogTestData('test-data-booleans')) {
      let stringifiedTestData = JSON.stringify(testDatas);
      this.logTestData(() => (this.createLogMessage(`${key} | [boolean] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testDatas, null, 2))))), 'test-data-booleans', '310940d0-4c29-4bde-9bb9-03af004d7934');
    }
    return testDatas;
  },
  getTestDataArrayBooleans(key, defaultValue, formatter) {
    let testDatas = this._getTestData(key, defaultValue);
    if(formatter) {
      testDatas = Reflect.get(formatter, testDatas);
      if(undefined === testDatas) {
        testDatas = defaultValue;
      }
    }
    else if(Array.isArray(testDatas)) {
      testDatas = testDatas.map((testData) => {
        if(Array.isArray(testData)) {
          return testData;
        }
        else {
          return JSON.parse(testData);
        }
      });
    }
    else {
      testDatas = JSON.parse(testDatas);
    }
    if(this.isLogTestData('test-data-array-booleans')) {
      const stringifiedTestData = JSON.stringify(testDatas);
      this.logTestData(() => (this.createLogMessage(`${key} | [[boolean]] | ${stringifiedTestData}`, new LogInner(stringifiedTestData, new LogInner(JSON.stringify(testDatas, null, 2))))), 'test-data-array-booleans', 'fc7fdde8-8ddc-477a-b40d-f1acc3f85630');
    }
    return testDatas;
  },
  getTestDataObject(key, defaultValue, formatter) {
    const testData = this._getTestData(key, defaultValue);
    let testDataObject = {};
    if(formatter) {
      testDataObject = Reflect.get(formatter, testData);
      if(undefined === testDataObject) {
        testDataObject = defaultValue;
      }
    }
    else {
      if('object' === typeof testData) {
        testDataObject = testData;
      }
      else {
        testDataObject = JSON.parse(testData);
      }
    }
    if('object' === typeof testData) {
      this.logTestData(() => `${key} | object | ${JSON.stringify(testData)}`, 'test-data-object', 'c09755ae-6ee7-4806-aae8-9235a83f2da3');
    }
    else {
      this.logTestData(() =>`${key} | object | ${testDataObject}`, 'test-data-object', 'd76025e6-f235-4835-94d9-0c61ea71bc9c');
    }
    return testDataObject;
  },
  _formatTestDataStringValue(value) {
    const matches = value.match(/\$\[([^\]]+)\]/g);
    if(null !== matches) {
      matches.forEach((match) => {
        if('$[undefined]' === match) {
          value = undefined;
        }
        else if('$[null]' === match) {
          value = null;
        }
        else {
          if(undefined !== value && null !== value) {
            value = value.replace(match, this._getTestData(match.substring(2, match.length - 1)));
          }
        }
      });
    }
    return value;
  },
  _formatTestDataString(testData) {
    if('value' === testData.type) {
      const value = testData.value;
      if(undefined !== value && null !== value) {
        if('string' === typeof value) {
          return this._formatTestDataStringValue(value);
        }
        else if('[string]' === typeof value) {
          throw new Error(`Formatting Test Data type '[string]' is not implemented.`);
        }
      }
      return value;
    }
    else if('array' ===  testData.type) {
      const values = testData.value.slice(0);
      values.forEach((value, index, array) => {
        if(undefined !== value && null !== value) {
          if(typeof value === 'string') {
            array[index] = this._formatTestDataStringValue(value);
          }
          else if(Array.isArray(value)) {
            value.forEach((val, valIndex, valArray) => {
              if(undefined !== val && null !== val) {
                if(typeof val === 'string') {
                  valArray[valIndex] = this._formatTestDataStringValue(val);
                }
              }
            });
          }
        }
      });
      return values;
    }
  },
  _getTestData(key, defaultValue, isObject = false) {
    let valueData = this.executionContext.testData.getOverrideTestData(key);
    if(undefined === valueData) {
      valueData = this.testDataActor.get(key);
      if(undefined === valueData) {
        valueData = this._getTestDataIteration(key);
        if(undefined === valueData) {
          valueData = this.tcData.testDatasTestCase.get(key);
          if(undefined === valueData) {
            valueData = this.executionContext.testData.getTestData(key, (key) => {
              return this.tcData.testDatasTestCaseStatic.get(key);
            });
          }
        }
      }
    }
    if(undefined !== valueData) {
      return this._formatTestDataString(valueData);
    }
    if(undefined !== defaultValue) {
      if('function' === typeof defaultValue) {
        return defaultValue();
      }
      else {
        return defaultValue;
      }
    }
    throw new Error(`TestData: '${key}' not found. Default value undefined.`);
  },
  _getTestDataIteration(key) {
    const phase = this.tcData.testDatasActor.get(ActorPhaseConst.getName(this.phaseId));
    if(undefined !== phase) {
      const type = phase.get(this.type);
      if(undefined !== type) {
        const data = type.get(key);
        if(undefined !== data) {
          const valueData = data.values[data.currentIndex];
          if(++data.currentIndex == data.values.length) {
            data.currentIndex = 0;
          }
          return valueData;
        }
      }
    }
  },
  _setTestDataActor(key, value, description) {
    if(key.endsWith('[]')) {
      const arrayName = key.substr(0, key.length - 2);
      let valueData = this.testDataActor.get(arrayName);
      if(undefined === valueData) {
        valueData = {
          type: 'array',
          value: []
        };
        this.testDataActor.set(arrayName, valueData);
      }
      valueData.value.push(value);
    }
    else {
      this.testDataActor.set(key, {
        type: 'value',
        value: value
      });
    }
  },
  _setTestDatasActor(testDataActor) {
    if(undefined !== testDataActor && '' !== testDataActor) {
      try {
        const testDatas = JSON.parse(testDataActor);
        if(Array.isArray(testDatas)) {
          testDatas.forEach((testData) => {
            if(Array.isArray(testData)) {
              let key;
              let value;
              let description;
              switch(testData.length) {
                case 3:
                  description = testData[2];
                case 2:
                  value = testData[1];
                  key = testData[0];
                  this._setTestDataActor(key, value, description);
                  break;
              }
            }
          });
        }
      }
      catch(err) {
        
      }
    }
  }
};

module.exports = TestDataMixin;
