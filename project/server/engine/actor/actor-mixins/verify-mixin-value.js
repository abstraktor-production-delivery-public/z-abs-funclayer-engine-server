
'use strict';


const VerifyMixinValue = {
  _calculateExpectedValueType(value) {
    if(Array.isArray(value)) {
      const valueTypes = value.map((val) => {
        return `[${this._calculateExpectedValueType(val)}]`;
      });
      return valueTypes[0]; // TODO: What to do if the types differ?
    }
    else {
      if('string' === typeof value) {
        return 'string';
      }
      else if('number' === typeof value) {
        return 'number';
      }
      else if('boolean' === typeof value) {
        return 'boolean';
      }
      else if('object' === typeof value) {
        return 'object';
      }
      else {
        return 'unknown';
      }
    }
  },
  _calculateExpected(expectedValue, operation) {
    if(!Array.isArray(expectedValue)) {
      return {
        type: 'value',
        value: expectedValue,
        operation: operation,
        valueType: this._calculateExpectedValueType(expectedValue)
      };
    }
    else {
      const valueTypes = [];
      const operations = [];
      expectedValue.forEach((val) => {
        valueTypes.push(`${this._calculateExpectedValueType(val)}`);
        operations.push(operation);
      });
      return {
        type: 'array',
        value: expectedValue,
        operation: operations,
        valueType: valueTypes
      };
    }
  },
  VERIFY_VALUE(expectedValue, actualValue, description, operation = '===') {
    this._verify(!description ? 'Value' : `${description}. Value`, this._calculateExpected(expectedValue, operation), actualValue);
  },
  VERIFY_VALUE_INDEX(expectedValueArray, actualValueArray, index, description, operation = '===') {
    this._verify(!description ? 'Value' : `${description}. Value`, this._calculateExpected(expectedValueArray, operation), actualValueArray, index);
  },
  VERIFY_CONTENT_VALUE(expectedContent, actualSize, actualContent, description, operation = '===') {
    let result = this._verifyValue(expectedContent.size, actualSize, operation, `Content size:`);
    result = this._verifyContent(expectedContent, actualContent, operation, `Content buffer`) && result;
    if(!result) {
      throw new Error('verify__asdf1234');
    }
  }
};

module.exports = VerifyMixinValue;
