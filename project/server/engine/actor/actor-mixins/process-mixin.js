
'use strict';

const ChildProcess = require('child_process');


const ProcessMixin = {
  forkChildProcess(modulePath, args, options) {
    let cancel = false;
    const pendingId = this.setPending(() => {
      cancel = true;
    });
    const childProcess = ChildProcess.fork(modulePath, args, options);
    childProcess.once('message', (msg) => {
      if(!cancel) {
        this.pendingSuccess(pendingId, childProcess);
      }
    });
  },
  closeChildProcess(childProcess) {
    let cancel = false;
    const pendingId = this.setPending(() => {
      cancel = true;
    });
    this.childProcess.once('exit', (code) => {
      if(!cancel) {
        this.pendingSuccess(pendingId, childProcess);
      }
    });
    childProcess.send({cmd: 'close'});
  }
};

module.exports = ProcessMixin;
