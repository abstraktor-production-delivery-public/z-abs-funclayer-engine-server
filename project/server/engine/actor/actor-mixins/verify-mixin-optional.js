
'use strict';

const ContentUndefined = require('../../data/content-undefined');


const VerifyMixinOptional = {
  VERIFY_OPTIONAL(expectedKey, actualValue, index1, index2) {
    const expectedValue = this._getVerificationData(expectedKey);
    const optionalText = `Optional verification key '${expectedKey}'`;
    if(undefined === expectedValue) {
      this.logVerifySuccess(() => `${optionalText} is not set`, '6f0a8906-62da-46e5-a8a4-566679ddf507'); 
    }
    else {
      this._verify(optionalText, expectedValue, actualValue, index1, index2);
    }
  },
  *VERIFY_CONTENT_OPTIONAL(expectedKey, actualSize, actualContent) {
    const expected = this._getVerificationData(expectedKey);
    const optionalText = `Optional Content['${expectedKey}']`;
    if(undefined === expected) {
      this.logVerifySuccess(() => `Optional verification key '${expectedKey}' is not set`, 'c7c94cb0-ca5d-4267-a084-70013174b424');
    }
    else if('value' !== expected.type || 'string' !== expected.valueType) {
      this.logVerifyFailure(() => `${optionalText} the value must be the content key of type string`, '671d4bd1-f9e8-47c0-946f-cb03e652c7ac');
      throw new Error('verify__asdf1234');
    }
    else {
      const expectedContent = yield* this.getContentByName(expected.value);
      if(expectedContent instanceof ContentUndefined) {
        this.logVerifyFailure(() => `${optionalText} is not set.`, '6943d1a8-4dbf-44e3-ac36-1eb326e33478');
        throw new Error('verify__asdf1234');
      }
      let result = this._verifyValue(expectedContent.size, actualSize, expected.operation, `Optional Content['${expectedKey}' is '${expected.value}'] size:`);
      result = this._verifyContent(expectedContent, actualContent, expected.operation, `Optional Content['${expectedKey}' is '${expected.value}'] buffer`) && result;
      if(!result) {
        throw new Error('verify__asdf1234');
      }
    }
  }
};

module.exports = VerifyMixinOptional;
