
'use strict';

const NodejsApi = require('nodejs-api');
const CancelObject = require('../../../stack/stacks/cancel-object');


const AsynchMixin = {
  *all(array) {
    
  },
  *async(asyncFunction) {
    const cancelObject = new CancelObject();
    const pendingId = this.setPending(() => {
      cancelObject.set();
    });
    return yield this._async(pendingId, cancelObject, asyncFunction);
  },
  *asyncAll(...promises) {
    const cancelObject = new CancelObject();
    const pendingId = this.setPending(() => {
      cancelObject.set();
    });
    return yield this._asyncAll(pendingId, cancelObject, promises);
  },
  *callback(callbackFunction) {
    const cancelObject = new CancelObject();
    const pendingId = this.setPending(() => {
      cancelObject.set();
    });
    return yield this._callback(pendingId, cancelObject, callbackFunction);
  },
  require(name) {
    const object = Reflect.get(NodejsApi, name.substring('nodejs-api-'.length));
    return new object(this);
  },
  async _asyncAll(pendingId, cancelObject, promises) {
    Promise.all(promises)
      .then((values) => {
        if(!cancelObject.cancel) {
          this.pendingSuccess(pendingId);
        }
      })
      .catch((e) => {
        if(!cancelObject.cancel) {
          return this.handlePendingIdError(pendingId, e);
        }
      });
  },
  async _async(pendingId, cancelObject, asyncFunction) {
    try {
      const value = await asyncFunction();
      if(!cancelObject.cancel) {
        this.pendingSuccess(pendingId, value);
      }
    }
    catch(e) {
      if(!cancelObject.cancel) {
        return this.handlePendingIdError(pendingId, e);
      }
    }
  },
  _callback(pendingId, cancelObject, callbackFunction) {
    process.nextTick(() => {
      try {
        callbackFunction((e, value) => {
          if(!e) {
            if(!cancelObject.cancel) {
              this.pendingSuccess(pendingId, value);
            }
          }
          else {
            if(!cancelObject.cancel) {
              return this.handlePendingIdError(pendingId, e);
            }
          }
        });
      }
      catch(e) {
        if(!cancelObject.cancel) {
          return this.handlePendingIdError(pendingId, e);
        }
      }
    });
  },
  handlePendingIdSuccess(pendingId, value) {
    this.pendingSuccess(pendingId, value);
  },
  handlePendingIdError(pendingId, err) {
    this.pendingError(pendingId, err);
  },
  handlePendingId(pendingId, err, value) {
    if(err) {
      this.pendingError(pendingId, err);
    }
    else {
      this.pendingSuccess(pendingId, value);
    }
  }
};

module.exports = AsynchMixin;
