
'use strict';

const CancelObject = require('../../../stack/stacks/cancel-object');


const SharedExecutionDataMixin = {
  *setSharedExecutionData(key, value) {
    const sharedDataobject = this.executionContext.sharedRuntimeData.set(key, value);
    this._handlePendings(key, sharedDataobject);
    const cancelObject = new CancelObject();
    const pendingId = this.setPending(() => {
      cancelObject.set();
    });
    process.nextTick(() => {
      if(!cancelObject.cancel) {
        this.handlePendingIdSuccess(pendingId, value);
      }
    });
    return yield;
  },
  setSharedExecutionDataSync(key, value) {
    const sharedDataobject = this.executionContext.sharedRuntimeData.set(key, value);
    this._handlePendings(key, sharedDataobject);
    return value;
  },
  getSharedExecutionData(key, defaultValue) {
    const sharedData = this.executionContext.sharedRuntimeData.get(key);
    if(undefined !== sharedData) {
      return sharedData.value;
    }
    else {
      return defaultValue;
    }
  },
  *waitForSharedExecutionData(key, expectedValue) {
    const value = this.executionContext.sharedRuntimeData.wait(key, this, expectedValue, () => {
      const cancelObject = new CancelObject();
      const pendingId = this.setPending(() => {
        cancelObject.set();
      });
      return {
        id: pendingId,
        cancelObject: cancelObject
      };
    });
    if(null !== value) {
      return value;
    }
    else {
      return yield;
    }
  }
};


module.exports = SharedExecutionDataMixin;
