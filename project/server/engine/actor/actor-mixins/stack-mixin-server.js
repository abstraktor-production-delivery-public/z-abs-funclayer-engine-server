
'use strict';

const Address = require('../../../address/address');
const NetworkType = require('../../../stack/network/network-type');


const StackMixinServer = {
  createServer(stackName, options) {
    const optionsConnection = {
      connectionOptions: null,
      transportLayer: {
        transportType: null,
        transportProperties: null
      },
      messageSelector: null,
      connectionWorker: null,
      stackContentCache: this.executionContext.stackContentCache
    };
    let srvIndex = 0;
    let dstIndex = 0;
    let srvOptionsAddress = undefined;
    let dstOptionsAddress = undefined;
    this.getTransportLayerData(optionsConnection, options);
    if(undefined !== options) {
      if(undefined !== options.srvIndex) {
        srvIndex = options.srvIndex;
      }
      if(undefined !== options.dstIndex) {
        dstIndex = options.dstIndex;
      }
      if(undefined !== options.messageSelector) {
        if(!options.messageSelector.interceptor || (options.messageSelector.interceptor && 'inter' === this.type)) {
          optionsConnection.messageSelector = options.messageSelector;
        }
        else {
          throw new Error('MessageSelector with interceptor == true can only be used by ActorInterception.');
        }
      }
      if(undefined !== options.connectionOptions) {
        optionsConnection.connectionOptions = options.connectionOptions;
      }
      if(undefined !== options.port && undefined !== options.host && undefined !== options.family) {
        srvOptionsAddress = new Address('anonymous-(options)', options.host, options.family, options.port, 'server');
      }
    }
    const srvAddress = undefined === srvOptionsAddress ? this.getSrvAddress(srvIndex) : srvOptionsAddress;
    const dstAddress = this.getDstAddress(dstIndex);
    this.executionContext.serverManager.getServer(stackName, this, srvAddress, dstAddress, optionsConnection);
  },
  switchProtocol(stackName, currentConnection) {
    return this.executionContext.serverManager.switchProtocol(stackName, currentConnection, this);
  },
  getSrvAddress(srvIndex = 0) {
    // TODO: add link to help!!!
    if(0 === this.srv.length) {
      const serverManager = this.executionContext.serverManager;
      const srvAddress = serverManager.addressDefaultManager.getSrv(serverManager.actors.get(this));
      //connection.logWarning(`There is no server srv address name. Default server srv address will be used. ${srvAddress.host}:${srvAddress.port} - name: ${srvAddress.addressName}`, 'core', '0649e7cc-a67a-4cf5-bb3e-b7fad4b88695', 1);
      return srvAddress;
    }
    else if(srvIndex >= this.srv.length) {
      throw new Error(`StackServer: No server address name with index: ${srvIndex}. Server address names: ${JSON.parse(this.srv)}`);
    }
    const addressData = this.srv[srvIndex];
    if(0 === addressData.type) {
      return this._getStackAddress((addressName) => {
        return this.executionContext.addresses.getSrv(addressName);
      }, addressData.value);
    }
    else {
      return addressData.value;
    }
  },
  getDstAddress(dstIndex = 0) {
    // TODO: add link to help!!!
    if(0 === this.dst.length) {
      return null;
    }
    else if(dstIndex >= this.dst.length) {
      throw new Error(`No destination address name with index: ${dstIndex}. Destination address names: ${JSON.parse(this.dst)}`);
    }
    const addressData = this.dst[dstIndex];
    if(0 === addressData.type) {
      return this._getStackAddress((addressName) => {
        return this.executionContext.addresses.getDst(addressName);
      }, addressData.value);
    }
    else {
      return addressData.value;
    }
  }
};

module.exports = StackMixinServer;
