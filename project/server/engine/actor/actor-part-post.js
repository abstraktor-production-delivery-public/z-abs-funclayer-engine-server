
'use strict';

const PartAsynchMixin = require('./actor-part-mixins/part-asynch-mixin');
const PartLoggerMixin = require('./actor-part-mixins/part-logger-mixin');
const PartSharedRuntimeDataMixin = require('./actor-part-mixins/part-shared-runtime-data-mixin');
const PartTimerMixin = require('./actor-part-mixins/part-timer-mixin');
const PartVerifyMixinMandatory = require('./actor-part-mixins/part-verify-mixin-mandatory');
const PartVerifyMixinOptional = require('./actor-part-mixins/part-verify-mixin-optional');
const PartVerifyMixinValue = require('./actor-part-mixins/part-verify-mixin-value');


class ActorPartPost {
  constructor(parent) {
    this.parent = parent;
    this.index = 0;
    this.type = 1;       // Because the ActorPartPost is exported through actor-api the instanceof will not work
    this.name = this.constructor.name;
  }
  
  init(parent, index) {
    this.parent = parent;
    this.index = index;
    return this;
  }
}

Object.assign(ActorPartPost.prototype, PartAsynchMixin);
Object.assign(ActorPartPost.prototype, PartLoggerMixin);
Object.assign(ActorPartPost.prototype, PartSharedRuntimeDataMixin);
Object.assign(ActorPartPost.prototype, PartTimerMixin);
Object.assign(ActorPartPost.prototype, PartVerifyMixinMandatory);
Object.assign(ActorPartPost.prototype, PartVerifyMixinOptional);
Object.assign(ActorPartPost.prototype, PartVerifyMixinValue);


module.exports = ActorPartPost;
