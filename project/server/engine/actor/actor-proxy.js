
'use strict';

const Actor = require('./actor');
const StackMixinClient = require('./actor-mixins/stack-mixin-client');
const StackMixinServer = require('./actor-mixins/stack-mixin-server');
const StackMixin = require('./actor-mixins/stack-mixin');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorProxy extends Actor {
  constructor(actorParts) {
    super(ActorTypeConst.PROXY, actorParts);
  }
  
  init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes) {
    if('' === actorData.execution) {
      actorData.execution = 'parallel';
    }
    return super.init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes);
  }
}

Object.assign(ActorProxy.prototype, StackMixinClient);
Object.assign(ActorProxy.prototype, StackMixinServer);
Object.assign(ActorProxy.prototype, StackMixin);


module.exports = ActorProxy;
