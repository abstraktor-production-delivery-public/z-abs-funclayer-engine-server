
'use strict';

const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class ActorStateTimeout {
  constructor(process, timeout) {
    this.setTimeout = process ? process.setTimeout.bind(process) : setTimeout;
    this.clearTimeout = process ? process.clearTimeout.bind(process) : clearTimeout;
    this.timeout = timeout;
    this.timeoutCancelId = null;
  }
  
  start(cb) {
    if(this.timeout > 0) {
      this.timeoutCancelId = this.setTimeout(() => {
        this.timeoutCancelId = null;
        cb();
      }, this.timeout);
    }
  }
  
  stop() {
    if(this.timeout > 0) {
      if(null !== this.timeoutCancelId) {
        this.clearTimeout(this.timeoutCancelId);
      }
      this.timeoutCancelId = null;
    }
  }
}


module.exports = ActorStateTimeout;
