
'use strict';

const PartAsynchMixin = require('./actor-part-mixins/part-asynch-mixin');
const PartLoggerMixin = require('./actor-part-mixins/part-logger-mixin');
const PartSharedRuntimeDataMixin = require('./actor-part-mixins/part-shared-runtime-data-mixin');
const PartTimerMixin = require('./actor-part-mixins/part-timer-mixin');
const PartVerifyMixinMandatory = require('./actor-part-mixins/part-verify-mixin-mandatory');
const PartVerifyMixinOptional = require('./actor-part-mixins/part-verify-mixin-optional');
const PartVerifyMixinValue = require('./actor-part-mixins/part-verify-mixin-value');


class ActorPartPre {
  constructor(parent) {
    this.parent = parent;
    this.index = 0;
    this.type = 0;         // Because the ActorPartPre is exported through actor-api the instanceof will not work
    this.name = this.constructor.name;
  }
  
  init(parent, index) {
    this.parent = parent;
    this.index = index;
    return this;
  }
}

Object.assign(ActorPartPre.prototype, PartAsynchMixin);
Object.assign(ActorPartPre.prototype, PartLoggerMixin);
Object.assign(ActorPartPre.prototype, PartSharedRuntimeDataMixin);
Object.assign(ActorPartPre.prototype, PartTimerMixin);
Object.assign(ActorPartPre.prototype, PartVerifyMixinMandatory);
Object.assign(ActorPartPre.prototype, PartVerifyMixinOptional);
Object.assign(ActorPartPre.prototype, PartVerifyMixinValue);


module.exports = ActorPartPre;
