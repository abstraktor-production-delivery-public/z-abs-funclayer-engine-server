
'use strict';

const Actor = require('./actor');
const StackMixinClient = require('./actor-mixins/stack-mixin-client');
const StackMixin = require('./actor-mixins/stack-mixin');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorOriginating extends Actor {
  constructor(actorParts) {
    super(ActorTypeConst.ORIG, actorParts);
  }
  
  init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes) {
    if('' === actorData.execution) {
      actorData.execution = 'serial';
    }
    return super.init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes);
  }
}

Object.assign(ActorOriginating.prototype, StackMixinClient);
Object.assign(ActorOriginating.prototype, StackMixin);


module.exports = ActorOriginating;
