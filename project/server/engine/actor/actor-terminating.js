
'use strict';

const Actor = require('./actor');
const StackMixinServer = require('./actor-mixins/stack-mixin-server');
const StackMixin = require('./actor-mixins/stack-mixin');
const ActorTypeConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-type-const');


class ActorTerminating extends Actor {
  constructor(actorParts, expectInterrupt) {
    super(ActorTypeConst.TERM, actorParts, expectInterrupt);
  }
  
  init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes) {
    if('' === actorData.execution) {
      actorData.execution = 'parallel';
    }
    return super.init(actorData, instanceIndex, cbMessage, executionContext, tcData, config, slowAwaitTime, nodes);
  }
}

Object.assign(ActorTerminating.prototype, StackMixinServer);
Object.assign(ActorTerminating.prototype, StackMixin);


module.exports = ActorTerminating;
