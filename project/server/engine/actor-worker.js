
const WebWorkerThreads = require('webworker-threads');


class ActorWorker {
  constructor() {
    this.threadsIdle = [];
    this.threadsWorking = new Map();
    this._createThreadPool(15);
    this.jobQueue = [];
  }
  
  load() {
    
  }
  
  doData(cmd) {
    this._addJob(cmd);
  }
  
  doInitServer(cmd) {
    this._addJob(cmd);
  }
  
  doInitClient(cmd) {
    this._addJob(cmd);
  }

  doRun(cmd) {
    this._addJob(cmd);
  }
  
  doExit(cmd) {
    this._addJob(cmd);
  }
  
  _createThreadPool(nbrOfThreads) {
    for(let i = 0; i < nbrOfThreads; ++i) {
      let thread = WebWorkerThreads.create();
      this.threadsIdle.push(thread);
      thread.load('C:\\Users\\A\\Documents\\ActorBase\\Actor\\dist\\serverWorkers\\serverWorkerActor\\server-worker-actor-bundle.js');
      thread.on('response', (data) => {
        let response = JSON.parse(data);
        let thread = this.threadsWorking.get(response.threadId);
        if(0 === this.jobQueue.length) {
          this.threadsIdle.push(thread);
          this.threadsWorking.delete(response.threadId);
        }
        else {
          thread.emit('message', this.jobQueue.pop());
        }
      });
    }
  }
  
  _addJob(msg) {
    if(0 !== this.threadsIdle.length) {
      let thread = this.threadsIdle.pop();
      this.threadsWorking.set(thread.id, thread);
      thread.emit('message', msg);
    }
    else {
      this.jobQueue.push(msg);
    }
  }
}

module.exports = new ActorWorker();
