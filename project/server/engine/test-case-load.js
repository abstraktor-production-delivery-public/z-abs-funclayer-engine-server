
'use strict';


class TestCaseLoad {
  constructor() {
    this.iterationsTc = 1;
    this.execution = TestCaseLoad.EXECUTION_SERIAL;
    this.mode = TestCaseLoad.MODE_TC;
    this.lambda = 0;
    this.distribution = TestCaseLoad.DISTRIBUTION_STATIC = 0;
  }
}

TestCaseLoad.EXECUTION_SERIAL = 0;
TestCaseLoad.EXECUTION_PARALLEL = 1;

TestCaseLoad.MODE_TC = 0;
TestCaseLoad.MODE_EXECUTION = 1;
TestCaseLoad.MODE_RUN = 2;

TestCaseLoad.DISTRIBUTION_STATIC = 0;


module.exports = TestCaseLoad;
