
'use strict';


class TestCaseResultType {
  constructor() {
    this.testCaseResultTypeIds = new Map();
    for(let i = 0; i <= 7; ++i) {
      this.testCaseResultTypeIds.set(TestCaseResultType.names[i], i);
    }
  }
  
  static getName(id) {
    return TestCaseResultType.names[id];
  }
  
 static getId(name) {
    return this.testCaseResultTypeIds.get(name);
  }
}

TestCaseResultType.names = [
  'none',
  'pre',
  'exec',
  'pre_exec',
  'post',
  'pre_post',
  'exec_post',
  'all'
];

TestCaseResultType.NONE = 0;
TestCaseResultType.PRE = 1;
TestCaseResultType.EXEC = 2;
TestCaseResultType.PRE_EXEC = 3;
TestCaseResultType.POST = 4;
TestCaseResultType.PRE_POST = 5;
TestCaseResultType.EXEC_POST = 6;
TestCaseResultType.ALL = 7;

module.exports = new TestCaseResultType();
