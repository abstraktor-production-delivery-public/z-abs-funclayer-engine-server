
'use strict';

const ActorResultConst = require('z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const');


class TestStatisticsTestSuite {
  constructor() {
    this.resultsCurrent = [];
    this.resultsIterations = [];
  }
  
  add(resultId) {
    this.resultsCurrent.push(resultId);
  }
  
  calculate() {
    this.resultsIterations.push(this.resultsCurrent.reduce((acc, current) => {
      return Math.max(acc, current);
    }, ActorResultConst.NONE));
    this.resultsCurrent = [];
  }
    
  calculateSum() {
    const result = {
      totalSum: ActorResultConst.NONE,
      sums: new Array(ActorResultConst.RESULTS_CONSOLE.length)
    };
    result.totalSum = this.resultsIterations.reduce((acc, current) => {
      return Math.max(acc, current);
    }, ActorResultConst.NONE);
    if(!this.isSumOk(result.totalSum)) {
      for(let i = 0; i < ActorResultConst.RESULTS_CONSOLE.length; ++i) {
        result.sums[i] = 0;
      }
      this.resultsIterations.forEach((resultsIteration) => {
        ++result.sums[resultsIteration];
      });
    }
    return result;
  }
  
  isSumOk(result) {
    return ActorResultConst.SUCCESS >= result;
  }
  
  getLastIterationResult() {
    return this.resultsIterations[this.resultsIterations.length - 1];
  }
  
  outputLastIterationResult() {
    return ActorResultConst.RESULTS_CONSOLE[this.getLastIterationResult()];
  }
  
  outputResult(result) {
    return ActorResultConst.RESULTS_CONSOLE[result];
  }
}


module.exports = TestStatisticsTestSuite;
