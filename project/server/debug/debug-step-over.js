
'use strict';


class DebugStepOver {
  constructor(client) {
    this.client = client;
    this.lineNumber = 0;
    this.topStack = null;
    this.breakPoint = null;
  }
  
  async setBreakpoint(topStack) {
    this.topStack = topStack;
    const location = topStack?.location;
    if(location) {
      ++location.lineNumber;
      if(location.lineNumber > this.lineNumber) {
        this.lineNumber = location.lineNumber;
        try {
          const result = await this.client.Debugger.getPossibleBreakpoints({
            start: location
          });
          if(0 !== result?.locations.length) {
            const locations = result.locations;
            let location = result.locations[0];
            for(let i = 0; i < locations.length; ++i) {
              if('call' === locations[i].type) {
                location = locations[i];
                break;
              }
            }
            this.breakPoint = await this.client.Debugger.setBreakpoint({location:location});
          }
        }
        catch(e) {}
      }
    }
  }
  
  async removeBreakpoint() {
    if(this.breakPoint) {
      await this.client.Debugger.removeBreakpoint({breakpointId:this.breakPoint.breakpointId});
      this.breakPoint = null;
    }
  }
  
  sameScript(scriptName) {
    return this.topStack?.scriptName === scriptName;
  }
}


module.exports = DebugStepOver;
