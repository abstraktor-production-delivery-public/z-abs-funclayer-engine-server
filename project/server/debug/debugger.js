
'use strict';

const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const Path = require('path');


class Debugger {
  constructor() {
    this.breakpoints = [];
    this.runBreakpoints = [];
    this.scriptCacheScriptId = new Map();
    this.scriptCacheUrl = new Map();
  }
  
  static calculateBreakpointUrl(sut, fut, tc, breakpoint) {
    let url = `${ActorPathDist.getActorDistActorsPath()}/${breakpoint.name}`;
    if(url.startsWith('/')) {
      url = `file://${Path.normalize(url)}`;
    }
    else {
      url = `file:///${Path.normalize(url)}`;
    }
    url = url.replace(/\\/g, '/');
    return url;
  }
  
  setBreakpoints(breakpoints) {
    this.breakpoints = breakpoints;
  }
  
  getBreakpoints() {
    return this.breakpoints;
  }
  
  setBreakpoint(breakpoint) {
    const index = this._findIndexBreakpoint(breakpoint);
    if(-1 === index) {
      this.breakpoints.push(breakpoint);
    }
    else {
      this.breakpoints[index] = breakpoint;
    }
  }
  
  getBreakpoint(breakpoint) {
    const index = this._findIndexBreakpoint(breakpoint);
    if(-1 !== index) {
      return this.breakpoints[index];
    }
  }
  
  removeBreakpoint(breakpoint) {
    const index = this._findIndexBreakpoint(breakpoint);
    if(-1 !== index) {
      this.breakpoints.splice(index, 1);
    }
  }
  
  setRunBreakpoint(breakpoint) {
    this.runBreakpoints.push(breakpoint);
  }
  
  clearRunBreakpoints() {
    this.runBreakpoints = [];
  }
  
  scriptParsed(parameters) {
    this.scriptCacheScriptId.set(parameters.scriptId, parameters);
    this.scriptCacheUrl.set(parameters.url, parameters);
  }
  
  getScriptParameters(scriptId) {
   return this.scriptCacheScriptId.get(scriptId);
  }
  
  getScriptParametersFromUrl(url) {
    return this.scriptCacheUrl.get(url);
  }
  
  _findIndexBreakpoint(breakpoint) {
    return this.breakpoints.findIndex((foundBreakpoint) => {
      return foundBreakpoint.name === breakpoint.name
        && foundBreakpoint.lineNumber === breakpoint.lineNumber;
    });
  }
  
  findbreakpointInBreakpoints(breakpoint) {
    return !!this.breakpoints.find((serchBreakpoint) => {
      return serchBreakpoint.lineNumber === breakpoint.lineNumber;
    });
  }
  
  findbreakpointInRunBreakpoints(breakpoint) {
    return !!this.runBreakpoints.find((serchBreakpoint) => {
      return serchBreakpoint.lineNumber === breakpoint.lineNumber;
    });
  }
}


module.exports = Debugger;
